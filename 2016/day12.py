import sys

inp = [line.strip().split() for line in sys.stdin]


def run(code, regs):

    def val(x):
        if x in regs:
            return regs[x]
        else:
            return int(x)

    ip = 0
    while True:
        try:
            if code[ip][0] == "cpy":
                regs[code[ip][2]] = val(code[ip][1])
                ip += 1
            elif code[ip][0] == "inc":
                regs[code[ip][1]] += 1
                ip += 1
            elif code[ip][0] == "dec":
                regs[code[ip][1]] -= 1
                ip += 1
            elif code[ip][0] == "jnz":
                if val(code[ip][1]) != 0:
                    ip += val(code[ip][2])
                else:
                    ip += 1
        except:
            return regs['a']

def part1(inp=inp):
    regs = {"a": 0, "b": 0, "c": 0, "d": 0}
    return run(inp, regs)

print(part1())

def part2(inp=inp):
    regs = {"a": 0, "b": 0, "c": 1, "d": 0}
    return run(inp, regs)

print(part2())
