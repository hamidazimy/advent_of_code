import sys
import numpy as np
from queue import Queue

inp = int(sys.stdin.readline().strip())

def part1(inp=inp):
    N = inp
    id = np.cumsum(np.ones((N, ), dtype=np.uint32))
    gift = np.ones((N, ), dtype=np.uint32)
    while N > 1:
        if N % 2 == 0:
            id = id[::2]
            gift = gift[::2] + gift[1::2]
        else:
            id = id[::2]
            temp = gift[:-1:2] + gift[1:-1:2]
            temp = np.append(temp, [temp[0] + gift[-1]])
            gift = temp[1:]
            id = id[1:]
        N = len(id)
    return id[0]

print(part1())

i = 0
id = []
q = Queue()

def next(N):
    global id, i, q
    if i < N:
        ret = id[i]
    else:
        ret = q.get(False)
    i += 1
    return ret

def solve(N):
    global id, i, q
    h = N // 2
    id = [x + 1 for x in range(N)]
    id = id[h:] + id[:h]
    i = 0
    q = Queue()
    if N < 3:
        return 1
    if N % 2 == 1:
        next(N)
        survivor = next(N)
        q.put(survivor)
    try:
        while True:
            next(N)
            next(N)
            survivor = next(N)
            q.put(survivor)
    except:
        return survivor

def part2(inp=inp):
    return solve(inp)

print(part2())
