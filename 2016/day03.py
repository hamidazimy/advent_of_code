import sys
import numpy as np

inp = np.array([[int(x) for x in line.strip().split()] for line in sys.stdin])

def part1(inp=inp):
    inp = np.sort(inp, axis=1)
    result = np.sum((np.sum(inp[:, 0:2], axis=1) - inp[:, 2]) > 0)
    return result

print(part1())

def part2(inp=inp):
    n, _ = np.shape(inp)
    inp = np.reshape(inp, (n // 3, 3, 3))
    inp = np.transpose(inp, (0, 2, 1))
    inp = np.reshape(inp, (n, 3))
    inp = np.sort(inp, axis=1)
    result = np.sum((np.sum(inp[:, 0:2], axis=1) - inp[:, 2]) > 0)
    return result

print(part2())
