import sys

inp = sys.stdin.readline().strip()

def length(inp):
    if inp.find("(") == -1:
        return len(inp)
    ret = 0
    b = 0
    e = 0
    i = inp.find("(", b)
    while i != -1:
        j = inp.find(")", i)
        l, c = [int(x) for x in inp[i + 1:j].split('x')]
        sub = inp[j + 1:j + 1 + l]
        ret += len(inp[b:i]) + length(sub) * c
        b = j + 1 + l
        i = inp.find("(", b)
    return ret

def part1(inp=inp):
    result = ""
    b = 0
    e = 0
    i = inp.find("(", b)
    while i != -1:
        j = inp.find(")", i)
        l, c = [int(x) for x in inp[i + 1:j].split('x')]
        sub = inp[j + 1:j + 1 + l]
        result += inp[b:i] + sub * c
        b = j + 1 + l
        i = inp.find("(", b)
    return len(result)

print(part1())

def part2(inp=inp):
    return length(inp)

print(part2())
