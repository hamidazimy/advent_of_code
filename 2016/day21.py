import sys
from scanf import scanf

inp = [line.strip() for line in sys.stdin]

def swapp(pswd, X, Y):
    temp = pswd[X]
    pswd[X] = pswd[Y]
    pswd[Y] = temp
    return pswd

def swapl(pswd, X, Y):
    x = pswd.index(X)
    y = pswd.index(Y)
    return swapp(pswd, x, y)

def rotatel(pswd, X):
    pswd = pswd[X:] + pswd[:X]
    return pswd

def rotater(pswd, X):
    pswd = pswd[-X:] + pswd[:-X]
    return pswd

def rotateb(pswd, X):
    x = pswd.index(X)
    y = (1 + x + (x >= 4)) % len(pswd)
    return rotater(pswd, y)

def reversep(pswd, X, Y):
    pswd = pswd[:X] + pswd[X:Y+1][::-1] + pswd[Y+1:]
    return pswd

def movep(pswd, X, Y):
    temp = pswd.pop(X)
    pswd.insert(Y, temp)
    return pswd

def unswapp(pswd, X, Y):
    temp = pswd[X]
    pswd[X] = pswd[Y]
    pswd[Y] = temp
    return pswd

def unswapl(pswd, X, Y):
    x = pswd.index(X)
    y = pswd.index(Y)
    return unswapp(pswd, x, y)

def unrotatel(pswd, X):
    pswd = pswd[-X:] + pswd[:-X]
    return pswd

def unrotater(pswd, X):
    pswd = pswd[X:] + pswd[:X]
    return pswd

def unrotateb(pswd, X):
    x = pswd.index(X)
    y = [1, 1, 6, 2, 7, 3, 0, 4][x]
    return unrotater(pswd, y)

def unreversep(pswd, X, Y):
    pswd = pswd[:X] + pswd[X:Y+1][::-1] + pswd[Y+1:]
    return pswd

def unmovep(pswd, X, Y):
    temp = pswd.pop(Y)
    pswd.insert(X, temp)
    return pswd

def part1(inp=inp):
    pswd = list("abcdefgh")
    for l in inp:
        parsed = scanf("swap position %d with position %d", l)
        if parsed is not None:
            pswd = swapp(pswd, parsed[0], parsed[1])
        parsed = scanf("swap letter %s with letter %s", l)
        if parsed is not None:
            pswd = swapl(pswd, parsed[0], parsed[1])
        parsed = scanf("rotate left %d step", l)
        if parsed is not None:
            pswd = rotatel(pswd, parsed[0])
        parsed = scanf("rotate right %d step", l)
        if parsed is not None:
            pswd = rotater(pswd, parsed[0])
        parsed = scanf("rotate based on position of letter %s", l)
        if parsed is not None:
            pswd = rotateb(pswd, parsed[0])
        parsed = scanf("reverse positions %d through %d", l)
        if parsed is not None:
            pswd = reversep(pswd, parsed[0], parsed[1])
        parsed = scanf("move position %d to position %d", l)
        if parsed is not None:
            pswd = movep(pswd, parsed[0], parsed[1])
    return ''.join(pswd)

print(part1())

def part2(inp=inp):
    inp = inp[::-1]
    pswd = list("fbgdceah")
    for l in inp:
        parsed = scanf("swap position %d with position %d", l)
        if parsed is not None:
            pswd = unswapp(pswd, parsed[0], parsed[1])
        parsed = scanf("swap letter %s with letter %s", l)
        if parsed is not None:
            pswd = unswapl(pswd, parsed[0], parsed[1])
        parsed = scanf("rotate left %d step", l)
        if parsed is not None:
            pswd = unrotatel(pswd, parsed[0])
        parsed = scanf("rotate right %d step", l)
        if parsed is not None:
            pswd = unrotater(pswd, parsed[0])
        parsed = scanf("rotate based on position of letter %s", l)
        if parsed is not None:
            pswd = unrotateb(pswd, parsed[0])
        parsed = scanf("reverse positions %d through %d", l)
        if parsed is not None:
            pswd = unreversep(pswd, parsed[0], parsed[1])
        parsed = scanf("move position %d to position %d", l)
        if parsed is not None:
            pswd = unmovep(pswd, parsed[0], parsed[1])
    return ''.join(pswd)

print(part2())
