import sys
from scanf import scanf
import numpy as np
from queue import PriorityQueue

inp = [line.strip() for line in sys.stdin]

def part1(inp=inp):
    size = []
    used = []
    avai = []
    for l in inp[2:]:
        d = l.strip().split()
        x, y = scanf("/dev/grid/node-x%d-y%d", d[0])
        size.append(int(d[1][:-1]))
        used.append(int(d[2][:-1]))
        avai.append(int(d[3][:-1]))
    result = 0
    for a in range(len(used)):
        if used[a] == 0:
            continue
        for b in range(len(avai)):
            if a == b:
                continue
            if used[a] <= avai[b]:
                result += 1
    return result

print(part1())

def part2(inp=inp):
    W, H = scanf("/dev/grid/node-x%d-y%d", inp[-1].strip().split()[0])
    W += 1
    H += 1
    map = np.zeros((H, W), dtype=np.uint8)
    GOALX = W - 1
    EMPTY = None
    for l in inp[2:]:
        d = l.strip().split()
        x, y = scanf("/dev/grid/node-x%d-y%d", d[0])
        used = int(d[2][:-1])
        if used == 0:
            map[y, x] = ord('_')
            EMPTY = (y, x)
        elif used > 100:
            map[y, x] = ord('#')
        else:
            map[y, x] = ord('.')
    map[0, -1] = ord('G')
    dirs = [(-1, 0), (0, 1), (0, -1), (1, 0)]

    def valid_coordinate(y, x):
        return 0 <= y < H and 0 <= x < W

    def steps_to_move_empty_to_next(NEXTX):
        queue = PriorityQueue()
        visited = set()
        y, x = EMPTY
        g = 0
        h = y + abs(x - NEXTX)
        queue.put((g + h, g, h, y, x))
        visited.add((y, x))
        while True:
            f_, g_, h_, y_, x_ = queue.get()
            # print(f_, g_, h_, y_, x_)
            for dy, dx in dirs:
                y, x = y_ + dy, x_ + dx
                if y_ == 0 and x_ == NEXTX:
                    return g_ + 1
                elif valid_coordinate(y, x) and (y, x) not in visited and map[y, x] == ord('.'):
                    g = g_ + 1
                    h = y + abs(x - NEXTX)
                    queue.put((g + h, g, h, y, x))
                    visited.add((y, x))
        return 1

    steps = 0
    while 0 != GOALX:
        NEXTX = GOALX - 1
        steps += steps_to_move_empty_to_next(NEXTX)
        map[EMPTY]    = ord('.')
        map[0, GOALX] = ord('_')
        map[0, NEXTX] = ord('G')
        EMPTY = (0, GOALX)
        GOALX = NEXTX
        # print('\n'.join([''.join([chr(x) for x in row]) for row in map]))
    return steps

print(part2())
