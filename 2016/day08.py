import sys
from scanf import scanf
import numpy as np

inp = [line for line in sys.stdin]

H = 6
W = 50
display = np.zeros((H, W), dtype=np.uint8)

for l in inp:
    rect = scanf("rect %dx%d\n", l)
    if rect is not None:
        display[0:rect[1], 0:rect[0]] = 1
    rot_r = scanf("rotate row y=%d by %d\n", l)
    if rot_r is not None:
        row, rot = rot_r
        display[row, :] = np.roll(display[row, :], rot)
    rot_c = scanf("rotate column x=%d by %d\n", l)
    if rot_c is not None:
        col, rot = rot_c
        display[:, col] = np.roll(display[:, col], rot)

def part1(display=display):
    return np.sum(display)

print(part1())

def part2(display=display):
    return '\n'.join([''.join([' █'[x] * 2 for x in row]) for row in display])

print(part2())
