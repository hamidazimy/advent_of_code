import sys
import numpy as np

inp = sys.stdin.readline().strip()

def solve(N):
    a = np.array([int(x) for x in inp])
    while len(a) < N:
        a = np.append(np.append(a, [0]), 1 - a[::-1])
    c = a[:N]
    while len(c) % 2 == 0:
        e = c[::2]
        o = c[1::2]
        c = np.array(e == o, dtype=np.uint32)
    return ''.join([str(x) for x in c.tolist()])

def part1():
    return solve(272)

print(part1())

def part2():
    return solve(35651584)

print(part2())
