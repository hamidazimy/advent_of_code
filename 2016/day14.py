import sys
import hashlib

inp = sys.stdin.readline().strip()

def is_aaa(digest):
    for k in range(len(digest) - 2):
        piece = digest[k:k+3]
        if piece[0] == piece[1] and piece[1] == piece[2]:
            return piece[0]
    return False

def is_xxxxx(digest):
    ret = set()
    for k in range(len(digest) - 4):
        piece = digest[k:k+5]
        if piece[0] == piece[1] and piece[1] == piece[2] and piece[2] == piece[3] and piece[3] == piece[4]:
            ret.add(piece[0])
    return list(ret)

def solve(salt=inp, stretch=0):
    i = 0
    keys = []
    maybe_keys = []
    fives = []
    while len(keys) < 64:
        digest = hashlib.md5("{}{}".format(salt, i).encode()).hexdigest()
        for h in range(stretch):
            digest = hashlib.md5(digest.encode()).hexdigest()
        xxxxx = is_xxxxx(digest)
        if xxxxx != []:
            fives.append((xxxxx, i))
            j = 0
            while j < len(maybe_keys):
                aaa, key = maybe_keys[j]
                if aaa in xxxxx:
                    if i - key <= 1000:
                        keys.append((aaa, key))
                        del maybe_keys[j]
                        j -= 1
                elif i - key > 1000:
                    del maybe_keys[j]
                    j -= 1
                j += 1
        aaa = is_aaa(digest)
        if aaa != False:
            maybe_keys.append((aaa, i))
        i += 1
    return sorted(keys, key=lambda x: x[1])[63][1]

def part1(inp=inp):
    return solve()

print(part1())

def part2(inp=inp):
    return solve(stretch=2016)

print(part2())
