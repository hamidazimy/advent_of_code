import sys
import numpy as np
from queue import PriorityQueue
import hashlib

inp = sys.stdin.readline().strip()

def part1(inp=inp):
    q = PriorityQueue()
    xd, yd = 4, 4
    g = 0
    h = xd + yd
    q.put((g + h, g, h, 0, 0, ''))
    dir = [(-1, 0, 'U'), (1, 0, 'D'), (0, -1, 'L'), (0, 1, 'R')]
    while True:
        f, g, h, x, y, d = q.get()
        passcode = f"{inp}{d}"
        digest = hashlib.md5(passcode.encode()).hexdigest()
        if x == xd - 1 and y == yd - 1:
            return d
        for k in range(4):
            if digest[k] in 'bcdef':
                i, j, d_ = dir[k]
                x_, y_ = x + i, y + j
                if x_ > -1 and x_ < xd and y_ > -1 and y_ < yd:
                    g_ = g + 1
                    h_ = abs(xd - x_) + abs(yd - y_)
                    q.put((g_ + h_, g_, h_, x_, y_, d + d_))

print(part1())

def part2(inp=inp):
    q = PriorityQueue()
    xd, yd = 4, 4
    g = 0
    h = xd + yd
    q.put((g + h, g, h, 0, 0, ''))
    dir = [(-1, 0, 'U'), (1, 0, 'D'), (0, -1, 'L'), (0, 1, 'R')]
    longest_path_length = 0
    while not q.empty():
        f, g, h, x, y, d = q.get()
        passcode = f"{inp}{d}"
        digest = hashlib.md5(passcode.encode()).hexdigest()
        if x == xd - 1 and y == yd - 1:
            if len(d) > longest_path_length:
                longest_path_length = len(d)
            continue
        for k in range(4):
            if digest[k] in 'bcdef':
                i, j, d_ = dir[k]
                x_, y_ = x + i, y + j
                if x_ > -1 and x_ < xd and y_ > -1 and y_ < yd:
                    g_ = g + 1
                    h_ = abs(xd - x_) + abs(yd - y_)
                    q.put((g_ + h_, g_, h_, x_, y_, d + d_))
    return longest_path_length

print(part2())
