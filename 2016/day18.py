import sys
import numpy as np

inp = sys.stdin.readline().strip()

def solve(N):
    global inp
    map = np.zeros((N, len(inp))) == 1
    map[0, :] = np.array([x == '^' for x in inp], )
    for i in range(1, N):
        l = np.append([False], map[i - 1, :-1])
        r = np.append(map[i - 1, 1:], [False])
        map[i, :] = np.logical_xor(l, r)
    return np.sum(map == False)

def part1():
    return solve(40)

print(part1())

def part2():
    return solve(400000)

print(part2())
