import sys

blacklist = [[int(x) for x in line.strip().split('-')] for line in sys.stdin]
blacklist.sort(key=lambda x: x[0])

def part1(blacklist=blacklist):
    first_available = 0
    for item in blacklist:
        if first_available >= item[0]:
            first_available = max(first_available, item[1] + 1)
        else:
            return first_available

print(part1())

def part2(blacklist=blacklist):
    first_available = 0
    count_available = 0
    for item in blacklist:
        if first_available >= item[0]:
            first_available = max(first_available, item[1] + 1)
        else:
            count_available += (item[0] - first_available)
            first_available = max(first_available, item[1] + 1)
    return count_available

print(part2())
