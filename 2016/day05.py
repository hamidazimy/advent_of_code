import sys
import hashlib

inp = sys.stdin.readline().strip()

def part1(inp=inp, verbose=False):
    password = list('________')
    i = -1
    while '_' in password:
        i += 1
        digest = hashlib.md5("{}{}".format(inp, i).encode()).hexdigest()
        if digest[:5] == "00000":
            password[password.index('_')] = digest[5]
            if verbose:
                print(''.join(password), end='\r')
    return ''.join(password)

print(part1(verbose=True))

def part2(inp=inp, verbose=False):
    password = list('________')
    i = -1
    while '_' in password:
        i += 1
        digest = hashlib.md5("{}{}".format(inp, i).encode()).hexdigest()
        if digest[:5] == "00000" and digest[5] in list('01234567') and password[int(digest[5])] == '_':
            password[int(digest[5])] = digest[6]
            if verbose:
                print(''.join(password), end='\r')
    return ''.join(password)

print(part2(verbose=True))
