import sys
from scanf import scanf

bots = {}

comps = {}

for l in sys.stdin:
    cmp = scanf("bot %s gives low to %s %s and high to %s %s\n", l)
    if cmp is not None:
        comps[cmp[0]] = cmp[1:]

    val = scanf("value %d goes to bot %s\n", l)
    if val is not None:
        bot_vals = bots.get(val[1], [])
        bot_vals.append(val[0])
        bots[val[1]] = bot_vals

def part1(bots=bots, comps=comps):
    while True:
        ready = [x for x, y in bots.items() if len(y) == 2][0]
        values = sorted(bots[ready])
        if values == [17, 61]:
            return ready
        bots[ready] = []
        if comps[ready][0] == "bot":
            bots[comps[ready][1]] = bots.get(comps[ready][1], [])
            bots[comps[ready][1]].append(values[0])
        if comps[ready][2] == "bot":
            bots[comps[ready][3]] = bots.get(comps[ready][3], [])
            bots[comps[ready][3]].append(values[1])

print(part1())

def part2(bots=bots, comps=comps):
    zot = [None, None, None]
    while True:
        if None not in zot:
            return zot[0] * zot[1] * zot[2]
        ready = [x for x, y in bots.items() if len(y) == 2][0]
        values = sorted(bots[ready])
        bots[ready] = []
        if comps[ready][0] == "bot":
            bots[comps[ready][1]] = bots.get(comps[ready][1], [])
            bots[comps[ready][1]].append(values[0])
        elif comps[ready][1] in ['0', '1', '2']:
            zot[int(comps[ready][1])] = values[0]
        if comps[ready][2] == "bot":
            bots[comps[ready][3]] = bots.get(comps[ready][3], [])
            bots[comps[ready][3]].append(values[1])
        elif comps[ready][3] in ['0', '1', '2']:
            zot[int(comps[ready][3])] = values[1]

print(part2())
