import sys
import numpy as np
from scipy import stats
from collections import Counter

matrix = np.array([[ord(x) for x in l.strip()] for l in sys.stdin])

def part1(matrix=matrix):
    result, _ = stats.mode(matrix)
    return ''.join([chr(x) for x in result[0, :].tolist()])

print(part1())

def part2(matrix=matrix):
    matrix = np.transpose(matrix).tolist()
    result = []
    for row in matrix:
        c = Counter(row)
        result.append(c.most_common()[-1][0])
    return ''.join([chr(x) for x in result])

print(part2())
