import sys
from scanf import scanf

# Chinese Remainder Theorem

inp = [line.strip() for line in sys.stdin]

def solve(inp):
    N = 1
    n_ = []
    a_ = []

    for l in inp:
        di, ni, pi = scanf("Disc #%d has %d positions; at time=0, it is at position %d.", l)
        N *= ni
        n_.append(ni)
        a_.append((- di - pi) % ni)

    N_ = [N // ni for ni in n_]
    x_ = [pow(x, p - 2, p) for x, p in zip(N_, n_)]

    X = sum([ai * Ni * xi for ai, Ni, xi in zip(a_, N_, x_)]) % N

    return X

def part1(inp=inp):
    return solve(inp)

print(part1())

def part2(inp=inp):
    inp.append(f"Disc #{len(inp) + 1} has {11} positions; at time=0, it is at position {0}.")
    return solve(inp)

print(part2())
