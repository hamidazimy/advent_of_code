import sys

inp = [list(l.strip()) for l in sys.stdin]

def part1(inp=inp):
    dx = {'L': -1, 'R': +1}
    dy = {'U': -1, 'D': +1}
    keypad = [['1', '2', '3'],
              ['4', '5', '6'],
              ['7', '8', '9']]
    y, x = 1, 1
    result = ''
    for l in inp:
        for c in l:
            x = max(0, min(2, x + dx.get(c, 0)))
            y = max(0, min(2, y + dy.get(c, 0)))
        result += keypad[y][x]
    return result

print(part1())

def part2(inp=inp):
    dx = {'L': -1, 'R': +1}
    dy = {'U': -1, 'D': +1}

    keypad = [[' ', ' ', '1', ' ', ' '],
              [' ', '2', '3', '4', ' '],
              ['5', '6', '7', '8', '9'],
              [' ', 'A', 'B', 'C', ' '],
              [' ', ' ', 'D', ' ', ' ']]
    y, x = 2, 0
    result = ''
    for l in inp:
        for c in l:
            x_ = max(0, min(4, x + dx.get(c, 0)))
            y_ = max(0, min(4, y + dy.get(c, 0)))
            if keypad[y_][x_] != ' ':
                x = x_
                y = y_
        result += keypad[y][x]
    return result

print(part2())
