import sys
import re

inp = [line.strip() for line in sys.stdin]

def is_ABBA(ip_seg):
    for i in range(len(ip_seg) - 3):
        sub = ip_seg[i:i + 4]
        if sub == sub[::-1] and sub[0] != sub[1]:
            return True
    return False

def IP_supports_TLS(ip):
    m = re.split(r"\[|\]", ip)
    ret = False
    for i in range(len(m)):
        if is_ABBA(m[i]):
            if i % 2 == 1:
                return False
            else:
                ret = True
    return ret

def extract_ABA(ip_seg):
    ret = []
    for i in range(len(ip_seg) - 2):
        sub = ip_seg[i:i + 3]
        if sub[0] == sub[2] and sub[0] != sub[1]:
            ret.append(sub)
    return ret

def BAB_list_to_ABA(ls):
    return [str(x[1:3] + x[1]) for x in ls]

def IP_supports_SSL(ip):
    m = re.split(r"\[|\]", ip)
    aba_set = set()
    bab_set = set()
    for i in range(0, len(m), 2):
        for x in extract_ABA(m[i]):
            aba_set.add(x)
    for i in range(1, len(m), 2):
        for x in BAB_list_to_ABA(extract_ABA(m[i])):
            bab_set.add(x)
    return len(aba_set.intersection(bab_set)) > 0

def part1(inp=inp):
    return sum([IP_supports_TLS(ip.strip()) for ip in inp])

print(part1())

def part2(inp=inp):
    return sum([IP_supports_SSL(ip.strip()) for ip in inp])

print(part2())
