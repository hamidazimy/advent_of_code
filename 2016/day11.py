import sys
from copy import copy, deepcopy
from queue import Queue
import itertools

#  input processed manually!!
#  extremely slow. might need optimization.

class State:
    elements = ("H_", "Li")

    def __init__(self):
        self.E = 3
        self.floors = []
        for i in range(4):
            self.floors.append({"mic": set(), "gen": set()})
        self.steps = 0

    def __str__(self):
        return self.to_string()

    def __repr__(self):
        return self.to_string()

    def is_valid(self):
        for i in range(4):
            if not (len(self.floors[i]["gen"]) == 0 or self.floors[i]["mic"].issubset(self.floors[i]["gen"])):
                return False
        return True

    def to_string(self):
        result = ""
        for i in range(4):
            result += "F{} {}  ".format(4 - i, 'E' if self.E == i else '.')
            for e in State.elements:
                result += "{} {} ".format(
                                e + 'G' if e in self.floors[i]["gen"] else " . ",
                                e + 'M' if e in self.floors[i]["mic"] else " . ")
            result += "\n"
        return result

    def neighbours(self):
        for i in range(4):
            if abs(self.E - i) == 1:
                for eg in list(self.floors[self.E]["gen"]):
                    nei = deepcopy(self)
                    nei.floors[self.E]["gen"].remove(eg)
                    nei.E = i
                    nei.floors[nei.E]["gen"].add(eg)
                    nei.steps = self.steps + 1
                    if nei.is_valid():
                        yield nei
                for em in list(self.floors[self.E]["mic"]):
                    nei = deepcopy(self)
                    nei.floors[self.E]["mic"].remove(em)
                    nei.E = i
                    nei.floors[nei.E]["mic"].add(em)
                    nei.steps = self.steps + 1
                    if nei.is_valid():
                        yield nei
                for egg in itertools.combinations(self.floors[self.E]["gen"], 2):
                    nei = deepcopy(self)
                    nei.floors[self.E]["gen"] -= set(egg)
                    nei.E = i
                    nei.floors[nei.E]["gen"].update(egg)
                    nei.steps = self.steps + 1
                    if nei.is_valid():
                        yield nei
                for emm in itertools.combinations(self.floors[self.E]["mic"], 2):
                    nei = deepcopy(self)
                    nei.floors[self.E]["mic"] -= set(emm)
                    nei.E = i
                    nei.floors[nei.E]["mic"].update(emm)
                    nei.steps = self.steps + 1
                    if nei.is_valid():
                        yield nei
                for eg in list(self.floors[self.E]["gen"]):
                    for em in list(self.floors[self.E]["mic"]):
                        nei = deepcopy(self)
                        nei.floors[self.E]["gen"].remove(eg)
                        nei.floors[self.E]["mic"].remove(em)
                        nei.E = i
                        nei.floors[nei.E]["gen"].add(eg)
                        nei.floors[nei.E]["mic"].add(em)
                        nei.steps = self.steps + 1
                        if nei.is_valid():
                            yield nei

    def is_final(self):
        if self.E == 0 and len(self.floors[0]["gen"]) == len(State.elements) and len(self.floors[0]["mic"]) == len(State.elements):
            return True
        return False

    def solve(self):
        q = Queue()
        q.put(self)
        visited = set()
        c = 0
        while True:
            c += 1
            current = q.get()
            if str(current) in visited:
                continue
            visited.add(str(current))
            for nei in current.neighbours():
                if str(nei) not in visited:
                    if nei.is_final():
                        return nei.steps
                    q.put(nei)

def part1():
    init1 = State()
    State.elements = ("Co", "Ru", "Pm", "Tm", "Po")
    init1.floors[2]["mic"].add(State.elements[2])
    init1.floors[2]["mic"].add(State.elements[4])
    init1.floors[3]["mic"].add(State.elements[0])
    init1.floors[3]["mic"].add(State.elements[1])
    init1.floors[3]["mic"].add(State.elements[3])
    init1.floors[3]["gen"].add(State.elements[0])
    init1.floors[3]["gen"].add(State.elements[1])
    init1.floors[3]["gen"].add(State.elements[2])
    init1.floors[3]["gen"].add(State.elements[3])
    init1.floors[3]["gen"].add(State.elements[4])
    return init1.solve()

print(part1())

def part2():
    init2 = State()
    State.elements = ("Co", "Ru", "Pm", "Tm", "Po", "El", "Di")
    init2.floors[2]["mic"].add(State.elements[2])
    init2.floors[2]["mic"].add(State.elements[4])
    init2.floors[3]["mic"].add(State.elements[0])
    init2.floors[3]["mic"].add(State.elements[1])
    init2.floors[3]["mic"].add(State.elements[3])
    init2.floors[3]["mic"].add(State.elements[5])
    init2.floors[3]["mic"].add(State.elements[6])
    init2.floors[3]["gen"].add(State.elements[0])
    init2.floors[3]["gen"].add(State.elements[1])
    init2.floors[3]["gen"].add(State.elements[2])
    init2.floors[3]["gen"].add(State.elements[3])
    init2.floors[3]["gen"].add(State.elements[4])
    init2.floors[3]["gen"].add(State.elements[5])
    init2.floors[3]["gen"].add(State.elements[6])
    return init2.solve()

print(part2())
