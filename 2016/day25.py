import sys

code = [line.strip().split() for line in sys.stdin]
regs = {}

def val(x):
    if x in regs:
        return regs[x]
    else:
        return int(x)

def run_and_check_for_alternative_signal(a=0):
    global regs
    regs = {"a": a, "b": 0, "c": 0, "d": 0}
    ip = 0
    last_out = 1
    i = 0
    while True:
        if code[ip][0] == "cpy":
            regs[code[ip][2]] = val(code[ip][1])
            ip += 1
        elif code[ip][0] == "inc":
            regs[code[ip][1]] += 1
            ip += 1
        elif code[ip][0] == "dec":
            regs[code[ip][1]] -= 1
            ip += 1
        elif code[ip][0] == "jnz":
            if val(code[ip][1]) != 0:
                ip += val(code[ip][2])
            else:
                ip += 1
        elif code[ip][0] == "out":
            if val(code[ip][1]) == last_out:
                return False
            else:
                last_out = val(code[ip][1])
                i += 1
                if i > 1000:
                    return True
            ip += 1

a = -1
solved = False
while not solved:
    a += 1
    solved = run_and_check_for_alternative_signal(a)

print(a)
