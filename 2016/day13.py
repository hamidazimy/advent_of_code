import sys
import numpy as np
from queue import Queue, PriorityQueue
from time import sleep

fav = int(sys.stdin.readline())

H, W = 80, 80

x = np.cumsum(np.ones((H, W), dtype=np.uint32), axis=1) - 1
y = np.cumsum(np.ones((H, W), dtype=np.uint32), axis=0) - 1

temp = x*x + 3*x + 2*x*y + y + y*y + fav

bin = np.zeros((H, W), dtype=np.uint32)

while np.sum(temp) > 0:
    bin += (temp % 2)
    temp = (temp // 2)

map = 0 + (bin % 2 == 0)

xd, yd = 31, 39

def part1():
    q = PriorityQueue()
    g = 0
    h = xd + yd
    q.put((g + h, g, h, 1, 1))
    dir = [(0, 1), (1, 0), (-1, 0), (0, -1)]
    visited = set()
    prev = {}
    while True:
        f, g, h, x, y = q.get()
        visited.add((x, y))
        if x == xd and y == yd:
            return f
        for i, j in dir:
            x_, y_ = x + i, y + j
            if x_ > -1 and x_ < W and y_ > -1 and y_ < H and (x_, y_) not in visited:
                if map[y_, x_] == 1:
                    prev[(y_, x_)] = (x, y)
                    g_ = g + 1
                    h_ = abs(xd - x_) + abs(yd - y_)
                    q.put((g_ + h_, g_, h_, x_, y_))

print(part1())

def part2():
    q = Queue()
    g = 0
    h = xd + yd
    q.put((g + h, g, h, 1, 1))
    dir = [(0, 1), (1, 0), (-1, 0), (0, -1)]
    foo = np.ones((H, W), dtype=np.uint32) * -1
    visited = set()
    prev = {}
    while True:
        f, g, h, x, y = q.get()
        visited.add((x, y))
        if g > 50:
            return np.sum(foo > -1)
        foo[y, x] = g
        for i, j in dir:
            x_, y_ = x + i, y + j
            if x_ > -1 and x_ < W and y_ > -1 and y_ < H and (x_, y_) not in visited:
                if map[y_, x_] != 0:
                    prev[(y_, x_)] = (x, y)
                    g_ = g + 1
                    h_ = abs(xd - x_) + abs(yd - y_)
                    q.put((g_ + h_, g_, h_, x_, y_))

print(part2())
