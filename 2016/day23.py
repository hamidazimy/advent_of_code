import sys

inp = [line.strip().split() for line in sys.stdin]

def run(code, regs):
    toggle_to = {"tgl": "inc", "inc": "dec", "dec": "inc", "jnz": "cpy", "cpy": "jnz"}

    def val(x):
        if x in regs:
            return regs[x]
        else:
            return int(x)

    ip = 0
    while ip in range(len(code)):
        if code[ip][0] == "cpy":
            if code[ip][2] in regs:
                regs[code[ip][2]] = val(code[ip][1])
            ip += 1
        elif code[ip][0] == "inc":
            regs[code[ip][1]] += 1
            ip += 1
        elif code[ip][0] == "dec":
            regs[code[ip][1]] -= 1
            ip += 1
        elif code[ip][0] == "jnz":
            if val(code[ip][1]) != 0:
                ip += val(code[ip][2])
            else:
                ip += 1
        elif code[ip][0] == "tgl":
            try:
                tp = ip + val(code[ip][1])
                code[tp][0] = toggle_to[code[tp][0]]
            except:
                pass
            ip += 1
    return regs['a']


def part1(inp=inp):
    regs = {"a": 7, "b": 0, "c": 0, "d": 0}
    code = [[x for x in ins] for ins in inp]
    return run(code, regs)

print(part1())

def part2(inp=inp):
    regs = {"a": 12, "b": 0, "c": 0, "d": 0}
    code = [[x for x in ins] for ins in inp]
    return run(code, regs)

print(part2())
