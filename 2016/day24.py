import sys
from queue import Queue, PriorityQueue

map = [list(line.strip()) for line in sys.stdin]
H = len(map)
W = len(map[0])

points = []

for i in range(H):
    for j in range(W):
        if map[i][j] != '.' and map[i][j] != '#':
            points.append((i, j))
            if map[i][j] == '0':
                start_point = (i, j)
            if map[i][j] == '1':
                end_point = (i, j)

P = len(points)

def a_star(start, end):
    Y, X = end
    q = PriorityQueue()
    visited = set()
    q.put((0, 0, None, start))
    while True:
        f, g, h, current = q.get()
        if current in visited:
            continue
        visited.add(current)
        if current == end:
            return f
        y, x = current

        for dy, dx in [(1, 0), (0, 1), (-1, 0), (0, -1)]:
            y_ = y + dy
            x_ = x + dx
            if y_ in range(H) and x_ in range(W) and map[y_][x_] != '#':
                g_ = g + 1
                h_ = abs(Y - y_) + abs(X - x_)
                q.put((g_ + h_, g_, h_, (y_, x_)))

distances = [[0] * P for i in range(P)]

for i in range(P):
    for j in range(P):
        if i == j:
            continue
        distances[i][j] = a_star(points[i], points[j])

def part1():
    q = PriorityQueue()
    q.put((0, 0, [0]))
    c = 0
    while True:
        l, _, path = q.get()
        V = set(path)
        if V == set(range(P)):
            return l#, path)
        for v in range(P):
            if v == path[-1] or (v in V and v != 0):
                continue
            c += 1
            new_l = l + distances[path[-1]][v]
            new_path = path[:]
            new_path.append(v)
            q.put((new_l, c, new_path))

print(part1())

def part2():
    q = PriorityQueue()
    q.put((0, 0, [0]))
    c = 0
    while True:
        l, _, path = q.get()
        V = set(path)
        if V == set(range(P)) and path[-1] == 0:
            return l#, path)
        for v in range(P):
            if v == path[-1] or (v in V and v != 0):
                continue
            c += 1
            new_l = l + distances[path[-1]][v]
            new_path = path[:]
            new_path.append(v)
            q.put((new_l, c, new_path))
    return

print(part2())
