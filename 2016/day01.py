import sys

steps = sys.stdin.readline().strip().split(", ")
turn = [x[0] for x in steps]
walk = [int(x[1:]) for x in steps]

def part1(turn=turn, walk=walk):
    dirs = [+1, +1, -1, -1]
    dir = 0
    x, y = 0, 0
    for i in range(len(steps)):
        dir = (dir + (1 if turn[i] == 'R' else -1)) % 4
        x += (dir % 2 != 0) * dirs[dir] * walk[i]
        y += (dir % 2 == 0) * dirs[dir] * walk[i]
    return abs(x) + abs(y)

print(part1())

def part2(turn=turn, walk=walk):
    dirs = [+1, +1, -1, -1]
    dir = 0
    x, y = 0, 0
    S = set()
    for i in range(len(steps)):
        dir = (dir + (1 if turn[i] == 'R' else -1)) % 4
        for w in range(walk[i]):
            x += (dir % 2 != 0) * dirs[dir]
            y += (dir % 2 == 0) * dirs[dir]
            if (x, y) in S:
                return abs(x) + abs(y)
            else:
                S.add((x, y))

print(part2())
