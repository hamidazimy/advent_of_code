import sys
import re
from collections import Counter

rooms = []

for line in sys.stdin:
    m = re.match(r"(.*)-([0-9]*)\[(.*)\]\n", line)
    c = Counter(sorted(m[1].replace('-', '')))
    top5 = ''.join(sorted([x[0] for x in c.most_common(5)]))
    if top5 == ''.join(sorted(m[3])):
        rooms.append({"name": m[1], "id": int(m[2])})

def part1(rooms=rooms):
    result = 0
    for room in rooms:
        result += room["id"]
    return result

print(part1())

def part2(rooms=rooms):
    for room in rooms:
        name = ''.join([chr((ord(x) - ord('a') + room["id"]) % 26 + ord('a')) if x != '-' else ' ' for x in room["name"]])
        if name.find("northpole") >= 0:
            return room["id"]

print(part2())
