import sys
from scanf import scanf
import numpy as np
from time import sleep

ip, = scanf("#ip %d", sys.stdin.readline().strip())
instructions = [scanf("%s %d %d %d", l.strip()) for l in sys.stdin]


def addr(reg, ins):
    reg[ins[3]] = reg[ins[1]] + reg[ins[2]]


def addi(reg, ins):
    reg[ins[3]] = reg[ins[1]] + ins[2]


def mulr(reg, ins):
    reg[ins[3]] = reg[ins[1]] * reg[ins[2]]


def muli(reg, ins):
    reg[ins[3]] = reg[ins[1]] * ins[2]


def banr(reg, ins):
    reg[ins[3]] = reg[ins[1]] & reg[ins[2]]


def bani(reg, ins):
    reg[ins[3]] = reg[ins[1]] & ins[2]


def borr(reg, ins):
    reg[ins[3]] = reg[ins[1]] | reg[ins[2]]


def bori(reg, ins):
    reg[ins[3]] = reg[ins[1]] | ins[2]


def setr(reg, ins):
    reg[ins[3]] = reg[ins[1]]


def seti(reg, ins):
    reg[ins[3]] = ins[1]


def gtir(reg, ins):
    reg[ins[3]] = int(ins[1] > reg[ins[2]])


def gtri(reg, ins):
    reg[ins[3]] = int(reg[ins[1]] > ins[2])


def gtrr(reg, ins):
    reg[ins[3]] = int(reg[ins[1]] > reg[ins[2]])


def eqir(reg, ins):
    reg[ins[3]] = int(ins[1] == reg[ins[2]])


def eqri(reg, ins):
    reg[ins[3]] = int(reg[ins[1]] == ins[2])


def eqrr(reg, ins):
    reg[ins[3]] = int(reg[ins[1]] == reg[ins[2]])


values = set()
last_value = -1
def eqrr2(reg, ins):
    global values, last_value
    if reg[ins[1]] in values:
        print(last_value)
        reg[ins[3]] = 1
    else:
        values.add(reg[ins[1]])
        reg[ins[3]] = 0
        print("{}".format(reg[ins[1]]))
        print("len: {}".format(len(values)))
        return True


def part1():
    reg = [0, 0, 0, 0, 0, 0]
    while True:
        globals()[instructions[reg[ip]][0]](reg, instructions[reg[ip]])
        reg[ip] += 1

        # Catch the first time that there is a chance for a halt (ins[29]).
        # Then, return the value of reg[5], which should be the same as the
        # value of reg[0] for the code to halt.
        # If your input is different (in instruction # and reg #),
        # change those values accordingly.
        if reg[ip] == 29:
            return reg[5]

print(part1())

def part2():
    r = 0x010000
    t = 0 # the value!
    values = set()
    last_value = -1
    flag = False
    while t not in values:
        last_value = t
        t = 0x73C1AC
        while r != 0:
            if flag:
                print("{:X}".format(r))
            t += r & 0xFF
            t *= 0x1016B
            t &= 0xFFFFFF
            r >>= 8
        r = t | 0x010000
        values.add(last_value)
    return last_value

print(part2())
