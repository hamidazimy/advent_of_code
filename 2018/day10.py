import sys
from scanf import scanf
import numpy as np
from time import sleep

lines = sys.stdin.readlines()
N = len(lines)
points = np.zeros((N, 2), dtype=np.int32)
velocities = np.zeros((N, 2), dtype=np.int32)
x, y = 0, 1
for i in range(N):
    points[i, x] = int(lines[i][10:16])
    points[i, y] = int(lines[i][18:24])
    velocities[i, x] = int(lines[i][36:38])
    velocities[i, y] = int(lines[i][40:42])

total_seconds = 0

def part1(points=points, velocities=velocities):
    global total_seconds
    step = 100
    while True:
        points += velocities * step
        diff = np.max(points, axis=0) - np.min(points, axis=0)
        total_seconds += step
        if np.max(diff) < 2000:
            step = 1
        if np.min(diff) < 10:
            break

    mins = np.min(points, axis=0)
    maxs = np.max(points, axis=0)

    rang = maxs - mins + 1

    message = np.zeros(rang, dtype=np.int32)

    for n in range(N):
        message[points[n, x] - mins[x], points[n, y] - mins[y]] = 1

    result = '\n'.join([''.join([" █"[message[j, i]] for j in range(rang[x])]) for i in range(rang[y])])
    return result

print(part1())

def part2(points=points, velocities=velocities):
    return total_seconds

print(part2())
