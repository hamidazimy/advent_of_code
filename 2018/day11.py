import sys
import numpy as np

sn = int(sys.stdin.readline())

C = 300

x = np.cumsum(np.ones((C, C), dtype=np.int32), axis=1)
y = np.cumsum(np.ones((C, C), dtype=np.int32), axis=0)

powers = (((x + 10) * y + sn) * (x + 10)) // 100 % 10 - 5

def print_powers(grid):
    h, w = np.shape(grid)
    for i in range(h):
        for j in range(w):
            print("{:>3}".format(grid[i, j]), end='')
        print()

def part1(powers=powers):
    powers = np.copy(powers)
    cell = np.zeros((C, C), dtype=np.int32)
    maxx = 0
    max_i = 0
    max_j = 0

    for i in range(1, C - 1):
        for j in range(1, C - 1):
            cell[i, j] = np.sum(powers[i - 1: i + 2, j - 1: j + 2])
            if cell[i, j] > maxx:
                maxx = cell[i, j]
                max_i = i
                max_j = j

    return f"{max_j},{max_i}"

print(part1())

def part2(powers=powers):
    powers = np.copy(powers)
    maxx = -1000
    result = ""

    for s in range(1, C + 1):
        # print("===={}====".format(s))
        for i in range(1, C + 2 - s):
            for j in range(1, C + 2 - s):
                temp = np.sum(powers[i - 1: i - 1 + s, j - 1: j - 1 + s])
                if temp > maxx:
                    maxx = temp
                    result = "{},{},{}".format(j, i, s)
                    # print(result)
        # Remove this! this is because I know my answer is not over 16.
        if s > 16:
            break

    return result

print(part2())
