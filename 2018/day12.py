import sys
from scanf import scanf

tran = {}

rpad = 86
lpad = 6

init, = scanf("initial state: %s", sys.stdin.readline().strip())
init = '.' * lpad + init + '.' * rpad
L = len(init)
sys.stdin.readline()

for line in sys.stdin.readlines():
    state, result = scanf("%s => %c", line.strip())
    tran[state] = result

def part1(init=init, tran=tran):
    prev = init
    # print("{:3}: {}".format(0, prev))
    for gen in range(20):
        next = list(prev)
        for i in range(2, L - 2):
            next[i] = tran.get(prev[i - 2 : i + 3], '.')
        prev = ''.join(next)

        res = 0
        for i in range(len(next)):
            if next[i] == '#':
                res += (i - lpad)

        # print("{:3}: {} [{}]".format(gen + 1, prev, res))

    return res

print(part1())

def part2(init=init, tran=tran):
    prev = init
    zero_index = lpad
    res = 0
    for i in range(len(prev)):
        if prev[i] == '#':
            res += (i - zero_index)
    # print("{:4}: [{:4}] {} [{:5}]".format(0, zero_index, prev, res))

    score = [res]

    for gen in range(200):
        next = list(prev)
        for i in range(2, L - 2):
            next[i] = tran.get(prev[i - 2 : i + 3], '.')
        prev = ''.join(next)

        res = 0
        for i in range(len(next)):
            if next[i] == '#':
                res += (i - zero_index)

        new_lpad = prev.find('#')
        prev = '.' * lpad + prev[new_lpad:] + '.' * L
        prev = prev[:L]
        zero_index -= new_lpad - lpad

        # print("{:4}: [{:4}] {} [{:5}]".format(gen + 1, zero_index, prev, res))
        score.append(res)

    return score[200] + (score[200] - score[199]) * (50000000000 - 200)

print(part2())
