import sys
from scanf import scanf
import numpy as np
from time import sleep

lines = [line.strip() for line in sys.stdin]
map = np.ones((len(lines) + 2, len(lines[0]) + 2), dtype=np.int32) * -1
ind = [' ', '|', '#', '@']

for i in range(len(lines)):
    for j in range(len(lines[0])):
        if lines[i][j] == '.':
            map[i + 1][j + 1] = 0
        elif lines[i][j] == '|':
            map[i + 1][j + 1] = 1
        elif lines[i][j] == '#':
            map[i + 1][j + 1] = 2

def map_str(map):
    H, W = np.shape(map)
    return '\n' + '\n'.join([''.join([ind[map[i, j]] for j in range(W)]) for i in range(H)])

def step(map):
    H, W = np.shape(map)
    temp = np.ones(np.shape(map), dtype=np.int32) * -1
    for i in range(1, H - 1):
        for j in range(1, W - 1):
            adjs = map[i - 1:i + 2, j - 1:j + 2].copy()
            adjs [1, 1] = -1
            temp[i, j] =  map[i, j]
            if map[i, j] == 0 and np.sum(adjs == 1) >= 3:
                temp[i, j] = 1
            elif map[i, j] == 1 and np.sum(adjs == 2) >= 3:
                temp[i, j] = 2
            elif map[i, j] == 2 and (np.sum(adjs == 1) == 0 or np.sum(adjs == 2) == 0):
                temp[i, j] = 0
    for i in range(H):
        for j in range(W):
            map[i, j] = temp[i, j]
    return map



def part1(map=map, visualize=False):
    map = np.copy(map)
    for i in range(10):
        map = step(map)
        if visualize:
            print(map_str(map))
    return np.sum(map == 1) * np.sum(map == 2)

print(part1())

def part2(map=map, visualize=False):
    map = np.copy(map)
    resources = [0] * 1000
    for i in range(1000):
        resources[i] = np.sum(map == 1) * np.sum(map == 2)
        map = step(map)
        if visualize:
            print(map_str(map))

    loop = []
    while True:
        loop.append(np.sum(map == 1) * np.sum(map == 2))
        map = step(map)
        if loop[-1] == resources[-1]:
            break

    return loop[(1000000000 - 1000) % len(loop)]

print(part2())
