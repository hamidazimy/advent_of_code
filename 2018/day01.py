import sys

inp = [int(line) for line in sys.stdin]

def part1(inp=inp):
    return sum(inp)

print(part1())

def part2(inp=inp):
    s = set()
    freq = 0
    while True:
        for i in inp:
            if freq in s:
                return freq
            s.add(freq)
            freq += i

print(part2())
