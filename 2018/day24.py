import sys
from scanf import scanf
import re
import copy


class Group:
    def __init__(self, str, type):
        self.type = type
        match = re.match(r"(\d+) units each with (\d+) hit points(.*)?with an attack that does (\d+) (\S*) damage at initiative (\d+)", str)
        self.units = int(match.group(1))
        self.hitpoint = int(match.group(2))
        self.damage = int(match.group(4))
        self.attack = match.group(5)
        self.initiative = int(match.group(6))
        self.weaknesses = []
        self.immunities = []
        self.target = None

        weim = [] if match.group(3) == " " else match.group(3)[2: -2].split("; ")
        for j in weim:
            wi, ls = j.split(" to ")
            if wi == "weak":
                self.weaknesses = ls.split(", ")
            if wi == "immune":
                self.immunities = ls.split(", ")

    def __repr__(self):
        return "{} units each with {} hit points with an attack that does {} {} damage at initiative {}. (weak to {}; immune to {})".format(
            self.units,
            self.hitpoint,
            self.damage,
            self.attack,
            self.initiative,
            ", ".join(self.weaknesses),
            ", ".join(self.immunities)
        )

    def effective_power(self):
        return self.units * self.damage

    def damage_to(self, defender):
        if defender is None:
            return -2
        if defender.type == self.type:
            return -1
        damage = self.effective_power()
        if self.attack in defender.weaknesses:
            damage *= 2
        elif self.attack in defender.immunities:
            damage = 0
        return damage

    def boosted(self, boost):
        x = copy.copy(self)
        x.damage += boost
        return x


imms = []
infs = []

sys.stdin.readline() # "Immune System:"
while True:
    imm = sys.stdin.readline().strip()
    if imm == "":
        break
    imms.append(Group(imm, "imm"))

sys.stdin.readline() # "Infection:"
while True:
    inf = sys.stdin.readline().strip()
    if inf == "":
        break
    infs.append(Group(inf, "inf"))


def battle(imms=imms, infs=infs, boost=0):
    boosted_imms = [x.boosted(boost) for x in imms]
    cloned_infs = [copy.copy(x) for x in infs]
    all = boosted_imms + cloned_infs
    total_units_killed = 1
    while total_units_killed > 0:
        total_units_killed = 0

        # Target Selection
        all.sort(key=lambda x: x.effective_power() + x.initiative / 100, reverse=True)
        targetted = []
        for attacker in all:
            defenders = [d for d in all if d.type != attacker.type and d not in targetted]
            defenders.sort(key=lambda x: attacker.damage_to(x) * 100000 + x.effective_power() + x.initiative / 100, reverse=True)
            attacker.target = None if len(defenders) == 0 or attacker.damage_to(defenders[0]) == 0 else defenders[0]
            targetted.append(attacker.target)

        # Attacking
        all.sort(key=lambda x: x.initiative, reverse=True)
        for attacker in all:
            if attacker.units == 0 or attacker.target is None:
                continue
            damage = attacker.damage_to(attacker.target)
            units_killed = damage // attacker.target.hitpoint
            attacker.target.units = max(0, attacker.target.units - units_killed)
            total_units_killed += units_killed

        all = [u for u in all if u.units != 0]

    if 0 != sum([x.units for x in all if x.type == "imm"]) * sum([x.units for x in all if x.type == "inf"]):
        # Incomplete battle!
        return float("NaN")

    return sum([x.units for x in all]) * (-1 if all[0].type == "inf" else 1)


def part1():
    return abs(battle())

print(part1())

def part2():
    def binary_search(start, end):
        middle = (start + end) // 2
        if middle == start:
            s = battle(boost=start)
            e = battle(boost=end)
            if s > 0:
                return s
            else:
                return e
        m = battle(boost=middle)
        if m > 0:
            return binary_search(start, middle)
        else:
            return binary_search(middle, end)

    return binary_search(0, 1024)

print(part2())
