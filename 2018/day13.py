import sys
from time import sleep

map = [line[:-1] for line in sys.stdin]

faces = {'<': [0, -1, '-'], '>': [0, 1, '-'], '^': [-1, 0, '|'], 'v': [1, 0, '|']}

tran =  {
            '<': {'-': '<', '\\': '^', '/': 'v'},
            '>': {'-': '>', '\\': 'v', '/': '^'},
            '^': {'|': '^', '\\': '<', '/': '>'},
            'v': {'|': 'v', '\\': '>', '/': '<'},
        }

cros =  {
            '<': ['v', '<', '^'],
            '>': ['^', '>', 'v'],
            '^': ['<', '^', '>'],
            'v': ['>', 'v', '<'],
        }

def str_replace(str, index, char):
    return str[:index] + char + str[index + 1:]

def next_face(face, next, p):
    if next == '+':
        return cros[face][p % 3], p + 1
    return tran[face][next], p

def map_str(map, carts_map=None, with_numbers=True):
    H, W = len(map), len(map[0])
    map_with_carts = []
    if with_numbers:
        map_with_carts.append("   " + ''.join([str(j // 10 % 10) for j in range(W)]))
        map_with_carts.append("   " + ''.join([str(j    %    10) for j in range(W)]))
    for i in range(H):
        row = "{:3}".format(i) if with_numbers else ""
        for j in range(W):
            print()
            row += carts_map[i][j] if carts_map is not None and carts_map[i][j] != ' ' else map[i][j]
        map_with_carts.append(row)
    return '\n'.join(map_with_carts)

def part1(map=map[:]):
    carts = []
    for i in range(len(map)):
        for j in range(len(map[i])):
            if map[i][j] in faces.keys():
                carts.append([i, j, 0, faces[map[i][j]][2]])

    while True:
        for c in range(len(carts)):
            i, j, p, prev = carts[c]
            face = map[i][j]
            di, dj, trash = faces[face]
            i_ = i + di
            j_ = j + dj
            next = map[i_][j_]
            try:
                face_, p_ = next_face(face, next, p)
            except KeyError as e:
                # print(map_str(map))
                return "{},{}".format(j_, i_)
            map[i_] = str_replace(map[i_], j_, face_)
            map[i] = str_replace(map[i], j, prev)
            carts[c] = [i_, j_, p_, next]

        carts = sorted(carts)

print(part1())

def part2(map=map[:]):
    carts_map = []
    carts = []
    for i in range(len(map)):
        carts_map.append(' ' * len(map[i]))
        for j in range(len(map[i])):
            if map[i][j] in faces.keys():
                carts_map[i] = str_replace(carts_map[i], j, map[i][j])
                map[i] = str_replace(map[i], j, faces[map[i][j]][2])
                carts.append([i, j, 0])

    x = 0
    while True:
        x += 1
        k = 0
        while k < len(carts):
            i, j, p = carts[k]
            if carts_map[i][j] == ' ':
                # print("[{}] cart {} deleted".format(x, carts[k]))
                del carts[k]
            else:
                k += 1
        if len(carts) == 1:
            return "{},{}".format(carts[0][1], carts[0][0])
        for c in range(len(carts)):
            i, j, p = carts[c]
            face = carts_map[i][j]
            try:
                di, dj, trash = faces[face]
            except KeyError as e:
                # print("[{}] cart {} has been destroyed previously!".format(x, c))
                continue
            i_ = i + di
            j_ = j + dj
            next = map[i_][j_]
            face_, p_ = next_face(face, next, p)
            if carts_map[i_][j_] == ' ':
                carts_map[i_] = str_replace(carts_map[i_], j_, face_)
            else:
                carts_map[i_] = str_replace(carts_map[i_], j_, ' ')
            carts_map[i] = str_replace(carts_map[i], j, ' ')
            carts[c] = [i_, j_, p_]

        carts = sorted(carts)
        # print("\x1b[H\x1b[2J")
        # print(map_str(map, carts_map))
        # sleep(1)

print(part2())
