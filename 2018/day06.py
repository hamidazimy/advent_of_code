import sys
from scanf import scanf
import numpy as np

coords = np.array([list(scanf("%d, %d", line.strip())) for line in sys.stdin])

def grid_str(grid):
    H, W = np.shape(grid)
    return '\n'.join([''.join([f"{int(grid[i, j]):>8}" for j in range(W)]) for i in range(H)])

def part1(coords=coords):
    min_x = np.min(coords[:, 0])
    max_x = np.max(coords[:, 0])
    min_y = np.min(coords[:, 1])
    max_y = np.max(coords[:, 1])

    grid_size = max(min_x + max_x, min_y + max_y)
    grid = np.zeros((grid_size, grid_size))

    for i in range(grid_size):
        for j in range(grid_size):
            distances = np.sum(np.absolute(coords - [i, j]), axis=1)
            argmin = np.argmin(distances)
            if sum(distances == distances[argmin]) == 1:
                grid[i, j] = argmin
            else:
                grid[i, j] = -1

    infs = set()
    for i in range(-1000, max_x + 1000):
        j = -1000
        nearest = np.argmin(np.sum(np.absolute(coords - [i, j]), axis=1))
        infs.add(nearest)
        j = +1000
        nearest = np.argmin(np.sum(np.absolute(coords - [i, j]), axis=1))
        infs.add(nearest)

    for j in range(-1000, max_y + 1000):
        i = -1000
        nearest = np.argmin(np.sum(np.absolute(coords - [i, j]), axis=1))
        infs.add(nearest)
        i = 1000
        nearest = np.argmin(np.sum(np.absolute(coords - [i, j]), axis=1))
        infs.add(nearest)

    num_near = {}
    for i in range(len(coords)):
        if i in infs:
            continue
        num_near[i] = np.sum(grid == i)

    return max(num_near.values())

print(part1())

def part2(coords=coords):
    min_x = np.min(coords[:, 0])
    max_x = np.max(coords[:, 0])
    min_y = np.min(coords[:, 1])
    max_y = np.max(coords[:, 1])

    grid_size = max(min_x + max_x, min_y + max_y)
    grid = np.zeros((grid_size, grid_size))

    for i in range(grid_size):
        for j in range(grid_size):
            grid[i, j] = np.sum(np.absolute(coords - [i, j]))

    return np.sum(grid < 10000)

print(part2())
