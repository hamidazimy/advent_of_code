import sys
import re
from scanf import scanf
import numpy as np

notes = [line.strip() for line in sys.stdin]
notes.sort()
sleeps = {}
timesheet = {}
day = 0
elf = 0
for note in notes:
    if re.match(".*begins shift", note) is not None:
        lastasleep = 0
        foo = scanf("%s %s Guard #%d begins shift", note)
        elf = foo[-1]
        if elf not in timesheet:
            timesheet[elf] = np.array([0] * 60)
    else:
        if re.match(".*falls asleep", note) is not None:
            foo = scanf("%s 00:%d] falls asleep", note)
            lastasleep = foo[-1]
        else:
            foo = scanf("%s 00:%d] wakes up", note)
            wakeup = foo[-1]
            sleeps[elf] = sleeps.get(elf, 0) + (wakeup - lastasleep)
            timesheet[elf][lastasleep:wakeup] += 1

def part1(notes=notes, sleeps=sleeps):
    gaurd = max(sleeps, key=sleeps.get)
    matrix = np.zeros((365, 60))
    day = 0

    for note in notes:
        if re.match(".*begins shift", note) is not None:
            lastasleep = 0
            foo = scanf("%s %s Guard #%d begins shift", note)
            elf = foo[-1]
            day += 1
        else:
            if elf != gaurd:
                continue
            if re.match(".*falls asleep", note) is not None:
                foo = scanf("%s 00:%d] falls asleep", note)
                lastasleep = foo[-1]
            else:
                foo = scanf("%s 00:%d] wakes up", note)
                wakeup = foo[-1]
                matrix[day, lastasleep:wakeup] = 1

    minute = np.argmax(np.sum(matrix, axis=0))

    return gaurd * minute

print(part1())

def part2(timesheet=timesheet):
    max_elf = 0
    max_sleep = 0
    may_minute = 0

    for temp_elf in timesheet:
        temp_sleep = np.max(timesheet[temp_elf])
        temp_minute = np.argmax(timesheet[temp_elf])
        if temp_sleep > max_sleep:
            max_elf = temp_elf
            max_sleep = temp_sleep
            max_minute = temp_minute

    return max_elf * max_minute

print(part2())
