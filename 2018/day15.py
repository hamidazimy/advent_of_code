import sys
import numpy as np
from time import sleep
import queue

str_map = [line[:-1] for line in sys.stdin]

def log(str):
    return
    sys.stderr.write("\x1b[2m" + str + "\x1b[0m")

def logln(str):
    return
    sys.stderr.write("\x1b[2m" + str + "\x1b[0m\n")

def neigh(map, i, j):
    neighs = []
    if map[i - 1, j] != -1:
        neighs.append((i - 1, j, map[i - 1, j]))
    if map[i, j - 1] != -1:
        neighs.append((i, j - 1, map[i, j - 1]))
    if map[i, j + 1] != -1:
        neighs.append((i, j + 1, map[i, j + 1]))
    if map[i + 1, j] != -1:
        neighs.append((i + 1, j, map[i + 1, j]))
    return neighs

def dijkstra(map, i, j):
    dist = np.ones(np.shape(map)) * np.Inf
    prev = {}
    q = queue.PriorityQueue()
    dist[i, j] = 0
    q.put((0, i, j))
    while not q.empty():
        d, y, x = q.get()
        for y_, x_, c_ in neigh(map, y, x):
            alt = d + 1
            if alt < dist[y_, x_]:
                if c_ == 0:
                    q.put((alt, y_, x_))
                dist[y_, x_] = alt
                prev[(y_, x_)] = (y, x)
    return dist, prev

def lets_battle(attack_power, elves_should_win=True):
    global str_map
    H, W = len(str_map), len(str_map[0])
    map = np.zeros((H, W), dtype=np.int8)
    ind = ['.', 'E', 'G', '#']
    rev_ind = {'.': 0, 'E': 1, 'G': 2, '#': -1}
    map = np.zeros((H, W), dtype=np.int8)
    units = []
    for i in range(H):
        for j in range(W):
            map[i, j] = rev_ind[str_map[i][j]]
            if map[i, j] > 0:
                units.append((i, j, 200, str_map[i][j]))
    round = 0
    while True:
        # print("\x1b[H\x1b[2J")
        # print("After {} rounds:".format(round))
        map = (map == -1) * -1
        details = [[] for z in range(H)]
        for y, x, h, c in units:
            map[y, x] = rev_ind[c]
            details[y].append("{}({})".format(c, h))
        # for i in range(H):
        #     for j in range(W):
        #         print(ind[map[i, j]], end='')
        #     print("\t{}".format(", ".join(details[i])))

        if len(set([u[-1] for u in units])) == 1:
            break
        round += 1

        u = 0
        while u < len(units):
            logln("unit[{}]: {}".format(u, units[u]))
            i, j, h, c = units[u]
            dist, prev = dijkstra(map, i, j)

            nearest_distance = np.Inf
            nearest_index = -1
            for t in range(len(units)):
                if units[t][-1] == c:
                    continue
                if dist[units[t][0], units[t][1]] < nearest_distance:
                    nearest_distance = dist[units[t][0], units[t][1]]
                    logln("\t>>>{}".format(nearest_distance))
                    nearest_index = t
            if nearest_index < 0:
                logln("\tNO nearest target")
                u += 1
                continue
            logln("\tNearest target: {}".format(units[nearest_index]))
            i_, j_, h_, c_ = units[nearest_index]
            if prev[(i_, j_)] != (i, j):
                while prev[i_, j_] != (i, j):
                    i_, j_ = prev[(i_, j_)]
                units[u] = (i_, j_, h, c)
                map[i, j] = 0
                i, j = i_, j_
                map[i, j] = rev_ind[c]
                logln("\tMoving to {}".format((i_, j_)))

            weakest_hp = np.Inf
            weakest_index = -1
            for a in range(len(units)):
                if units[a][-1] == c:
                    continue
                log("\t")
                if abs(units[a][0] - i) + abs(units[a][1] - j) == 1:
                    log("target: ")
                    if units[a][2] < weakest_hp:
                        weakest_hp = units[a][2]
                        weakest_index = a
                logln("{}".format(units[a]))
            if weakest_index >= 0:
                logln("\tAttacking {}".format(units[weakest_index]))
                p, q, g, d = units[weakest_index]
                g -= attack_power[c]
                units[weakest_index] = (p, q, g, d)

                if g <= 0:
                    if elves_should_win and d == 'E':
                        return -1
                    map[p][q] = 0
                    units.pop(weakest_index)
                    if weakest_index < u:
                        u -= 1
            else:
                logln("\tNO target in range. Skip attacking")
            u += 1
            if len(set([u[-1] for u in units])) == 1:
                break

        if u != len(units):
            round -= 1
        units.sort()

    hps = sum([u[2] for u in units])
    # print("Outcome: {} * {} = {}".format(round, hps, round * hps))
    return round * hps

def part1():
    return lets_battle({'G': 3, 'E': 3}, False)

print(part1())

def part2():
    attack_power = {'G': 3, 'E': 3}
    while True:
        attack_power['E'] += 1
        outcome = lets_battle(attack_power)
        if outcome > 0:
            return outcome

print(part2())
