from __future__ import annotations
from dataclasses import dataclass
import sys
from scanf import scanf

pnum, last = scanf("%d players; last marble is worth %d points\n", sys.stdin.readline())


@dataclass
class Node:
    value : int
    prev : Node
    next : Node


def solve(pnum, last):
    scores = [0] * pnum
    zero = Node(0, None, None)
    zero.prev = zero
    zero.next = zero
    current = zero
    for round in range(1, last + 1):
        if round % 23 == 0:
            scores[round % pnum] += round
            for i in range(7):
                current = current.prev
            prevc = current.prev
            nextc = current.next
            prevc.next = nextc
            nextc.prev = prevc
            scores[round % pnum] += current.value
            current = nextc
        else:
            next1 = current.next
            next2 = current.next.next
            marble = Node(round, next1, next2)
            next1.next = marble
            next2.prev = marble
            current = marble
    return max(scores)

def part1(pnum=pnum, last=last):
    return solve(pnum, last)

print(part1())

def part2(pnum=pnum, last=last):
    return solve(pnum, last * 100)

print(part2())
