import sys
from scanf import scanf
import numpy as np
from queue import PriorityQueue
from time import sleep

lines = [l.strip() for l in sys.stdin]

n = len(lines)
bots = np.zeros((n, 3), dtype=np.int32)
rads = np.zeros((n), dtype=np.uint32)
i = 0
for line in lines:
    x, y, z, r = scanf("pos=<%d,%d,%d>, r=%d", line)
    bots[i, :] = [x, y, z]
    rads[i] = r
    i += 1

def part1(bots=bots, rads=rads):
    strongest = np.argmax(rads)
    return np.sum(np.sum(np.abs(bots - bots[strongest, :]), axis=1) <= rads[strongest])

print(part1())

def part2(bots=bots, rads=rads):
    q = PriorityQueue()

    r = 2 ** 29
    o = np.array([0, 0, 0])
    mins = o - r
    maxs = o + r
    w = np.abs(-1 + (bots > mins) + (bots > maxs))
    v = np.min(np.abs(np.array([bots - mins, bots - maxs])), axis=0)
    d = np.reshape(np.sum(np.abs(w * v), axis=1), (n,))
    c = np.sum(rads < d)
    q.put((c, np.sum(np.abs(o)) - r, -r, o[0], o[1], o[2]))

    dirs = np.array([
                [ 1,  1,  1],
                [ 1,  1, -1],
                [ 1, -1,  1],
                [ 1, -1, -1],
                [-1,  1,  1],
                [-1,  1, -1],
                [-1, -1,  1],
                [-1, -1, -1],
            ])

    while True:
        c, _, _r, x, y, z = q.get()
        o = np.array([x, y, z])
        r = -_r
        if r == 0:
            return np.sum(np.abs(o))

        r_2 = r // 2
        if r_2 > 0:
            for i in range(8):
                o_2 = o + dirs[i] * r_2
                mins_2 = o_2 - r_2
                maxs_2 = o_2 + r_2
                w_2 = np.abs(-1 + (bots > mins_2) + (bots > maxs_2))
                v_2 = np.min(np.abs(np.array([bots - mins_2, bots - maxs_2])), axis=0)
                d_2 = np.reshape(np.sum(np.abs(w_2 * v_2), axis=1), (n,))
                c_2 = np.sum(rads < d_2)
                q.put((c_2, np.sum(np.abs(o_2)), -r_2, o_2[0], o_2[1], o_2[2]))
        else:
            for x_2 in [x - 1, x, x + 1]:
                for y_2 in [y - 1, y, y + 1]:
                    for z_2 in [z - 1, z, z + 1]:
                        o_2 = [x_2, y_2, z_2]
                        d_2 = np.reshape(np.sum(np.abs(bots - o_2), axis=1), (n,))
                        c_2 = np.sum(rads < d_2)
                        q.put((c_2, np.sum(np.abs(o_2)), -r_2, o_2[0], o_2[1], o_2[2]))

print(part2())
