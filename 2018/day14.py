import sys
from scanf import scanf
import numpy as np
from time import sleep

inp = int(sys.stdin.readline())

class Node:
    def __init__(self, prev, valu, next):
        self.prev = prev
        self.valu = valu
        self.next = next

def seq_str(first, elf1, elf2, score_ind=None):
    result = ""
    temp = first
    while True:
        if temp == score_ind:
            result += f"|{temp.valu}| "
        elif temp == elf1:
            result += f"({temp.valu}) "
        elif temp == elf2:
            result += f"[{temp.valu}] "
        else:
            result += f" {temp.valu}  "
        temp = temp.next
        if temp == first:
            break
    return result

def part1(inp=inp):
    l = 2

    elf1 = Node(None, 3, None)
    elf2 = Node(elf1, 7, elf1)
    elf1.prev = elf2
    elf1.next = elf2
    first = elf1

    while l < inp + 10:
        new = [int(x) for x in str(elf1.valu + elf2.valu)]

        for valu in new:
            last = first.prev
            temp = Node(last, valu, first)
            last.next = temp
            first.prev = temp
            l += 1

        for i in range(elf1.valu + 1):
            elf1 = elf1.next

        for i in range(elf2.valu + 1):
            elf2 = elf2.next

        # print(seq_str(first, elf1, elf2))

    score = first
    for i in range(inp):
        score = score.next

    result = ""
    for i in range(10):
        result += str(score.valu)
        score = score.next
    return result

print(part1())

def part2(inp=inp):
    l = 2

    elf1 = Node(None, 3, None)
    elf2 = Node(elf1, 7, elf1)
    elf1.prev = elf2
    elf1.next = elf2
    first = elf1
    score_ind = first

    while True:
        if l - 6 == 0:
            score_ind = first
        new = [int(x) for x in str(elf1.valu + elf2.valu)]

        for valu in new:
            last = first.prev
            temp = Node(last, valu, first)
            last.next = temp
            first.prev = temp
            l += 1

            score_ind = score_ind.next
            temp_ind = score_ind
            score = ""
            for i in range(6):
                score = score + str(temp_ind.valu)
                temp_ind = temp_ind.next

            # print("{:4}: {}".format(l - 6, score))
            if score == str(inp):
                return l - 6

        for i in range(elf1.valu + 1):
            elf1 = elf1.next

        for i in range(elf2.valu + 1):
            elf2 = elf2.next

print(part2())
