import sys
from scanf import scanf
import numpy as np
import queue
from time import sleep

reg = sys.stdin.readline().strip()[1:-1]

def point(X, y=None):
    p = np.zeros((2), dtype=np.uint8)
    if type(X) == type(p):
        p[0] = X[0]
        p[1] = X[1]
    else:
        p[0] = X
        p[1] = y
    return p
maxs = 208

sym = ['█', ' ']

map = np.zeros((maxs, maxs), dtype=np.uint32)
pos = np.ones((2), dtype=np.uint8) * (maxs // 2)
dir = {'N': point(-1, 0), 'E': point(0, 1), 'W': point(0, -1), 'S': point(1, 0)}
vis = np.zeros((maxs, maxs), dtype=np.uint32)

def pretty_map_str(map=map):
    halves = ['█', '▀', '▄', ' ']
    H, W = np.shape(map)
    result = ""
    for i in range(0, H, 2):
        for j in range(W):
            two = 2 * map[i, j] + map[i + 1, j]
            result += halves[two]
        result += '\n'
    return result

def map_str(map=map):
    H, W = np.shape(map)
    result = ""
    for i in range(H):
        for j in range(W):
            if i == maxs // 2 and j == maxs // 2:
                result += '*'
            elif map[i, j] == 0:
                result += '█'
            else:
                result += ' '#str(map[i, j] % 10)
        result += '\n'
    return result

stack = [point(pos)]

try:
    # map[pos[0], pos[1]] = 0
    for c in reg:
        if c in dir:
            # sleep(.02)
            pos += dir[c]
            map[pos[0], pos[1]] = 1
            pos += dir[c]
            map[pos[0], pos[1]] = 1
        elif c == '(':
            stack.append(point(pos))
        elif c == ')':
            pos = stack.pop()
        elif c == '|':
            pos = point(stack[-1])
finally:
    # print(map_str(map))
    pass

map = map * -1

pos = np.ones((2), dtype=np.uint8) * (maxs // 2)
map[pos[0], pos[1]] = 0
dis = 0

def dfs(pos, dis, prev):
    for i in dir:
        if i != prev:
            next = pos + dir[i]
            wall = map[next[0], next[1]]
            if wall != 0:
                next = next + dir[i]
                map[next[0], next[1]] = map[pos[0], pos[1]] + 1
                dfs(next, dis, i)

def bfs():
    q = queue.Queue()
    vis[pos[0], pos[1]] = 1
    q.put(pos)
    while q.qsize()>0:
        curr = q.get()
        for i in dir:
            next = curr + dir[i]
            wall = map[next[0], next[1]]
            if wall != 0:
                next = next + dir[i]
                if vis[next[0], next[1]] >= 1:
                    continue
                vis [next[0], next[1]] = vis[curr[0], curr[1]] + 1
                q.put(next)
    return vis

vis = bfs()

# print(map_str(map))

def part1(vis=vis):
    return np.max(vis) - 1

print(part1())

def part2(vis=vis):
    return np.sum(vis >= 1001)

print(part2())
