import sys
from scanf import scanf
import numpy as np
from copy import deepcopy

steps = set()
deps = {}

for line in sys.stdin:
    x, y = scanf("Step %c must be finished before step %c can begin.\n", line)
    steps.add(x)
    steps.add(y)

    if y not in deps:
        deps[y] = set()
    deps[y].add(x)

steps = list(steps)
steps.sort()

def part1(steps=steps, deps=deps):
    deps = deepcopy(deps)
    instructions = []
    result = ""
    while len(instructions) < len(steps):
        for i in steps:
            if i not in instructions:
                if 0 == len(deps.get(i, set())):
                    instructions.append(i)
                    result += i
                    for s in deps:
                        deps[s] = deps[s] - set(i)
                    break
    return result

print(part1())

def part2(steps=steps, deps=deps):
    deps = deepcopy(deps)
    steps = {x: ord(x) - 4 for x in steps}

    for i in steps:
        deps[i] = deps.get(i, set())

    def get_next(deps):
        nexts = []
        for i in deps:
            if len(deps[i]) == 0:
                return i
        return None

    instructions = []

    secs = 0
    workers = [['.', 0]] * 5
    # print("\t{:8}{:8}{:8}{:8}{:8}".format(1, 2, 3, 4, 5))
    while len(instructions) < len(steps):
        # print("\n{:3}\t".format(secs), end='')
        for i in range(len(workers)):
            if workers[i][1] > 1:
                workers[i][1] -= 1
            elif workers[i][0] != '.':
                instructions.append(workers[i][0])
                for s in deps:
                    deps[s] = deps[s] - set(workers[i][0])
                workers[i] = ['.', 0]
        for i in range(len(workers)):
            if workers[i][1] == 0:
                next = get_next(deps)
                if next is not None:
                    workers[i] = [next, steps[next]]
                    del deps[next]
            # print("{:>4}[{:2}]".format(workers[i][0], workers[i][1]), end='')
        secs += 1
    secs -= 1
    return secs

print(part2())
