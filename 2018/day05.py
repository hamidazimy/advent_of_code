import sys

polymer = list(sys.stdin.readline().strip())

def part1(polymer=polymer):
    finished = False

    while not finished:
        finished = True
        i = 0
        while i + 1 < len(polymer):
            if abs(ord(polymer[i]) - ord(polymer[i + 1])) == 32:
                polymer.pop(i)
                polymer.pop(i)
                finished = False
            else:
                i += 1
    return len(polymer)

print(part1())

def part2(polymer=polymer):
    minimum = float("Inf")

    for c in range(ord('a'), ord('z') + 1):
        q = polymer[:]

        i = 0
        while i < len(q):
            if q[i].lower() == chr(c):
                q.pop(i)
            else:
                i += 1

        finished = False
        while not finished:
            finished = True
            i = 0
            while i + 1 < len(q):
                if abs(ord(q[i]) - ord(q[i + 1])) == 32:
                    q.pop(i)
                    q.pop(i)
                    finished = False
                else:
                    i += 1

        minimum = min(len(q), minimum)

    return minimum

print(part2())
