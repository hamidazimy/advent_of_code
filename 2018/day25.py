import sys
from scanf import scanf
import numpy as np
from time import sleep

lines = [l.strip() for l in sys.stdin]
n = len(lines)
d = 4

points = np.zeros((n, 4))

for i in range(n):
    points[i, :] = scanf("%d,%d,%d,%d", lines[i])

cons = []
c = set()
c.add(0)
cons.append(c)
added = set()
added.add(0)
checked = set()

foo = np.repeat(np.reshape(points, (1, n, d)), n, axis=0)
bar = np.repeat(np.reshape(points, (n, 1, d)), n, axis=1)
dists = np.sum(np.abs(foo - bar), axis=2)

while len(added) < n:
    len_before = len(cons[-1])
    for i in list(cons[-1]):
        if i not in checked:
            for j in range(n):
                if j not in added:
                    if dists[i, j] <= 3:
                        cons[-1].add(j)
                        added.add(j)
            checked.add(i)
    if len_before == len(cons[-1]):
        for i in range(n):
            if i not in added:
                c = set()
                c.add(i)
                cons.append(c)
                break

print(len(cons))
