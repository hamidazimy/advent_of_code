import sys
from scanf import scanf
import numpy as np
from time import sleep

inp = [int(i) for i in sys.stdin.readline().strip().split()]

class Node:
    def __init__(self, remch, metas):
        self.parent = None
        self.metadata = []
        self.metas = metas
        self.children = []
        self.remch = remch
        self.value = 0

    def __str__(self):
        str = "[" + ",".join(["{}".format(x) for x in self.metadata]) + "]"
        str += "({})".format(self.value)
        str += "\nchildren:\n"
        for c in self.children:
            for l in c.__str__().split("\n"):
                str += "\t" + l + "\n"
        return str

def part1(inp=inp):
    stack = [[inp[0], inp[1]]]
    i = 2
    res = 0
    while len(stack) > 0:
        top = stack[-1]
        if top[0] == 0:
            for m in range(top[1]):
                res += inp[i]
                i += 1
            stack.pop()
        else:
            top[0] -= 1
            stack.append([inp[i], inp[i + 1]])
            i += 2
    return res

print(part1())

def part2(inp=inp):
    root = Node(inp[0], inp[1])
    stack = [root]
    i = 2

    while len(stack) > 0:
        top = stack[-1]
        if top.remch == 0:
            for m in range(top.metas):
                top.metadata.append(inp[i])
                i += 1
            if len(top.children) == 0:
                top.value = sum(top.metadata)
            else:
                for j in top.metadata:
                    top.value += top.children[j - 1].value if 0 < j <= len(top.children) else 0
            stack.pop()
        else:
            top.remch -= 1
            child = Node(inp[i], inp[i + 1])
            i += 2
            top.children.append(child)
            stack.append(child)

    return top.value

print(part2())
