import sys
from scanf import scanf
import numpy as np
# import matplotlib.pyplot as plt
# import matplotlib.animation as animation
from queue import Queue, PriorityQueue
# import readchar
from time import sleep

source = 500

map = np.ones((2000, 1000), dtype=np.uint8) * ord('.')

minx = 10000
maxx = -1000
maxy = 0
miny = 10000

SPRING = ord('+')
SAND = ord('.')
CLAY = ord('#')
FALL = ord('|')
REST = ord('~')

for l in sys.stdin:
    inp = scanf("%c=%d, %c=%d..%d", l)
    for k in range(inp[3], inp[4] + 1):
        if inp[0] == 'x':
            map[k, inp[1]] = CLAY
            maxx = max(maxx, inp[1])
            minx = min(minx, inp[1])
            maxy = max(maxy, inp[3], inp[4])
            miny = min(miny, inp[3], inp[4])
        else:
            map[inp[1], k] = CLAY
            maxx = max(maxx, inp[3], inp[4])
            minx = min(minx, inp[3], inp[4])
            maxy = max(maxy, inp[1])
            miny = min(miny, inp[1])

minx -= 1
maxx += 2
maxy += 1

source -= minx

map = map[0:maxy, minx:maxx]

for i in range(0, miny):
    map[i, source] = SPRING

display_height = 60

def print_map(offset=0, pretty=False, whole=False):
    if pretty:
        print_pretty_map(offset, whole)
        return
    H, W = np.shape(map)
    rows = []
    if whole:
        top = 0
        bottom = H
    else:
        offset = min(H, offset + display_height // 4)
        top = offset - display_height
        bottom = offset
    for i in range(top, bottom):
        row = ""
        for j in range(W):
            row += chr(map[i, j])
        rows.append(row)
    print("\x1b[H\x1b[2J")
    print()
    print("\n".join(rows))

def print_pretty_map(offset=0, whole=False):
    blocks = [' ', '▀', '▄', '█']
    colors = {SAND:"230;200;180", CLAY:"100;70;50", FALL:"200;220;255", REST:"40;100;255", SPRING:"0;0;255"}
    H, W = np.shape(map)
    rows = []
    if whole:
        top = 1
        bottom = H
    else:
        offset = min(H, offset + display_height // 2)
        top = offset - display_height * 2
        bottom = offset
    for i in range(top, bottom, 2):
        row = ""
        for j in range(W):
            if map[i, j] == map[i + 1, j]:
                if map[i, j] == SAND:
                    row += ' '
                else:
                    row += "\x1b[38;2;{}m█".format(colors[map[i, j]])
            elif map[i, j] == SAND:
                row += "\x1b[38;2;{}m▄".format(colors[map[i + 1, j]])
            elif map[i + 1, j] == SAND:
                row += "\x1b[38;2;{}m▀".format(colors[map[i, j]])
            else:
                row += "\x1b[38;2;{}m\x1b[48;2;{}m▀\x1b[48;2;{}m".format(colors[map[i, j]], colors[map[i + 1, j]], colors[SAND])

        rows.append(row)
    print("\x1b[H\x1b[2J\x1b[48;2;{}m".format(colors[SAND]), end='')
    print("\x1b[0m\n\x1b[48;2;{}m".format(colors[SAND]).join(rows), end='')
    print("\x1b[0m")

def solve():
    global map
    offset = display_height
    H, W = np.shape(map)
    q = Queue()
    map[miny, source] = FALL
    q.put((miny, source))
    while not q.empty():
        i, j = q.get()
        try:
            if map[i + 1, j] == SAND:
                map[i + 1, j] = FALL
                q.put((i + 1, j))
            elif map[i + 1, j] == CLAY or  map[i + 1, j] == REST:
                a = j
                z = j + 1
                FILL = REST
                while True:
                    if map[i, a] == CLAY or map[i, a] == REST:
                        break
                    if map[i + 1, a] == SAND or map[i + 1, a] == FALL:
                        FILL = FALL
                        a -= 1
                        break
                    a -= 1
                while True:
                    if map[i, z] == CLAY or map[i, z] == REST:
                        break
                    if map[i + 1, z] == SAND or map[i + 1, z] == FALL:
                        FILL = FALL
                        z += 1
                        break
                    z += 1
                map[i, a + 1:z] = FILL
                if FILL == REST:
                    for s in range(a, z):
                        if map[i - 1, s] == FALL:
                            q.put((i - 1, s))
                if FILL == FALL:
                    if map[i + 1, a + 1] == SAND:
                        q.put((i, a + 1))

                    if map[i + 1, z - 1] == SAND:
                        q.put((i, z - 1))
        except IndexError as e:
            pass

        last_fall = np.max(np.where(np.any(map == FALL, axis=1)))
        offset = max(offset, last_fall)
        # if offset > 1900:
        #     print_map(offset=offset)
        #     sleep(0.05)

solve()

def part1():
    return np.sum(map == FALL) + np.sum(map == REST)

print(part1())

def part2():
    return np.sum(map == REST)

print(part2())
