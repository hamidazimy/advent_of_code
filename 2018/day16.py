import sys
from scanf import scanf
import numpy as np
from time import sleep

samples = []
while True:
    bef = scanf("Before: [%d, %d, %d, %d]", sys.stdin.readline())
    ins = scanf("%d %d %d %d", sys.stdin.readline())
    if ins is None:
        break
    aft = scanf("After: [%d, %d, %d, %d]", sys.stdin.readline())
    sys.stdin.readline()
    samples.append({"bef": bef, "ins": ins, "aft": aft})

tests = [line.strip() for line in sys.stdin]


def addr(bef, ins):
    reg = list(bef)
    reg[ins[3]] = reg[ins[1]] + reg[ins[2]]
    return tuple(reg)

def addi(bef, ins):
    reg = list(bef)
    reg[ins[3]] = reg[ins[1]] + ins[2]
    return tuple(reg)

def mulr(bef, ins):
    reg = list(bef)
    reg[ins[3]] = reg[ins[1]] * reg[ins[2]]
    return tuple(reg)

def muli(bef, ins):
    reg = list(bef)
    reg[ins[3]] = reg[ins[1]] * ins[2]
    return tuple(reg)

def banr(bef, ins):
    reg = list(bef)
    reg[ins[3]] = reg[ins[1]] & reg[ins[2]]
    return tuple(reg)

def bani(bef, ins):
    reg = list(bef)
    reg[ins[3]] = reg[ins[1]] & ins[2]
    return tuple(reg)

def borr(bef, ins):
    reg = list(bef)
    reg[ins[3]] = reg[ins[1]] | reg[ins[2]]
    return tuple(reg)

def bori(bef, ins):
    reg = list(bef)
    reg[ins[3]] = reg[ins[1]] | ins[2]
    return tuple(reg)

def setr(bef, ins):
    reg = list(bef)
    reg[ins[3]] = reg[ins[1]]
    return tuple(reg)

def seti(bef, ins):
    reg = list(bef)
    reg[ins[3]] = ins[1]
    return tuple(reg)

def gtir(bef, ins):
    reg = list(bef)
    reg[ins[3]] = int(ins[1] > reg[ins[2]])
    return tuple(reg)

def gtri(bef, ins):
    reg = list(bef)
    reg[ins[3]] = int(reg[ins[1]] > ins[2])
    return tuple(reg)

def gtrr(bef, ins):
    reg = list(bef)
    reg[ins[3]] = int(reg[ins[1]] > reg[ins[2]])
    return tuple(reg)

def eqir(bef, ins):
    reg = list(bef)
    reg[ins[3]] = int(ins[1] == reg[ins[2]])
    return tuple(reg)

def eqri(bef, ins):
    reg = list(bef)
    reg[ins[3]] = int(reg[ins[1]] == ins[2])
    return tuple(reg)

def eqrr(bef, ins):
    reg = list(bef)
    reg[ins[3]] = int(reg[ins[1]] == reg[ins[2]])
    return tuple(reg)

opcodes = [addr, addi, mulr, muli, banr, bani, borr, bori, setr, seti, gtir, gtri, gtrr, eqir, eqri, eqrr]

def matches(bef, ins, aft, opc):
    return aft == opc(bef, ins)

def part1(samples=samples, opcodes=opcodes):
    gt3 = 0
    for s in samples:
        matched_opcodes = 0
        for opc in opcodes:
            matched_opcodes += matches(opc=opc, **s)
        gt3 += matched_opcodes >= 3
    return gt3

print(part1())

def part2(samples=samples, opcodes=opcodes, tests=tests):
    known = [None] * len(opcodes)

    while None in known:
        for s in samples:
            matched_opcodes = 0
            last_op = ""
            last_opcode = -1
            for i in range(len(opcodes)):
                if opcodes[i] not in known:
                    if matches(opc=opcodes[i], **s):
                        matched_opcodes += 1
                        last_op = opcodes[i]
                        last_opcode = s["ins"][0]

            if matched_opcodes == 1:
                known[last_opcode] = last_op

    opcodes = known

    reg = (0, 0, 0, 0)
    for test in tests:
        ins = tuple([int(i) for i in test.split()])
        reg = opcodes[ins[0]](reg, ins)

    return reg[0]

print(part2())
