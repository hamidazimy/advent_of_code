import sys
from scanf import scanf
import numpy as np
import math
from time import sleep

ip, = scanf("#ip %d", sys.stdin.readline().strip())
instructions = [scanf("%s %d %d %d", l.strip()) for l in sys.stdin]


def addr(reg, ins):
    reg[ins[3]] = reg[ins[1]] + reg[ins[2]]


def addi(reg, ins):
    reg[ins[3]] = reg[ins[1]] + ins[2]


def mulr(reg, ins):
    reg[ins[3]] = reg[ins[1]] * reg[ins[2]]


def muli(reg, ins):
    reg[ins[3]] = reg[ins[1]] * ins[2]


def banr(reg, ins):
    reg[ins[3]] = reg[ins[1]] & reg[ins[2]]


def bani(reg, ins):
    reg[ins[3]] = reg[ins[1]] & ins[2]


def borr(reg, ins):
    reg[ins[3]] = reg[ins[1]] | reg[ins[2]]


def bori(reg, ins):
    reg[ins[3]] = reg[ins[1]] | ins[2]


def setr(reg, ins):
    reg[ins[3]] = reg[ins[1]]


def seti(reg, ins):
    reg[ins[3]] = ins[1]


def gtir(reg, ins):
    reg[ins[3]] = int(ins[1] > reg[ins[2]])


def gtri(reg, ins):
    reg[ins[3]] = int(reg[ins[1]] > ins[2])


def gtrr(reg, ins):
    reg[ins[3]] = int(reg[ins[1]] > reg[ins[2]])


def eqir(reg, ins):
    reg[ins[3]] = int(ins[1] == reg[ins[2]])


def eqri(reg, ins):
    reg[ins[3]] = int(reg[ins[1]] == ins[2])


def eqrr(reg, ins):
    reg[ins[3]] = int(reg[ins[1]] == reg[ins[2]])


def part1(instructions=instructions, ip=ip):
    reg = [0, 0, 0, 0, 0, 0]
    while True:
        try:
            # print("\x1b[H\x1b[2J")
            # print("[{:5},{:5},{:5},{:5},{:5},{:5}]".format(reg[0], reg[1], reg[2], reg[3], reg[4], reg[5]))
            # for i in range(len(instructions)):
            #     print("{} {}\x1b[0m".format('\x1b[34m' if i == reg[ip] else '', ' '.join([str(x) for x in instructions[i]])))
            globals()[instructions[reg[ip]][0]](reg, instructions[reg[ip]])
            reg[ip] += 1
        except IndexError as e:
            break
    return reg[0]

print(part1())

def part2(instructions=instructions, ip=ip):
    reg = [1, 0, 0, 0, 0, 0]
    while True:
        # flag = False
        try:
            # print("\x1b[H\x1b[2J")
            # print("[{:8},{:8},{:8},{:8},{:8},{:8}]".format(reg[0], reg[1], reg[2], reg[3], reg[4], reg[5]))
            # for i in range(len(instructions)):
            #     print("{} {}\x1b[0m".format('\x1b[34m' if i == reg[ip] else '', ' '.join([str(x) for x in instructions[i]])))
            globals()[instructions[reg[ip]][0]](reg, instructions[reg[ip]])
            reg[ip] += 1
            if reg[1] == 2:
                break
        except IndexError as e:
            break

    a = reg[4]
    sqrt_a = int(math.sqrt(a))
    res = 0

    for i in range(1, sqrt_a):
        if a % i == 0:
            res += i + a / i
    if a % sqrt_a == 0:
        res += sqrt_a
    return int(res)

print(part2())
