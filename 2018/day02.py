import sys
from collections import Counter

inp = [line.strip() for line in sys.stdin]

def part1(inp=inp):
    c2s = 0
    c3s = 0
    for line in inp:
        c = Counter(line)
        if 2 in c.values():
            c2s += 1
        if 3 in c.values():
            c3s += 1
    return c2s * c3s

print(part1())

def part2(inp=inp):
    ls = [[ord(y) for y in line] for line in inp]
    le = len(ls[0])
    for i in ls:
        for j in ls:
            diff = [int(x != y) for x, y in zip(i, j)]
            if sum(diff) == 1:
                return ''.join([chr(i[c]) if diff[c] == 0 else '' for c in range(len(i))])

print(part2())
