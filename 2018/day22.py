import sys
from scanf import scanf
import numpy as np
from queue import PriorityQueue
from time import sleep

depth, = scanf("depth: %d", sys.stdin.readline())
targetX, targetY = scanf("target: %d,%d", sys.stdin.readline())
mapW = targetX + 20
mapH = targetY + 200
target = (targetY, targetX)

geo = np.zeros((mapH, mapW), dtype=np.int32)
ero = np.zeros((mapH, mapW), dtype=np.int32)

for i in range(mapH):
    for j in range(mapW):
        if i == 0:
            if j == 0:
                geo[i, j] = 0
                ero[i, j] = (geo[i, j] + depth) % 20183
            else:
                geo[i, j] = j * 16807
                ero[i, j] = (geo[i, j] + depth) % 20183
        else:
            if j == 0:
                geo[i, j] = i * 48271
                ero[i, j] = (geo[i, j] + depth) % 20183
            else:
                geo[i, j] = ero[i - 1, j] * ero[i, j - 1]
                ero[i, j] = (geo[i, j] + depth) % 20183

geo[target] = 0
ero[target] = 0

ero = ero % 3

def part1(ero=ero, target=target, visualize=False):
    if visualize:
        colors = ['200;200;200', '0;100;200', '200;20;100', '130;130;10']
        ero[target] = -1
        map_chars = ""
        for i in range(target[0] + 1):
            for j in range(target[1] + 1):
                map_chars += f"\x1b[38;2;{colors[ero[i, j]]}m██"
            map_chars += "\x1b[0m\n"
        ero[target] = 0
        print(map_chars)
    return np.sum(ero[:target[0] + 1, :target[1] + 1])

print(part1())

def part2(ero=ero, target=target, visualize=False):
    def h(i, j):
        return abs(targetY - i) + abs(targetX - j)

    camefrom = {}
    NEITH = 0
    TORCH = 1
    CGEAR = 2

    def adjs(stat, equi, i, j):
        global mapW
        yield equi, i + 1, j, 1
        if j + 1 < mapW:
            yield equi, i, j + 1, 1
        if j > 0:
            yield equi, i, j - 1, 1
        if i > 0:
            yield equi, i - 1, j, 1
        other_equi = 3 - equi - stat
        yield other_equi, i, j, 7

    g = np.ones((3,) + np.shape(ero), dtype=np.uint32) * np.Infinity

    def a_star():
        q = PriorityQueue()
        g[TORCH, 0, 0] = 0
        q.put((h(0, 0), TORCH, 0, 0))
        while True:
            f, equi, i, j = q.get()
            stat = ero[i, j]
            if h(i, j) == 0:
                return int(g[TORCH, targetY, targetX])
            for equi_, i_, j_, cost in adjs(stat, equi, i, j):
                if ero[i_, j_] != equi:
                    new_g = g[equi, i, j] + cost
                    if new_g < g[equi_, i_, j_]:
                        g[equi_, i_, j_] = new_g
                        q.put((new_g + h(i_, j_), equi_, i_, j_))
                        camefrom[(equi_, i_, j_)] = (equi, i, j)

    result = a_star()

    path = []

    if visualize:
        temp = (TORCH, targetY, targetX)
        while True:
            path.append(temp)
            try:
                temp = camefrom[temp]
            except KeyError:
                break
        equipments = ['-', '*', '^']
        colors = ['200;200;200', '0;100;200', '200;20;100', '130;130;10']
        ero[target] = -1
        map_chars = ""
        for i in range(targetY + 10):
            for j in range(mapW):
                back = f"\x1b[48;2;{colors[ero[i, j]]}m"
                marker = ' '
                for e in range(3):
                    if (e, i, j) in path:
                        marker = equipments[e]
                map_chars += f"{back}{marker} "
            map_chars += "\x1b[0m\n"
        print(map_chars)
        ero[target] = 0

    return result

print(part2())
