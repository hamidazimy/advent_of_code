import sys
import numpy as np

inp = [line.strip() for line in sys.stdin]

matrix = np.zeros((1000, 1000))

for line in inp:
    parts = line.split()
    offsets = parts[2][:-1].split(',')
    offx = int(offsets[0])
    offy = int(offsets[1])
    lengths = parts[3].split('x')
    lenx = int(lengths[0])
    leny = int(lengths[1])
    matrix[offx:offx + lenx, offy:offy + leny] += 1

def part1(matrix=matrix):
    return np.sum(matrix > 1)

print(part1())

def part2(matrix=matrix, inp=inp):
    for line in inp:
        parts = line.split()
        offsets = parts[2][:-1].split(',')
        offx = int(offsets[0])
        offy = int(offsets[1])
        lengths = parts[3].split('x')
        lenx = int(lengths[0])
        leny = int(lengths[1])

        if np.all(np.matrix(matrix[offx:offx + lenx, offy:offy + leny]) == 1):
            return parts[0][1:]

print(part2())
