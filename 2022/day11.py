import sys


if ("-s" in sys.argv) or ("--sample" in sys.argv):
    from io import StringIO
    sample = StringIO("""\
Monkey 0:
  Starting items: 79, 98
  Operation: new = old * 19
  Test: divisible by 23
    If true: throw to monkey 2
    If false: throw to monkey 3

Monkey 1:
  Starting items: 54, 65, 75, 74
  Operation: new = old + 6
  Test: divisible by 19
    If true: throw to monkey 2
    If false: throw to monkey 0

Monkey 2:
  Starting items: 79, 60, 97
  Operation: new = old * old
  Test: divisible by 13
    If true: throw to monkey 1
    If false: throw to monkey 3

Monkey 3:
  Starting items: 74
  Operation: new = old + 3
  Test: divisible by 17
    If true: throw to monkey 0
    If false: throw to monkey 1
""")
    sys.stdin = sample

inp =  sys.stdin.read().split("\n\n")


class Monkey:
    monkeys = []
    modulus = 1

    def __init__(self, note):
        self.items = [int(x) for x in note[1].strip().split(':')[1].split(',')]
        operation = note[2].split("= old")[1].strip().split()
        if operation == ["*", "old"]:
            operation = ["^", "2"]
        self.operator = {'+': int.__add__, '*': int.__mul__, '^': int.__pow__}[operation[0]]
        self.operand = int(operation[1])
        self.test = int(note[3].split("by")[1])
        self.throw = [int(note[5].split("monkey")[1]), int(note[4].split("monkey")[1])]
        self.total = 0
        Monkey.modulus *= self.test

    def inspect(self, relief=1):
        while len(self.items) > 0:
            self.total += 1
            wl = self.items.pop(0)
            wl = self.operator(wl, self.operand)
            if relief == 1:
                wl %= Monkey.modulus
            else:
                wl //= relief
            Monkey.monkeys[self.throw[wl % self.test == 0]].items.append(wl)

    @classmethod
    def init(cls, inp):
        cls.modulus = 1
        cls.monkeys = [Monkey(m.strip().split('\n')) for m in inp]
        return cls

    @classmethod
    def solve(cls, rounds, relief=1):
        for _ in range(rounds):
            for m in Monkey.monkeys:
                m.inspect(relief)
        totals = sorted([m.total for m in Monkey.monkeys], reverse=True)
        monkey_business = totals[0] * totals[1]
        return monkey_business


def part1(inp=inp):
    return Monkey.init(inp).solve(rounds=20, relief=3)

print(part1())


def part2(inp=inp):
    return Monkey.init(inp).solve(rounds=10_000)

print(part2())
