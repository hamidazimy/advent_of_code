import sys
import re
import numpy as np

inp = np.array([[int(x) for x in re.match(r"Sensor at x=([-]?\d+), y=([-]?\d+): closest beacon is at x=([-]?\d+), y=([-]?\d+)", l.strip()).groups()] for l in sys.stdin])

def part1(inp=inp):
    Yr = 2_000_000
    void = set()
    for Xs, Ys, Xb, Yb in inp:
        dist_SB = abs(Ys - Yb) + abs(Xs - Xb)
        dist_SY = abs(Ys - Yr)
        diff = dist_SB - dist_SY
        if diff < 0:
            continue
        for i in range(Xs - diff, Xs + diff + 1):
            void.add(i)
    for Xs, Ys, Xb, Yb in inp:
        if Yr == Yb:
            try:
                void.remove(Xb)
            except:
                pass
    return len(void)

print(part1())


def part2(inp=inp):
    maxc = 4_000_000
    def radius(sensor):
        Xs, Ys, Xb, Yb = sensor
        return abs(Ys - Yb) + abs(Xs - Xb)
    sensors = [(s[0], s[1], radius(s)) for s in inp]

    def voidY(Yr, sen):
        Xs, Ys, rad = sen
        diff = rad - abs(Ys - Yr)
        if diff < 0:
            return None
        return (max(Xs - diff, 0), min(Xs + diff + 1, maxc))

    def merge(ranges):
        S, E = 0, 1
        ranges = sorted(ranges)
        merged = []
        while len(ranges) > 1:
            for i in range(1, len(ranges)):
                if ranges[i][S] <= ranges[0][E]:
                    ranges[0] = (ranges[0][S], max(ranges[0][E], ranges[i][E]))
                    ranges.pop(i)
                    break
            else:
                merged.append(ranges.pop(0))
        return merged + ranges

    for Yr in range(maxc + 1):
        ranges = [r for r in [voidY(Yr, sen) for sen in sensors] if r is not None]
        merged = merge(ranges)
        if len(merged) > 1:
            # print(Yr, merged)
            return Yr + merged[0][1] * 4_000_000

print("\x1b[38;5;1mWARNING: Not an efficient solution! Might take a few minutes...\x1b[0m", file=sys.stderr)
print(part2())
