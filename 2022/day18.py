import sys
import numpy as np

inp = np.array([[int(x) for x in line.strip().split(',')] for line in sys.stdin])

d = np.max(inp) + 3

map = np.zeros((d,) * 3, dtype=np.int8)
for i, j, k in inp:
    map[i + 1, j + 1, k + 1] = 1

sides = [(-1, 0, 0), (1, 0, 0), (0, -1, 0), (0, 1, 0), (0, 0, -1), (0, 0, 1)]


def part1():
    result = 0
    for i, j, k in inp:
        result += 6
        for di, dj, dk in sides:
            result -= map[i + 1 + di, j + 1 + dj, k + 1 + dk]
    return result

print(part1())


def part2():
    def bfs():
        airs = [(0, 0, 0)]
        to_expand = 0
        while to_expand != len(airs):
            x, y, z = airs[to_expand]
            for di, dj, dk in sides:
                if 0 <= x + di < d and  0 <= y + dj < d and  0 <= z + dk < d:
                    if map[x + di, y + dj, z + dk] == 0 and (x + di, y + dj, z + dk) not in airs:
                        airs.append((x + di, y + dj, z + dk))
            to_expand += 1
        return airs
    result = 0
    for x, y, z in bfs():
        for di, dj, dk in sides:
            if 0 <= x + di < d and  0 <= y + dj < d and  0 <= z + dk < d:
                if map[x + di, y + dj, z + dk] == 1:
                    result += 1
    return result

print(part2())
