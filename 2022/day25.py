import sys

if ("-s" in sys.argv) or ("--sample" in sys.argv):
    from io import StringIO
    sample = StringIO("""\
1=-0-2
12111
2=0=
21
2=01
111
20012
112
1=-1=
1-12
12
1=
122
""")
    sys.stdin = sample

DEBUG = ("-d" in sys.argv) or ("--debug" in sys.argv)

def log(msg=""):
    if DEBUG:
        print(msg)


def snafu_to_dec(num):
    digits = {"=": -2, "-": -1, "0": 0, "1": 1, "2": 2}
    result = 0
    for i, d in enumerate(num[::-1]):
        result += digits[d] * (5 ** i)
    return result

def dec_to_snafu(num):
    result = ""
    digits = "012=-"
    carry = 0
    while num > 0 or carry > 0:
        d = num % 5 + carry
        carry = 0
        if d > 2:
            d -= 5
            carry = 1
        result += digits[d]
        num //= 5
    return result[::-1]

inp = [line.strip() for line in sys.stdin]

log("   SNAFU  Decimal")
sum = 0
for line in inp:
    sum += snafu_to_dec(line)
    log(f"{line:>8} {snafu_to_dec(line):>8}")

print(dec_to_snafu(sum))
