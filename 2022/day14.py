import sys
import numpy as np

VISUALIZE = ("-v" in sys.argv) or ("--visualize" in sys.argv)

inp = [line.strip() for line in sys.stdin]

AIR_ = 0
ROCK = 1
SAND = 2
VOID = 3
HOLE = 4

hole = (0, 500)
N = hole[1] * 2 + 1

map = np.zeros((hole[1] * 2 + 1, ) * 2, dtype=np.int64)
for line in inp:
    rocks = [[int(x) for x in dot.split(',')][::-1] for dot in line.split(" -> ")]
    for i in range(1, len(rocks)):
        s = rocks[i - 1]
        e = rocks[i]
        if s[0] == e[0]:
            map[s[0], min(s[1], e[1]):max(s[1], e[1]) + 1] = ROCK
        else:
            map[min(s[0], e[0]):max(s[0], e[0]) + 1, s[1]] = ROCK

max_d = np.max(np.nonzero(map)[0]) + 2


def visualize(map, to_void=None):
    global VISUALIZE, max_d, hole
    if not VISUALIZE:
        return
    from time import sleep
    sleep(.3)
    map_str = "\x1bc"
    legend = " █o~+"
    map = np.copy(map)
    total = np.sum(map == SAND)
    max_y = min(np.max(np.nonzero(map)[1]), hole[1] + max_d) + 2
    min_y = max(np.min(np.nonzero(map)[1]), hole[1] - max_d) - 2
    if to_void is not None:
        for i, j in to_void:
            map[i, j] = VOID
    if map[hole] != SAND:
        map[hole] = HOLE
    map_str += '┌' + '─' * (max_y - min_y + 1) + '┐' + '\n'
    for i in range(max_d + 1):
        map_str += '│'
        for j in range(min_y, max_y + 1):
            map_str += legend[map[i, j]]
        map_str += '│\n'
        # print()
    map_str += '└' + f"┤{total:05}├" + '─' * (max_y - min_y + 1 - 7) + '┘' + "\n"
    print(map_str, flush=True)


def fall(map):
    global VISUALIZE, max_d, hole
    ns = list(hole)
    running = []
    while map[hole] == AIR_ and ns[0] < max_d:
        if map[ns[0] + 1, ns[1]] == AIR_:
            ns[0] += 1
        elif map[ns[0] + 1, ns[1] - 1] == AIR_:
            ns[0] += 1
            ns[1] -= 1
        elif map[ns[0] + 1, ns[1] + 1] == AIR_:
            ns[0] += 1
            ns[1] += 1
        else:
            return tuple(ns), running[:-1]
        running.append(tuple(ns)) if VISUALIZE else None
    return None, running


def solve(map):
    visualize(map)
    while True:
        next_sand, running = fall(map)
        if next_sand is None:
            visualize(map, running)
            break
        map[next_sand] = SAND
        visualize(map, running)
    return np.sum(map == SAND)


def part1(map=map):
    map = np.copy(map)
    return solve(map)

print(part1())


def part2(map=map):
    map = np.copy(map)
    map[max_d, :] = 1
    return solve(map)

print(part2())
