import sys

inp = [[int(cal) for cal in elf.strip().split('\n')] for elf in sys.stdin.read().split('\n\n')]


def part1(inp=inp):
    return max([sum(elf) for elf in inp])

print(part1())


def part2(inp=inp):
    return sum(sorted([sum(elf) for elf in inp])[-3:])

print(part2())
