import sys
import re
import numpy as np

print("\x1b[38;5;1mWARNING: Not an efficient solution! Might take a few minutes...\x1b[0m", file=sys.stderr)

if ("-s" in sys.argv) or ("--sample" in sys.argv):
    from io import StringIO
    sample = StringIO("""\
Blueprint 1:\
 Each ore robot costs 4 ore.\
 Each clay robot costs 2 ore.\
 Each obsidian robot costs 3 ore and 14 clay.\
 Each geode robot costs 2 ore and 7 obsidian.
\
Blueprint 2:\
 Each ore robot costs 2 ore.\
 Each clay robot costs 3 ore.\
 Each obsidian robot costs 3 ore and 8 clay.\
 Each geode robot costs 3 ore and 12 obsidian.
""")
    sys.stdin = sample

ORE = 0
CLY = 1
OBS = 2
GEO = 3
ORE_ROBOT = 4
CLY_ROBOT = 5
OBS_ROBOT = 6
GEO_ROBOT = 7
TIME_IDX = 8

RESRCS = np.array([ORE, CLY, OBS, GEO])
ROBOTS = np.array([ORE_ROBOT, CLY_ROBOT, OBS_ROBOT, GEO_ROBOT])

resource = lambda rbt: rbt - 4
collector = lambda res: res + 4

pattern = r"Blueprint (\d+): Each ore robot costs (\d+) ore. Each clay robot costs (\d+) ore. Each obsidian robot costs (\d+) ore and (\d+) clay. Each geode robot costs (\d+) ore and (\d+) obsidian."
inp = [[int(x) for x in re.match(pattern, line.strip()).groups()] for line in sys.stdin]
bps = [
        {                     #   ORE   CLY   OBS   GEO
            ORE_ROBOT: np.array([i[1],    0,    0,    0]),
            CLY_ROBOT: np.array([i[2],    0,    0,    0]),
            OBS_ROBOT: np.array([i[3], i[4],    0,    0]),
            GEO_ROBOT: np.array([i[5],    0, i[6],    0]),
        }
    for i in inp
]

max_robots_needed = [np.ones(4) * np.Inf for _ in bps]
for bp_index, bp in enumerate(bps):
    for res in [ORE, CLY, OBS]:
        max_robots_needed[bp_index][res] = max([bp[rbt][res] for rbt in ROBOTS])

start = None
best = None
mem = None

def reset(deadline=24):
    global DEADLINE, start, best, mem
    DEADLINE = deadline
    start = np.array([0] * 9)
    start[ORE_ROBOT] = 1
    best = 0
    mem = set()


def optimistic_geodes(state):
    rem = DEADLINE - state[TIME_IDX]
    return state[GEO] + state[GEO_ROBOT] * rem + (rem) * (rem + 1) // 2


def next(state, bpi):
    if optimistic_geodes(state) <= best:
        return
    state_ = np.copy(state[:])
    # Production stage:
    state_[TIME_IDX] += 1
    state_[RESRCS] += state[collector(RESRCS)]
    # Construction stage:
    for ROBOT in ROBOTS[::-1]:
        if state_[ROBOT] >= max_robots_needed[bpi][resource(ROBOT)]:
            continue
        if np.all(state[RESRCS] >= bps[bpi][ROBOT]): # It's "state" instead of "state_", because we need the resources at the beginning of the round, not after the production stage.
            state__ = np.copy(state_)
            state__[RESRCS] -= bps[bpi][ROBOT]
            state__[ROBOT] += 1
            yield state__
            if ROBOT == GEO_ROBOT:
                return
    yield state_


def dfs(bpi, state):
    global best, mem
    if tuple(state) in mem:
        return
    mem.add(tuple(state))
    if state[TIME_IDX] == DEADLINE:
        best = max(best, state[GEO])
        return
    elif state[TIME_IDX] > DEADLINE:
        raise IndexError("⚠ Time is out of range.")
    for state_ in next(state, bpi):
        dfs(bpi, state_)


def solve(bp_num=len(bps), deadline=24):
    global start, best
    output = []
    for bpi in range(bp_num):
        reset(deadline=deadline)
        dfs(bpi, start)
        output.append(best)
    return np.array(output)


def part1():
    output = solve()
    number = np.cumsum(np.ones(len(bps), dtype=np.int8))
    return np.sum(output * number)

print(part1())


def part2():
    output = solve(bp_num=3, deadline=32)
    return np.prod(output)

print(part2())
