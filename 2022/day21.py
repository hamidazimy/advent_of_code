import sys
import numpy as np
Polynomial = np.polynomial.Polynomial
np.polynomial.set_default_printstyle("unicode")

if ("-s" in sys.argv) or ("--sample" in sys.argv):
    from io import StringIO
    sample = StringIO("""\
root: pppw + sjmn
dbpl: 5
cczh: sllz + lgvd
zczc: 2
ptdq: humn - dvpt
dvpt: 3
lfqf: 4
humn: 5
ljgn: 2
sjmn: drzm * dbpl
sllz: 4
pppw: cczh / lfqf
lgvd: ljgn * ptdq
drzm: hmdt - zczc
hmdt: 32
""")
    sys.stdin = sample

inp = [line.strip() for line in sys.stdin]
monkeys = {}

class Moneky:
    def __init__(self, name, op):
        self.name = name
        op = op.split()
        operations = {"+": int.__add__, "-": int.__sub__, "*": int.__mul__, "/": int.__floordiv__}
        if len(op) == 1:
            self._value = int(op[0])
            self._oprands = None
            self._operation = None
        else:
            self._value = 0
            self._oprands = (op[0], op[2])
            self._operation = operations[op[1]]

    @property
    def value(self):
        if self._operation is not None:
            return self._operation(monkeys[self._oprands[0]].value, monkeys[self._oprands[1]].value)
        return self._value


def part1(inp=inp):
    global monkeys
    for line in inp:
        name, op = line.split(": ")
        monkeys[name] = Moneky(name, op)
    return monkeys["root"].value

print(part1())


class PolyFrac:
    def __init__(self, num=Polynomial([0]), den=Polynomial([1, 0])):
        # qou, rem = np.polynomial.polynomial.polydiv(Polynomial(num), den)
        # if rem.coef == [0.0]:
        #     self.num = qou
        #     self.den = Polynomial([1, 0])
        # else:
        self.num = num
        self.den = den

    def __add__(self, that):
        num = self.num * that.den + that.num * self.den
        den = self.den * that.den
        return PolyFrac(num, den)

    def __sub__(self, that):
        num = self.num * that.den - that.num * self.den
        den = self.den * that.den
        return PolyFrac(num, den)

    def __mul__(self, that):
        try:
            num = self.num * that.num
            den = self.den * that.den
        except Exception as e:
            print(e)
        return PolyFrac(num, den)

    def __truediv__(self, that):
        num = self.num * that.den
        den = self.den * that.num
        return PolyFrac(num, den)

    def __repr__(self):
        foo = self.num.__repr__()
        bar = self.den.__repr__()
        l = max(len(foo), len(bar))
        # return '\n'.join([foo.center(l), "─" * l, bar.center(l)])
        # return f"{foo} % {bar}"
        print(self.num)
        print("─" * 40)
        print(self.den)
        return ""


polyfracmonkeys = {}

class PolyFracMoneky:
    def __init__(self, name, op):
        self.name = name
        op = op.split()
        operations = {"+": PolyFrac.__add__, "-": PolyFrac.__sub__, "*": PolyFrac.__mul__, "/": PolyFrac.__truediv__}
        if len(op) == 1:
            if self.name == "humn":
                # self._value = PolyFrac(num=Polynomial([int(op[0]), 0]))
                self._value = PolyFrac(num=Polynomial([0, 1]))
            else:
                self._value = PolyFrac(num=Polynomial([int(op[0]), 0]))
            self._oprands = None
            self._operation = None
        else:
            self._value = 0
            self._oprands = (op[0], op[2])
            self._operation = operations[op[1]]

    @property
    def value(self):
        if self.name == "root":
            a = polyfracmonkeys[self._oprands[0]].value
            b = polyfracmonkeys[self._oprands[1]].value
            # print(a)
            # print(b)
            eq = a.num * b.den - b.num * a.den
            # print(eq)
            return eq.roots()
            # return polyfracmonkeys[self._oprands[0]].value - polyfracmonkeys[self._oprands[1]].value
            return
        if self._operation is not None:
            return self._operation(polyfracmonkeys[self._oprands[0]].value, polyfracmonkeys[self._oprands[1]].value)
        return self._value

    @property
    def equation(self) -> Polynomial:
        lside = polyfracmonkeys[self._oprands[0]].value
        rside = polyfracmonkeys[self._oprands[1]].value
        return lside.num * rside.den - rside.num * lside.den

    def __repr__(self):
        if self._operation is not None:
            return f"{self._oprands[0]}, {self._oprands[1]}"
        return self._value.__repr__()


def part2(inp=inp):
    global polyfracmonkeys
    for line in inp:
        name, op = line.split(": ")
        polyfracmonkeys[name] = PolyFracMoneky(name, op)
    r = int(polyfracmonkeys["root"].value[0])
    # eq: Polynomial = polyfracmonkeys["root"].equation
    # print(-eq.coef[0] / eq.coef[1])
    # print(np.polynomial.polynomial.polyval(r, eq.coef))
    return int(round(polyfracmonkeys["root"].value[0]))

print(part2())
