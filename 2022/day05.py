import sys
import re


if ("-s" in sys.argv) or ("--sample" in sys.argv):
    from io import StringIO
    sample = StringIO("""\
    [D]
[N] [C]
[Z] [M] [P]
 1   2   3

move 1 from 2 to 1
move 3 from 1 to 3
move 2 from 2 to 1
move 1 from 1 to 2
""")
    sys.stdin = sample


stacks, commands = sys.stdin.read().split('\n\n')

stacks = stacks.split('\n')
stacks = [[]] + [[s[i] for s in stacks[::-1] if i < len(s) and s[i] != ' '] for i in range(1, len(stacks[-1]), 4)]

inp = [re.match(r"move (\d+) from (\d+) to (\d+)", line).groups() for line in commands.strip().split('\n')]

def part1(inp=inp):
    global stacks
    stx = [x[:] for x in stacks]
    for n, i, j in inp:
        for _ in range(int(n)):
            stx[int(j)].append(stx[int(i)].pop())
    return ''.join([x[-1] for x in stx[1:]])

print(part1())


def part2(inp=inp):
    global stacks
    stx = [x[:] for x in stacks]
    for n, i, j in inp:
        for _ in range(int(n)):
            stx[int(0)].append(stx[int(i)].pop())
        for _ in range(int(n)):
            stx[int(j)].append(stx[int(0)].pop())
    return ''.join([x[-1] for x in stx[1:]])

print(part2())
