import sys
import numpy as np

VISUALIZE = ("-v" in sys.argv) or ("--visualize" in sys.argv)

if ("-s" in sys.argv) or ("--sample" in sys.argv):
    from io import StringIO
    sample = StringIO("""\
..............
..............
.......#......
.....###.#....
...#...#.#....
....#...##....
...#.###......
...##.#.##....
....#..#......
..............
..............
..............
""")
    sys.stdin = sample

B = 500
map = np.array([[int(c != '.') for c in row.strip()] for row in sys.stdin], dtype=np.int32)
H, W = np.shape(map)
extended_map = np.zeros((2 * B + H, 2 * B + W), dtype=np.int32)
extended_map[B:B + H,  B:B + W] = map
map = extended_map


def propose(i, j, r):
    global map
    policies = "NSWE"
    hints = {
        'N': (-1,  0) + (-1,  2) + (-1,  0),
        'S': ( 1,  2) + (-1,  2) + ( 1,  0),
        'W': (-1,  2) + (-1,  0) + ( 0, -1),
        'E': (-1,  2) + ( 1,  2) + ( 0,  1),
    }
    for x in range(4):
        policy = policies[(r + x) % 4]
        ib, id, jb, jd, i_, j_ = hints[policy]
        if np.sum(map[i + ib:i + id, j + jb:j + jd]) == 0:
            return (i + i_, j + j_)
    return (i, j)


def step(r):
    global map
    moved = False
    proposed = {}

    # 1st half
    for i, j in np.argwhere(map == 1):
        if np.sum(map[i - 1:i + 2, j - 1:j + 2]) > 1:
            i_, j_ = propose(i, j, r)
            if (i_, j_) in proposed:
                proposed[(i_, j_)] = None
            else:
                proposed[(i_, j_)] = (i, j)

    # 2nd half
    for (i_, j_) in proposed:
        if proposed[(i_, j_)] is not None:
            moved = True
            map[proposed[(i_, j_)]] = 0
            map[(i_, j_)] = 1

    return moved


def visualize():
    if not VISUALIZE:
        return
    from time import sleep
    D = 0
    str_map = ""#\x1bc"
    for i in range(B - D, B + D + H):
        for j in range(B - D, B + D + W):
            str_map += ".#"[map[i, j]]
        str_map += '\n'
    print(str_map)
    sleep(5)


def part1():
    visualize()
    for r in range(10):
        step(r)
        visualize()
    ix, jy = np.nonzero(map)
    return np.sum(map[np.min(ix):np.max(ix) + 1, np.min(jy):np.max(jy) + 1] == 0)

print(part1())


def part2():
    r = 10
    while True:
        moved = step(r)
        visualize()
        r += 1
        if not moved:
            return r

print(part2())
