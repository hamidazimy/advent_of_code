import sys
import re
import heapq
import numpy as np
from itertools import combinations

DEBUG = ("-d" in sys.argv) or ("--debug" in sys.argv)

def log(msg=""):
    if DEBUG:
        print(msg)

if ("-s" in sys.argv) or ("--sample" in sys.argv) or sys.stdin.isatty():
    from io import StringIO
    sample = StringIO("""\
Valve AA has flow rate=0; tunnels lead to valves DD, II, BB
Valve BB has flow rate=13; tunnels lead to valves CC, AA
Valve CC has flow rate=2; tunnels lead to valves DD, BB
Valve DD has flow rate=20; tunnels lead to valves CC, AA, EE
Valve EE has flow rate=3; tunnels lead to valves FF, DD
Valve FF has flow rate=0; tunnels lead to valves EE, GG
Valve GG has flow rate=0; tunnels lead to valves FF, HH
Valve HH has flow rate=22; tunnel leads to valve GG
Valve II has flow rate=0; tunnels lead to valves AA, JJ
Valve JJ has flow rate=21; tunnel leads to valve II
""")
    optimal_path = ('##', 'DD', 'BB', 'JJ', 'HH', 'EE', 'CC')
    sys.stdin = sample

inp = [line.strip().replace("valves", "valve") for line in sys.stdin]

valves = {}

class Valve:
    def __init__(self, name, rate, idx=-1):
        self.idx = idx
        self.name = name
        self.rate = int(rate)
        self.next = []

for idx, line in enumerate(inp):
    c = Valve(*re.match(r"Valve (\S+) has flow rate=(\d+);", line).groups() + (idx,))
    c.next = [x for x in line.split(" valve ")[1].split(", ")]
    valves[c.name] = c

valves["##"] = Valve("##", 0)

def Floyd_Warshall(valves=valves):
    # This might not be necessary,
    # but I felt like implementing it for fun!
    # and use it for heuristic in my A*
    dist = np.ones((len(valves),) * 2) * np.inf
    for i, vi in enumerate(valves.keys()):
        for j, vj in enumerate(valves.keys()):
            if i == j:
                dist[i, j] = 0
            elif vj in valves[vi].next:
                dist[i, j] = 1
    dist[-1, valves["AA"].idx] = 0
    for k, vk in enumerate(valves.keys()):
        for i, vi in enumerate(valves.keys()):
            for j, vj in enumerate(valves.keys()):
                if dist[i, j] > dist[i, k] + dist[k, j]:
                    dist[i, j] = dist[i, k] + dist[k, j]
    return dist

dist = Floyd_Warshall() + 1

def print_dists():
    str_mat  = "====" + "=" * len(valves) * 4 + '\n'
    str_mat += "   |" + ''.join([f"{valves[v].name:>4}" for v in valves]) + '\n'
    str_mat += "---+" + "-" * len(valves) * 4 + '\n'
    for iv in valves:
        str_mat += f"{valves[iv].name:3}|"
        for jv in valves:
            str_mat += f"{dist[valves[iv].idx, valves[jv].idx]:4.0f}"
        str_mat +='\n'
    str_mat += "---+" + "-" * len(valves) * 4 + '\n'
    str_mat += "   |" + ''.join([f"{valves[v].name:>4}" for v in valves]) + '\n'
    str_mat += "====" + "=" * len(valves) * 4 + '\n'
    log(str_mat)

all_valves = valves
nonzero_valves = {v: valves[v] for v in valves if valves[v].rate != 0 or valves[v].name == "##"}
valves = nonzero_valves

print_dists()

total_pressure = np.sum([valves[v].rate for v in valves])

rem = list(valves.keys())[:-1]


def rates(path):
    """Returns a list of release rates in a 'path'"""
    return [valves[v].rate for v in path]

def diffs(path):
    """Returns the distances between every pair of consecutive valves in a 'path'"""
    return [dist[valves[path[i - 1]].idx, valves[path[i]].idx] for i in range(1, len(path))]

def times(path):
    """Returns the time of opening for each valve in a 'path'"""
    return np.cumsum([0] + diffs(path))

def release(path, end=30):
    """Returns the total release for a 'path'"""
    timex = times(path)
    return int(np.sum([max(0, end - timex[i]) * valves[v].rate for i, v in enumerate(path)]))

def release2_(path_u, path_e, end=30):
    timeu = times(path_u)
    timee = times(path_e)
    return int(np.sum([max(0, end - timeu[i]) * valves[v].rate for i, v in enumerate(path_u)])) +\
           int(np.sum([max(0, end - timee[i]) * valves[v].rate for i, v in enumerate(path_e)]))

def cost(path, is_leaf=False):
    """Returns the cost of a path, meaning the amount of wasted 'release' to pursue a 'path'"""
    diffx = diffs(path)
    diffx = diffx + [30 - sum(diffx) if is_leaf else 0]
    costx = total_pressure - np.cumsum(rates(path))
    cost_path = np.sum(np.multiply(costx, diffx))
    return int(cost_path)


# optimal_path = ('##', 'TA', 'QK', 'JA', 'VK', 'ID', 'DW', 'EQ')
# cost_optimal = cost(optimal_path, is_leaf=True)
# log(f"optimal path:\n\t{optimal_path}")
# log(f"times of the optimal path:\n\t{times(optimal_path)}")
# log(f"cost of the optimal path:\n\t{cost_optimal}")
# log(f"release of the optimal path:\n\t{release(optimal_path)}")
# log(f"total_pressure * 30 - cost_optimal:\n\t{total_pressure * 30 - cost_optimal}")
# log("-" * 80)


def heu(path, times):
    nodes_remaining = [v for v in valves if v not in path]
    rates_remaining = [valves[v].rate for v in nodes_remaining]
    times_remaining = [max(0, 30 - times[-1] - dist[valves[path[-1]].idx, valves[v].idx]) for v in nodes_remaining]
    return -sum([i * j for i, j in zip(rates_remaining, times_remaining)])


def a_star(subset=valves):
    best_release = 0
    best_path = None
    q = [(0, 0, 0, 0, 0, ("##",))]
    while len(q) > 0:
        f, g, h, rel, time, path = heapq.heappop(q)
        is_leaf = True
        for v in valves:
            if v in path or v not in subset:
                continue
            to_v = dist[valves[path[-1]].idx, valves[v].idx]
            time_ = time + to_v
            if time_ > 30:
                continue
            is_leaf = False
            path_ = path[:] + (v,)
            rel_ = rel + valves[v].rate
            g_ = g + (total_pressure - rel) * to_v
            h_ = 0 # HEURISTIC?!!
            heapq.heappush(q, (g_ + h_, g_, h_, rel_, time_, path_))
        if is_leaf:
            total_release = release(path)
            if best_release < total_release:
                best_release = total_release
                best_path = path
    return best_release, best_path


def solve_with_elephant():
    dist[-1, :] += 4
    print_dists()

    best_release = 0
    best_path = None

    for n in range(len(valves)):
        for subset in combinations(list(valves.keys())[:-1], n):
            release1, path1 = a_star(subset)
            release2, path2 = a_star([v for v in valves if v not in subset])
            if release1 + release2 > best_release:
                best_release = release1 + release2
                best_path = (path1, path2)
                log(f"new best release: {best_release} {best_path}")
                print(f"{best_release}", end='\r')
    return best_release, best_path


def aa_star():
    global best_release, best_path
    q = [(0, 0, 0, 0, 0, 0, 0, ("##",), ("##",))]
    while len(q) > 0:
        f, g, h, rel_u, rel_e, time_u, time_e, path_u, path_e = heapq.heappop(q)
        # log(f"{path_u}, {path_e}")
        is_leaf = True
        for v in valves:
            if v in path_u or v in path_e:
                continue
            if time_u < time_e:
                to_v = dist[valves[path_u[-1]].idx, valves[v].idx]
                time_u_ = time_u + to_v
                if time_u_ > 30:
                    continue
                is_leaf = False
                path_u_ = path_u[:] + (v,)
                rel_u_ = rel_u + valves[v].rate
                g_ = g + (total_pressure - rel_u) * to_v
                h_ = 0 # HEURISTIC?!!
                heapq.heappush(q, (g_ + h_, g_, h_, rel_u_, rel_e, time_u_, time_e, path_u_, path_e))
            else:
                to_v = dist[valves[path_e[-1]].idx, valves[v].idx]
                time_e_ = time_e + to_v
                if time_e_ > 30:
                    continue
                is_leaf = False
                path_e_ = path_e[:] + (v,)
                rel_e_ = rel_e + valves[v].rate
                g_ = g + (total_pressure - rel_e) * to_v
                h_ = 0 # HEURISTIC?!!
                heapq.heappush(q, (g_ + h_, g_, h_, rel_u, rel_e_, time_u, time_e_, path_u, path_e_))
        if is_leaf:
            total_release = release2_(path_u, path_e)
            if best_release < total_release:
                best_release = total_release
                best_path = path_u, path_e

# aa_star()
# log(f"best release from A*:\n\t{best_release}")
# log(f"best path from A*:\n\t{best_path}")


def exhaustive_search():
    def all_permutations(path=[]):
        if len(path) == len(rem):
            yield ["##",] + path
        if times(path)[-1] >= 30:
            yield ["##",] + path[:-1]
        else:
            for i in rem:
                if i not in path:
                    yield from all_permutations(path + [i])

    best = 0
    best_path = None

    for path in all_permutations():
        rel = release(path)
        if best < rel:
            best = rel
            best_path = path

    log(f"best path:\n\t{best_path}")
    log(f"times for the best path:\n\t{times(best_path)}")
    log(f"best release:\n\t{best}")
    return best


def part1():
    best_release, best_path = a_star()
    log(f"best release from A*:\n\t{best_release}")
    log(f"best path from A*:\n\t{best_path}")
    return best_release

print(part1())


def part2():
    best_release, best_path = solve_with_elephant()
    log(f"best release:\n\t{best_release}")
    log(f"best path\n\t{best_path}")
    return best_release

print("\x1b[38;5;1mWARNING: Not an efficient solution! Might take a few minutes...\x1b[0m", file=sys.stderr)
print(part2())
