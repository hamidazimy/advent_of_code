import sys
import re
import numpy as np

VISUALIZE = ("-v" in sys.argv) or ("--visualize" in sys.argv)

DEBUG = ("-d" in sys.argv) or ("--debug" in sys.argv)

def log(msg=""):
    if DEBUG:
        print(msg)

if ("-s" in sys.argv) or ("--sample" in sys.argv):
    from io import StringIO
    sample = StringIO("""\
        ...#    \n\
        .#..    \n\
        #...    \n\
        ....    \n\
...#.......#    \n\
........#...    \n\
..#....#....    \n\
..........#.    \n\
        ...#....\n\
        .....#..\n\
        .#......\n\
        ......#.\n\

10R5L5R10L4R5L5
""")
    sys.stdin = sample

"""
┏━━━━━━━━━━━━━━━┓
┃               ┃
┃    ▄  ▄▄▄     ┃
┃    █ █   █    ┃
┃    ▀▀▀  ▀     ┃
┃               ┃
┣━━━━━━━━━━━━━━━╋━━━━━━━━━━━━━━━┓
┃               ┃               ┃
┃      █ █      ┃     ▄▄ ▄▄     ┃
┃     ▄▀ █      ┃    █  █  █    ┃
┃     ▀▀▀█▀     ┃     ▀   ▀     ┃
┃               ┃               ┃
┗━━━━━━━━━━━━━━━╋━━━━━━━━━━━━━━━╋━━━━━━━━━━━━━━━┓
                ┃               ┃               ┃
                ┃     ▄▀▀▀▄     ┃          ▄    ┃
                ┃       ▄▀      ┃    █▀▀▀▀▀█    ┃
                ┃     ▄█▄▄▄     ┃               ┃
                ┃               ┃               ┃
                ┗━━━━━━━━━━━━━━━╋━━━━━━━━━━━━━━━┫
                                ┃               ┃
                                ┃     ▄▀▀▀▄     ┃
                                ┃     █▄▞▀█     ┃
                                ┃     ▀▄▄▄▀     ┃
                                ┃               ┃
                                ┗━━━━━━━━━━━━━━━┛


                ┏━━━━━━━━━━━━━━━┓
                ┃               ┃
                ┃          ▄    ┃
                ┃    █▀▀▀▀▀█    ┃
                ┃               ┃
                ┃               ┃
┏━━━━━━━━━━━━━━━╋━━━━━━━━━━━━━━━╋━━━━━━━━━━━━━━━┓
┃               ┃               ┃               ┃
┃     ▄▄   ▄    ┃     ▄▀▀▀▄     ┃    ▄  ▄▄▄     ┃
┃    █  ▀▄ █    ┃     █▄▞▀█     ┃    █ █   █    ┃
┃     ▀   ▀▀    ┃     ▀▄▄▄▀     ┃    ▀▀▀  ▀     ┃
┃               ┃               ┃               ┃
┗━━━━━━━━━━━━━━━╋━━━━━━━━━━━━━━━╋━━━━━━━━━━━━━━━┛
                ┃               ┃
                ┃      ▄▄       ┃
                ┃      █ ▀▀     ┃
                ┃    ▀▀█▀▀▀▀    ┃
                ┃               ┃
                ┗━━━━━━━━━━━━━━━┛

                ┏━━━━━━━━━━━━━━━┓
                ┃               ┃
                ┃    ▄▄   ▄     ┃
                ┃    █ ▀▄  █    ┃
                ┃    ▀   ▀▀     ┃
                ┃               ┃
┏━━━━━━━━━━━━━━━╋━━━━━━━━━━━━━━━╋━━━━━━━━━━━━━━━┓
┃               ┃               ┃               ┃
┃     ▄▄▄▄▄     ┃      ▀█       ┃     ▄   ▄     ┃
┃   |█  ▀▄ █    ┃       █       ┃    █  █  █    ┃
┃     ▀▀▀▀▀     ┃      ▄█▄      ┃     ▀▀ ▀▀     ┃
┃               ┃               ┃               ┃
┗━━━━━━━━━━━━━━━╋━━━━━━━━━━━━━━━╋━━━━━━━━━━━━━━━┛
                ┃               ┃
                ┃    ▄  ▄▄▄     ┃
                ┃    █ █   █    ┃
                ┃    ▀▀▀  ▀     ┃
                ┃               ┃
                ┗━━━━━━━━━━━━━━━┛
"""

WALL = +1
OPEN = 00
VOID = -1

map_str, inp_str = sys.stdin.read().split("\n\n")

path = re.findall("\d+|[LR]", inp_str.strip())

map_str = map_str.split('\n')

encoding = {' ': -1, '.': 0, '#': 1}
Hei, Wid = len(map_str), max([len(line) for line in map_str])
map = np.zeros((Hei, Wid), dtype=np.int8) - 1
for i, line in enumerate(map_str):
    for r, char in enumerate(line):
        map[i, r] = encoding[char]
D = min(np.min(np.sum(map != VOID, axis=0)), np.min(np.sum(map != VOID, axis=1)))

def coord(z: complex):
    return (int(z.imag), int(z.real))

def turn(facing: complex, to: str):
    return facing * (-1j if to == 'L' else 1j)

def password(you, facing):
    return int((you.imag + 1) * 1000 + (you.real + 1) * 4 + int(np.angle(facing, deg=True) // 90) % 4)


def next2d(you, facing):
    front = you
    while True:
        front += facing
        i_, r_ = int(front.imag) % Hei, int(front.real) % Wid
        front = 1j * i_ + r_
        if map[i_, r_] != VOID:
            break
    if map[i_, r_] == WALL:
        return you
    return front


def part1():
    you = np.argwhere(map == OPEN)[0][1]
    facing = 1
    commands = path[:]
    while len(commands) > 0:
        command, *commands = commands
        if command in ('L', 'R'):
            facing = turn(facing, command)
        else:
            for _ in range(int(command)):
                you_ = next2d(you, facing)
                if you_ == you:
                    break
                you = you_
    return password(you, facing)

print(part1())

N_panel = lambda i: (i + 1) % 6
E_panel = lambda i: (i + 5 - 3 * (i % 2)) % 6
W_panel = lambda i: (i + 2 + 3 * (i % 2)) % 6
S_panel = lambda i: (i + 4) % 6

def make3d():
    panels_map = np.array([[np.any(map[i:i + D, j:j + D] != VOID) for j in range(0, Wid, D)] for i in range(0, Hei, D)], dtype=np.int8) * -1 -1
    panels = [None] * 6
    panel_spins = [-1] * 6

    y, x = np.argwhere(panels_map == -2)[0]
    log(f"setting up the first panel, 0 @ {y, x} with the default spin {1+0j}\n")
    panels_map[y, x] = 0
    panels[0] = map[y * D:y * D + D, x * D:x * D + D]
    panel_spins[0] = 1
    log(panels_map)
    log()

    processed = []
    while len(np.argwhere(panels_map == -2)) > 0:
        for k in range(6):
            panel_k = np.argwhere(panels_map == k)
            if k in processed or len(panel_k) == 0:
                continue
            processed.append(k)
            y, x = panel_k[0]
            z = 1j * y + x
            rot_k = panel_spins[k]
            log(f"processing neighbours of {k} @ {y, x} with spin {rot_k}\n")
            for dz, dk in [(-1j, N_panel), (1, E_panel), (-1, W_panel), (1j, S_panel)]:
                y_, x_ = coord(z + dz * rot_k)
                try:
                    if panels_map[y_, x_] == -2:
                        s_ = panel_spins[k] * (1j if dz == 1j else -1j) * (-1 if k % 2 == 1 else 1)
                        k_ = dk(k)
                        panels_map[y_, x_] = k_
                        panel_spins[k_] = s_
                        spins = int(np.angle(s_, deg=True) // 90)
                        panels[k_] = np.rot90(map[y_ * D : y_ * D + D, x_ * D : x_ * D + D], spins)
                        log(f"found one at {y_, x_}, should be {k_} with spin {s_}\n")
                        log(panels_map)
                        log("")
                except IndexError:
                    log(f"nothing @ {y_, x_}\n")
    log("#" * 80)
    return panels_map, panels, panel_spins

panels_map, panels, panel_spins = make3d()

def to2d(panel, you, facing):
    pi, pr = np.argwhere(panels_map == panel)[0]
    cc_rotations = int(np.angle(panel_spins[panel], deg=True) // -90) % 4
    for _ in range(cc_rotations):
        facing *= -1j
        you = 1j * (D - 1 - int(you.real)) + (int(you.imag))
    you = 1j * (you.imag + pi * D) + (you.real + pr * D)
    return you, facing

def rotate_cw(you: complex, facing: complex):
    i, r = coord(you)
    if facing.real == 0:
        i_ = D - 1 - r
        r_ = D - 1 - i
    else:
        i_ = r
        r_ = i
    return r_ + i_ * 1j, facing * -1j

def rotate_cc(you: complex, facing: complex):
    i, r = coord(you)
    if facing.real == 0:
        i_ = r
        r_ = i
    else:
        i_ = D - 1 - r
        r_ = D - 1 - i
    return r_ + i_ * 1j, facing * 1j

def next3d(panels, panel, you, facing):
    front = you + facing
    i_, r_ = coord(front)
    if 0 <= i_ < D and 0 <= r_ < D: # staying in the same panel, with the same facing
        panel_ = panel
        facing_ = facing
    else: # moving to a different panel

        if i_ == D:
            panel_ = S_panel(panel)
        elif i_ == -1:
            panel_ = N_panel(panel)
        elif r_ == D:
            panel_ = E_panel(panel)
        elif r_ == -1:
            panel_ = W_panel(panel)

        front, facing_ = rotate_cc(you, facing) if (panel % 2 == 0) ^ (i_ == D) else rotate_cw(you, facing)
        i_, r_ = coord(front)
    if panels[panel_][i_, r_] == WALL:
        return panel, you, facing
    else:
        return panel_, front, facing_


def part2():
    panel, you, facing = 0, 0.0, 1.0
    commands = path[:]
    while len(commands) > 0:
        command, *commands = commands
        if command in ('L', 'R'):
            # facing *= -1j if command == 'L' else 1j
            facing = turn(facing, command)
        else:
            for _ in range(int(command)):
                panel_, you_, facing = next3d(panels, panel, you, facing)
                if panel_ == panel and you_ == you:
                    break
                panel, you = panel_, you_
    return password(*to2d(panel, you, facing))

print(part2())
