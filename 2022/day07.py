import sys

inp = [line.strip() for line in sys.stdin]


class Node:
    def __init__(self, name="/", size="dir", parent=None):
        self.name = name
        if size == "dir":
            self.content = {}
        else:
            self.content = int(size)
        self.parent = parent

    @property
    def size(self):
        if isinstance(self.content, int):
            return self.content
        return sum([x.size for x in self.content.values()])

    def __repr__(self) -> str:
        return f"{self.name} [{self.size}]"


def file_system(inp=inp):
    root = Node()
    cwd = root
    dirs = [root]
    i = 1
    while i < len(inp):
        if inp[i][0] == "$":
            cmd = inp[i].split()
            if cmd[1] == "cd":
                if cmd[2] == "..":
                    cwd = cwd.parent
                else:
                    dirs.append(cwd.content[cmd[2]])
                    cwd = cwd.content[cmd[2]]
            else:
                pass
        i += 1
        while i < len(inp) and inp[i][0] != "$":
            size, name = inp[i].split()
            cwd.content[name] = Node(name, size, cwd)
            i += 1
    return dirs


dirs = file_system(inp)


def part1():
    global dirs
    return sum([x.size for x in dirs if x.size <= 100_000])

print(part1())


def part2():
    global dirs
    deficit = dirs[0].size - 40_000_000
    return min([x.size for x in dirs if x.size >= deficit])

print(part2())
