import sys
import heapq

print("\x1b[38;5;1mWARNING: Not an efficient solution! Might take a few minutes...\x1b[0m", file=sys.stderr)

VISUALIZE = ("-v" in sys.argv) or ("--visualize" in sys.argv)

if ("-s" in sys.argv) or ("--sample" in sys.argv):
    from io import StringIO
    sample = StringIO("""\
#.######
#>>.<^<#
#.<..<<#
#>v.><>#
#<^v^^>#
######.#
""")
    sys.stdin = sample

map = [line.strip() for line in sys.stdin]
H, W = len(map), len(map[0])
goal = (W - 2) + (H - 1) * 1j
here = 1

blizzards = []
dirs = {'^': -1j, 'v': 1j, '>': 1, '<': -1, '!': 0}
for y in range(1, len(map) - 1):
    for x in range(1, len(map[1]) - 1):
        if map[y][x] in dirs:
            blizzards.append((x + y * (1j), dirs[map[y][x]]))


def location(org, dir, step):
    r = (org.real - 1 + dir.real * step) % (W - 2) + 1
    i = (org.imag - 1 + dir.imag * step) % (H - 2) + 1
    return r + i * (1j)


def visualize(blizzards=blizzards, step=0, here=1, goal=goal):
    global VISUALIZE
    if not VISUALIZE:
            return
    import numpy as np
    from time import sleep
    legend = ".#@^>v<222223333444"
    map = np.zeros((H, W), dtype=np.int8)
    map[ 0, :] = 1
    map[-1, :] = 1
    map[:,  0] = 1
    map[:, -1] = 1
    for org, dir in blizzards:
        loc = location(org, dir, step)
        map[int(loc.imag), int(loc.real)] += int(np.angle(dir) / np.pi * 2 + 4)
    map[int(here.imag), int(here.real)] = 2
    str_map = "\x1bc"
    for y in range(H):
        for x in range(W):
            str_map += legend[map[y, x]]
        str_map += '\n'
    print(str_map)
    sleep(.3)


def heuristic(here, goal):
    return abs(goal.real - here.real) + abs(goal.imag - here.imag)


def a_star(here, goal, step=0):
    h = heuristic(here, goal)
    q = [(h + step, step, h, here.imag, here.real)]
    visited = set()
    prev = {}
    while len(q) > 0:
        f_, g_, h_, you_imag, you_real = heapq.heappop(q)
        you_ = you_real + you_imag * (1j)
        if h_ == 0:
            return g_, prev
        for nei in dirs.values():
            next = you_ + nei
            if H <= next.imag or next.imag < 0:
                continue
            if map[int(next.imag)][int(next.real)] == '#':
                continue
            for org, dir in blizzards:
                if location(org, dir, g_ + 1) == next:
                    break
            else:
                g = g_ + 1
                h = heuristic(next, goal)
                node = (g + h, g, h, next.imag, next.real)
                if node not in visited:
                    prev[(g, next)] = (g_, you_)
                    heapq.heappush(q, node)
                    visited.add(node)

# print(length)
# path = []
# curr = (length, (H - 1) * (1j) + (W - 2))
# while curr is not None:
#     path.append(curr)
#     curr = prev.get(curr, None)
# print(path)

length = 0


def part1():
    global length
    length, _ = a_star(here=here, goal=goal)
    return length

print(part1())


def part2():
    global length
    if length == 0:
        length, _ = a_star(here=here, goal=goal)
    length, _ = a_star(here=goal, goal=here, step=length)
    length, _ = a_star(here=here, goal=goal, step=length)
    return length

print(part2())
