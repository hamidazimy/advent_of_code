import sys
import numpy as np

map = np.array([[ord(x) - ord('0') for x in line.strip()] for line in sys.stdin])


def part1(map=map):
    result = 0
    for i in range(len(map)):
        for j in range(len(map[0])):
            if map[i, j] > np.max(map[i,     :j], initial=-1) or \
               map[i, j] > np.max(map[i, j + 1:], initial=-1) or \
               map[i, j] > np.max(map[:i,     j], initial=-1) or \
               map[i, j] > np.max(map[i + 1:, j], initial=-1):
                result += 1
    return result

print(part1())


def part2(map=map):
    result = -1
    for i in range(len(map)):
        for j in range(len(map[0])):
            score = 1
            scenes = [
                map[:i, j][::-1],
                map[i + 1:, j],
                map[i, :j][::-1],
                map[i, j + 1:]
            ]
            t = map[:i, j][::-1]
            for scene in scenes:
                scene_score = 0
                for k in range(len(scene)):
                    scene_score += 1
                    if map[i, j] <= scene[k]:
                        break
                score *= scene_score
            result = max(result, score)
    return result

print(part2())
