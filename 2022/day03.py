import sys

inp = [line.strip() for line in sys.stdin]


def priority(item):
    p = ord(item) - ord('a') + 1
    if item.isupper():
        p += ord('a') - ord('A') + 26
    return p


def part1(inp=inp):
    result = 0
    for x in inp:
        comp1 = set(list(x[:len(x) // 2]))
        comp2 = set(list(x[len(x) // 2:]))
        common = list(comp1.intersection(comp2))[0]
        result += priority(common)
    return result

print(part1())


def part2(inp=inp):
    result = 0
    for i in range(0, len(inp), 3):
        elf1 = set(list(inp[i]))
        elf2 = set(list(inp[i + 1]))
        elf3 = set(list(inp[i + 2]))
        common = list(elf1.intersection(elf2).intersection(elf3))[0]
        result += priority(common)
    return result

print(part2())
