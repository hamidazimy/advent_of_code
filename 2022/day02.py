import sys

inp = [line.strip().split() for line in sys.stdin]


def part1(inp=inp):
    strategy = {'X': 'A', 'Y': 'B', 'Z': 'C'}
    result = 0
    for i, j in inp:
        shape = ord(j) - ord('X') + 1
        outcome = (ord(strategy[j]) - ord(i) + 1) % 3
        result += shape + outcome * 3
    return result

print(part1())


def part2(inp=inp):
    result = 0
    for i, j in inp:
        outcome = ord(j) - ord('X')
        shape = (ord(i) - ord('A') - 1 + outcome) % 3 + 1
        result += shape + outcome * 3
    return result

print(part2())
