import sys
import numpy as np

VISUALIZE = ("-v" in sys.argv) or ("--visualize" in sys.argv)

if ("-s" in sys.argv) or ("--sample" in sys.argv):
    from io import StringIO
    sample = StringIO("""\
>>><<><>><<<>><>>><<<>>><<<><<<>><>><<>>
""")
    sys.stdin = sample

inp = sys.stdin.readline().strip()
N = len(inp)

rocks = [
    np.array([[1, 1, 1, 1]]
            ),
    np.array([[0, 1, 0],
              [1, 1, 1],
              [0, 1, 0]]
            ),
    np.array([[0, 0, 1],
              [0, 0, 1],
              [1, 1, 1]]
            ),
    np.array([[1],
              [1],
              [1],
              [1]]
            ),
    np.array([[1, 1],
              [1, 1]]
            ),
]


def visualize(chamber, tower, top, left, rock):
    global VISUALIZE
    if not VISUALIZE:
        return
    from time import sleep
    sleep(.05)
    T = 40
    start = min(tower - 7, len(chamber) - T)
    window = np.copy(chamber[start:start + T, :])
    h, w = np.shape(rock)
    window[top - start:top - start + h, left:left + w] = rock * 2
    legend = ".#@"
    str_map = "\x1bc"
    for i in range(T):
        str_map += "|"
        try:
            str_map += ''.join([legend[window[i, j]] for j in range(7)])
        except:
            print(window[i, :])
            exit(0)
        str_map += "|\n"
    print(str_map, flush=True)

def solve(I=2022):
    global inp
    chamber = np.zeros((10000, 7), dtype=np.int8)
    chamber[-1, :] = 1
    states = {}
    tower = len(chamber) - 1
    height = 0
    cycle_length = 0
    cycle_height = 0
    cyclces_skipped = 0
    steps = 0
    round = 0
    while round < I:
        heights = (chamber != 0).argmax(axis=0)
        heights = tuple(heights - min(heights))
        state = (round % 5, steps % N, heights)
        if cyclces_skipped == 0:
            if state in states:
                r_, h_ = states[state]
                cycle_length = round - r_
                cycle_height = height - h_
                cyclces_skipped = I // (cycle_length) - 2
                round += cycle_length * cyclces_skipped
        states[state] = (round, height)
        rock = rocks[round % 5]
        h, w = np.shape(rock)
        top = tower - 3 - h
        left = 2
        visualize(chamber, tower, top, left, rock)
        while True:
            if inp[steps % N] == '>':
                new_left = min(left + 1, 7 - w)
            else:
                new_left = max(left - 1, 0)
            here = chamber[top:top + h, new_left:new_left + w] + rock
            if np.all(here != 2):
                left = new_left

            visualize(chamber, tower, top, left, rock)
            steps += 1

            new_top = top + 1
            here = chamber[new_top:new_top + h, left:left + w] + rock
            if np.all(here != 2):
                top = new_top
            else:
                chamber[top:top + h, left:left + w] += rock
                break

            visualize(chamber, tower, new_top, left, rock)

        tower = min(tower, top)
        height = len(chamber) - tower - 1
        round += 1
    return height + cyclces_skipped * cycle_height


def part1():
    return solve()

print(part1())


def part2():
    return solve(I=1_000_000_000_000)

print(part2())
