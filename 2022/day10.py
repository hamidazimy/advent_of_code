import sys

inp = [line.strip() for line in sys.stdin]

N = [1]
for ins in inp:
    N.append(0)
    if ins != "noop":
        N.append(int(ins.split(' ')[1]))
N.pop()

def part1():
    return sum([i * sum(N[:i]) for i in range(20, len(N), 40)])

print(part1())


def part2():
    S = ""
    legend = " █"
    for i in range(len(N)):
        if i % 40 == 0:
            S += '\n'
        S += legend[int(abs(sum(N[:i + 1]) - i % 40) <= 1)] * 2
    return S

print(part2())
