import sys
import numpy as np
from queue import PriorityQueue

map = np.array([[ord(x) - ord('a') for x in line.strip()] for line in sys.stdin])
H, W = np.shape(map)
here = tuple(np.argwhere(map == ord('S') - ord('a'))[0])
goal = tuple(np.argwhere(map == ord('E') - ord('a'))[0])
map[here] = 0
map[goal] = 26

def manhatan(here, goal):
    return abs(here[0] - goal[0]) + abs(here[1] - goal[1])

dirs = [(0, 1), (1, 0), (0, -1), (-1, 0)]

def a_star(here=here):
    q = PriorityQueue()
    h = manhatan(here, goal)
    q.put((h, 0, h) + here)
    visited = set()
    while True:
        if q.empty():
            return np.inf
        f_, g_, h_, y_, x_ = q.get()
        if manhatan((y_, x_), goal) == 0:
            return g_
        for dx, dy in dirs:
            y = y_ + dy
            x = x_ + dx
            if 0 <= y < H and 0 <= x < W:
                if (y, x) not in visited:
                    if map[y, x] <= map[y_, x_] + 1:
                        g = g_ + 1
                        h = manhatan((y, x), goal)
                        f = g + h
                        q.put((f, g, h, y, x))
                        visited.add((y, x))


def part1(map=map):
    return a_star()

print(part1())


def part2(map=map):
    result = np.inf
    for here in np.argwhere(map == 0):
        steps = a_star(tuple(here))
        result = min(result, steps)
    return result

print(part2())
