import sys

inp = sys.stdin.readline().strip()

def solve(inp, n=4):
    for i in range(n, len(inp)):
        if (len(set(list(inp[i - n:i])))) == n:
            return i

def part1(inp=inp):
    return solve(inp)

print(part1())


def part2(inp=inp):
    return solve(inp, n=14)

print(part2())
