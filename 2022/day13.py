import sys
import json
import functools

inp = sys.stdin.read().strip()


def compare(left, right):
    for i in range(len(right)):
        if i == len(left):
            # left ran out of items
            return -1
        if type(left[i]) != type(right[i]):
            # wraping and comparing mixed types
            if type(left[i]) == int:
                t = compare([left[i]], right[i])
                if t != 0:
                    return t
            else:
                t = compare(left[i], [right[i]])
                if t != 0:
                    return t
        elif type(left[i]) == list:
            # recursive comparison if lists
            t = compare(left[i], right[i])
            if t != 0:
                return t
        else:
            # integer comparison if numbers
            if left[i] < right[i]:
                return -1
            elif left[i] > right[i]:
                return 1
    # return if whether right ran out of numbers, or lists are equal
    return int(len(right) < len(left))


def part1(inp=inp):
    inp1 = [[json.loads(x) for x in pair.strip().split('\n')] for pair in inp.split("\n\n")]
    result = 0
    for i, pair in enumerate(inp1):
        t = compare(pair[0], pair[1])
        result += (i + 1) * (t == -1)
    return result

print(part1())


def part2(inp=inp):
    two, six = [[2]], [[6]]
    inp2 = [json.loads(x) for x in inp.split("\n") if x != '']
    inp2 += [two, six]
    ordered = sorted(inp2, key=functools.cmp_to_key(compare))
    return (1 + ordered.index(two)) * (1 + ordered.index(six))

print(part2())
