import sys

VISUALIZE = ("-v" in sys.argv) or ("--visualize" in sys.argv)

inp = [line.strip().split() for line in sys.stdin]

dirs = {"U": -1j, "D": 1j, "R": 1, "L": -1}


def follow(H, T):
    if abs(H - T) == 2.0:
        T = (H + T) / 2
    elif abs(H - T) == 5 ** .5:
        if abs(H.real - T.real) == 1:
            T = H.real + ((H.imag + T.imag) / 2) * 1j
        else:
            T = (H.real + T.real) / 2 + H.imag * 1j
    elif abs(H - T) == 8 ** .5:
            T = (H.real + T.real) / 2 + ((H.imag + T.imag) / 2) * 1j
    return T


def part1(inp=inp):
    global dirs
    H, T = 0j, 0j
    visited = set([T])
    for d, n in inp:
        for _ in range(int(n)):
            H += dirs[d]
            T = follow(H, T)
            visited.add(T)
    return len(visited)

print(part1())


def visualize(knots, n=15):
    global VISUALIZE
    if not VISUALIZE:
        return
    from time import sleep
    import numpy
    map = numpy.zeros((n * 2 + 1, n * 2 + 1), dtype=numpy.int64)
    if isinstance(knots, list):
        legend = ".H123456789"
        for i, k in reversed(list(enumerate(knots))):
            map[int(n + k.imag), int(n + k.real)] = i + 1
    else:
        legend = ".#S"
        for k in list(knots):
            map[int(n + k.imag), int(n + k.real)] = 1
        map[n, n] = 2

    print('\n'.join([''.join([legend[map[r, c]] for c in range(n * 2 + 1)]) for r in range(n * 2 + 1)]))
    print("\n\n")
    sleep(.1)


def part2(inp=inp):
    global dirs
    knots = [0j] * 10
    visited = set([knots[-1]])
    for d, n in inp:
        for _ in range(int(n)):
            knots[0] += dirs[d]
            for k in range(1, 10):
                knots[k] = follow(knots[k - 1], knots[k])
            visited.add(knots[-1])
        visualize(knots)
    visualize(visited)
    return len(visited)

print(part2())
