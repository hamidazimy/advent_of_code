import sys

if ("-s" in sys.argv) or ("--sample" in sys.argv):
    from io import StringIO
    sample = StringIO("""\
1
2
-3
3
-2
0
4
""")
    sys.stdin = sample

inp = [int(line.strip()) for line in sys.stdin]
N = len(inp)


DECRYPTION_KEY = 811589153

class Node:
    def __init__(self, num):
        self.num = num
        self.prev = None
        self.next = None

    def add_next(self, node):
        node.prev = self
        node.next = self.next
        self.next.prev = node
        self.next = node

    def pop_next(self):
        node = self.next
        self.next = node.next
        node.next.prev = self
        return node


def make_the_ring(inp=inp, dec_key=1):
    zero = None
    all = [None] * N
    all[0] = Node(inp[0] * dec_key)
    node = all[0]
    for i in range(1, N):
        all[i] = Node(inp[i] * dec_key)
        node.next = all[i]
        node.next.prev = node
        node = node.next
        if inp[i] == 0:
            zero = node
    node.next = all[0]
    all[0].prev = node
    return all, zero


def mix(all, rounds=1):
    for _ in range(rounds):
        for node in all:
            steps = node.num % (N - 1)
            if steps == 0:
                continue
            dest = node.prev
            node = dest.pop_next()
            for _ in range(steps):
                dest = dest.next
            dest.add_next(node)


def sum_grove_coordinates(zero):
    node = zero
    result = 0
    for _ in range(3):
        for _ in range(1000 % N):
            node = node.next
        result += node.num
    return result


def part1():
    all, zero = make_the_ring()
    mix(all)
    return sum_grove_coordinates(zero)

print(part1())


def part2():
    all, zero = make_the_ring(dec_key=DECRYPTION_KEY)
    mix(all, rounds=10)
    return sum_grove_coordinates(zero)

print(part2())
