import sys
import re

inp = [line.strip() for line in sys.stdin]


def part1(inp=inp):
    def contain(line):
        one_b, one_e, two_b, two_e = re.match(r"(\d+)-(\d+),(\d+)-(\d+)", line).groups()
        return (int(two_e) - int(one_e)) * (int(two_b) - int(one_b)) <= 0
    return sum([contain(line) for line in inp])

print(part1())


def part2(inp=inp):
    def overlap(line):
        one_b, one_e, two_b, two_e = re.match(r"(\d+)-(\d+),(\d+)-(\d+)", line).groups()
        return (int(two_e) - int(one_b)) * (int(two_b) - int(one_e)) <= 0
    return sum([overlap(line) for line in inp])

print(part2())
