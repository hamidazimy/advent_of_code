import sys
import numpy as np

if ("-s" in sys.argv) or ("--sample" in sys.argv):
    from io import StringIO
    sample = StringIO("""\
FF7FSF7F7F7F7F7F---7
L|LJ||||||||||||F--J
FL-7LJLJ||||||LJL-77
F--JF--7||LJLJIF7FJ-
L---JF-JLJIIIIFJLJJ7
|F|F-JF---7IIIL7L|7|
|FFJF7L7F-JF7IIL---7
7-L-JL7||F7|L7F-7F7|
L.L7LFJ|||||FJL7||LJ
L7JLJL-JLJLJL--JLJ.L
""")
    sys.stdin = sample

VISUALIZE = ("-v" in sys.argv) or ("--visualize" in sys.argv)

def visualize(sketch):
    if not VISUALIZE:
        return
    from time import sleep
    H, W = np.shape(sketch)
    legend = {
        ord('S'): 'S',
        ord('.'): ' ',
        ord('|'): '│',
        ord('-'): '─',
        ord('7'): '┐',
        ord('J'): '┘',
        ord('F'): '┌',
        ord('L'): '└',
        ord('*'): '░',
    }
    str_sketch = '\n'.join([''.join([legend[c] for c in row]) for row in sketch])
    print(str_sketch)
    sleep(2)

inp = [line.strip() for line in sys.stdin]
sketch = np.array([[ord(c) for c in line] for line in inp], dtype=np.uint16)
the_loop = []


def start_pipe():
    # I cheated here a bit and did it manually!
    # TODO: Implement a general solution.
    if ("-s" in sys.argv) or ("--sample" in sys.argv):
        return ord('7')
    return ord('|')


def next(y, x, y_=None, x_=None):
    if sketch[y, x] == ord('S'):
        s = start_pipe()
        if s in [ord('|'), ord('7'), ord('F')]:
            return y + 1, x
        elif s in [ord('-'), ord('L')]:
            return y, x + 1
        else: # s == ord('J')
            return y - 1, x
    if sketch[y, x] == ord('|'):
        return y + (y - y_), x
    if sketch[y, x] == ord('-'):
        return y, x + (x - x_)
    if sketch[y, x] == ord('7'):
        if y_ == y:
            return y + 1, x
        else:
            return y, x - 1
    if sketch[y, x] == ord('L'):
        if y_ == y:
            return y - 1, x
        else:
            return y, x + 1
    if sketch[y, x] == ord('J'):
        if y_ == y:
            return y - 1, x
        else:
            return y, x - 1
    if sketch[y, x] == ord('F'):
        if y_ == y:
            return y + 1, x
        else:
            return y, x + 1


def part1():
    global the_loop
    start = tuple(np.argwhere(sketch == ord('S'))[0])
    the_loop = [start]
    y_, x_, y, x = *start, *next(*start)
    while (y, x) != start:
        the_loop.append((y, x))
        y_, x_, y, x = y, x, *next(y, x, y_, x_,)
    return len(the_loop) // 2


def part2():
    global the_loop
    visualize(sketch)
    new_sketch = np.ones_like(sketch) * ord('.')
    H, W = np.shape(new_sketch)
    for y, x in the_loop:
        new_sketch[y, x] = sketch[y, x]
    new_sketch[the_loop[0]] = start_pipe()
    visualize(new_sketch)
    total_enclosed = 0
    for y in range(H):
        crossings = 0
        last_turn = None
        for x in range(W):
            if new_sketch[y, x] == ord('.') and crossings % 2 == 1:
                total_enclosed += 1
                new_sketch[y, x] = ord('*')
            elif new_sketch[y, x] == ord('|'):
                crossings += 1
            elif new_sketch[y, x] == ord('7') and last_turn == ord('L'):
                crossings += 1
            elif new_sketch[y, x] == ord('J') and last_turn == ord('F'):
                crossings += 1
            elif new_sketch[y, x] == ord('F') or sketch[y, x] == ord('L'):
                last_turn = new_sketch[y, x]
    visualize(new_sketch)
    return total_enclosed


print(part1())
print(part2())
