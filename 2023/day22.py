import sys
import numpy as np

if ("-s" in sys.argv) or ("--sample" in sys.argv):
    from io import StringIO
    sample = StringIO("""\
1,0,1~1,2,1
0,0,2~2,0,2
0,2,3~2,2,3
0,0,4~0,2,4
2,0,5~2,2,5
0,1,6~2,1,6
1,1,8~1,1,9
""")
    sys.stdin = sample

X, Y, Z = 0, 1, 2

inp = sorted([eval(f"({line.strip()})".replace('~', "),(")) for line in sys.stdin], key=lambda b: b[0][Z])
N = len(inp)
adj_mat = np.zeros((N, N), dtype=np.bool_)


def overlaps(brick_i, brick_j):
    if brick_i[0][X] > brick_j[1][X]:
        return False
    if brick_i[1][X] < brick_j[0][X]:
        return False
    if brick_i[0][Y] > brick_j[1][Y]:
        return False
    if brick_i[1][Y] < brick_j[0][Y]:
        return False
    return True


def setteled(brick, tower: list):
    bottom_Z = 0
    supports = []
    for i, b in enumerate(tower):
        if overlaps(b, brick):
            if b[1][Z] > bottom_Z:
                bottom_Z = b[1][Z]
                supports = [i]
            elif b[1][Z] == bottom_Z:
                supports.append(i)
    new_1_Z = bottom_Z + 1 - brick[0][Z] + brick[1][Z]
    new_0_Z = bottom_Z + 1
    return ((brick[0][X], brick[0][Y], new_0_Z), (brick[1][X], brick[1][Y], new_1_Z)), supports


def fall():
    tower = []
    for i, brick in enumerate(inp):
        brick, supports = setteled(brick, tower)
        tower.append(brick)
        adj_mat[supports, i] = 1
    return tower, adj_mat


tower, adj_mat = fall()


def part1():
    bricks_with_single_supports = (np.sum(adj_mat, axis=0) == 1)
    return N - np.sum(np.any(adj_mat[:, bricks_with_single_supports], axis=1))
    # # The old implementation.
    # non_disintegratable = set()
    # for j in range(N):
    #     if np.sum(adj_mat[:, j]) == 1:
    #         non_disintegratable.update(np.flatnonzero(adj_mat[:, j]))
    # return N - len(non_disintegratable)


def part2():
    def would_fall(i):
        global adj_mat
        falling = [i]
        k = 0
        while k < len(falling):
            for j in np.flatnonzero(adj_mat[falling[k], :]):
                if j in falling:
                    continue
                if all(x in falling for x in np.flatnonzero(adj_mat[:, j])):
                    falling.append(j)
            k += 1
        return len(falling) - 1
    return sum([would_fall(i) for i in range(N)])


print(part1())
print(part2())
