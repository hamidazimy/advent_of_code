import sys
from math import lcm

DEBUG = ("-d" in sys.argv) or ("--debug" in sys.argv)

def log(msg=""):
    if DEBUG:
        print(msg)

inp = [line.strip('\n') for line in sys.stdin]


LOW = False
HIGH = True

modules = None
num_lo, num_hi = 0, 0
total_presses = 0
rx_activators_cycles = None


class Module:
    def __init__(self, input_line):
        name, dest = input_line.split(" -> ")
        if name == "broadcaster":
            self.name = name
            self.type = "@"
        else:
            self.name = name[1:]
            self.type = name[0]
        self.state = False
        self.prevs = {}
        self.nexts = dest.split(", ") if dest != '' else []


def init():
    global total_presses, rx_activators_cycles

    modules = {"rx": Module("&rx -> ")}

    for line in inp:
        m = Module(line)
        modules[m.name] = m

    for m in modules:
        module = modules[m]
        for dest in module.nexts:
            if modules[dest].type == "&":
                modules[dest].prevs[m] = False

    total_presses = 0
    rx_prev = list(modules['rx'].prevs.keys())[0]
    rx_activators_cycles = {k: 0 for k in modules[rx_prev].prevs}

    return modules


def broadcast(pulse, src):
    global num_lo, num_hi, modules
    out_pulses = []
    if pulse is HIGH:
        num_hi += len(modules[src].nexts)
    else:
        num_lo += len(modules[src].nexts)
    for dst in modules[src].nexts:
        log(f"{src} -{'high' if pulse is HIGH else 'low'}-> {dst}")
        if modules[dst].nexts == "%":
            if pulse is LOW:
                out_pulses.append((pulse, src, dst))
        else:
            out_pulses.append((pulse, src, dst))
    return out_pulses


def press_button():
    global num_lo, num_hi, total_presses, modules, rx_activators_cycles
    total_presses += 1
    num_lo += 1
    log(f"button -low-> broadcaster")
    pulses = broadcast(LOW, "broadcaster")
    while len(pulses) > 0:
        pulse, src, dst = pulses.pop(0)
        if modules[dst].type == "%":
            if pulse is LOW:
                modules[dst].state = not modules[dst].state
                pulses += broadcast(modules[dst].state, dst)
        else: # if modules[dst].type == "&":
            modules[dst].prevs[src] = pulse
            all_high = LOW not in modules[dst].prevs.values()
            modules[dst].state = all_high
            if dst in rx_activators_cycles and not all_high:
                rx_activators_cycles[dst] = total_presses
            pulses += broadcast(not all_high, dst)


def part1():
    global modules
    modules = init()
    for _ in range(1000):
        press_button()
    return num_lo * num_hi


def part2():
    global modules
    modules = init()
    while True:
        press_button()
        if 0 not in rx_activators_cycles.values():
            return lcm(*list(rx_activators_cycles.values()))


print(part1())
print(part2())
