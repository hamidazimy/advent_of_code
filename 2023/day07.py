import sys

if ("-s" in sys.argv) or ("--sample" in sys.argv):
    from io import StringIO
    sample = StringIO("""\
32T3K 765
T55J5 684
KK677 28
KTJJT 220
QQQJA 483
""")
    sys.stdin = sample

inp = [line.strip().split() for line in sys.stdin]


HIGH_CARD      = "0"
ONE_PAIR       = "1"
TWO_PAIRS      = "2"
THREE_OF_A_KID = "3"
FULL_HOUSE     = "6"
FOUR_OF_A_KIND = "7"
FIVE_OF_A_KIND = "F"


def hand_type(cards_list: list, joker: bool):
    num_of_kinds = len(set(cards_list))
    max_of_a_kind = max([cards_list.count(i) for i in cards_list])
    jokers = cards_list.count('J') if joker else 0
    if num_of_kinds == 1:
        return FIVE_OF_A_KIND
    elif num_of_kinds == 2:
        if jokers > 0:
            return FIVE_OF_A_KIND
        elif max_of_a_kind == 3:
            return FULL_HOUSE
        else: # max_of_a_kind == 4:
            return FOUR_OF_A_KIND
    elif num_of_kinds == 3:
        if max_of_a_kind == 2:
            if jokers == 2:
                return FOUR_OF_A_KIND
            elif jokers == 1:
                return FULL_HOUSE
            else: # joker == 0:
                return TWO_PAIRS
        else: # max_of_a_kind == 3:
            if jokers > 0:
                return FOUR_OF_A_KIND
            else: # joker == 0:
                return THREE_OF_A_KID
    elif num_of_kinds == 4:
        if jokers > 0:
            return THREE_OF_A_KID
        else: # joker == 0:
            return ONE_PAIR
    else: # num_of_kinds == 5:
        if jokers == 1:
            return ONE_PAIR
        else: # joker == 0:
            return HIGH_CARD


def score(item: str, joker=False):
    hex_item = f"{hand_type(list(item), joker)}{item}"
    hex_item = hex_item.replace("A", "E")
    hex_item = hex_item.replace("K", "D")
    hex_item = hex_item.replace("Q", "C")
    hex_item = hex_item.replace("J", "1" if joker else "B")
    hex_item = hex_item.replace("T", "A")
    return int(hex_item, 16)


def part1():
    sorted_scores = sorted([(score(x[0]), x[0], int(x[1])) for x in inp])
    return sum([(i + 1) * item[2] for i, item in enumerate(sorted_scores)])


def part2():
    sorted_scores_with_joker = sorted([(score(x[0], joker=True), x[0], int(x[1])) for x in inp])
    return sum([(i + 1) * item[2] for i, item in enumerate(sorted_scores_with_joker)])


print(part1())
print(part2())
