import sys
import numpy as np

if ("-s" in sys.argv) or ("--sample" in sys.argv):
    from io import StringIO
    sample = StringIO("""\
...#......
.......#..
#.........
..........
......#...
.#........
.........#
..........
.......#..
#...#.....
""")
    sys.stdin = sample

inp = [line.strip() for line in sys.stdin]
img = np.array([[c == '#' for c in line] for line in inp])

Y, X = 0, 1

empty_rows_before = np.cumsum(np.sum(img, axis=X) == 0)
empty_cols_before = np.cumsum(np.sum(img, axis=Y) == 0)


def distance(g1, g2, expansion):
    return abs(g2[Y] - g1[Y]) + abs(g2[X] - g1[X]) +\
        (empty_rows_before[max(g1[Y], g2[Y])] - empty_rows_before[min(g1[Y], g2[Y])]) * (expansion - 1) +\
        (empty_cols_before[max(g1[X], g2[X])] - empty_cols_before[min(g1[X], g2[X])]) * (expansion - 1)


def solve(expansion=2):
    total_distance = 0
    galaxies = np.argwhere(img)
    for p in range(0, len(galaxies) - 1):
        for q in range(p + 1, len(galaxies)):
            total_distance += distance(galaxies[p], galaxies[q], expansion)
    return total_distance


def part1():
    return solve()


def part2():
    return solve(expansion=1_000_000)


print(part1())
print(part2())
