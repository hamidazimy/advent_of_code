import sys
import re

inp = [line.strip() for line in sys.stdin]

digits = {
    "1"    : 1,
    "2"    : 2,
    "3"    : 3,
    "4"    : 4,
    "5"    : 5,
    "6"    : 6,
    "7"    : 7,
    "8"    : 8,
    "9"    : 9,
    "one"  : 1,
    "two"  : 2,
    "three": 3,
    "four" : 4,
    "five" : 5,
    "six"  : 6,
    "seven": 7,
    "eight": 8,
    "nine" : 9,
}


def calibration(line: str, spelled_out_with_letters: bool=False):
    pattern = r"(?=(\d))"
    if spelled_out_with_letters:
        pattern = rf"(?=(\d|{'|'.join([d for d in digits])}))"
    matches = re.findall(pattern, line)
    return digits[matches[0]] * 10 + digits[matches[-1]]


def part1():
    result = 0
    for line in inp:
        result += calibration(line)
    return result


def part2():
    result = 0
    for line in inp:
        result += calibration(line, spelled_out_with_letters=True)
    return result


print(part1())
print(part2())
