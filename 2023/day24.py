import sys
import random

test_min = 200000000000000
test_max = 400000000000000

if ("-s" in sys.argv) or ("--sample" in sys.argv):
    from io import StringIO
    sample = StringIO("""\
19, 13, 30 @ -2,  1, -2
18, 19, 22 @ -1, -1, -2
20, 25, 34 @ -2, -2, -4
12, 31, 28 @ -1, -2, -1
20, 19, 15 @  1, -5, -3
""")
    sys.stdin = sample
    test_min = 7
    test_max = 27

inp = [eval(f"({line.strip()})".replace('@', "),(")) for line in sys.stdin]

P = 0
V = 1
X = 0
Y = 1
Z = 2


def part1():
    total = 0
    for i in range(len(inp) - 1):
        for j in range(i + 1, len(inp)):
            m_i = inp[i][V][Y] / inp[i][V][X]
            m_j = inp[j][V][Y] / inp[j][V][X]
            if m_i == m_j:
                continue
            X_inter = ((m_i * inp[i][P][X] - m_j * inp[j][P][X]) - (inp[i][P][Y] - inp[j][P][Y])) / (m_i - m_j)
            Y_inter = ((inp[i][P][Y] / m_i - inp[j][P][Y] / m_j) - (inp[i][P][X] - inp[j][P][X])) / (1 / m_i - 1 / m_j)
            if test_min <= X_inter <= test_max and test_min <= Y_inter <= test_max:
                if (X_inter - inp[i][P][X]) / inp[i][V][X] > 0 and (X_inter - inp[j][P][X]) / inp[j][V][X] > 0:
                    if (Y_inter - inp[i][P][Y]) / inp[i][V][Y] > 0 and (Y_inter - inp[j][P][Y]) / inp[j][V][Y] > 0:
                        total += 1
    return total


def part2():
    """
    This is an algebraic solution.
    """
    def gaussian_elimination(A, b):
        # Since we are dealing with large numbers, sometimes
        # the accumulated errors from processing floating point numbers
        # will give us a wrong answer.
        # TODO: implement Gaussian elimination from scratch using fractions.
        import numpy as np
        x = np.linalg.solve(A, b)
        return list(x.flatten())

    def solve():
        """
        This creates a system of linear equations with 6 unkonwns,
        using 4 randomly selected hailstones, then
        uses Gaussian elimination to solve for the unknowns.
        """
        def calc_b_i(hs1, hs2, d1):
            d2 = d1 - 2
            return hs1[P][d2] * hs1[V][d1] - hs1[P][d1] * hs1[V][d2] - hs2[P][d2] * hs2[V][d1] + hs2[P][d1] * hs2[V][d2]

        def calc_A_i(hs1, hs2, d1):
            d2 = d1 - 2
            ps = [0] * 3
            vs = [0] * 3
            ps[d1] = hs2[V][d2] - hs1[V][d2]
            ps[d2] = hs1[V][d1] - hs2[V][d1]
            vs[d1] = hs2[P][d2] - hs1[P][d2]
            vs[d2] = hs1[P][d1] - hs2[P][d1]
            return ps + vs

        hailstones = random.choices(inp, k=4)
        A = [None] * 6
        b = [None] * 6

        for i in range(6):
            hsi = hailstones[i // 2]
            hsj = hailstones[i // 2 + 1]

            b[i] = calc_b_i(hsi, hsj, i % 3)
            A[i] = calc_A_i(hsi, hsj, i % 3)

        x = gaussian_elimination(A, b)
        return round(sum(x[:3]))

    while True:
        try:
            # Since we choose the hailstones randomly, it is possible
            # that our system of linear equations does not have an answer,
            # and we get an error instead.
            # Therefore, we repeat until we get an answer.
            return solve()
        except Exception as e:
            print(e, file=sys.stderr)


print(part1())
print(part2())
