import sys
import math

if ("-s" in sys.argv) or ("--sample" in sys.argv):
    from io import StringIO
    sample = StringIO("""\
LLR

AAA = (BBB, BBB)
BBB = (AAA, ZZZ)
ZZZ = (ZZZ, ZZZ)
""")
    sys.stdin = sample

ins, net = sys.stdin.read().split("\n\n")
net = {line[0:3]:(line[7:10], line[12:15]) for line in net.strip().split("\n")}


def lcm(nums):
    if hasattr(math, "lcm"):
        return math.lcm(*nums)
    result = 1
    for num in nums:
        result *= num // math.gcd(result, num)
    return result


def part1():
    n = 0
    node = "AAA"
    while True:
        for i in ins:
            node = net[node][i == "R"]
            n += 1
            if node == "ZZZ":
                return n


def part2():
    n = 0
    nodes = [node for node in net if node[-1] == "A"]
    loops = [0] * len(nodes)
    while True:
        for i in ins:
            for k, node in enumerate(nodes):
                nodes[k] = net[node][i == "R"]
            n += 1
            for k, node in enumerate(nodes):
                if not loops[k]:
                    if node[-1] == "Z":
                        loops[k] = n
            if 0 not in loops:
                return lcm(loops)


print(part1())
print(part2())
