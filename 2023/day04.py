import sys

if ("-s" in sys.argv) or ("--sample" in sys.argv):
    from io import StringIO
    sample = StringIO("""\
Card 1: 41 48 83 86 17 | 83 86  6 31 17  9 48 53
Card 2: 13 32 20 16 61 | 61 30 68 82 17 32 24 19
Card 3:  1 21 53 59 44 | 69 82 63 72 16 21 14  1
Card 4: 41 92 73 84 69 | 59 84 76 51 58  5 54 83
Card 5: 87 83 26 28 32 | 88 30 70 12 93 22 82 36
Card 6: 31 18 13 56 72 | 74 77 10 23 35 67 36 11
""")
    sys.stdin = sample

inp = [line.strip() for line in sys.stdin]


def wins(line):
    card, nums = line.split(" | ")
    card = [int(x) for x in card.split(": ")[1].split()]
    nums = [int(x) for x in nums.split()]
    matches = 0
    for n in nums:
        if n in card:
            matches += 1
    return matches


def part1():
    points = [wins(line) for line in inp]
    points = [0 if w == 0 else 2 ** (w - 1) for w in points]
    return sum(points)


def part2():
    # card number = card index + 1
    scratchcards = {i: 1 for i in range(len(inp))}
    for i in range(len(inp)):
        pts = wins(inp[i])
        for j in range(i + 1, i + pts + 1):
            scratchcards[j] += scratchcards[i]
    return sum(scratchcards.values())


print(part1())
print(part2())
