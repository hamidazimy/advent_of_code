import sys
import numpy as np

VISUALIZE = ("-v" in sys.argv) or ("--visualize" in sys.argv)

def visualize(reachable=[]):
    if not VISUALIZE:
        return
    D = len(img)
    legend = {ROCK: "█", PLOT: " "}
    str_img = ""
    for i in range(D):
        for j in range(D):
            str_img += legend[img[i % D, j % D]] if (i, j) not in reachable else "░"
        str_img += "\n"
    print(str_img)

inp = [line.strip() for line in sys.stdin]
img = np.array([[c == '#' for c in line] for line in inp], dtype=np.int8)

mid = len(img) // 2
start = (mid, mid)

PLOT = 0
ROCK = 1


def nei(i, r):
    neis = []
    for di, dr in [(-1, 0), (0, -1), (0, 1), (1, 0)]:
        if img[(i + di) % 131, (r + dr) % 131] != ROCK:
            neis.append((i + di, r + dr))
    return neis


def plots_by_steps(n):
    plots = set([start])
    for _ in range(n):
        new_plots = set()
        for i, r in list(plots):
            new_plots.update(nei(i, r))
        plots = new_plots
    return plots


def plots_lazy(n):
    if (n - 65) % (2 * 131) != 0:
        raise ValueError("Lazy count only works for inputs in form of n = i * 2 * 131 + 65.")
    si, sr = start
    reachable_plots = 0
    for i in range(mid - n, mid + n + 1):
        for r in range(mid - n, mid + n + 1):
            distance = abs(i - si) + abs(r - sr)
            if distance % 2 == 1 and distance <= n:
                unreachable = True
                if img[i % 131, r % 131] == PLOT:
                    for di, dr in [(-1, 0), (0, -1), (0, 1), (1, 0)]:
                        if img[(i + di) % 131, (r + dr) % 131] == PLOT:
                            unreachable = False
                            break
                reachable_plots += int(not unreachable)
    return reachable_plots


def part1():
    plots = plots_by_steps(64)
    visualize(plots)
    return len(plots)


def part2():
    """
    The number of reachable plots by steps follows a particular pattern
    for the number of steps in form of n = i * 2 * 131 + 65:
    @ i = 0  =>  n =  65  => number of plots = C(0) = c0
    @ i = 1  =>  n = 327  => number of plots = C(1) = c0 + z * 4 + q * 8
    @ i = 2  =>  n = 589  => number of plots = C(2) = c0 + z * 4 + q * 8 + z * 12 + q * 16
    ...
    n = i * 2 * 131 + 65: => number of plots = C(i) = c0 + z * (4 * (i ^ 2)) + q * (4 * (i ^ 2 + i))

    So, count the number of plots for i = 1,2,3 using plots_lazy function;
    Then calculate c0, z and q from that;
    Then using the formula above, we can calculate the number of plots for the HUGE number of steps.
    """

    if mid != 65:
        sys.stderr.write("The solution for part 2 is heavily dependent on quite a few assumptions " +\
            "about the input and does not work on every arbitrary input.")
        exit(0)

    NUM_STEPS = 26501365

    C = lambda x: plots_lazy(65 + x * 2 * 131)
    Z = lambda x: 4 * (x ** 2)
    Q = lambda x: 4 * (x ** 2 + x)

    c0 = C(0)
    coeff = np.array([[Z(1), Q(1)], [Z(2), Q(2)]])
    ordin = np.array([C(1) - c0, C(2) - c0])
    z, q = np.linalg.solve(coeff, ordin)

    plots = lambda i: int(c0 + z * Z(i) + q * Q(i))

    return plots((NUM_STEPS - 65) // (2 * 131))


print(part1())
print(part2())
