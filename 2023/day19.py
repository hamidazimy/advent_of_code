import sys

if ("-s" in sys.argv) or ("--sample" in sys.argv):
    from io import StringIO
    sample = StringIO("""\
px{a<2006:qkq,m>2090:A,rfg}
pv{a>1716:R,A}
lnx{m>1548:A,A}
rfg{s<537:gd,x>2440:R,A}
qs{s>3448:A,lnx}
qkq{x<1416:A,crn}
crn{x>2662:A,R}
in{s<1351:px,qqz}
qqz{s>2770:qs,m<1801:hdj,R}
gd{a>3333:R,R}
hdj{m>838:A,pv}

{x=787,m=2655,a=1222,s=2876}
{x=1679,m=44,a=2067,s=496}
{x=2036,m=264,a=79,s=2244}
{x=2461,m=1339,a=466,s=291}
{x=2127,m=1623,a=2188,s=1013}
""")
    sys.stdin = sample

inp = sys.stdin.read().strip().split("\n\n")

def parse_part(line: str):
    part = {}
    for cat in line[1:-1].split(','):
        name, rate = cat.split('=')
        part[name] = int(rate)
    return part

parts = [parse_part(line) for line in inp[1].split("\n")]

def parse_workflow(line: str):
    return line.split('{')[1][:-1].split(',')

workflows = {line.split('{')[0]: parse_workflow(line) for line in inp[0].split("\n")}


def calc_rating(part: dict):
    wf = 'in'
    while wf not in ('A', 'R'):
        for rule in workflows[wf]:
            rule = rule.split(':')
            if len(rule) == 1:
                wf = rule[-1]
                break
            elif '>' in rule[0]:
                category, threshold = rule[0].split('>')
                if part[category] > int(threshold):
                    wf = rule[-1]
                    break
            elif '<' in rule[0]:
                category, threshold = rule[0].split('<')
                if part[category] < int(threshold):
                    wf = rule[-1]
                    break
    return sum(part.values()) if wf == 'A' else 0


def calc_accepted(megapart: dict, wf: str='in'):
    total = 0
    if wf == 'R':
        return total
    if wf == 'A':
        total = 1
        for category in megapart:
            rate = megapart[category]
            total *= (rate[1] - rate[0] + 1)
        return total
    for rule in workflows[wf]:
        rule = rule.split(':')
        next_wf = rule[-1]
        condition = rule[0]
        if len(rule) == 1:
            total += calc_accepted(megapart, next_wf)
        elif '>' in condition:
            category, threshold = condition.split('>')
            if megapart[category][0] <= int(threshold) <= megapart[category][1]:
                splitted_megapart = megapart.copy()
                splitted_megapart[category] = (int(threshold) + 1, megapart[category][1])
                total += calc_accepted(splitted_megapart, next_wf)
                megapart[category] = (megapart[category][0], int(threshold))
        elif '<' in condition:
            category, threshold = condition.split('<')
            if megapart[category][0] <= int(threshold) <= megapart[category][1]:
                splitted_megapart = megapart.copy()
                splitted_megapart[category] = (megapart[category][0], int(threshold) - 1)
                total += calc_accepted(splitted_megapart, next_wf)
                megapart[category] = (int(threshold), megapart[category][1])
    return total


def part1():
    return sum([calc_rating(part) for part in parts])


def part2():
    megapart = {'x': (1, 4000), 'm': (1, 4000), 'a': (1, 4000), 's': (1, 4000)}
    return calc_accepted(megapart)


print(part1())
print(part2())
