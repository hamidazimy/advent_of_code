import sys

if ("-s" in sys.argv) or ("--sample" in sys.argv):
    from io import StringIO
    sample = StringIO("""\
rn=1,cm-,qp=3,cm=2,qp-,pc=4,ot=9,ab=5,pc-,pc=6,ot=7
""")
    sys.stdin = sample

inp = sys.stdin.readline().strip().split(',')


def hash(step):
    result = 0
    for i in step:
        result = (result + ord(i)) * 17 % 256
    return result


def part1():
    return sum([hash(step) for step in inp])


def part2():
    boxes = [{} for _ in range(256)]
    for seq in inp:
        if seq[-1] == "-":
            label = seq[:-1]
            box_numer = hash(label)
            if label in boxes[box_numer]:
                boxes[box_numer].pop(label)
        else:
            label, focal_length = seq.split('=')
            box_numer = hash(label)
            # if label in boxes[box_numer]:
            #     boxes[box_numer][label] = int(focal_length)
            # else:
            #     boxes[box_numer][label] = int(focal_length)
            # # That is actually the default behaviour of Python dictionaries to
            # # updata a key if it's in the dictionary and add it to the end if it's not.
            boxes[box_numer][label] = int(focal_length)
    total = 0
    for i, box in enumerate(boxes):
        for j, label in enumerate(box):
            total += (i + 1) * (j + 1) * box[label]
    return total


print(part1())
print(part2())
