import sys
import numpy as np

if ("-s" in sys.argv) or ("--sample" in sys.argv):
    from io import StringIO
    sample = StringIO(r"""
.|...\....
|.-.\.....
.....|-...
........|.
..........
.........\
..../.\\..
.-.-/..|..
.|....-|.\
..//.|....
""")
    sys.stdin = sample
    sys.stdin.readline()

inp = np.array([[ord(c) for c in line.strip()] for line in sys.stdin])
hei, wid = np.shape(inp)


def coord(curr: compile):
    return (int(curr.imag), int(curr.real))


def next(curr: complex, face: complex):
    if inp[coord(curr)] == ord('.'):
        return [(curr + face, face)]
    elif inp[coord(curr)] == ord('\\'):
        if face.imag == 0:
            face_ = face *  1j
            return [(curr + face_, face_)]
        else:
            face_ = face * -1j
            return [(curr + face_, face_)]
    elif inp[coord(curr)] == ord('/'):
        if face.imag == 0:
            face_ = face * -1j
            return [(curr + face_, face_)]
        else:
            face_ = face *  1j
            return [(curr + face_, face_)]
    elif inp[coord(curr)] == ord('|'):
        if face.imag == 0:
            face1 = face *  1j
            face2 = face * -1j
            return [(curr + face1, face1), (curr + face2, face2)]
        else:
            return [(curr + face, face)]
    elif inp[coord(curr)] == ord('-'):
        if face.imag == 0:
            return [(curr + face, face)]
        else:
            face1 = face *  1j
            face2 = face * -1j
            return [(curr + face1, face1), (curr + face2, face2)]


def inside(curr: complex):
    return (0 <= curr.imag < hei) and (0 <= curr.real < wid)


def start_from(beam=(0, 1)):
    energized = np.zeros_like(inp)
    beams = [beam]
    all_beams = set(beams)
    while len(beams) > 0:
        new_beams = []
        for curr, face in beams:
            energized[coord(curr)] = 1
            for c, f in next(curr, face):
                if inside(c) and (c, f) not in all_beams:
                    new_beams.append((c, f))
                    all_beams.add((c, f))
        beams = new_beams
    return np.sum(energized != 0)


def part1():
    return start_from()


def part2():
    all_entries  =  [(0         + i * 1j,  1 ) for i in range(hei)] + \
                    [((wid - 1) + i * 1j, -1 ) for i in range(hei)] + \
                    [(i +         0 * 1j,  1j) for i in range(wid)] + \
                    [(i + (hei - 1) * 1j, -1j) for i in range(wid)]
    return max([start_from(beam) for beam in all_entries])


print(part1())
print(part2())
