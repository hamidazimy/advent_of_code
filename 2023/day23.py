import sys
import numpy as np

if ("-s" in sys.argv) or ("--sample" in sys.argv):
    from io import StringIO
    sample = StringIO("""\
#.#####################
#.......#########...###
#######.#########.#.###
###.....#.>.>.###.#.###
###v#####.#v#.###.#.###
###.>...#.#.#.....#...#
###v###.#.#.#########.#
###...#.#.#.......#...#
#####.#.#.#######.#.###
#.....#.#.#.......#...#
#.#####.#.#.#########v#
#.#...#...#...###...>.#
#.#.#v#######v###.###v#
#...#.>.#...>.>.#.###.#
#####v#.#.###v#.#.###.#
#.....#...#...#.#.#...#
#.#########.###.#.#.###
#...###...#...#...#.###
###.###.#.###v#####v###
#...#...#.#.>.>.#.>.###
#.###.###.#.###.#.#v###
#.....###...###...#...#
#####################.#
""")
    sys.stdin = sample

inp = [line.strip() for line in sys.stdin]
mapp = np.chararray((len(inp), len(inp[0])))
for r, line in enumerate(inp):
    for i, c in enumerate(line):
        mapp[r, i] = c

H, W = np.shape(mapp)

mapp[0, 1] = b'#'
mapp[-1, -2] = b'#'

HERE = 1 + 1j
GOAL = (H - 2) + (W - 2) * 1j

dirs = {1: b'v', 1j: b'>', -1: b'^', -1j: b'<'}


def coords(you: complex):
    return (int(you.real), int(you.imag))


def find_intersections():
    nodes = [HERE]
    for r in range(1, H - 1):
        for i in range(1, W - 1):
            you = r + i * 1j
            if mapp[coords(you)] != b'#':
                nonforest_neighbours = sum([mapp[coords(you + dir)] != b'#' for dir in dirs])
                if nonforest_neighbours > 2:
                    nodes.append(you)
    nodes.append(GOAL)
    return nodes


nodes = find_intersections()
N = len(nodes)


def solve(slippery=True):

    def create_graph():

        def follow_edge(edge_start, moving_dir):
            edge = [edge_start]
            next = edge_start + moving_dir
            while True:
                edge.append(next)
                for dir in dirs:
                    next = edge[-1] + dir
                    if next not in edge and mapp[coords(next)] != b'#':
                        if next in nodes:
                            return next, len(edge)
                        break

        adj_mat = np.zeros((N, N), dtype=np.int32)
        for node in nodes:
            for dir in dirs:
                edge_start = node + dir
                passable = b'.' + (dirs[dir] if slippery else b"v>^<")
                if mapp[coords(edge_start)] in passable:
                    dest, lenght = follow_edge(edge_start, dir)
                    adj_mat[nodes.index(node), nodes.index(dest)] = lenght
        return adj_mat

    adj_mat = create_graph()

    def longest_path(you=0, visited=[]):
        nonlocal adj_mat
        longest_length = 0
        visited = visited[:]
        visited.append(you)
        final_visited = visited[:]
        if you == N - 1:
            return longest_length, visited
        for next in np.flatnonzero(adj_mat[you, :]):
            if next in visited:
                continue
            new_path, new_visited = longest_path(next, visited)
            if new_path + adj_mat[you, next] + 1 > longest_length and (N - 1) in new_visited:
                longest_length = new_path + adj_mat[you, next] + 1
                final_visited = new_visited
        return longest_length, final_visited

    longest_walk, _ = longest_path()
    # To make things easier, I removed the first and the last step, so I have to add 2 to the length
    return longest_walk + 2


def part1():
    return solve()


def part2():
    return solve(slippery=False)


print(part1())
print(part2())
