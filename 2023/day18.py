import sys

if ("-s" in sys.argv) or ("--sample" in sys.argv):
    from io import StringIO
    sample = StringIO("""\
R 6 (#70c710)
D 5 (#0dc571)
L 2 (#5713f0)
D 2 (#d2c081)
R 2 (#59c680)
D 2 (#411b91)
L 5 (#8ceee2)
U 2 (#caa173)
L 1 (#1b58a2)
U 2 (#caa171)
R 2 (#7807d2)
U 3 (#a77fa3)
L 2 (#015232)
U 2 (#7a21e3)
""")
    sys.stdin = sample

VISUALIZE = ("-v" in sys.argv) or ("--visualize" in sys.argv)

if VISUALIZE:
    import numpy as np

def visualize(site):
    if not VISUALIZE:
        return
    str_site = ""
    hei, wid = np.shape(site)
    for i in range(hei):
        for r in range(wid):
            str_site += ".#"[site[i, r]]
        str_site += '\n'
    print(str_site)

inp = [line.strip() for line in sys.stdin]


def parse_input(line, hex=False):
    if hex:
        color = line.split()[2][2:-1]
        return "RDLU"[int(color[-1])], int(color[:-1], 16)
    seg = line.split()
    return seg[0], int(seg[1])


def follow_plan():
    max_X, min_X, max_Y, min_Y = 0, 0, 0, 0
    X, Y = 0, 0
    for dig in inp:
        dir, mtr = parse_input(dig)
        match dir:
            case 'R':
                X += mtr
                max_X = max(max_X, X)
            case 'L':
                X -= mtr
                min_X = min(min_X, X)
            case 'D':
                Y += mtr
                max_Y = max(max_Y, Y)
            case 'U':
                Y -= mtr
                min_Y = min(min_Y, Y)
    site = np.zeros((max_Y - min_Y + 1, max_X - min_X + 1), dtype=np.int8)
    X, Y = -min_X, -min_Y
    for dig in inp:
        dir, mtr = parse_input(dig)
        match dir:
            case 'R':
                site[Y, X:X + mtr + 1] = 1
                X += mtr
            case 'L':
                site[Y, X - mtr:X + 1] = 1
                X -= mtr
            case 'D':
                site[Y:Y + mtr + 1, X] = 1
                Y += mtr
            case 'U':
                site[Y - mtr:Y + 1, X] = 1
                Y -= mtr
    visualize(site)
    start_Y, start_X = 1, np.argwhere(site[1, :])[0][0] + 1
    fill = [(start_Y, start_X)]
    while len(fill) > 0:
        Y, X = fill.pop(0)
        site[Y, X] = 1
        for dy, dx in [(-1, 0), (0, 1), (1, 0), (0, -1)]:
            Y_, X_ = Y + dy, X + dx
            if site[Y_, X_] == 0 and (Y_, X_) not in fill:
                fill.append((Y_, X_))
    visualize(site)
    return np.sum(site)


def solve(part):
    X, Y = 0, 0
    corners = [None] * len(inp)
    boundry_len = 0
    for idx, dig in enumerate(inp):
        dir, mtr = parse_input(dig, hex=(part==2))
        match dir:
            case 'R':
                X += mtr
            case 'L':
                X -= mtr
            case 'D':
                Y += mtr
            case 'U':
                Y -= mtr
        boundry_len += mtr
        corners[idx] = (Y, X)
    return int(picks(corners, boundry_len) + boundry_len)


def shoelace(corners):
    area = 0
    for i in range(len(corners)):
        area += corners[i - 1][0] * corners[i][1] - corners[i - 1][1] * corners[i][0]
    return abs(area / 2)


def picks(corners, boundry_len):
    # A = i + ½b - 1 → i = A - ½b + 1
    area = shoelace(corners)
    return area - boundry_len / 2 + 1


def part1():
    if VISUALIZE:
        return follow_plan() # Brute-force solution!
    return solve(part=1)


def part2():
    return solve(part=2)


print(part1())
print(part2())
