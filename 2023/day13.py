import sys
import numpy as np

if ("-s" in sys.argv) or ("--sample" in sys.argv):# or True:
    from io import StringIO
    sample = StringIO("""\
#.##..##.
..#.##.#.
##......#
##......#
..#.##.#.
..##..##.
#.#.##.#.

#...##..#
#....#..#
..##..###
#####.##.
#####.##.
..##..###
#....#..#
""")
    sys.stdin = sample

inp = [[line for line in pattern.strip().split('\n')] for pattern in sys.stdin.read().split("\n\n")]
patterns = [np.array([[int(c == '#') for c in row] for row in pattern], dtype=np.uint8) for pattern in inp]


def find_mirror(pattern, smudge=0):
    H, W = np.shape(pattern)
    for i in range(1, W):
        l = min(i, W - i)
        h1 = pattern[:, :i][:, ::-1][:, :l]
        h2 = pattern[:, i:][:, :l]
        if np.sum(h1 != h2) == smudge:
            return i
    for j in range(1, H):
        l = min(j, H - j)
        h1 = pattern[:j, :][::-1, :][:l, :]
        h2 = pattern[j:, :][:l, :]
        if np.sum(h1 != h2) == smudge:
            return j * 100


def part1():
    return sum([find_mirror(pattern) for pattern in patterns])


def part2():
    return sum([find_mirror(pattern, smudge=1) for pattern in patterns])


print(part1())
print(part2())
