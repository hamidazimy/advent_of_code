import sys
import re
from functools import lru_cache

if ("-s" in sys.argv) or ("--sample" in sys.argv):
    from io import StringIO
    sample = StringIO("""\
???.### 1,1,3
.??..??...?##. 1,1,3
?#?#?#?#?#?#?#? 1,3,1,6
????.#...#... 4,1,1
????.######..#####. 1,6,5
?###???????? 3,2,1
""")
    sys.stdin = sample

inp = [line.strip().split() for line in sys.stdin]


def to_groups(line):
    return tuple([len(group) for group in re.findall(r"#+", line)])


def can_start_with_damaged(springs, groups):
    chunk, the_rest = springs[:groups[0]], springs[groups[0]:]
    if '.' in chunk or (the_rest and the_rest[0] == '#'):
        return False
    return True


# Lazy memoization!
@lru_cache
def combinations(springs, groups):
    if len(groups) == 0:
        if '#' not in springs:
            return 1
        return 0
    if '?' not in springs:
        if to_groups(springs) == groups:
            return 1
        return 0
    if len(springs) < sum(groups) + len(groups) - 1:
        # Not enough springs remained.
        return 0
    next_is_damaged, next_is_operational = 0, 0
    if can_start_with_damaged(springs, groups):
        next_is_damaged = combinations(springs[groups[0] + 1:], groups[1:])
    if springs[0] != '#': # can_start_with_operational
        next_is_operational = combinations(springs[1:], groups)
    return next_is_damaged + next_is_operational


def part1():
    total = 0
    for springs, groups in inp:
        groups = tuple([int(g) for g in groups.split(',')])
        total += combinations(springs, groups)
    return total


def part2():
    total = 0
    for springs, groups in inp:
        unfolded_groups = tuple([int(g) for g in ','.join([groups] * 5).split(',')])
        unfolded_springs = '?'.join([springs] * 5)
        total += combinations(unfolded_springs, unfolded_groups)
    return total


print(part1())
print(part2())
