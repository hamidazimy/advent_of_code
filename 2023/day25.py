import sys
import numpy as np
import networkx as nx

# I chose the lazy approach and
# used NX's Stoer-Wagner implementation
# instead of implementing it myself!

if ("-s" in sys.argv) or ("--sample" in sys.argv):
    from io import StringIO
    sample = StringIO("""\
jqt: rhn xhk nvd
rsh: frs pzl lsr
xhk: hfx
cmg: qnr nvd lhk bvb
rhn: xhk bvb hfx
bvb: xhk hfx
pzl: lsr hfx nvd
qnr: nvd
ntq: jqt hfx bvb xhk
nvd: lhk
lsr: lhk
rzs: qnr cmg lsr rsh
frs: qnr lhk lsr
""")
    sys.stdin = sample

inp = [line.strip() for line in sys.stdin]

adj = {}

for line in inp:
    key, vals = line.strip().split(": ")
    adj[key] = set(vals.split())

nodes = set(adj.keys())

for node in adj:
    nodes.update(adj[node])

nodes = {node: i for i, node in enumerate(nodes)}

N = len(nodes)
adj_mat = np.zeros((N, N), dtype=np.int8)

for node in nodes:
    for dest in adj.get(node, []):
        adj_mat[nodes[node], nodes[dest]] = 1
        adj_mat[nodes[dest], nodes[node]] = 1

graph = nx.Graph()
graph.add_edges_from(np.argwhere(adj_mat))

cut_value, partition = nx.stoer_wagner(graph)

print(len(partition[0]) * len(partition[1]))
