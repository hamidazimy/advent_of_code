import sys

if ("-s" in sys.argv) or ("--sample" in sys.argv):
    from io import StringIO
    sample = StringIO("""\
0 3 6 9 12 15
1 3 6 10 15 21
10 13 16 21 30 45
""")
    sys.stdin = sample

inp = [[int(val) for val in line.strip().split()] for line in sys.stdin]


def all_zero(seq):
    for val in seq:
        if val != 0:
            return False
    return True


def extrapolate(seq):
    if all_zero(seq):
        return 0
    next_seq = [seq[i] - seq [i - 1] for i in range(1, len(seq))]
    return seq[-1] + extrapolate(next_seq)


def part1():
    return sum([extrapolate(seq) for seq in inp])


def part2():
    return sum([extrapolate(seq[::-1]) for seq in inp])


print(part1())
print(part2())
