import sys

if ("-s" in sys.argv) or ("--sample" in sys.argv):
    from io import StringIO
    sample = StringIO("""\
seeds: 79 14 55 13

seed-to-soil map:
50 98 2
52 50 48

soil-to-fertilizer map:
0 15 37
37 52 2
39 0 15

fertilizer-to-water map:
49 53 8
0 11 42
42 0 7
57 7 4

water-to-light map:
88 18 7
18 25 70

light-to-temperature map:
45 77 23
81 45 19
68 64 13

temperature-to-humidity map:
0 69 1
1 0 69

humidity-to-location map:
60 56 37
56 93 4
""")
    sys.stdin = sample

inp = [line.strip() for line in sys.stdin.read().split("\n\n")]

seeds = [int(x) for x in inp[0].split(": ")[1].split()]
seed_ranges = [(seeds[i], seeds[i + 1]) for i in range(0, len(seeds), 2)]
almanac = [[[int(r) for r in x.split()] for x in t.split("\n")[1:]] for t in inp[1:]]


def seed_to_location(seed):
    def mapping(i, x):
        """
        Maps a number to the next in the chain.
        seed => soil => fertilizer => water => light => temperature => humidity => location

        :param int i: currnet stage in the chain
        :param int x: input number
        :return: output number
        """
        for d, s, r in almanac[i]: # destination start, source start, range length
            if s <= x < s + r:
                return d - s + x
        return x

    result = seed
    for i in range(len(almanac)):
        result = mapping(i, result)
    return result


def seed_range_to_location(start, length):
    def mapping(i, p, l):
        """
        Maps a range to the next range(s) in the chain.
        seed => soil => fertilizer => water => light => temperature => humidity => location

        :param int i: currnet stage in the chain
        :param int p: input range start
        :param int l: input range length
        """
        ranges = []
        for d, s, r in almanac[i]: # destination start, source start, range length
            if s <= p < s + r and s <= p + l <= s + r:
                # The current range is inside the current mapping range
                p_ = p
                l_ = l
                ranges.append((d - s + p_, l_))
                l -= l_
            elif s <= p < s + r and s + r < p + l:
                # The current range starts within the current mapping range and extends beyond it
                p_ = p
                l_ = s + r - p - 1
                ranges.append((d - s + p_, l_))
                l -= l_
                p = s + r
            elif p < s and s <= p + l <= s + r:
                # The current range starts before the current mapping range and ends within it
                p_ = s
                l_ = p + l - s
                ranges.append((d - s + p_, l_))
                l -= l_
            else:
                # The current range starts before the current mapping range and extends beyond it
                # Either I am lucky, or this never happens!
                # because my code works without considering this possibility.
                # Implementing this needs a great amount of modification in the code,
                # since it would create two unmapped ranges, which is not considered here.
                pass
        if l != 0:
            ranges.append((p, l))
        return ranges

    result_ranges = [(start, length)]
    for i in range(len(almanac)):
        new_ranges = []
        for p, l in result_ranges:
            new_ranges += mapping(i, p, l)
        result_ranges = new_ranges
    return result_ranges


def part1():
    return min([seed_to_location(seed) for seed in seeds])


def part2():
    result_ranges = []
    for p, l in seed_ranges:
        result_ranges += seed_range_to_location(p, l)
    # assert sum([r[1] for r in result_ranges]) == sum(([s[1] for s in seed_ranges]))
    return min([r[0] for r in result_ranges])


print(part1())
print(part2())
