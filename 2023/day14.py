import sys
import numpy as np

if ("-s" in sys.argv) or ("--sample" in sys.argv):
    from io import StringIO
    sample = StringIO("""\
O....#....
O.OO#....#
.....##...
OO.#O....O
.O.....O#.
O.#..O.#.#
..O..#O..O
.......O..
#....###..
#OO..#....
""")
    sys.stdin = sample

VISUALIZE = ("-v" in sys.argv) or ("--visualize" in sys.argv)

def visualize(platform):
    if not VISUALIZE:
        return
    from time import sleep
    print("\x1bc")
    print(to_string(platform))
    sleep(.25)

inp = [line.strip() for line in sys.stdin]

code = {'.': 0, '#': 1, 'O': 2}
platform = np.array([[code[c] for c in row] for row in inp], dtype=np.int8)


def to_string(platform):
    legend = '.#@'
    H, W = np.shape(platform)
    str_platform = ""
    for i in range(H):
        for j in range(W):
            str_platform += legend[platform[i, j]]
        str_platform += '\n'
    return str_platform


def calc_load(platform):
    H, W = np.shape(platform)
    total = 0
    for i in range(H):
        total += np.sum(platform[i, :] == code['O']) * (H - i)
    return total


def tilt(platform, dir):
    rots = {'N': 0, 'E': 1, 'S': 2, 'W': 3}
    platform = np.rot90(platform, rots[dir])
    H, W = np.shape(platform)
    for j in range(W):
        i = 0
        while i < H:
            k = i
            while i < H and platform[i, j] != code['#']:
                i += 1
            platform[k:i, j] = np.sort(platform[k:i, j])[::-1]
            while i < H and platform[i, j] == code['#']:
                i += 1
            k = i
    platform = np.rot90(platform, 4 - rots[dir])
    return platform


def cycle(platform):
    for dir in 'NWSE':
        tilt(platform, dir)
        visualize(platform)
    return platform


def part1():
    tilt(platform, dir='N')
    return calc_load(platform)


def part2():
    visualize(platform)
    hashes = []
    end = 1_000_000_000
    i = 0
    while i < end:
        hash = to_string(platform)
        if hash in hashes:
            j = hashes.index(hash)
            i += ((end - i) // (i - j)) * (i - j)
        else:
            hashes.append(hash)
        cycle(platform)
        i += 1
    return calc_load(platform)


print(part1())
print(part2())
