import sys
import numpy as np
import re

if ("-s" in sys.argv) or ("--sample" in sys.argv):
    from io import StringIO
    sample = StringIO("""\
467..114..
...*......
..35..633.
......#...
617*......
.....+.58.
..592.....
......755.
...$.*....
.664.598..
""")
    sys.stdin = sample

inp = [line.strip() for line in sys.stdin]
schematic = np.array([[ord(c) for c in line] for line in inp])


is_digit = lambda c: ord('0') <= c <= ord('9')


def neighbours(y, x):
    for dy in [-1, 0 , 1]:
        for dx in [-1, 0 , 1]:
            yield y + dy, x + dx


def solve_both():
    sum_of_gear_ratios = 0
    H, W = np.shape(schematic)
    schematic_wo_part_numbers = np.copy(schematic)
    symbols = np.argwhere(np.logical_and(schematic != ord('.'), np.logical_xor(ord('0') > schematic, schematic > ord('9'))))
    for y, x in symbols:
        gear_part_numbers = []
        for y_, x_ in neighbours(y, x):
            if is_digit(schematic_wo_part_numbers[y_, x_]):
                part_number = ''
                while x_ > 0 and is_digit(schematic_wo_part_numbers[y_, x_ - 1]):
                    x_ -= 1
                while x_ < W and is_digit(schematic_wo_part_numbers[y_, x_]):
                    part_number = part_number + chr(schematic_wo_part_numbers[y_, x_])
                    schematic_wo_part_numbers[y_, x_] = ord('.')
                    x_ += 1
                if schematic[y, x] == ord('*'):
                    gear_part_numbers.append(int(part_number))
        if len(gear_part_numbers) == 2:
            sum_of_gear_ratios += np.prod(gear_part_numbers)

    for y in range(H):
        for x in range(W):
            if is_digit(schematic_wo_part_numbers[y, x]):
                schematic[y, x] = ord('.')
    str_map = '\n'.join([''.join([chr(c) for c in row]) for row in schematic])
    part_numbers = re.finditer(r"(\d)+", str_map)
    sum_of_part_numbers = sum([int(n.group()) for n in part_numbers])
    return sum_of_part_numbers, sum_of_gear_ratios


sum_of_part_numbers, sum_of_gear_ratios = solve_both()


def part1():
    return sum_of_part_numbers


def part2():
    return sum_of_gear_ratios


print(part1())
print(part2())
