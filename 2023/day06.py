import sys
import math

if ("-s" in sys.argv) or ("--sample" in sys.argv):
    from io import StringIO
    sample = StringIO("""\
Time:      7  15   30
Distance:  9  40  200
""")
    sys.stdin = sample

inp = [line.strip() for line in sys.stdin]
times = [int(x) for x in inp[0].split(":")[1].split()]
dists = [int(x) for x in inp[1].split(":")[1].split()]
time_ = int(inp[0].split(":")[1].replace(' ', ''))
dist_ = int(inp[1].split(":")[1].replace(' ', ''))


def wins(time, dist):
    """DEPRECATED"""
    w = 0
    for i in range(1, time):
        if i * (time - i) > dist:
            w += 1
    return w


def wins_quadratic(time, dist):
    a, b, c = -1, time, -dist
    sqrt_delta = math.sqrt(b ** 2 - 4 * a * c)
    ans1 = (-b + sqrt_delta) / (2 * a)
    ans2 = (-b - sqrt_delta) / (2 * a)
    wins = math.ceil(ans2) - math.floor(ans1) - 1
    return wins


def part1():
    margin_of_error = 1
    for time, dist in zip(times, dists):
        margin_of_error *= wins_quadratic(time, dist)
    return margin_of_error


def part2():
    return wins_quadratic(time_, dist_)


print(part1())
print(part2())
