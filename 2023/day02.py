import sys

if ("-s" in sys.argv) or ("--sample" in sys.argv):
    from io import StringIO
    sample = StringIO("""\
Game 1: 3 blue, 4 red; 1 red, 2 green, 6 blue; 2 green
Game 2: 1 blue, 2 green; 3 green, 4 blue, 1 red; 1 green, 1 blue
Game 3: 8 green, 6 blue, 20 red; 5 blue, 4 red, 13 green; 5 green, 1 red
Game 4: 1 green, 3 red, 6 blue; 3 green, 6 red; 3 green, 15 blue, 14 red
Game 5: 6 red, 1 blue, 3 green; 2 blue, 1 red, 2 green
""")
    sys.stdin = sample

inp = [line.strip() for line in sys.stdin]


def part1():
    def playable(line):
        """Returns the game id if it's playable, 0 otherwise."""
        colors = {"red": 12, "green": 13, "blue": 14}
        game, cubes = line.split(": ")
        game = int(game[5:])
        cubes = cubes.split("; ")
        for reveal in cubes:
            reveal = reveal.split(", ")
            for revealed_cube in reveal:
                num, color = revealed_cube.split(" ")
                if int(num) > colors[color]:
                    return 0
        return int(game)

    return sum([playable(line) for line in inp])


def part2():
    def power(line):
        """Return the power of the minimum set of cubes in a game."""
        colors = {"red": 0, "green": 0, "blue": 0}
        game, cubes = line.split(": ")
        game = int(game[5:])
        cubes = cubes.split("; ")
        for reveal in cubes:
            reveal = reveal.split(", ")
            for revealed_cube in reveal:
                num, color = revealed_cube.split(" ")
                colors[color] = max(colors[color], int(num))
        return colors["red"] * colors["green"] * colors["blue"]

    return sum([power(line) for line in inp])


print(part1())
print(part2())
