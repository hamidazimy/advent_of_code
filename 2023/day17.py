import sys
import numpy as np
import heapq

if ("-s" in sys.argv) or ("--sample" in sys.argv):
    from io import StringIO
    sample = StringIO("""\
2413432311323
3215453535623
3255245654254
3446585845452
4546657867536
1438598798454
4457876987766
3637877979653
4654967986887
4564679986453
1224686865563
2546548887735
4322674655533
""")
    sys.stdin = sample

VISUALIZE = ("-v" in sys.argv) or ("--visualize" in sys.argv)

def visualize(prev, node):
    if not VISUALIZE:
        return
    path = [node[0]]
    while node in prev:
        node = prev[node]
        path.append(node[0])
    str_map = ""
    for i in range(hei):
        for r in range(wid):
            if 1j * i + r in path:
                str_map += f"\x1b[7m{inp[i, r]}\x1b[0m"
            else:
                str_map += f"{inp[i, r]}"
        str_map += '\n'
    print(str_map)

inp = np.array([[int(c) for c in line.strip()] for line in sys.stdin])
hei, wid = np.shape(inp)
goal = (wid - 1) + (hei - 1) * 1j


def coord(here: complex):
    return (int(here.imag), int(here.real))


def inside(here: complex):
    return (0 <= here.imag <= goal.imag) and (0 <= here.real <= goal.real)


def heuristic(here: complex):
    return (goal.real - here.real) + (goal.imag - here.imag)


def a_star(min_str8, max_str8):
    #     f  g  h  init  face  str8
    q = [(0, 0, 0, 0, 0, 1, 0, 1   ),\
         (0, 0, 0, 0, 0, 0, 1, 1   )]
    visited = set()
    prev = {}
    while len(q) > 0:
        _, g_, h_, here_imag, here_real, face_imag, face_real, str8_ = heapq.heappop(q)
        here_ = 1j * here_imag + here_real
        face_ = 1j * face_imag + face_real
        if here_ == goal and str8_ >= min_str8:
            return g_, prev, face_, str8_
        for face in [face_, face_ * 1j, face_ * -1j]:
            next = here_ + face
            if not inside(next):
                continue
            if face == face_:
                str8 = str8_ + 1
                if str8 > max_str8:
                    continue
            else:
                if str8_ < min_str8:
                    continue
                str8 = 1
            heat = inp[coord(next)]
            g = g_ + heat
            h = heuristic(next)
            node = (g + h, g, h, next.imag, next.real, face.imag, face.real, str8)
            if (next, face, str8) not in visited:
                prev[(next, face, str8)] = (here_, face_, str8_)
                heapq.heappush(q, node)
                visited.add((next, face, str8))


def part1():
    heat, prev, face, str8 = a_star(min_str8=0, max_str8=3)
    visualize(prev, (goal, face, str8))
    return heat


def part2():
    heat, prev, face, str8 = a_star(min_str8=4, max_str8=10)
    visualize(prev, (goal, face, str8))
    return heat


print(part1())
print(part2())
