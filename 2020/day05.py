import sys

lines = [line.strip() for line in sys.stdin]

seat_ids = []

for line in lines:
    seat_ids.append(int(line.replace('F', '0').replace('B', '1').replace('L', '0').replace('R', '1'), 2))

def part1(seat_ids=seat_ids):
    return max(seat_ids)

print(part1())

def part2(seat_ids=seat_ids):
    seat_ids = sorted(seat_ids)

    for i in range(len(seat_ids)):
        if seat_ids[i + 1] - seat_ids[i] != 1:
            return seat_ids[i] + 1

print(part2())
