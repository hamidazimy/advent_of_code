import sys

foods = []
all_ingredients = []
all_allergens = []

for line in sys.stdin:
    parts = line.strip().split(" (contains")
    foods.append((parts[0].split(), parts[1][1:-1].split(", ")))
    all_ingredients += foods[-1][0]
    all_allergens += foods[-1][1]

all_ingredients = list(set(all_ingredients))
all_allergens = list(set(all_allergens))

allergen_possibles = {k: set(all_ingredients[:]) for k in all_allergens}

for ingr, alle in foods:
    for a in alle:
        allergen_possibles[a] = allergen_possibles[a].intersection(set(ingr))


def part1():
    all_possible_allergens = set()
    for a in allergen_possibles:
        all_possible_allergens = all_possible_allergens.union(allergen_possibles[a])

    result = 0
    for ingr, alle in foods:
        for i in ingr:
            if i not in all_possible_allergens:
                result += 1
    return result

print(part1())


def part2():
    global allergen_possibles
    allergen_possibles = [(a, allergen_possibles[a]) for a in allergen_possibles]

    allergen_possibles.sort(key=lambda x: len(x[1]))

    def csp_dfs(translations=[]):
        global allergen_possibles
        if len(translations) == len(allergen_possibles):
            return translations
        for c in allergen_possibles[len(translations)][1]:
            if c not in [x[1] for x in translations]:
                result = csp_dfs(translations + [(allergen_possibles[len(translations)][0], c)])
                if result is not None:
                    return result

    allergens_translations = sorted(csp_dfs(), key=lambda x: x[0])

    return ','.join([x[1] for x in allergens_translations])

print(part2())
