import sys

rules = {}

while True:
    rule = sys.stdin.readline()
    if rule == "\n":
        break
    rule = rule.strip().split(": ")
    rules[int(rule[0])] = rule[1].replace('"', '')
messages = [message.strip() for message in sys.stdin.readlines()]

def expand(rule):
    global rules
    if rule in ['a', 'b']:
        return rule
    result = []
    sub_rules = rule.split(" | ")
    for sub_rule in sub_rules:
        parts = sub_rule.split()
        if len(parts) == 1:
            result += expand(rules[int(sub_rule)])
        elif len(parts) == 2:
            one = expand(rules[int(parts[0])])
            two = expand(rules[int(parts[1])])
            for i in one:
                for j in two:
                    result.append(f"{i}{j}")
    return result

def part1(messages=messages, rules=rules):
    expanded_0 = expand(rules[0])
    result = 0
    for m in messages:
        if m in expanded_0:
            result += 1
    return result

print(part1())

# def check_rule(message, rule):
#     global rules
#     if message == rule:
#         return True
#     if rule in ['a', 'b']:
#         return False
#     matched = False
#     sub_rules = rule.split(" | ")
#     for sub_rule in sub_rules:
#         parts = sub_rule.split()
#         sub_matched = False
#         if len(parts) == 1:
#             return check_rule(message, rules[int(parts[0])])
#         elif len(parts) == 2:
#             for i in range(1, len(message) - 2):
#                 sub_matched = sub_matched or (check_rule(message[:i], rules[int(parts[0])]) and check_rule(message[i + 1:], rules[int(parts[1])]))
#                 if sub_matched:
#                     return True
#             if matched:
#                 return True
#     return matched
#
#
# result = 0
#
# for m in messages:
#     print(m)
#     if check_rule(m, rules[0]):
#         result += 1
#     print(result)
# print(result)


min_len_mem = {}

def min_len(rule_num):
    if rule_num in min_len_mem:
        return min_len_mem[rule_num]
    if rules[rule_num] in ['a', 'b']:
        min_len_mem[rule_num] = 1
        return min_len_mem[rule_num]
    sub_rules = rules[rule_num].split(" | ")
    sub_min_length = []
    for sub_rule in sub_rules:
        sub_min_length.append(sum([min_len(int(p)) for p in sub_rule.split()]))
    min_len_mem[rule_num] = min(sub_min_length)
    return min_len_mem[rule_num]

min_len(0)

rules[8] = "42 | 42 8"
rules[11] = "42 31 | 42 11 31"

max_len_mem = {0: float("Inf"), 8: float("Inf"), 11: float("Inf")}

def max_len(rule_num):
    global max_len_mem, rules
    if rule_num in max_len_mem:
        return max_len_mem[rule_num]
    if rules[rule_num] in ['a', 'b']:
        max_len_mem[rule_num] = 1
        return max_len_mem[rule_num]
    sub_rules = rules[rule_num].split(" | ")
    sub_max_length = []
    for sub_rule in sub_rules:
        sub_max_length.append(sum([max_len(int(p)) for p in sub_rule.split()]))
    max_len_mem[rule_num] = max(sub_max_length)
    return max_len_mem[rule_num]

def check_rule(message, rule_num=0):
    global rules
    if len(message) < min_len(rule_num) or max_len(rule_num) < len(message):
        return False
    rule = rules[rule_num]
    matched = False
    sub_rules = rule.split(" | ")
    for sub_rule in sub_rules:
        parts = sub_rule.split()
        if len(parts) == 1:
            if parts[0] in ['a', 'b']:
                matched = matched or (message == parts[0])
            elif max_len(int(parts[0])) >= len(message) >= min_len(int(parts[0])):
                matched = matched or check_rule(message, int(parts[0]))
        elif len(parts) == 2:
            sub_matched = False
            p1, p2 = int(parts[0]), int(parts[1])
            for i in range(min_len(p1), len(message) + 1 - min_len(p2)):
                if rule in [0, 8, 11, 42]:
                    print(message[:i], message[i:])
                sub_matched = sub_matched or (check_rule(message[:i], p1) and check_rule(message[i:], p2))
                if sub_matched:
                    break
            matched = matched or sub_matched
        elif len(parts) == 3:
            sub_matched = False
            p1, p2, p3 = int(parts[0]), int(parts[1]), int(parts[2])
            for i in range(min_len(p1), len(message) + 1 - min_len(p2) - min_len(p3)):
                for j in range(min_len(p1) + min_len(p2), len(message) + 1 - min_len(p3)):
                    sub_matched = sub_matched or (check_rule(message[:i], p1) and check_rule(message[i:j], p2) and check_rule(message[j:], p3))
                    if sub_matched:
                        break
            matched = matched or sub_matched
        if matched:
            break
    return matched

def part2(messages=messages):
    result = 0
    for m in messages:
        if check_rule(m):
            result += 1
    return result

print(part2())
