import sys

lines = [line for line in sys.stdin]

indexes = {}

for line in lines:
    indexes[line.split(" bags contain ")[0]] = len(indexes)

indexes["other bags"] = len(indexes)

def part1(lines=lines, indexes=indexes):
    is_inside = {k: [] for k in indexes}

    for line in lines:
        bag, contain = line[:-2].split(" bags contain ")
        contain = contain.split(", ")
        for c in contain:
            bag_inside = ' '.join(c.split()[1:3])
            is_inside[bag_inside].append(bag)

    all = set()
    all.add("shiny gold")

    while True:
        set_prev_len = len(all)
        for b in list(all):
            for i in is_inside[b]:
                all.add(i)
        if len(all) == set_prev_len:
            break

    return len(all) - 1

print(part1())

def part2(lines=lines, indexes=indexes):
    contains = {k: {} for k in indexes}

    for line in lines:
        bag, contain = line[:-2].split(" bags contain ")
        contain = contain.split(", ")
        for c in contain:
            bag_inside = ' '.join(c.split()[1:3])
            contains[bag][bag_inside] = int(c.split()[0]) if c.split()[0] != "no" else 0

    contains_num = {}
    contains_num["other bags"] = 0

    while True:
        if "shiny gold" in contains_num:
            return contains_num["shiny gold"]
        for b in contains:
            num = 0
            for i in contains[b]:
                if i not in contains_num:
                    num = None
                    break
                num += (contains_num[i] + 1) * contains[b][i]
            if num is not None:
                contains_num[b] = num

print(part2())
