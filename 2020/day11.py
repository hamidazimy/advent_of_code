import sys
from time import sleep, time

tick = time()

map = [f".{line.strip()}." for line in sys.stdin]
map = ['.' * len(map[0])] + map + ['.' * len(map[0])]

def print_map(map):
    H, W = len(map), len(map[0])
    clr = {".": "\x1b[30m", "L": "\x1b[31m", "#": "\x1b[37m"}
    img = "\n"
    for i in range(H):
        for j in range(W):
            img += f"{clr[map[i][j]]}█"
        img += "\x1b[0m\n"
    print(img, end="")

# print_map(map)

def adjs_occup(map, i, j):
    H, W = len(map), len(map[0])
    result = 0
    for di in [-1, 0, 1]:
        for dj in [-1, 0, 1]:
            if di == 0 and dj == 0:
                continue
            result += int(map[i + di][j + dj] == '#')
    return result

def part1(map=map):
    H, W = len(map), len(map[0])
    steps = 0
    changed = True
    while changed:
        steps += 1
        changed = False
        new_map = ['.' * W]
        for i in range(1, H - 1):
            new_map += ['.']
            for j in range(1, W - 1):
                occ = adjs_occup(map, i, j)
                if map[i][j] == '.':
                    new_map[i] += '.'
                elif map[i][j] == 'L' and occ == 0:
                    new_map[i] += '#'
                    changed = True
                elif map[i][j] == '#' and occ >= 4:
                    new_map[i] += 'L'
                    changed = True
                else:
                    new_map[i] += map[i][j]
            new_map[i] += '.'
        new_map += ['.' * W]
        map = new_map
        # print_map(map)
        # sleep(.1)
    return sum([row.count('#') for row in map])

print(part1())


def valid(map, i, j):
    H, W = len(map), len(map[0])
    if -1 < i < H and -1 < j < W:
        return True
    return False

def sees_occup(map, i, j):
    H, W = len(map), len(map[0])
    result = 0
    for di in [-1, 0, 1]:
        for dj in [-1, 0, 1]:
            if di == 0 and dj == 0:
                continue
            i_, j_ = i + di, j + dj
            while valid(map, i_, j_):
                if map[i_][j_] == '#':
                    result += 1
                    break
                elif map[i_][j_] == 'L':
                    break
                i_, j_ = i_ + di, j_ + dj
    return result

def part2(map=map):
    H, W = len(map), len(map[0])
    steps = 0
    changed = True
    while changed:
        steps += 1
        changed = False
        new_map = ['.' * W]
        for i in range(1, H - 1):
            new_map += ['.']
            for j in range(1, W - 1):
                occ = sees_occup(map, i, j)
                if map[i][j] == '.':
                    new_map[i] += '.'
                elif map[i][j] == 'L' and occ == 0:
                    new_map[i] += '#'
                    changed = True
                elif map[i][j] == '#' and occ >= 5:
                    new_map[i] += 'L'
                    changed = True
                else:
                    new_map[i] += map[i][j]
            new_map[i] += '.'
        new_map += ['.' * W]
        map = new_map
        # print_map(map)
        # sleep(.1)
    return sum([row.count('#') for row in map])

print(part2())

print(time() - tick)
