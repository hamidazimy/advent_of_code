import sys

nums = [int(line) for line in sys.stdin]

def is_valid(i, nums=nums):
    flag = False
    for j in range(i - 25, i - 1):
        for k in range(j + 1, i):
            if nums[i] == nums[j] + nums[k]:
                flag = True
                break
        if flag:
            break
    if not flag:
        return nums[i], i


def part1(nums=nums):
    for i in range(25, len(nums)):
        result = is_valid(i)
        if result is not None:
            return result[0]

print(part1())

def part2(nums=nums):
    for i in range(25, len(nums)):
        result = is_valid(i)
        if result is not None:
            num, ind = result

    for p in range(0, ind - 2):
        for q in range(p + 2, ind):
            if sum(nums[p:q]) == num:
                return min(nums[p:q]) + max(nums[p:q])

print(part2())
