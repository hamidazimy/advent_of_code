import sys
from scanf import scanf

lines = [line for line in sys.stdin]


def part1(lines=lines):
    mem = {}
    mask = "X" * 36

    for line in lines:
        m = scanf("mask = %s", line)
        if m is not None:
            mask = m[0]
            continue
        address, value = scanf("mem[%d] = %d", line)

        value = value & int(mask.replace('X', '1'), 2)
        value = value | int(mask.replace('X', '0'), 2)

        mem[address] = value

    return sum(list(mem.values()))

print(part1())


def mask_address(address, mask):
    return [address[i] if mask[i] == '0' else mask[i] for i in range(len(mask))]

def produce_all(mask):
    num_floats = mask.count('X')
    for i in range(2 ** num_floats):
        addr = mask[:]
        comb = ("0" * num_floats + f"{i:b}")[-num_floats:]
        k = 0
        for j in range(len(addr)):
            if addr[j] == 'X':
                addr[j] = comb[k]
                k += 1
        yield addr

def part2(lines=lines):
    mem = {}
    mask = ['X'] * 36

    for line in lines:
        m = scanf("mask = %s", line)
        if m is not None:
            mask = list(m[0])
            continue
        address, value = scanf("mem[%d] = %d", line)
        address = list(f"{address:036b}")
        masked_address = mask_address(address, mask)
        for i in produce_all(masked_address):
            mem[''.join(i)] = value

    return sum(list(mem.values()))

print(part2())
