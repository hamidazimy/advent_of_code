import sys
inp = [int(i) for i in sys.stdin.readline().split(',')]

def part1(inp=inp):
    for i in range(2020 - len(inp)):
        last = inp[-1]
        age = 0
        for j in range(1, len(inp)):
            if inp[len(inp) - 1 - j] == last:
                age = j
                break
        inp.append(age)

    return inp[-1]

print(part1())


def part2(inp=inp):
    history = {inp[x]: x for x in range(len(inp) - 1)}
    lastnum = inp[-1]

    for round in range(len(inp), 30_000_000):
        current = round - history.get(lastnum, round - 1) - 1
        history[lastnum] = round - 1
        lastnum = current

    return current

print(part2())
