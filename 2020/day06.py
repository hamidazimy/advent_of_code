import sys

lines = [line for line in sys.stdin] + ["\n"]

def part1(lines=lines):
    result = 0

    group_answers = set()
    for l in lines:
        if l == "\n":
            result += len(group_answers)
            group_answers = set()
        else:
            group_answers = group_answers.union(set(list(l.strip())))

    return result

print(part1())

def part2(lines=lines):
    result = 0

    group_answers = set(list('abcdefghijklmnopqrstuvwxyz'))
    for l in lines:
        if l == "\n":
            result += len(group_answers)
            group_answers = set(list('abcdefghijklmnopqrstuvwxyz'))
        else:
            group_answers = group_answers.intersection(set(list(l.strip())))

    return result

print(part2())
