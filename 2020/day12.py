import sys

lines = [line.strip() for line in sys.stdin]

def part1(lines=lines):
    ship = 0
    face = 1
    turn = {'L': -1j, 'R': 1j}
    dist = {'N': -1j, 'E': 1, 'W': -1, 'S': 1j}

    for line in lines:
        c, n = line[0], int(line[1:])
        if c == 'F':
            ship += face * n
        elif c in turn:
            face *= turn[c] ** (n // 90)
        elif c in dist:
            ship += dist[c] * n

    return int(abs(ship.real) + abs(ship.imag))

print(part1())

def part2(lines=lines):
    ship = 0
    wpnt = 10 - 1j
    turn = {'L': -1j, 'R': 1j}
    dist = {'N': -1j, 'E': 1, 'W': -1, 'S': 1j}

    for line in lines:
        c, n = line[0], int(line[1:])
        if c == 'F':
            ship += wpnt * n
        elif c in turn:
            wpnt *= turn[c] ** (n // 90)
        elif c in dist:
            wpnt += dist[c] * n

    return int(abs(ship.real) + abs(ship.imag))

print(part2())
