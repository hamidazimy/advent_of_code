from __future__ import annotations
from dataclasses import dataclass
import sys

cups = [int(x) for x in sys.stdin.readline().strip()]

def argmax(l):
    return l.index(max(l))

def part1(cups=cups):
    for i in range(100):
        current_cup = cups[0]
        picked_up = cups[1:4]
        unpicked = cups[4:]
        dest = argmax([(x - current_cup) % 10 for x in unpicked])
        cups = unpicked[:dest + 1] + picked_up + unpicked[dest + 1:] + [current_cup]
    one = cups.index(1)
    return ''.join([str(x) for x in cups[one + 1:] + cups[:one]])

print(part1())


@dataclass
class Node:
    value : int
    prev : Node
    next : Node
    dest : Node


def init(cups=cups):
    B = min(cups)
    M = max(cups)
    prev = None

    initial_cups = {}
    current = None

    for n in cups:
        node = Node(n, prev, None, initial_cups.get(n - 1, None))
        if prev is not None:
            prev.next = node
        prev = node

        initial_cups[n] = node
        if current is None:
            current = node

    temp = current

    while temp.next is not None:
        temp.dest = initial_cups.get(temp.value - 1, None)
        temp = temp.next

    prev = Node(M + 1, temp, None, initial_cups[M])
    temp.next = prev

    for k in range(M + 2, 1_000_000 + 1):
        node = Node(k, prev, None, prev)
        if prev is not None:
            prev.next = node
        prev = node

    initial_cups[B].dest = node
    current.prev = node
    node.next = current

    return initial_cups, current


def play(current):
    picked = [current.next, current.next.next, current.next.next.next]
    dest = current.dest
    while dest in picked:
        dest = dest.dest
    current.next = picked[-1].next
    picked[-1].next.prev = current
    dest_next = dest.next
    dest.next = picked[0]
    picked[0].prev = dest
    picked[-1].next = dest_next
    dest_next.prev = picked[-1]
    return current.next


def part2():
    initial_cups, current = init()
    for i in range(10_000_000):
        current = play(current)
    return initial_cups[1].next.value * initial_cups[1].next.next.value

print(part2())
