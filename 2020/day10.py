import sys

adapters = sorted([int(line.strip()) for line in sys.stdin])
target = adapters[-1] + 3
adapters = [0] + adapters + [target]

def dfs(seq=(0,), adapters=adapters):
    if seq[-1] == target:
        return seq
    ind = adapters.index(seq[-1])
    for i in range(ind + 1, ind + 4):
        if adapters[i] - seq[-1] < 4:
            result = dfs(tuple(list(seq) + [adapters[i]]))
            if result is not None:
                return result
        else:
            break

def part1():
    seq = dfs()
    steps = [seq[i] - seq[i - 1] for i in range(1, len(seq))]
    return steps.count(1) * steps.count(3)

print(part1())


from functools import lru_cache

def memoize(func, /):
    return lru_cache(maxsize=None)(func)

@memoize
def ways(index):
    global adapters
    if index == 0:
        return 1
    result = 0
    for i in range(max(0, index - 3), index):
        if adapters[index] - adapters[i] < 4:
            result += ways(i)
    return result

def part2():
    return ways(len(adapters) - 1)

print(part2())
