import sys
from scanf import scanf
import numpy as np

tiles = {}

last_tile_number = 0
number_of_tiles = 0
while True:
    tile_header = sys.stdin.readline()
    if tile_header == '':
        break
    tile_num, = scanf("Tile %d:", tile_header.strip())
    tile = []
    while True:
        line = sys.stdin.readline()
        if line == "\n" or line == '':
            break
        tile.append([0 if c == '.' else 1 for c in line.strip()])
    tiles[tile_num] = np.array(tile)
    number_of_tiles += 1
    last_tile_number = tile_num

tH, tW = np.shape(tiles[last_tile_number])
pH, pW = int(number_of_tiles ** .5), int(number_of_tiles ** .5)

def visualize(tile, orientation=0, show=False):
    if type(tile) == int:
        tile = tiles[tile]
    tile = orient(tile, orientation)
    H, W = np.shape(tile)
    result = ""
    for i in range(H):
        for j in range(W):
            result += " █"[tile[i, j]] * 2
        result += "\n"
    if show:
        print(result)
    return(result)

def orient(tile, orientation):
    if orientation < 0 or 7 < orientation:
        raise ValueError(f"Invalid orientation: {orientation}")
    result = tile.copy()
    if orientation // 4 == 1:
        result = np.fliplr(result)
    for i in range(orientation % 4):
        result = np.rot90(result)
    return result

def borders(tile_num, orientation=0):
    global tiles
    tile = orient(tiles[tile_num], orientation)
    t = int(''.join([str(x) for x in tile[ 0, :]]), 2)
    l = int(''.join([str(x) for x in tile[:,  0]]), 2)
    r = int(''.join([str(x) for x in tile[:, -1]]), 2)
    b = int(''.join([str(x) for x in tile[-1, :]]), 2)
    return (t, l, r, b)

pic = np.zeros((2 * pH + 1, 2 * pW + 1), dtype=np.int32)
ori = -1 * np.ones((2 * pH + 1, 2 * pW + 1), dtype=np.int32)

used = set()

side = [[None, 2, 1], [3], [0]]
dirs = [(-1, 0), (0, -1), (0, 1), (1, 0)]

def make_pic(i=pH, j=pW):
    global tiles, pic, ori, used, side, dirs
    if pic[i, j] != 0:
        return
    restrictions = {}
    for di, dj in dirs:
        if pic[i + di, j + dj] in [0, -1]:
            continue
        restrictions[side[di][dj]] = borders(pic[i + di, j + dj], ori[i + di, j + dj])[side[-di][-dj]]

    candidates = []
    for t in tiles:
        if t not in used:
            for o in range(8):
                tile_borders = borders(t, o)
                is_candidate = True
                for s in restrictions:
                    if tile_borders[s] != restrictions[s]:
                        is_candidate = False
                        break
                if is_candidate:
                    candidates.append((t, o))
    if len(candidates) == 0:
        pic[i, j] = -1
    else:
        pic[i, j] = candidates[0][0]
        ori[i, j] = candidates[0][1]
        used.add(pic[i, j])
        for di, dj in dirs:
            make_pic(i + di, j + dj)

def part1():
    global tiles, pic, ori, used, side, dirs
    make_pic()

    ys = np.where(np.sum(pic, axis=1) > 0)[0][0]
    xs = np.where(np.sum(pic, axis=0) > 0)[0][0]

    pic = pic[ys:ys + pH, xs:xs + pW]
    ori = ori[ys:ys + pH, xs:xs + pW]

    return int(pic[0, 0]) * int(pic[0, -1]) * int(pic[-1, 0]) * int(pic[-1, -1])

print(part1())


def part2():
    global tiles, pH, pW, tH, tW
    tH, tW = tH - 2, tW - 2

    full_map = np.zeros((pH * tH, pW * tW), dtype=np.uint8)

    for i in range(pH):
        for j in range(pW):
            full_map[i * tH:i * tH + tH, j * tW:j * tW + tW] = orient(tiles[pic[i, j]], ori[i, j])[1:-1, 1:-1]

    # visualize(full_map, show=True)

    monster = [ "                  # ",
                "#    ##    ##    ###",
                " #  #  #  #  #  #   " ]

    monster = np.array([[1 if c == "#" else 0 for c in line] for line in monster], dtype=np.uint8)
    mH, mW = np.shape(monster)

    H, W = np.shape(full_map)

    max_monsters = 0

    for o in range(8):
        map = orient(full_map, o)
        monsters = 0
        for i in range(H - mH):
            for j in range(W - mW):
                if np.sum(monster > map[i:i + mH, j:j + mW]) == 0:
                    monsters += 1
        max_monsters = max(monsters, max_monsters)

    return int(np.sum(full_map) - np.sum(monster) * max_monsters)

print(part2())
