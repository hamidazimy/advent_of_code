import sys

p1_deck = []
p2_deck = []


line = sys.stdin.readline()
for line in sys.stdin:
    if line == "\n":
        break
    p1_deck.append(int(line))
line = sys.stdin.readline()
for line in sys.stdin.readlines():
    p2_deck.append(int(line))

def play1(p1_deck, p2_deck):
    while len(p1_deck) * len(p2_deck) != 0:
        p1_top = p1_deck[0]
        p2_top = p2_deck[0]
        p1_deck = p1_deck[1:]
        p2_deck = p2_deck[1:]
        if p1_top > p2_top:
            p1_deck += [p1_top, p2_top]
        else:
            p2_deck += [p2_top, p1_top]
    return p1_deck, p2_deck

def part1(p1_deck=p1_deck, p2_deck=p2_deck):
    p1_deck, p2_deck = play1(p1_deck, p2_deck)
    winner_deck = p1_deck if len(p2_deck) == 0 else p2_deck

    result = 0
    for i in range(len(winner_deck)):
        result += (len(winner_deck) - i) * winner_deck[i]
    return result

print(part1())


def play2(p1_deck, p2_deck):
    prev_rounds = set()
    while len(p1_deck) * len(p2_deck) != 0:
        round = f"{','.join([str(x) for x in p1_deck])}#{','.join([str(x) for x in p2_deck])}"
        if round in prev_rounds:
            return p1_deck, []
        prev_rounds.add(round)

        p1_top = p1_deck[0]
        p2_top = p2_deck[0]
        p1_deck = p1_deck[1:]
        p2_deck = p2_deck[1:]

        if len(p1_deck) >= p1_top and len(p2_deck) >= p2_top:
            p1_sub_deck, p2_sub_deck = play2(p1_deck[:p1_top], p2_deck[:p2_top])
            if len(p1_sub_deck) > 0:
                p1_deck += [p1_top, p2_top]
            else:
                p2_deck += [p2_top, p1_top]
        else:
            if p1_top > p2_top:
                p1_deck += [p1_top, p2_top]
            else:
                p2_deck += [p2_top, p1_top]

    return p1_deck, p2_deck

def part2(p1_deck=p1_deck, p2_deck=p2_deck):
    p1_deck, p2_deck = play2(p1_deck, p2_deck)
    winner_deck = p1_deck if len(p2_deck) == 0 else p2_deck

    result = 0
    for i in range(len(winner_deck)):
        result += (len(winner_deck) - i) * winner_deck[i]
    return result

print(part2())
