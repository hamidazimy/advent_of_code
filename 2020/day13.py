import sys

departure = int(sys.stdin.readline())
buses = sys.stdin.readline().strip().split(',')

def part1(buses=buses, departure=departure):
    buses = [int(b) for b in buses if b != 'x']
    min_wait = float("Inf")
    min_bus = None

    for bus in buses:
        wait = bus - departure % bus
        if wait < min_wait:
            min_wait = wait
            min_bus = bus

    return min_bus * min_wait

print(part1())


def part2(buses=buses):
    # create pairs of (divisor, remainder) for every available bus
    buses = sorted([(int(buses[i]), (int(buses[i]) - i) % int(buses[i]))
        for i in range(len(buses)) if buses[i] != 'x'], key=lambda x: x[0], reverse=True)

    result = 0
    increment = 1

    for bus in buses:
        while result % bus[0] != bus[1]:
            result += increment
        increment *= bus[0]

    return result

print(part2())


def chinese_remainder(pairs):
    from functools import reduce
    sum = 0
    prd = reduce(lambda x, y: x * y, [p[0] for p in pairs])
    for n_i, a_i in pairs:
        p = prd // n_i
        sum += a_i * mul_inv(p, n_i) * p
    return sum % prd

def mul_inv(a, b):
    if b == 1:
        return 1
    b0 = b
    x0, x1 = 0, 1
    while a > 1:
        q = a // b
        a, b = b, a % b
        x0, x1 = x1 - q * x0, x0
    return x1 % b0

def part2_with_CRT(buses=buses):
    buses = sorted([(int(buses[i]), (int(buses[i]) - i) % int(buses[i]))
        for i in range(len(buses)) if buses[i] != 'x'], key=lambda x: x[0], reverse=True)
    return chinese_remainder(buses)

# print(part2_with_CRT())
