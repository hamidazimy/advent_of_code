import sys

inp = [int(line) for line in sys.stdin]

def part1():
    for i in inp:
        for j in inp:
            if i + j == 2020:
                return i * j

print(part1())

def part2():
    for i in inp:
        for j in inp:
            for k in inp:
                if i + j + k == 2020:
                    return i * j * k

print(part2())
