import sys
import numpy as np

map = [[1 if c == "#" else 0 for c in line.strip()] for line in sys.stdin]


def part1(map=map):
    C = 6
    D, H, W = C * 2 + 3, C * 2 + len(map), C * 2 + len(map[0])

    map_3d = np.zeros((D, H, W), dtype=np.uint8)
    map_3d[C + 1, C:-C, C:-C] = np.array(map)

    for round in range(C):
        new_map_3d = np.zeros_like(map_3d)
        for z in range(C - round, C + round + 3):
            for y in range(H):
                for x in range(W):
                    neis = np.sum(map_3d[z - 1:z + 2, max(0, y - 1):min(H, y + 2), max(0, x - 1):min(W, x + 2)]) - map_3d[z, y, x]
                    if map_3d[z, y, x] == 1:
                        if neis in [2, 3]:
                            new_map_3d[z, y, x] = 1
                        else:
                            new_map_3d[z, y, x] = 0
                    else:
                        if neis == 3:
                            new_map_3d[z, y, x] = 1
                        else:
                            new_map_3d[z, y, x] = 0
        map_3d = new_map_3d

    return np.sum(map_3d)

print(part1())


def part2(map=map):
    C = 6
    T, D, H, W = C * 2 + 3, C * 2 + 3, C * 2 + len(map), C * 2 + len(map[0])

    map_4d = np.zeros((T, D, H, W), dtype=np.uint8)
    map_4d[C + 1, C + 1, C:-C, C:-C] = np.array(map)

    for round in range(C):
        new_map_4d = np.zeros_like(map_4d)
        for w in range(C - round, C + round + 3):
            for z in range(C - round, C + round + 3):
                for y in range(H):
                    for x in range(W):
                        neis = np.sum(map_4d[w - 1:w + 2, z - 1:z + 2, max(0, y - 1):min(H, y + 2), max(0, x - 1):min(W, x + 2)]) - map_4d[w, z, y, x]
                        if map_4d[w, z, y, x] == 1:
                            if neis in [2, 3]:
                                new_map_4d[w, z, y, x] = 1
                            else:
                                new_map_4d[w, z, y, x] = 0
                        else:
                            if neis == 3:
                                new_map_4d[w, z, y, x] = 1
                            else:
                                new_map_4d[w, z, y, x] = 0
        map_4d = new_map_4d

    return np.sum(map_4d)

print(part2())
