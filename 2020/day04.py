import sys

lines = [line for line in sys.stdin] + ["\n"]

mandatory_fields = ["byr", "iyr", "eyr", "hgt", "hcl", "ecl", "pid"]

def part1(lines):
    valids = 0

    passport = {}
    for line in lines:
        if line == "\n":
            # print(passport)
            is_valid = 1
            for field in mandatory_fields:
                if passport.get(field, None) is None:
                    # print(f"MISSING {field}")
                    is_valid = 0
                    break
            valids += is_valid
            # if is_valid == 1:
            #     print("VALID PASSPORT")
            passport = {}
        else:
            for pair in line.strip().split():
                key, val = pair.split(':')
                passport[key] = val

    return valids

print(part1(lines))


def is_valid_field(key, val):
    try:
        if key == "byr":
            if 1920 <= int(val) <= 2002:
                return True
        elif key == "iyr":
            if 2010 <= int(val) <= 2020:
                return True
        elif key == "eyr":
            if 2020 <= int(val) <= 2030:
                return True
        elif key == "hgt":
            if (val[-2:] == "cm" and 150 <= int(val[:-2]) <= 193) or (val[-2:] == "in" and 59 <= int(val[:-2]) <= 76):
                return True
        elif key == "hcl":
            if len(val) == 7 and val[0] == '#' and int(val[1:], 16) > -1:
                return True
        elif key == "ecl":
            if val in ["amb", "blu", "brn", "gry", "grn", "hzl", "oth"]:
                return True
        elif key == "pid":
            if len(val) == 9 and int(val) > -1:
                return True
    except:
        return False
    return False

def part2(lines):
    valids = 0

    passport = {}
    for line in lines:
        if line == "\n":
            # print(passport)
            is_valid = 1
            for field in mandatory_fields:
                if passport.get(field, None) is None or not is_valid_field(field, passport[field]):
                    # print(f"MISSING OR INVALID VALUE FOR {field} : {passport.get(field, None)}")
                    is_valid = 0
                    break
            valids += is_valid
            # if is_valid == 1:
            #     print("VALID PASSPORT")
            passport = {}
        else:
            for pair in line.strip().split():
                key, val = pair.split(':')
                passport[key] = val

    return valids

print(part2(lines))
