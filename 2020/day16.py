import sys
from scanf import scanf

rules = {}
all_rules = []

for line in sys.stdin:
    if line == "\n":
        break
    rule = scanf("%d-%d or %d-%d", line)
    name = line.split(":")[0]
    rules[name] = list(range(rule[0], rule[1] + 1)) + list(range(rule[2], rule[3] + 1))
    all_rules += rules[name]

all_rules = set(all_rules)

sys.stdin.readline()
my_ticket = [int(x) for x in sys.stdin.readline().strip().split(',')]
sys.stdin.readline()
sys.stdin.readline()

tickets = []
for line in sys.stdin:
    tickets.append([int(x) for x in line.strip().split(',')])


def part1(tickets=tickets):
    result = 0
    for ticket in tickets:
        for n in ticket:
            if n not in all_rules:
                result += n
    return result

print(part1())


def csp_dfs(cols, constraints):
    if len(cols) == len(constraints):
        return cols
    for c in constraints[len(cols)][1]:
        if c not in [x[1] for x in cols]:
            result = csp_dfs(cols + [(constraints[len(cols)][0], c)], constraints)
            if result is not None:
                return result

def part2(tickets=tickets, my_ticket=my_ticket):
    valid_tickets = []
    for ticket in tickets:
        is_valid = True
        for n in ticket:
            if n not in all_rules:
                is_valid = False
                break
        if is_valid:
            valid_tickets.append(ticket)
    tickets = valid_tickets

    constraints = []
    for r in rules:
        possible_cols = []
        for i in range(len(my_ticket)):
            is_possible = True
            for t in tickets:
                if t[i] not in rules[r]:
                    is_possible = False
                    break
            if is_possible:
                possible_cols.append(i)
        constraints.append((r, possible_cols))
    constraints.sort(key=lambda x: len(x[1]))

    result = 1
    order = csp_dfs([], constraints)
    for field in order:
        if "departure" in field[0]:
            result *= my_ticket[field[1]]
    return result

print(part2())
