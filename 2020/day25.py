import sys

modulus = 20201227

key_c = int(sys.stdin.readline())
key_d = int(sys.stdin.readline())

subject_num_c = 7
loop_size_c = 1
while subject_num_c != key_c:
    loop_size_c += 1
    subject_num_c = subject_num_c * 7 % modulus

subject_num_d = 7
loop_size_d = 1
while subject_num_d != key_d:
    loop_size_d += 1
    subject_num_d = subject_num_d * 7 % modulus

print(pow(key_c, loop_size_d, modulus))
# print(pow(key_d, loop_size_c, modulus))
