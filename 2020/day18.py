import sys
from itertools import takewhile

lines = [line.strip() for line in sys.stdin]

def process1(line):
    line = line.replace(' ', '')
    p = line.find('(')
    while p != -1:
        q = p
        m = 0
        inline = ""
        while True:
            q += 1
            if line[q] == ')':
                if m == 0:
                    break
                else:
                    m -= 1
            elif line[q] == '(':
                m += 1
            inline += line[q]
        line = line[:p] + str(process1(inline)) + line[q + 1:]
        p = line.find('(')
    while True:
        try:
            result = int(line)
            return result
        except ValueError as E:
            pass
        a = ''.join(takewhile(str.isdigit, line))
        op = line[len(a)]
        b = ''.join(takewhile(str.isdigit, line[len(a) + 1:]))
        line = str(int(a) * int(b) if op == "*" else int(a) + int(b)) + line[len(a) + 1 + len(b):]

def part1():
    return sum([process1(line) for line in lines])

print(part1())


def process2(line):
    line = line.replace(' ', '')
    p = line.find('(')
    while p != -1:
        q = p
        n = 0
        inline = ""
        while True:
            q += 1
            if line[q] == ')':
                if n == 0:
                    break
                else:
                    n -= 1
            elif line[q] == '(':
                n += 1
            inline += line[q]
        line = line[:p] + str(process2(inline)) + line[q + 1:]
        p = line.find('(')
    s = line.find('+')
    while s != -1:
        a = ''.join(takewhile(str.isdigit, line[s - 1::-1]))[::-1]
        b = ''.join(takewhile(str.isdigit, line[s + 1:]))
        line = line[:s - len(a)] + str(int(a) + int(b)) + line[s + len(b) + 1:]
        s = line.find('+')
    while True:
        try:
            result = int(line)
            return result
        except ValueError as E:
            pass
        a = ''.join(takewhile(str.isdigit, line))
        op = line[len(a)] # should be '*'
        b = ''.join(takewhile(str.isdigit, line[len(a) + 1:]))
        line = str(int(a) * int(b)) + line[len(a) + 1 + len(b):]

def part2():
    return sum([process2(line) for line in lines])

print(part2())
