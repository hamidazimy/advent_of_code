import sys
lines = [line.strip() for line in sys.stdin]

moves = {
    'ne' : lambda p, q: (p + 1, q    ),
    'nw' : lambda p, q: (p    , q + 1),
     'e' : lambda p, q: (p + 1, q - 1),
     'w' : lambda p, q: (p - 1, q + 1),
    'se' : lambda p, q: (p    , q - 1),
    'sw' : lambda p, q: (p - 1, q    )
}

def directions(line):
    i = 0
    while i < len(line):
        if line[i] in ['e', 'w']:
            yield line[i]
            i += 1
        else:
            yield line[i:i + 2]
            i += 2

def part1(lines=lines):
    blax = set()

    for line in lines:
        p, q = 0, 0
        for d in directions(line):
            p, q = moves[d](p, q)
        if (p, q) in blax:
            blax.remove((p, q))
        else:
            blax.add((p, q))

    return blax

blax = part1()
print(len(blax))


def part2(blax=blax):
    ps = [x[0] for x in list(blax)]
    qs = [x[1] for x in list(blax)]

    minp = min(ps) - 2
    minq = min(qs) - 2
    maxp = max(ps) + 3
    maxq = max(qs) + 3

    for r in range(100):
        new_blax = set()
        for p in range(minp, maxp):
            for q in range(minq, maxq):
                numb_of_blk_neis = sum([int(moves[d](p, q) in blax) for d in moves])
                if (p, q) in blax:
                    if 0 < numb_of_blk_neis < 3:
                        new_blax.add((p, q))
                else:
                    if numb_of_blk_neis == 2:
                        new_blax.add((p, q))
        blax = new_blax

        minp -= 1
        minq -= 1
        maxp += 1
        maxq += 1

    return blax

blax = part2()
print(len(blax))
