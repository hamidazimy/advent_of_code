import sys

map = [[1 if c == '#' else 0 for c in line.strip()] for line in sys.stdin]

def part1(map):
    H, W = len(map), len(map[0])
    res = 0
    for i in range(H):
        res += map[i][(i * 3) % W]
    return res

print(part1(map))


def part2(map):
    H, W = len(map), len(map[0])
    res = 1

    temp = 0
    for i in range(H):
        temp += map[i][(i * 1) % W]
    res *= temp

    temp = 0
    for i in range(H):
        temp += map[i][(i * 3) % W]
    res *= temp

    temp = 0
    for i in range(H):
        temp += map[i][(i * 5) % W]
    res *= temp

    temp = 0
    for i in range(H):
        temp += map[i][(i * 7) % W]
    res *= temp

    temp = 0
    for i in range(0, H, 2):
        temp += map[i][(i // 2) % W]
    res *= temp

    return res

print(part2(map))
