import sys
from scanf import scanf

lines = [line.strip() for line in sys.stdin]

def part1(lines=lines):
    acc = 0
    ip = 0
    ran = set()

    while True:
        prev_acc = acc
        ins, arg = scanf("%s %d", lines[ip])
        if ins == "nop":
            ip += 1
        elif ins == "acc":
            acc += arg
            ip += 1
        elif ins == "jmp":
            ip += arg

        if ip in ran:
            return prev_acc
        ran.add(ip)

print(part1())

def change_and_run(d, lines=lines):
    acc = 0
    ip = 0
    ran = set()
    while ip < len(lines):
        prev_acc = acc
        line = lines[ip]
        if ip == d:
            if "nop" in line:
                line = line.replace("nop", "jmp")
            elif "jmp" in line:
                line = line.replace("jmp", "nop")
            else:
                return
        ins, arg = scanf("%s %d", line)
        if ins == "nop":
            ip += 1
        elif ins == "acc":
            acc += arg
            ip += 1
        elif ins == "jmp":
            ip += arg
        if ip in ran:
            return
        ran.add(ip)
    return acc

def part2(lines=lines):
    for d in range(len(lines)):
        acc = change_and_run(d)
        if acc is not None:
            return acc

print(part2())
