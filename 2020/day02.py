import sys
from scanf import scanf

inp = [scanf("%d-%d %c: %s\n", line) for line in sys.stdin]

def part1(inp):
    res = 0
    for i, j, c, password in inp:
        if i <= list(password).count(c) <= j:
            res += 1
    return res

print(part1(inp))

def part2(inp):
    res = 0
    for i, j, c, password in inp:
        if (password[i - 1] == c) != (password[j - 1] == c):
            res += 1
    return res

print(part2(inp))
