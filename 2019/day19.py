import sys
from intcode import Intcode
import numpy as np

mem = [int(x) for x in sys.stdin.readline().strip().split(',')]

def part1(mem=mem):
    H, W = 50, 50

    map = np.zeros((H, W), dtype=np.int8)

    # print(np.shape(map))

    for y in range(H):
        for x in range(W):
            code = Intcode(mem)
            code.run(x)
            map[y, x] = code.run(y)
            # print('.' if map[y, x] == 0 else '#', end='')
        # print()

    return np.sum(map)

print(part1())

def affected(y, x, mem=mem):
    code = Intcode(mem)
    code.run(x)
    return (1 == code.run(y))

def fits(y, x):
    return affected(y + 99, x) and affected(y, x + 99) and affected(y, x) and affected(y + 99, x + 99)

def part2():
    max_ang = (1, 1000)
    min_ang = (1000, 1)

    for y in range(1, 20):
        for x in range(1, 20):
            if affected(y, x):
                if y / x > max_ang[0] / max_ang[1]:
                    max_ang = (y, x)
                if y / x < min_ang[0] / min_ang[1]:
                    min_ang = (y, x)

    for y in range(1000, 5000):
        x = int(y / max_ang[0] * max_ang[1]) - 2
        while not affected(y, x):
            x += 1
        if fits(y - 99, x):
            return x * 10000 + (y - 99)
            break

print(part2())
