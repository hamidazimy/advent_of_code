import sys
from intcode import Intcode

mem = [int(x) for x in sys.stdin.readline().strip().split(',')]

def part1(mem=mem):
    code = Intcode(mem)
    return code.run(1)

print(part1())

def part2(mem=mem):
    code = Intcode(mem)
    return code.run(2)

print(part2())
