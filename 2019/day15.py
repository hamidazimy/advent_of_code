import sys
from intcode import Intcode
import numpy as np
from queue import Queue
from time import sleep

mem = [int(x) for x in sys.stdin.readline().strip().split(',')]

def area_str(area, amp=None):
    return '\n'.join([''.join(["&" if (i, j) == amp else "█.@#O"[area[i, j]] for j in range(-21, 20)]) for i in range(-21, 20)])

def build_area(mem=mem, visualize=None):
    code = Intcode(mem)

    area = np.ones((50, 50), dtype=np.int8) * 3
    y, x = 0, 0
    area[y, x] = 1

    direction = [(-1, 0), (0, 1), (1, 0), (0, -1)]
    input = [1, 4, 2, 3]
    d = 1
    while True:
        output = code.run(input[d])
        if output is True:
            break
        dy, dx = direction[d]
        if 0 == output:
            area[y + dy, x + dx] = 0
            d = (d + 1) % 4
        else:
            area[y + dy, x + dx] = output
            y += dy
            x += dx
            d = (d - 1) % 4
            if x == 0 and y == 0:
                break
        if visualize:
            print("\x1bc" + area_str(area, (y, x)))
            sleep(.05)
    area %= 3
    if visualize is not None:
        print(area_str(area))
    return area

area = build_area()

def part1(area=area):
    y, x = 0, 0
    direction = [(-1, 0), (0, 1), (1, 0), (0, -1)]
    q = Queue()
    q.put((y, x, 0))
    visited = set()
    while True:
        y, x, s = q.get()
        visited.add((y, x))
        for dy, dx in direction:
            y_ = y + dy
            x_ = x + dx
            if area[y_, x_] == 2:
                return s + 1
            elif area[y_, x_] == 1 and (y_, x_) not in visited:
                q.put((y_, x_, s + 1))

print(part1())

def part2(area=area, visualize=False):
    y, x = np.argwhere(area == 2)[0]
    direction = [(-1, 0), (0, 1), (1, 0), (0, -1)]
    q = Queue()
    q.put((y, x, 0))
    visited = set()
    while not q.empty():
        y, x, s = q.get()
        visited.add((y, x))
        area[y, x] = 4
        if visualize:
            print("\x1bc" + area_str(area))
            sleep(.01)
        for dy, dx in direction:
            y_ = y + dy
            x_ = x + dx
            if area[y_, x_] == 1 and (y_, x_) not in visited:
                q.put((y_, x_, s + 1))
    return s

print(part2())
