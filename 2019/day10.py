import sys
import numpy as np
from time import sleep

map = np.array([[int(x == '#') for x in line.strip()] for line in sys.stdin])

def print_map(map, O, ast, destroyed):
    H, W = np.shape(map)
    _, n = np.shape(ast)
    map = [['.' for x in range(W)] for y in range(H)]
    map[O[0]][O[1]] = 'O'
    for k in range(n):
        if destroyed[k]:
            map[ast[0][k]][ast[1][k]] = '.'
        else:
            map[ast[0][k]][ast[1][k]] = '#'

    print(' ' * W)
    print('\n'.join([''.join(l) for l in map]))
    print(' ' * W)

def part1(map=map):
    ast = np.nonzero(map)
    _, n = np.shape(ast)
    ast_y_n1 = ast[0].reshape((n, 1))
    ast_y_1n = ast[0].reshape((1, n))
    ast_x_n1 = ast[1].reshape((n, 1))
    ast_x_1n = ast[1].reshape((1, n))

    Y = np.broadcast_arrays(ast_y_n1, ast_y_1n)
    X = np.broadcast_arrays(ast_x_n1, ast_x_1n)

    Y_diff = Y[0] - Y[1]
    X_diff = X[1] - X[0]

    angles = np.arctan2(Y_diff, X_diff)
    angles = np.multiply(angles, (1 - np.eye(n))) + np.eye(n) * 100 / 9

    detectable = np.array([len(list(set(angles[x]))) - 1 for x in range(n)])
    return np.max(detectable), np.argmax(detectable)

print(part1()[0])

def part2(map=map, visualize=False):
    Y, X = 0, 1
    target = 200

    ast = np.nonzero(map)
    _, n = np.shape(ast)

    O_ind = part1(map)[1]
    O = (ast[Y][O_ind], ast[X][O_ind])

    indices = np.array(range(n))
    ast = np.delete(ast, O_ind, axis=1)
    indices = np.delete(indices, O_ind)
    n -= 1

    angles = np.arctan2(O[Y] - ast[Y], ast[X] - O[X])
    distances = (O[Y] - ast[Y]) ** 2 + (ast[X] - O[X]) ** 2
    destroyed = np.zeros((n), dtype=np.bool)

    result = None
    alpha = np.pi / 2
    while np.sum(destroyed) < n:
        to_vap = np.where(np.logical_and((angles - alpha) == 0, np.logical_not(destroyed)))[0]
        if len(to_vap) > 0:
            to_vap_1st = to_vap[np.argmin(distances[to_vap])]
            destroyed[to_vap_1st] = True
            if visualize:
                print_map(map, O, ast, destroyed)
                sleep(.05)
            if np.sum(destroyed) == target:
                result = ast[Y][to_vap_1st] + ast[X][to_vap_1st] * 100

        ang_diffs = alpha - angles
        change = np.min(np.where(ang_diffs > 0, ang_diffs, np.inf))
        if change == np.inf:
            alpha += 2 * np.pi
            ang_diffs = alpha - angles
            change = np.min(np.where(ang_diffs > 0, ang_diffs, np.inf))
        alpha -= change

    return result

print(part2())
