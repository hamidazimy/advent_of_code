import sys
from intcode import Intcode
import numpy as np
from time import sleep

mem = [int(x) for x in sys.stdin.readline().strip().split(',')]

H, W = 24, 44

def board_str(board, score):
    global H, W
    sym = " █#_o"
    board_string = '\n'.join([''.join([sym[k] for k in board[j, :]]) for j in range(H)])
    board_string += f"\n█{'▀' * (W - 2)}█\n█ Score: {score:<33d} █\n█{'▄' * (W - 2)}█\n"
    return board_string

def part1(mem=mem):
    code = Intcode(mem)

    board = np.zeros((H, W), dtype=np.uint8)
    while True:
        x = code.run()
        if x is True:
            break
        y = code.run()
        t = code.run()
        board[y, x] = t
    return np.sum(board == 2)

print(part1())

def part2(mem=mem, visualize=False):
    mem[0] = 2
    code = Intcode(mem)
    inp = None

    score = 0
    board = np.zeros((H, W), dtype=np.uint8)
    speed = float("Inf")
    while True:
        x = code.run(inp)
        inp = None
        if visualize:
            sleep(1 / speed)
            print("\x1bc" + board_str(board, score))
        if x is True:
            return score
        elif x is None:
            _, ball_x = np.argwhere(board == 4)[0]
            _, padd_x = np.argwhere(board == 3)[0]
            inp = np.sign(ball_x - padd_x)
            continue
        y = code.run()
        t = code.run()
        if x == -1 and y == 0:
            score = t
            speed = 500
        else:
            board[y, x] = t

print(part2())
