import sys

class Intcode():

    def __init__(self, code, id=None, debug=False):
        self.id = id
        self.debug = debug
        self.code = code
        self.mem = dict(zip(range(len(code)), code[:]))
        self.input_queue = []
        self.ip = 0
        self.rb = 0
        self.ic = 0
        self.halted = False

    def enqueue_input(self, input):
        if input is None:
            return
        if type(input) != list:
            input = [input]
        self.input_queue += input

    def __log(self, msg, lvl=0):
        if self.debug:
            # sys.stderr.write(f"{str(msg)}\n")
            print(f"LOG{lvl}: {str(msg)}")

    def __spinner(self):
        if self.debug:
            print("{}\r".format("|/-\\"[self.ic % 4]), end='')
            self.ic += 1

    def __param(self, ith, value=None):
        mem, ip, rb = self.mem, self.ip, self.rb
        mode = f"{mem[ip]:05}"[3::-1][ith]
        address = mem.get(ip + ith, 0) + rb * (mode == '2')
        if value == None:
            return mem.get(ip + ith, 0) if mode == '1' else mem.get(address, 0)
        else:
            mem[address] = value

    def __param1(self, value=None):
        return self.__param(1, value)

    def __param2(self, value=None):
        return self.__param(2, value)

    def __param3(self, value=None):
        return self.__param(3, value)

    def __add(self):
        self.__param3(self.__param1() + self.__param2())
        self.ip += 4

    def __mul(self):
        self.__param3(self.__param1() * self.__param2())
        self.ip += 4

    def __inp(self, input):
        self.__param1(input)
        self.ip += 2

    def __out(self):
        output = self.__param1()
        self.ip += 2
        return output

    def __jnz(self):
        if self.__param1() != 0:
            self.ip = self.__param(2)
        else:
            self.ip += 3

    def __jif(self):
        if self.__param1() == 0:
            self.ip = self.__param(2)
        else:
            self.ip += 3

    def __ilt(self):
        if self.__param1() < self.__param2():
            self.__param3(1)
        else:
            self.__param3(0)
        self.ip += 4

    def __eqs(self):
        if self.__param1() == self.__param2():
            self.__param3(1)
        else:
            self.__param3(0)
        self.ip += 4

    def __arb(self):
        self.rb += self.__param1()
        self.ip += 2

    def run(self, input=None, stop_after_input=False):
        self.enqueue_input(input)
        while not self.halted:
            if self.ip >= len(self.mem):
                break
            opcode = self.mem[self.ip] % 100
            if opcode == 99:  # HALT
                self.__log(f"INTCODE MACHINE HALTED.")
                self.ip += 1
                self.halted = True
                break
            elif opcode == 1: # ADD
                self.__add()
            elif opcode == 2: # MULTIPLY
                self.__mul()
            elif opcode == 3: # INPUT
                if len(self.input_queue) == 0:
                    return
                self.__inp(self.input_queue.pop(0))
                input = None
                if stop_after_input and len(self.input_queue) == 0:
                    return
            elif opcode == 4: # OUTPUT
                return self.__out()
            elif opcode == 5: # JUMP IF TRUE
                self.__jnz()
            elif opcode == 6: # JUMP IF FALSE
                self.__jif()
            elif opcode == 7: # IS LESS THAN
                self.__ilt()
            elif opcode == 8: # EQUALS
                self.__eqs()
            elif opcode == 9: # ADJUST THE RELATIVE BASE
                self.__arb()
            else:
                raise Exception(f"EXECUTING INVALID OPCODE: {opcode}")
            self.__spinner()
        else:
            raise Exception("INTCODE MACHINE IS HALTED!")
        return True

    #### ASCII CAPABALITY ####

    def readline(self, silent=True):
        line = ""
        while True:
            i = self.run()
            if i is None:
                break
            elif type(i) == bool or (i < 32 and i != 10) or i >= 127:
                if not silent:
                    print(i)
                return i
            line += chr(i)
            if i == ord('\n'):
                break
        if not silent:
            print(line, end='')
        return line

    def feedline(self, line, silent=True):
        if not silent:
            print(line, end='')
        self.run(input=[ord(c) for c in line], stop_after_input=True)
