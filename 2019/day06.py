import sys
from queue import Queue

class Node:
    def __init__(self, txt):
        self.txt = txt
        self.parent = None
        self.children = []
        self.score_ = -1

    def score(self):
        if self.score_ != -1:
            return self.score_
        if self.txt == "COM":
            self.score_ = 0
        else:
            self.score_ = self.parent.score() + 1
        return self.score_

inp = [tuple(x.strip().split(')')) for x in sys.stdin]

def part1(inp=inp):
    d = {}
    for i, j in inp:
        p = d.get(i, Node(i))
        c = d.get(j, Node(j))
        c.parent = p
        p.children.append(c)
        d[i] = p
        d[j] = c
    return sum([x.score() for x in d.values()])

print(part1())

def part2(inp=inp):
    d = {}
    for i, j in inp:
        p = d.get(i, Node(i))
        c = d.get(j, Node(j))
        c.parent = p
        p.children.append(c)
        d[i] = p
        d[j] = c

    distance = -1
    q = Queue()
    q.put((d["YOU"], distance))
    visited = []

    while True:
        current, distance = q.get()
        visited.append(current)
        nei = current.children[:]
        if current.parent is not None:
            nei.append(current.parent)
        if "SAN" in [x.txt for x in nei]:
            return distance
        for i in nei:
            if i not in visited:
                q.put((i, distance + 1))

print(part2())
