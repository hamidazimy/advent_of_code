import sys
import numpy as np

one = sys.stdin.readline().strip().split(',')
two = sys.stdin.readline().strip().split(',')

c = 16000

def print_grid(grid):
    H, W = np.shape(grid)
    grid_str = ""
    for i in range(H):
        for j in range(W):
            grid_str += f"{grid[i, j] if grid[i, j] != 0 else ' ':3}"
        grid_str += "\n"
    print(grid_str)

def part1():
    def path(input):
        global c
        mat = np.zeros((c * 2 + 1, c * 2 + 1), dtype=np.uint32)
        x, y = 0, 0
        for d in input:
            n = int(d[1:])
            if d[0] == 'U':
                mat[c + x - n : c + x, c + y] = 1
                x = x - n
            elif d[0] == 'D':
                mat[c + x + 1 : c + x + n + 1, c + y] = 1
                x = x + n
            elif d[0] == 'L':
                mat[c + x, c + y - n : c + y] = 1
                y = y - n
            elif d[0] == 'R':
                mat[c + x, c + y + 1 : c + y + n + 1] = 1
                y = y + n
        return mat

    res = np.argwhere(np.multiply(path(one), path(two)) == 1)
    return np.min(np.sum(np.abs(res - c), axis=1))

print(part1())

def part2():
    def path(input):
        global c
        mat = np.zeros((c * 2 + 1, c * 2 + 1), dtype=np.uint32)
        x, y = 0, 0
        i = 0
        for d in input:
            n = int(d[1:])
            if d[0] == 'U':
                mat[c + x - n : c + x, c + y] = range(i + n, i, -1)
                x = x - n
            elif d[0] == 'D':
                mat[c + x + 1 : c + x + n + 1, c + y] = range(i + 1, i + n + 1)
                x = x + n
            elif d[0] == 'L':
                mat[c + x, c + y - n : c + y] = range(i + n, i, -1)
                y = y - n
            elif d[0] == 'R':
                mat[c + x, c + y + 1 : c + y + n + 1] = range(i + 1, i + n + 1)
                y = y + n
            i = i + n
        return mat

    mat1 = path(one)
    mat2 = path(two)

    res = np.multiply(np.multiply(mat1, mat2) != 0, mat1 + mat2)
    return np.min(res[np.nonzero(res)])

print(part2())
