import sys
from queue import Queue, PriorityQueue
from time import sleep

map = [line.strip() for line in sys.stdin]

INF = float('Inf')

def find(c):
    occ = []
    j = -1
    for i in range(len(map)):
        j = map[i].find(c, j + 1)
        while j != -1:
            occ.append((i, j))
            j = map[i].find(c, j + 1)
    return occ

def bfs(S, map):
    dir = [(-1, 0), (0, -1), (1, 0), (0, 1)]
    dists = {}
    btwns = {}
    q = Queue()
    s = set()
    y, x = S
    q.put((0, [], y, x))
    while not q.empty():
        dd, iw, y, x = q.get()
        p = map[y][x]
        if dd != 0 and p.isalpha():
            dists[p] = dd
            btwns[p] = iw[:]
            iw = iw + [p]
        if (y, x) in s:
            continue
        s.add((y, x))
        for dy, dx in dir:
            y_ = y + dy
            x_ = x + dx
            if (y_, x_) in s:
                continue
            if map[y_][x_] != '#':
                q.put((dd + 1, iw, y_, x_))
    return dists, btwns

def set_of_keys_in_paths(paths):
    s = set()
    for path in paths:
        s = s.union(set(path))
    return s

def current_state(paths):
    s = tuple()
    for path in paths:
        s = s + (tuple(sorted(path[:-1])) + (path[-1],),)
    return s

def dijkstra(map):
    keys = []
    doors = []
    robots = []

    distances = {}
    inbetween = {}

    for i in range(len(map)):
        for j in range(len(map[0])):
            if map[i][j].islower():
                keys.append(map[i][j])
            elif map[i][j].isupper():
                doors.append(map[i][j])
            elif map[i][j] not in '.#':
                robots.append(map[i][j])

    for k in range(len(keys)):
        distances[keys[k]], inbetween[keys[k]] = bfs(find(keys[k])[0], map)

    for d in range(len(doors)):
        distances[doors[d]], inbetween[doors[d]] = bfs(find(doors[d])[0], map)

    for r in range(len(robots)):
        distances[robots[r]], inbetween[robots[r]] = bfs(find(robots[r])[0], map)

    q = PriorityQueue()
    visited = set()
    q.put((0, [[x] for x in robots]))
    while not q.empty():
        d, paths = q.get()
        path = paths[0]
        keychain = set_of_keys_in_paths(paths)
        rem = len(set(keys).difference(keychain))
        if rem == 0:
            return d
        state = current_state(paths)
        if state in visited:
            continue
        visited.add(state)
        for k in keys:
            if k in keychain:
                continue
            for r in range(len(robots)):
                c = paths[r][-1]
                if distances[c].get(k, INF) == INF:
                    continue
                k_in_path = set([x for x in inbetween[c][k] if x.islower()])
                if len(k_in_path.difference(keychain)) != 0:
                    continue
                k_needed = set([x.lower() for x in inbetween[c][k] if x.isupper()])
                if len(k_needed.difference(keychain)) != 0:
                    continue
                new_paths = [path[:] for path in paths]
                new_paths[r] += [k]
                q.put((d + distances[c][k], new_paths))

def part1(map=map):
    return dijkstra(map)

print(part1())

def part2(map=map):
    y, x = find('@')[0]
    # My dumb solution could not distinguish between two @s! (Yes, I'm a lazy ass guy!)
    # Solution?! Use different symbols for each robot!
    map[y - 1] = map[y - 1][:x - 1] + "*#$" + map[y - 1][x + 2:]
    map[  y  ] = map[  y  ][:x - 1] + "###" + map[  y  ][x + 2:]
    map[y + 1] = map[y + 1][:x - 1] + "%#&" + map[y + 1][x + 2:]
    return dijkstra(map)

print(part2())
