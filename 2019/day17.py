import sys
from intcode import Intcode
import numpy as np
from time import sleep

mem = [int(x) for x in sys.stdin.readline().strip().split(',')]

def part1(mem=mem):
    code = Intcode(mem)

    map = []

    while True:
        l = code.readline()
        if l == True or l == '\n':
            break
        map.append([ord(c) for c in l.strip()])

    map = np.array(map)
    H, W = np.shape(map)

    result = 0

    intersection = np.array([[46, 35, 46], [35, 35, 35], [46, 35, 46]])

    for i in range(1, H - 1):
        for j in range(1, W - 1):
            if np.equal(map[i - 1: i + 2, j - 1 : j + 2], intersection).all():
                result += i * j

    return result

print(part1())

def part2(mem=mem):
    mem[0] = 2
    code = Intcode(mem)

    faces = "^>v<"
    face = -1
    dirs = [(-1, 0), (0, 1), (1, 0), (0, -1)]
    direction = -1

    map = []

    while True:
        l = code.readline()
        if l == True or l == '\n':
            break
        map.append([ord(c) for c in l.strip()])
        if face == -1:
            for f in faces:
                idx = l.find(f)
                if idx != -1:
                    face = ord(l[idx])
                    direction = faces.index(chr(face))
                    break

    map = np.array(map)
    H, W = np.shape(map)
    bordered_map = np.ones((H + 2, W + 2), dtype=np.int32) * ord('.')
    bordered_map[1:-1, 1:-1] = map
    map = bordered_map
    H, W = np.shape(map)
    y, x = np.argwhere(map == face)[0]

    steps = []
    while True:
        dy_l, dx_l = dirs[(direction - 1) % 4]
        dy_r, dx_r = dirs[(direction + 1) % 4]

        if map[y + dy_l, x + dx_l] == ord('#'):
            steps.append('L,')
            direction = (direction - 1) % 4
        elif map[y + dy_r, x + dx_r] == ord('#'):
            steps.append('R,')
            direction = (direction + 1) % 4
        else:
            break

        dy, dx = dirs[direction]
        s = 0
        while map[y + dy, x + dx] != ord('.'):
            y += dy
            x += dx
            s += 1
        steps[-1] += str(s)

    coded_path = ''
    labels = {}
    label = 'D'

    for step in steps:
        if step not in labels:
            labels[step] = label
            labels[label] = step
            label = chr(ord(label) + 1)
        coded_path += labels[step]

    def solve(cd, at=3, sp=[]): # TODO: naming!
        if at == 0:
            if cd == []:
                return sp
            else:
                return None
        for i in range(1, min(7, len(cd[0]) + 1)):
            new_cd = []
            for j in range(len(cd)):
                new_cd += list(filter(None, cd[j].split(cd[0][:i])))
            new_sp = solve(new_cd, at - 1, sp + [cd[0][:i]])
            if new_sp is not None:
                return new_sp

    funcs = solve([coded_path])

    label = 'A'
    for func in funcs:
        labels[func] = label
        labels[label] = func
        label = chr(ord(label) + 1)

    main = coded_path.replace(funcs[0], labels[funcs[0]]).replace(funcs[1], labels[funcs[1]]).replace(funcs[2], labels[funcs[2]])
    main = ','.join([x for x in main]) + '\n'

    decoded_funcs = [main]
    for f in funcs:
        decoded_funcs.append(','.join([labels[x] for x in f]) + '\n')

    silent = True

    i = 0
    while i < len(decoded_funcs):
        code.readline(silent=silent)
        code.feedline(decoded_funcs[i], silent=silent)
        i += 1

    code.readline(silent=silent)
    code.feedline('N\n', silent=silent)
    code.readline(silent=silent)

    frame = ""
    while True:
        l = code.readline()
        if l == '\n':
            if not silent:
                print(frame)
            frame = ""
        elif type(l) == int:
            return l
        frame += l

print(part2())
