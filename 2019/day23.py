import sys
from intcode import Intcode
from queue import Queue

mem = [int(x) for x in sys.stdin.readline().strip().split(',')]

def part1(mem=mem):
    N = 50

    nodes = [Intcode(mem) for s in range(N)]
    queues = [Queue() for s in range(N)]

    for s in range(N):
        queues[s].put(s)

    def next_input(d):
        if queues[d].empty():
            return -1
        else:
            return queues[d].get()

    while True:
        for s in range(len(nodes)):
            d = nodes[s].run(next_input(s))
            if d is not None:
                X = nodes[s].run(next_input(s))
                Y = nodes[s].run(next_input(s))
                if d == 255:
                    return Y
                queues[d].put(X)
                queues[d].put(Y)

print(part1())

def part2(mem=mem):
    N = 50

    nodes = [Intcode(mem) for s in range(N)]
    queues = [Queue() for s in range(N)]

    for s in range(N):
        queues[s].put(s)

    def next_input(d):
        if queues[d].empty():
            return -1
        else:
            return queues[d].get()

    NAT = (-1, -1)
    NAT_Y_just_sent = -1

    while True:
        idle = True
        for s in range(len(nodes)):
            if not queues[s].empty():
                idle = False
            d = nodes[s].run(next_input(s))
            if d is not None:
                X = nodes[s].run(next_input(s))
                Y = nodes[s].run(next_input(s))
                if d == 255:
                    NAT = (X, Y)
                    continue
                queues[d].put(X)
                queues[d].put(Y)
        if idle:
            queues[0].put(NAT[0])
            queues[0].put(NAT[1])
            if NAT[1] == NAT_Y_just_sent:
                return NAT[1]
            NAT_Y_just_sent = NAT[1]

print(part2())
