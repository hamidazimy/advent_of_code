import sys
import numpy as np

inp = sys.stdin.readline().strip()
vec = np.array([int(x) for x in inp], dtype=np.int32)
offset = int(inp[:7])

def part1(vec=vec):
    vec = np.copy(vec)
    l = len(vec)

    trmx = np.zeros((l, l), dtype=np.int32)
    for i in range(l):
        trmx [:, i] = np.tile(np.repeat([[0, 1, 0, -1]], i + 1), l // (i + 1))[1 : 1 + l]

    for c in range(100):
        vec = np.abs(np.matmul(vec, trmx)) % 10

    return ''.join([str(x) for x in vec[:8]])

print(part1())

def part2(vec=vec, offset=offset):
    vec = np.tile(vec, 10000)
    vec = vec[offset:][::-1]

    for c in range(100):
        vec = np.abs(np.cumsum(vec)) % 10

    return ''.join([str(x) for x in vec[-8:][::-1]])

print(part2())
