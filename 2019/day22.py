import sys
from mod import Mod

lines = [line.strip() for line in sys.stdin]

def shuffle(b, m, N, lines=lines):
    for line in lines:
        pcs = line.strip().split()
        if pcs[-2] == "new":
            m *= (N - 1)
            b *= (N - 1)
            b -= 1
        elif pcs[-2] == "cut":
            S = int(pcs[-1])
            b -= S
        elif pcs[-2] == "increment":
            S = int(pcs[-1])
            m *= S
            b *= S
    return b, m

def part1():
    N = 10007
    T = 1
    R = Mod(2019, N)
    b, m = Mod(0, N), Mod(1, N)
    b, m = shuffle(b, m, N)
    return int(b + m * R)

print(part1())

def part2():
    N = 119315717514047
    T = 101741582076661
    P = Mod(2020, N)
    b, m = Mod(0, N), Mod(1, N)
    b, m = shuffle(b, m, N)
    mt = m ** T
    bt = b * ((mt - 1) // (m - 1))
    return int((P - bt) // mt)

print(part2())
