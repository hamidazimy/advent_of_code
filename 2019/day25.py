import sys
from intcode import Intcode
from itertools import combinations
from scanf import scanf

mem = [int(x) for x in sys.stdin.readline().strip().split(',')]
code = Intcode(mem)

# DISCLAIMER!:  This is a combination of manual work and exhaustive search!
#               It's not an intelligent solution by any means!
def generate_commands():
    with open("day25_commands.txt") as F:
        for line in F:
            yield line.strip()
    items = ["mutex", "hologram", "mug", "astronaut ice cream", "antenna", "space heater"]
    for n in range(1, len(items)):
        comb = list(combinations(items, n))
        for c in comb:
            for item in c:
                yield f"take {item}"
            yield "inv"
            yield "east"
            for item in c:
                yield f"drop {item}"

commands = generate_commands()

while True:
    line = code.readline()
    sys.stderr.write(line)
    if line.startswith('"Oh, hello!'):
        result = scanf("typing %d on", line)[0]
        print(result)
        exit(0)
    if line == "Command?\n":
        command = next(commands)
        sys.stderr.write(command)
        code.feedline(f"{command}\n")
