import sys
from intcode import Intcode
import numpy as np

mem = [int(x) for x in sys.stdin.readline().strip().split(',')]

def hull_str(hull):
    return '\n'.join([''.join([" █"[hull[i, j]] * 2 for j in range(0, -40, -1)]) for i in range(6)])

def part1(mem=mem, visualize=False):
    hull = np.zeros((100, 100), dtype=np.int8)
    face = 0 - 1j
    y, x = 0, 0
    painted = set()
    code = Intcode(mem)
    while True:
        paint = code.run(hull[y, x])
        if paint is True:
            break
        hull[y, x] = paint
        painted.add((y, x))
        turn = code.run()
        face *= (0 + 1j) * (1 - 2 * turn)
        x += int(face.real)
        y += int(face.imag)
        if visualize:
            print(hull_str(hull))
    return len(painted)

print(part1())

def part2(mem=mem, visualize=False):
    hull = np.zeros((100, 100), dtype=np.int8)
    face = 0 - 1j
    y, x = 0, 0
    hull[y, x] = 1
    code = Intcode(mem)
    while True:
        paint = code.run(hull[y, x])
        if paint is True:
            break
        hull[y, x] = paint
        turn = code.run()
        face *= (0 + 1j) * (1 - 2 * turn)
        x += int(face.real)
        y += int(face.imag)
        if visualize:
            print(hull_str(hull))
    return hull_str(hull)

print(part2())
