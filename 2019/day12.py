import sys
from scanf import scanf
import numpy as np

moons = np.zeros((4, 3), dtype=np.int32)

for i in range(4):
    moons[i, :] = scanf("<x=%d, y=%d, z=%d>", sys.stdin.readline())

def part1(moons=moons):
    moons = np.copy(moons)
    velos = np.zeros((4, 3), dtype=np.int32)

    for i in range(1000):
        temp1 = np.repeat(np.reshape(moons, (4, 1, 3)), 4, axis=1)
        temp2 = np.repeat(np.reshape(moons, (1, 4, 3)), 4, axis=0)

        velos += np.sum((temp1 > temp2) * 2 - 1 + (temp1 == temp2), axis=0)
        moons += velos

    pot = np.sum(np.abs(moons), axis=1)
    kin = np.sum(np.abs(velos), axis=1)

    return np.sum(pot * kin)

print(part1())

def part2(moons=moons):
    moons = np.copy(moons)
    velos = np.zeros((4, 3), dtype=np.int32)

    X, Y, Z = 0, 1, 2

    steps = 0
    visited = [{}, {}, {}]

    periods = [None] * 3

    while None in periods:
        states = [  tuple(moons[:, X]) + tuple(velos[:, X]),
                    tuple(moons[:, Y]) + tuple(velos[:, Y]),
                    tuple(moons[:, Z]) + tuple(velos[:, Z])]

        d = 2
        for d in range(3):
            if states[d] in visited[d]:
                if periods[d] is None:
                    periods[d] = steps
            else:
                visited[d][states[d]] = steps

        temp1 = np.repeat(np.reshape(moons, (4, 1, 3)), 4, axis=1)
        temp2 = np.repeat(np.reshape(moons, (1, 4, 3)), 4, axis=0)
        velos += np.sum((temp1 > temp2) * 2 - 1 + (temp1 == temp2), axis=0)
        moons += velos
        steps += 1

    return np.lcm.reduce(periods)

print(part2())
