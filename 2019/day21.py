import sys
from intcode import Intcode

mem = [int(x) for x in sys.stdin.readline().strip().split(',')]

def go(instructions, mem=mem):
    code = Intcode(mem)

    code.readline()
    for i in range(len(instructions)):
        code.feedline(f"{instructions[i]}\n")

    try:
        while True:
            l = code.readline()#silent=False)
            if type(l) == int:
                return l
    except Exception as e:
        return False

def part1():
    instructions = [
        "NOT A J",
        "NOT C T",
        "OR T J",
        "AND D J",
        "WALK",
    ]
    return go(instructions)

print(part1())

def part2():
    instructions = [
        "NOT B J ",
        "NOT C T",
        "OR T J",
        "AND D J",
        "AND H J",
        "NOT A T",
        "OR T J ",
        "RUN"
    ]
    return go(instructions)

print(part2())
