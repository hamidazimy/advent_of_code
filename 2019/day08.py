import sys
import numpy as np

inp = np.array([int(x) for x in sys.stdin.readline().strip()])
SIF = np.reshape(inp, (len(inp) // 6 // 25, 6, 25))

def part1(SIF=SIF):
    least_zero_layer = np.argmin(np.sum(np.sum(SIF == 0, axis=1), axis=1))
    return np.sum(SIF[least_zero_layer, :, :] == 1) * np.sum(SIF[least_zero_layer, :, :] == 2)

print(part1())

def part2(SIF=SIF):
    D, H, W = np.shape(SIF)
    ind = np.argmax(SIF != 2, axis=0)
    return '\n'.join([''.join([' █'[SIF[ind[i, j], i, j]] for j in range(W)]) for i in range(H)])

print(part2())
