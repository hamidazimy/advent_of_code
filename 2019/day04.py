import sys
from scanf import scanf

a, z = scanf("%d-%d", sys.stdin.readline().strip())

def part1(a=a, z=z):
    def is_valid(pswd):
        duplicate = False
        for i in range(1, 6):
            if pswd[i] < pswd[i - 1]:
                return False
            if pswd[i] == pswd[i - 1]:
                duplicate = True
        return duplicate

    count = 0
    for pswd in range(a, z + 1):
        if is_valid(str(pswd)):
            count += 1
    return count

print(part1())

def part2(a=a, z=z):
    def is_valid(pswd):
        pswd = f"${pswd}^"
        duplicate = False
        for i in range(2, 7):
            if pswd[i] < pswd[i - 1]:
                return False
            if pswd[i] == pswd[i - 1]:
                if pswd[i] != pswd[i - 2] and pswd[i] != pswd[i + 1]:
                    duplicate = True
        return duplicate

    count = 0
    for pswd in range(a, z + 1):
        if is_valid(str(pswd)):
            count += 1
    return count

print(part2())
