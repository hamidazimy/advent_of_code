import sys
from intcode import Intcode
from itertools import permutations

mem = [int(x) for x in sys.stdin.readline().strip().split(',')]

def part1():
    def signal(pss):
        global mem
        prev_output = 0
        for i in range(5):
            amp = Intcode(mem[:])
            amp.run(pss[i])
            prev_output = amp.run(prev_output)
        return prev_output

    perm = permutations([0, 1, 2, 3, 4])
    best_signal = 0
    best_pss = None
    for pss in list(perm):
        sig = signal(pss)
        if sig >= best_signal:
            best_signal = sig
            best_pss = pss[:]

    return best_signal

print(part1())

def part2():
    def signal(pss):
        global mem
        amps = [Intcode(mem[:], f"Amp {chr(ord('A') + x)}") for x in range(5)]
        for x in range(5):
            amps[x].run(pss[x])
        prev_output = 0
        result = 0
        finished = [False] * 5
        i = 0
        while True:
            if type(prev_output) != bool:
                prev_output = amps[i].run(prev_output)
            if prev_output is True:
                finished[i] = True
            elif i == 4 and prev_output > result:
                result = prev_output
            if sum(finished) == 5:
                break
            i = (i + 1) % 5
        return result

    perm = permutations([5, 6, 7, 8, 9])
    best_signal = 0
    best_pss = None
    for pss in list(perm):
        sig = signal(pss)
        if sig >= best_signal:
            best_signal = sig
            best_pss = pss[:]

    return best_signal

print(part2())
