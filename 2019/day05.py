import sys
from intcode import Intcode

mem = [int(x) for x in sys.stdin.readline().strip().split(',')]

def part1(mem=mem):
    code = Intcode(mem)
    output = code.run(1)
    while output == 0:
        output = code.run()
    return output

print(part1())

def part2(mem=mem):
    code = Intcode(mem)
    return code.run(5)

print(part2())
