import sys

inp = [int(x) for x in sys.stdin]

def part1(inp=inp):
    return sum([x // 3 - 2 for x in inp])

print(part1())

def fuel(n):
    if n < 9:
        return 0
    f = n // 3 - 2
    return f + fuel(f)

def part2(inp=inp):
    return sum([fuel(int(x)) for x in inp])

print(part2())
