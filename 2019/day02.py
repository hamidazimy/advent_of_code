import sys
from intcode import Intcode

mem = [int(x) for x in sys.stdin.readline().strip().split(',')]

def run(mem, noun, verb):
    mem[1] = noun
    mem[2] = verb

    code = Intcode(mem)
    code.run()
    return code.mem[0]

def part1(mem=mem):
    return run(mem, 12, 2)

print(part1())

def part2(mem=mem):
    for n in range(100):
        for v in range(100):
            if run(mem, n, v) == 19690720:
                return 100 * n + v

print(part2())
