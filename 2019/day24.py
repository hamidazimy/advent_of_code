import sys
import numpy as np

map = np.array([[int(x == '#') for x in line.strip()] for line in sys.stdin])

def part1(map=map):
    H, W = np.shape(map)
    emap = np.zeros((H + 2, W + 2), dtype=np.int32)
    emap[1:-1, 1:-1] = map
    # print(emap[1:-1, 1:-1])
    next = np.copy(emap)

    rate_matrix = 2 ** (np.cumsum(np.ones((25,), dtype=np.int32)) - 1).reshape((5, 5))
    # print(rate_matrix)
    mask = np.array([[0, 1, 0], [1, 0, 1], [0, 1, 0]])

    states = set()
    s = -1
    while s not in states:
        states.add(s)
        for i in range(1, 6):
            for j in range(1, 6):
                if emap[i, j] == 1 and np.sum(mask * emap[i - 1:i + 2, j - 1: j + 2]) != 1:
                    next[i, j] = 0
                elif emap[i, j] == 0 and 0 < np.sum(mask * emap[i - 1:i + 2, j - 1: j + 2]) < 3:
                    next[i, j] = 1
        emap = np.copy(next)
        s = np.sum(rate_matrix * emap[1:-1, 1:-1])
        # break
        # print(s)
        # print(emap[1:-1, 1:-1])

    # print(s)
    # print(emap[1:-1, 1:-1])
    # print(len(states))
    return s

print(part1())

def part2(map=map):
    H, W = np.shape(map)
    emap = np.zeros((H, W, 500), dtype=np.uint8)
    emap[:, :, 0] = map
    emap[2, 2, :] = 2

    def neighbours(i, j, k):
        n = 1 + i * 5 + j
        # print(f"{n} or {chr(ord('A') - 1 + n)}:")
        # top
        if i == 0:
            # print(8)
            yield 1, 2, k - 1
        elif n == 18:
            for x in range(5):
                # print(chr(ord('A') + 4 * 5 + x))
                yield 4, x, k + 1
        else:
            # print(1 + (i - 1) * 5 + j)
            yield i - 1, j, k
        # bottom
        if i == 4:
            # print(18)
            yield 3, 2, k - 1
        elif n == 8:
            for x in range(5):
                # print(chr(ord('A') + 0 * 5 + x))
                yield 0, x, k + 1
        else:
            # print(1 + (i + 1) * 5 + j)
            yield i + 1, j, k
        # left
        if j == 0:
            # print(12)
            yield 2, 1, k - 1
        elif n == 14:
            for y in range(5):
                # print(chr(ord('A') + y * 5 + 4))
                yield y, 4, k + 1
        else:
            # print(1 + i * 5 + j - 1)
            yield i, j - 1, k
        # right
        if j == 4:
            # print(14)
            yield 2, 3, k - 1
        elif n == 12:
            for y in range(5):
                # print(chr(ord('A') + y * 5 + 0))
                yield y, 0, k + 1
        else:
            # print(1 + i * 5 + j + 1)
            yield i, j + 1, k

        # print("=" * 12)

    def print_emap(k1, k2):
        chx = np.array([ord('.'), ord('#'), ord('?')])
        for k in range(k1, k2 + 1):
            print(f"Depth {k}:")
            print('\n'.join([''.join([chr(x) for x in chx[emap[i, :, k]]]) for i in range(5)]))
            print()

    next = np.copy(emap)
    for t in range(200):
        for k in range(-t - 1, t + 2):
            for i in range(5):
                for j in range(5):
                    number_of_alive_neighour_bugs = sum([emap[p, q, r] for p, q, r in neighbours(i, j, k)])
                    # print(i, j, k, number_of_alive_neighour_bugs)
                    if emap[i, j, k] == 1 and number_of_alive_neighour_bugs != 1:
                        next[i, j, k] = 0
                    elif emap[i, j, k] == 0 and 0 < number_of_alive_neighour_bugs < 3:
                        next[i, j, k] = 1
        emap = np.copy(next)

    # print_emap(-5, 5)

    emap[2, 2, :] = 0
    return np.sum(emap)

print(part2())
