import sys
import math
from time import sleep

formula = {}
amounts = {}
ore = 0

for line in sys.stdin:
    cons, prod = line.strip().split(" => ")
    prod_n, prod_l = prod.split()
    formula[prod_l] = (int(prod_n), [(y[0], int(y[1])) for y in [x.split()[::-1] for x in cons.split(", ")]])

def conduce(resource, amount): # consume, but first produce if needed
    global formula, amounts, ore

    if resource == 'ORE':
        ore += amount
        return

    shortcoming = amount - amounts.get(resource, 0)
    if shortcoming > 0:
        n, deps = formula[resource]
        t = math.ceil(shortcoming / n)
        for r, m in deps:
            conduce(r, m * t)
        amounts[resource] = amounts.get(resource, 0) + n * t
    amounts[resource] = amounts.get(resource, 0) - amount

    return ore

def reset():
    global amounts, ore
    ore = 0
    for i in amounts:
        amounts[i] = 0

def part1():
    return conduce('FUEL', 1)

print(part1())

def part2():
    def bin_search(b=0, e=1024):
        reset()
        m = math.ceil((b + e) / 2)
        if m == e and b != 0:
            return b
        elif conduce('FUEL', m) < 1000000000000:
            return bin_search(m * (b != 0), e + e * (b == 0))
        else:
            return bin_search(b + (m // 2) * (b == 0), m)
    return bin_search()

print(part2())
