import sys
from queue import Queue

map = [line[:-1] for line in sys.stdin]
H, W = len(map), len(map[0])

dirs = [(1, 0), (0, -1), (-1, 0), (0, 1)]

temp_portals = {}
portals = {}
portal_labels = {}
S = None
D = None

def create_portal(P, portal_label):
    global S, D, portals, temp_portals
    if portal_label == "AA":
        S = P
    elif portal_label == "ZZ":
        D = P
    elif portal_label in temp_portals:
        portals[P] = temp_portals[portal_label]
        portals[temp_portals[portal_label]] = P
        portal_labels[P] = portal_label
        del temp_portals[portal_label]
    else:
        temp_portals[portal_label] = P
        portal_labels[P] = portal_label

def rect(H, W, Y, X):
    for j in range(W - 1):
        yield (Y, X + j, 0)
    for i in range(H - 1):
        yield (Y + i, X + W - 1, 1)
    for j in range(W - 1):
        yield (Y + H - 1, X + W - 1 - j, 2)
    for i in range(H - 1):
        yield (Y + H - 1 - i, X, 3)

for i in range(2, len(map)):
    dw = map[i][2:-2].find(' ')
    if dw != -1:
        break

for i, j, d in rect(len(map), len(map[0]), 0, 0):
    if map[i][j].isalpha():
        portal_label = map[i][j] + map[i + dirs[d][0]][j + dirs[d][1]]
        if 0 < d < 3:
            portal_label = portal_label[::-1]
        P = (i + dirs[d][0] * 2, j + dirs[d][1] * 2)
        create_portal(P, portal_label)

for i, j, d in rect(len(map) - (dw + 4) * 2 + 2, len(map[0]) - (dw + 4) * 2 + 2, dw + 3, dw + 3):
    if map[i][j].isalpha():
        portal_label = map[i][j] + map[i + dirs[d - 2][0]][j + dirs[d - 2][1]]
        if not (0 < d < 3):
            portal_label = portal_label[::-1]
        P = (i + dirs[d - 2][0] * 2, j + dirs[d - 2][1] * 2)
        create_portal(P, portal_label)

del temp_portals

def is_inner_portal(y, x):
    return (3 < y < H - 3) and (3 < x < W - 3)

def neighbours(y, x, l=-1):
    if (y, x) in portals:
        if (l == -1) or \
           (l == 0 and (D == (y, x) or is_inner_portal(y, x))) or \
           (l > 0 and (S != (y, x) and D != (y, x))):
            yield portals[(y, x)] + (l if l == -1 else (l + 1 if is_inner_portal(y, x) else l - 1),)
            # yield portals[(y, x)] + (l + (l != -1) * (2 * is_inner_portal(y, x) - 1),)
    for dy, dx in dirs:
        y_ = y + dy
        x_ = x + dx
        if map[y_][x_] == '.':
            yield (y_, x_, l)

def bfs(part):
    global S, D
    D_ = D + (-1 + (part == 2),)
    S_ = S + (-1 + (part == 2),)
    q = Queue()
    q.put(S_ + (0,))
    visited = set()
    visited.add(S)
    while not q.empty():
        y, x, l, n = q.get()
        for y_, x_, l_ in neighbours(y, x, l):
            if (y_, x_, l_) == D_:
                return n + 1
            elif (y_, x_, l_) not in visited:
                q.put((y_, x_, l_, n + 1,))
            visited.add((y_, x_, l_))

def part1():
    return bfs(1)

print(part1())

def part2():
    return bfs(2)

print(part2())
