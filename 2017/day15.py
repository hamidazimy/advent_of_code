import sys

a = int(sys.stdin.readline().strip().split(" with ")[1])
b = int(sys.stdin.readline().strip().split(" with ")[1])

def part1(a=a, b=b):
    ans = 0

    for _ in range(40000000):
        a = (a * 16807) % 2147483647
        b = (b * 48271) % 2147483647

        ans += ((a % 65536) == (b % 65536))

    return ans

print(part1())

def part2(a=a, b=b):
    ans = 0

    for _ in range(5000000):
        while True:
            a = (a * 16807) % 2147483647
            if a % 4 == 0:
                break
        while True:
            b = (b * 48271) % 2147483647
            if b % 8 == 0:
                break

        ans += ((a % 65536) == (b % 65536))

    return ans

print(part2())
