import sys
import numpy as np

inp = [line.strip() for line in sys.stdin]
grid = [[int(x == '#') * 2 for x in line] for line in inp]

def part1(grid=grid):
    x, y = 500, 500
    i, j = -1, 0

    temp = np.zeros([999, 999], dtype=bool)

    temp[500 - 12 : 500 + 13, 500 - 12 : 500 + 13] = np.array(grid)

    grid = temp

    ans = 0

    for _ in range(10000):

        # os.system("clear")
        # p = 0
        # while p < 25:
        #     q = 0
        #     while q < 25:
        #
        #         print("%c%c%c"%('[' if 500 - 12 + p == x and 500 - 12 + q == y else ' ', '#' if grid[500 - 12 + p][500 - 12 + q] else '.', ']' if 500 - 12 + p == x and 500 - 12 + q == y else ' '), end='')
        #
        #         q += 1
        #     print()
        #     p += 1
        # time.sleep(1)

        if grid[x][y]:      # infected
            i, j = j, -i
        else:               # clean
            i, j = -j, i
            ans += 1

        grid[x][y] ^= True

        x += i
        y += j

    return ans

print(part1())

def part2(grid=grid):
    x, y = 500, 500
    i, j = -1, 0

    temp = np.zeros([999, 999], dtype=int)

    temp[500 - 12 : 500 + 13, 500 - 12 : 500 + 13] = np.array(grid)

    grid = temp

    ans = 0

    state = ['.', 'W', '#', 'F']

    for _ in range(10000000):

        # os.system("clear")
        # p = 0
        # while p < 25:
        #     q = 0
        #     while q < 25:
        #
        #         print("%c%c%c"%('[' if 500 - 12 + p == x and 500 - 12 + q == y else ' ', state[grid[500 - 12 + p][500 - 12 + q]], ']' if 500 - 12 + p == x and 500 - 12 + q == y else ' '), end='')
        #
        #         q += 1
        #     print()
        #     p += 1
        # time.sleep(1)

        if grid[x][y] == 0:     # clean
            i, j = -j, i
        elif grid[x][y] == 1:   # weakened
            ans += 1
        elif grid[x][y] == 2:   # infected
            i, j = j, -i
        else:                   # flagged
            i, j = -i, -j

        grid[x][y] = ((grid[x][y] + 1) % 4)

        x += i
        y += j

    return ans

print(part2())
