import sys

inp = [[int(x) for x in line.strip().split()] for line in sys.stdin]

def part1(inp=inp):
    checksum = 0
    for row in inp:
        checksum += max(row) - min(row)
    return checksum

print(part1())

def part2(inp=inp):
    checksum = 0
    for row in inp:
        for i in range(len(row)):
            for j in range(len(row)):
                if i == j:
                    continue
                if row[i] % row[j] == 0:
                    checksum += row[i] // row[j]
                    break
    return checksum

print(part2())
