import sys

graph = {}

for l in sys.stdin:
    l = l.strip()
    fm = l.split(' <-> ')[0]
    to = l.split(' <-> ')[1].split(', ')
    graph[fm] = to

def part1(graph=graph):
    s = set(['0'])
    q = ['0']
    i = 0
    while i < len(q):
        for j in graph[q[i]]:
            if j not in s:
                s |= set([j])
                q.append(j)
        i += 1

    return len(s)

print(part1())

def part2(graph=graph):
    nodes = set(x for x in graph.keys())
    ans = 0
    while len(nodes) > 0:
        s = set([list(nodes)[0]])
        q = [list(nodes)[0]]
        i = 0
        while i < len(q):
            for j in graph[q[i]]:
                if j not in s:
                    s |= set([j])
                    q.append(j)
            i += 1
        nodes -= s
        ans += 1

    return ans

print(part2())
