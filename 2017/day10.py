import sys

inp = sys.stdin.readline().strip()

def part1(inp=inp):
    data = [int(x) for x in inp.split(',')]
    h = list(range(256))

    skip = 0

    i = 0

    for d in data:
        temp = list(h)
        for j in range(d):
            temp[(i + j) % 256] = h[(i + d - 1 - j) % 256]
        h = list(temp)
        i += d + skip
        skip += 1

    return h[0] * h[1]

print(part1())

def part2(inp=inp):
    data = list(int(x) for x in bytearray(inp, "utf-8")) + [17, 31, 73, 47, 23]
    h = list(range(256))

    skip = 0

    i = 0

    for r in range(64):
        for d in data:
            temp = list(h)
            for j in range(d):
                temp[(i + j) % 256] = h[(i + d - 1 - j) % 256]
            h = list(temp)
            i += d + skip
            skip += 1

    xored = [0 for _ in range(16)]

    res = ""

    for j in range(16):
        for k in range(16):
            xored[j] ^= h[j * 16 + k]
        res += "%02x"%(xored[j])

    return res

print(part2())
