import sys

inp = sys.stdin.readline().strip().split(',')

def part1(inp=inp):
    q = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p']

    for i in inp:
        if i[0] == 's':
            idx = int(i[1:])
            q = q[-idx:] + q[:16 - idx]
        elif i[0] == 'x':
            idx = i[1:].split('/')
            q[int(idx[0])], q[int(idx[1])] = q[int(idx[1])], q[int(idx[0])]
        elif i[0] == 'p':
            idx = i[1:].split('/')
            b = q.index(idx[0])
            d = q.index(idx[1])
            q[b], q[d] = q[d], q[b]

    return ''.join(q)

print(part1())

def part2(inp=inp):
    q = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p']

    l = len(q)

    counter = 0

    while counter < 1000000000:
        counter += 1

        for i in inp:
            if i[0] == 's':
                idx = int(i[1:])
                q = q[-idx:] + q[:l - idx]
            elif i[0] == 'x':
                idx = i[1:].split('/')
                q[int(idx[0])], q[int(idx[1])] = q[int(idx[1])], q[int(idx[0])]
            elif i[0] == 'p':
                idx = i[1:].split('/')
                b = q.index(idx[0])
                d = q.index(idx[1])
                q[b], q[d] = q[d], q[b]

        if ''.join(q) == 'abcdefghijklmnop':
            counter = (1000000000 // counter) * counter

    return ''.join(q)

print(part2())
