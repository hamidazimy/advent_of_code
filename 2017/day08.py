import sys

inp = [line.strip() for line in sys.stdin]
instructions = [line.split(' ') for line in inp]
regs = dict((k, 0) for k in list(set(ins[0] for ins in instructions)))

max_value = -1000000

for i in instructions:
    cond_reg = i[4]
    condition = i[5]
    cond_val = int(i[6])

    result = {
        '<' : lambda x: x <  cond_val,
        '<=': lambda x: x <= cond_val,
        '>' : lambda x: x >  cond_val,
        '>=': lambda x: x >= cond_val,
        '==': lambda x: x == cond_val,
        '!=': lambda x: x != cond_val
    }[condition](regs[cond_reg])

    if result:
        regs[i[0]] += (-1 if i[1] == 'dec' else 1) * int(i[2])
        if max_value < regs[i[0]]:
            max_value = regs[i[0]]

def part1():
    global regs
    return regs[max(regs, key=regs.get)]

print(part1())

def part2(instructions=instructions):
    global max_value
    return max_value

print(part2())
