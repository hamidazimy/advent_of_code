import sys

inp = [[int(x) for x in line.strip().split('/')] for line in sys.stdin]

strongest1 = 0
strongest2 = 0
longest = 0

def dfs(cur, x=0, s=0, l=0):
    for pair in cur:
        for j in range(2):
            if pair[j] == x:
                nxt = cur[:]
                nxt.remove(pair)
                dfs(nxt, pair[1 - j], s + sum(pair), l + 1)

    global longest, strongest1, strongest2
    strongest1 = max(strongest1, s)
    if l >= longest:
        longest = l
        strongest2 = max(strongest2, s)

dfs(inp)

def part1():
    global strongest1
    return strongest1

print(part1())

def part2():
    global strongest2
    return strongest2

print(part2())
