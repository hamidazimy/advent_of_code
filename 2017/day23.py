#  input processed manually!!

def part1():
    ans = 0
    a = 0
    b = 57
    c = b
    d = 0
    e = 0
    f = 0
    g = 0
    h = 0
    if a:
        ans += 1
        b *= 100
        b += 100000
        c = b
        c += 17000

    while True:
        f = 1
        d = 2
        while True:
            e = 2
            while True:
                ans += 1
                if d * e == b:
                    f = 0
                e += 1
                if e == b:
                    break
            d += 1
            if d == b:
                break
        if f == 0:
            h += 1
        if b == c:
            break
        b += 17

    return ans

print(part1())

def part2():
    b = 57 * 100 + 100000
    c = b + 17000
    h = 0
    def is_prime(n):
        i = 2
        while i * i < n:
            if n % i == 0:
                return False
            i += 1
        return True

    for i in range(b, c + 1, 17):
        if not is_prime(i):
            h += 1

    return h

print(part2())
