import sys

inp = int(sys.stdin.readline())

def part1(inp=inp):
    current = 0;
    b = [0]

    for i in range(2017):
        current = ((current + inp) % len(b)) + 1
        b.insert(current, i + 1)

    idx = b.index(2017)

    return b[idx + 1]

print(part1())

def part2(inp=inp):
    current = 0;
    ans = 0

    for i in range(50000000):
        current = ((current + inp) % (i + 1)) + 1
        if current == 1:
            ans = i + 1

    return ans

print(part2())
