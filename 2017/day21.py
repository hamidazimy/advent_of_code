import sys
import numpy as np

inp = [line.strip() for line in sys.stdin]

by2 = {}
by3 = {}

def to_num(mat):
    ans = 0
    dim = mat.shape[0]
    for i in mat.reshape(1, dim * dim)[0]:
        ans <<= 1
        ans += i == 1
    return ans

def to_mat(pat):
    dim = 3
    if len(pat) == 5:
        dim = 2
    elif len(pat) == 19:
        dim = 4

    mat = np.empty([dim, dim], dtype=np.int)
    i = 0
    while i < dim:
        j = 0
        while j < dim:
            mat[i][j] = int(pat[i * (dim + 1) + j] == '#')
            j += 1
        i += 1
    return mat

for l in inp:
    transform = l.split(" => ")
    mat = to_mat(transform[0])
    k = 0
    while k < 8:
        if k == 3:
            mat = np.flip(mat, 1)

        if len(transform[0]) == 5:
            by2[to_num(mat)] = transform[1]
        else:
            by3[to_num(mat)] = transform[1]

        mat = np.rot90(mat, 1)
        k += 1

def solve(iters):
    art = np.array([[0, 1, 0], [0, 0, 1], [1, 1, 1]])
    dim = art.shape[0]
    step = 2 if dim % 2 == 0 else 3

    for r in range(iters):
        new_dim = int(dim / 2 * 3 if dim % 2 == 0 else dim / 3 * 4)
        new_art = np.empty([new_dim, new_dim], dtype=np.int)
        i = 0
        while i < dim / step:
            j = 0
            while j < dim / step:
                if dim % 2 == 0:
                    enhanced = by2[to_num(art[i * step : (i + 1) * step, j * step: (j + 1) * step])]
                else:
                    enhanced = by3[to_num(art[i * step : (i + 1) * step, j * step: (j + 1) * step])]
                new_art[i * (step + 1) : (i + 1) * (step + 1), j * (step + 1) : (j + 1) * (step + 1)] = to_mat(enhanced)
                j += 1
            i += 1
        art = new_art
        dim = art.shape[0]
        step = 2 if dim % 2 == 0 else 3

    return np.sum(art)

def part1(iters=5):
    return solve(iters)

print(part1())

def part2(iters=18):
    return solve(iters)

print(part2())
