import sys

inp = sys.stdin.readline().strip()

def part1(inp=inp):
    ans = 0
    lvl = 0
    i = 0
    while i < len(inp):
        if inp[i] == '<':
            i += 1
            while inp[i] != '>':
                if inp[i] == '!':
                    i += 1
                i += 1
        elif inp[i] == '{':
            lvl += 1
        elif inp[i] == '}':
            ans += lvl
            lvl -= 1
        i += 1
    return ans

print(part1())

def part2(inp=inp):
    ans = 0
    i = 0
    while i < len(inp):
        if inp[i] == '<':
            while True:
                i += 1
                if inp[i] == '!':
                    i += 1
                elif inp[i] == '>':
                    break
                else:
                    ans += 1
        i += 1
    return ans

print(part2())
