import sys

grid = sys.stdin.readlines()

def part1(grid=grid):
    x, y = 0, 0
    r, b = 1, 1

    while grid[0][x] != '|':
        x += 1

    while True:
        y += b
        while grid[y][x] != '+':
            if grid[y][x] != '|' and grid[y][x] != '-':
                print(grid[y][x], end='')
            y += b

        if grid[y][x + 1] == '-':
            r = 1
        elif grid[y][x - 1] == '-':
            r = -1

        x += r
        while grid[y][x] != '+' and grid[y][x] != ' ':
            if grid[y][x] != '|' and grid[y][x] != '-':
                print(grid[y][x], end='')
            x += r

        if grid[y][x] == ' ':
            break

        if grid[y + 1][x] == '|':
            b = 1
        elif grid[y - 1][x] == '|':
            b = -1

    return ''

print(part1())

def part2(grid=grid):
    x, y = 0, 0
    r, b = 1, 1

    steps = 0
    while grid[0][x] != '|':
        x += 1

    while True:
        y += b
        steps += 1
        while grid[y][x] != '+':
            # if grid[y][x] != '|' and grid[y][x] != '-':
            #     print(grid[y][x])
            y += b
            steps += 1

        if grid[y][x + 1] == '-':
            r = 1
        elif grid[y][x - 1] == '-':
            r = -1

        x += r
        steps += 1
        while grid[y][x] != '+' and grid[y][x] != ' ':
            # if grid[y][x] != '|' and grid[y][x] != '-':
            #     print(grid[y][x])
            x += r
            steps += 1

        if grid[y][x] == ' ':
            break

        if grid[y + 1][x] == '|':
            b = 1
        elif grid[y - 1][x] == '|':
            b = -1

    return steps

print(part2())
