import sys

inp = sys.stdin.readline().strip()
direction = inp.split(',')

n, u = 0, 0

m = 0

for d in direction:

    n, u = {
        'n'  : lambda n, u: (n + 1, u    ),
        'nw' : lambda n, u: (n + 1, u + 1),
        'sw' : lambda n, u: (n    , u + 1),
        's'  : lambda n, u: (n - 1, u    ),
        'se' : lambda n, u: (n - 1, u - 1),
        'ne' : lambda n, u: (n    , u - 1)
    }[d](n, u)

    m = max(m, abs(n), abs(u))

def part1(inp=inp):
    global n, u
    return max(abs(n), abs(u))

print(part1())

def part2(inp=inp):
    global m
    return m

print(part2())
