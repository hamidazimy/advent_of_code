import sys
from statistics import mode

inp = [line.strip() for line in sys.stdin]

def part1(inp=inp):
    lines = inp[:]
    progs = set([line.split(' (')[0] for line in lines])

    supported = []

    for line in lines:
        temp = line.split('-> ')
        if len(temp) == 2:
            supported += temp[1].split(', ')

    supported = set(supported)

    ans = list(progs - supported)

    return ans[0]

print(part1())

def part2(inp=inp):
    lines = inp[:]
    weights = {}
    leaves = {}
    children = {}

    for line in lines:
        k = line.split(' (')[0]
        v = int(line.split(' (')[1].split(')')[0])
        weights[k] = v

        l = line.split(' -> ')
        if len(l) != 2:
            leaves[k] = v
        else:
            children[k] = l[1].split(', ')

    progs = list(weights.keys())
    n = len(progs)

    i = -1

    while True:
        i = (i + 1) % n
        i_prog = progs[i]
        if i_prog in leaves:
            continue
        i_children = children[i_prog]
        ready = True;
        for i_child in i_children:
            if i_child not in leaves:
                ready = False
        if not ready:
            continue
        children_weights = [leaves[j] for j in i_children]
        if len(set(children_weights)) == 1:
            leaves[i_prog] = sum(children_weights) + weights[i_prog]
        else:
            # print("%s (%d) -> "%(i_prog, weights[i_prog]), i_children)
            # print(children_weights)
            correct_weight = mode(children_weights)
            for wi in range(len(children_weights)):
                wdiff = correct_weight - children_weights[wi]
                if wdiff != 0:
                    return weights[i_children[wi]] + wdiff
            break

print(part2())
