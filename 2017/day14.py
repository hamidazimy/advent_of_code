import sys

inp = sys.stdin.readline().strip()

def part1(inp=inp):
    def ones(hexdigit):
        return {
                0 : 0, # \x0000
                1 : 1, # \x0001
                2 : 1, # \x0010
                3 : 2, # \x0011
                4 : 1, # \x0100
                5 : 2, # \x0101
                6 : 2, # \x0110
                7 : 3, # \x0111
                8 : 1, # \x1000
                9 : 2, # \x1001
                10: 2, # \x1010
                11: 3, # \x1011
                12: 2, # \x1100
                13: 3, # \x1101
                14: 3, # \x1110
                15: 4, # \x1111
            }[hexdigit]

    ans = 0

    for row in range(128):
        data = list(int(x) for x in bytearray("{}-{}".format(inp, row), "utf-8")) + [17, 31, 73, 47, 23]

        h = list(range(256))

        skip = 0

        i = 0

        for r in range(64):
            for d in data:
                temp = list(h)
                for j in range(d):
                    temp[(i + j) % 256] = h[(i + d - 1 - j) % 256]
                h = list(temp)
                i += d + skip
                skip += 1

        xored = [0 for _ in range(16)]

        for j in range(16):
            for k in range(16):
                xored[j] ^= h[j * 16 + k]
            ans += ones(xored[j] % 16) + ones(xored[j] // 16)

    return ans

print(part1())

def part2(inp=inp):
    mat = []

    for row in range(128):
        data = list(int(x) for x in bytearray("{}-{}".format(inp, row), "utf-8")) + [17, 31, 73, 47, 23]

        h = list(range(256))

        skip = 0

        i = 0

        for r in range(64):
            for d in data:
                temp = list(h)
                for j in range(d):
                    temp[(i + j) % 256] = h[(i + d - 1 - j) % 256]
                h = list(temp)
                i += d + skip
                skip += 1

        xored = [0 for _ in range(16)]

        mat_row = [False for _ in range(128)]

        for j in range(16):
            for k in range(16):
                xored[j] ^= h[j * 16 + k]

            mask = 256
            for k in range(8):
                mask //= 2
                mat_row[j * 8 + k] = (xored[j] & mask != 0)
                #print("#" if mat_row[j * 8 + k] else ".", end='')
        mat.append(mat_row)
        #print()

    def find_true():
        for i in range(128):
            for j in range(128):
                if mat[i][j]:
                    return i, j
        return -1, -1

    ans = -1

    while True:
        ans += 1
        x, y = find_true()
        if -1 == x:
            break

        q = [[x, y]]

        while len(q) > 0:
            x, y = q[0]

            mat[x][y] = False
            del q[0]

            if x != 0 and mat[x - 1][y]:
                q.append([x - 1, y])

            if x != 127 and y != 128 and mat[x + 1][y]:
                q.append([x + 1, y])

            if y != 0 and mat[x][y - 1]:
                q.append([x, y - 1])

            if y != 127 and mat[x][y + 1]:
                q.append([x, y + 1])

    return ans

print(part2())
