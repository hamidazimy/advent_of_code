import sys
import numpy as np

inp = [line.strip() for line in sys.stdin]

pos = []
vel = []
acc = []

for l in inp:
    pos.append([int(x) for x in l.split('<')[1].split('>')[0].split(',')])
    vel.append([int(x) for x in l.split('<')[2].split('>')[0].split(',')])
    acc.append([int(x) for x in l.split('<')[3].split('>')[0].split(',')])

pos = np.array(pos)
vel = np.array(vel)
acc = np.array(acc)

def part1(pos=np.copy(pos), vel=np.copy(vel), acc=np.copy(acc)):
    iterations = 0

    while iterations < 500: # This is because I know(!) 500 iterations is my 'long term'!
        iterations += 1
        vel += acc
        pos += vel

        m_d = np.absolute(pos).sum(axis=1).argmin()
        # print("\r{}".format(m_d), end='')

    return m_d

print(part1())

def part2(inp=inp):
    global pos, vel, acc

    def remove_collisions():
        global pos, vel, acc
        to_be_removed = []
        n = pos.shape[0]
        i = -1
        while i < n - 1:
            i += 1
            j = -1
            while j < n - 1:
                j += 1
                if i == j:
                    continue
                eq = (pos[i] == pos[j])
                if eq[0] and eq[1] and eq[2]:
                    to_be_removed.append(i)
                    to_be_removed.append(j)
        pos = np.delete(pos, (to_be_removed), axis=0)
        vel = np.delete(vel, (to_be_removed), axis=0)
        acc = np.delete(acc, (to_be_removed), axis=0)

    iterations = 0

    while iterations < 50: # This is because I know(!) that after 50 iterations all collisions are resolved!
        iterations += 1
        remove_collisions()
        vel += acc
        pos += vel

    return len(pos)

print(part2())
