import sys

inp = [line.strip() for line in sys.stdin]

def part1(inp=inp):
    ans = 0
    for line in inp:
        foo = line.split()
        bar = set(foo)
        if len(foo) == len(bar):
            ans += 1
    return ans

print(part1())

def part2(inp=inp):
    ans = 0
    for line in inp:
        foo = line.split()
        buz = [''.join(sorted(word)) for word in foo]
        bar = set(buz)
        if len(foo) == len(bar):
            ans += 1
    return ans

print(part2())
