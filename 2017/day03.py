import sys

inp = int(sys.stdin.readline())

def part1(inp=inp):
    s = 1
    while (s + 1) * (s + 1) <= inp:
        s += 1

    i = s * s
    e = (s + 1) // 2
    d = s - 1

    if i != inp:
        i += 1
        d += 1

    while i != inp and d > e:
        i += 1
        d -= 1

    while i != inp and d < e * 2:
        i += 1
        d += 1

    while i != inp and d > e:
        i += 1
        d -= 1

    while i != inp and d < e * 2:
        i += 1
        d += 1

    return d

print(part1())

def part2(inp=inp):
    x_edge = 1
    x_dir = 1
    x_index = 0
    x_i = [0]

    for _x in range(50):
        for _ in range(x_edge):
            x_index += x_dir
            x_i.append(x_index)

        for _ in range(x_edge):
            x_i.append(x_index)

        x_edge += 1
        x_dir *= -1

    y_edge = 1
    y_dir = -1
    y_index = 0
    y_i = [0]

    for _y in range(50):
        for _ in range(y_edge):
            y_i.append(y_index)

        for _ in range(y_edge):
            y_index += y_dir
            y_i.append(y_index)

        y_edge += 1
        y_dir *= -1


    matrix = [[0 for x in range(20)] for y in range(20)]

    matrix [10][10] = 1

    i = 0
    ans = 0

    while ans < inp:
        i += 1

        i_x = 10 + x_i[i]
        i_y = 10 + y_i[i]

        slic = [matrix[k][i_x - 1 : i_x + 2] for k in range(i_y - 1, i_y + 2)]
        ans = sum(sum(slic, []))
        matrix[i_y][i_x] = ans

    return ans

print(part2())
