import sys

mem = [int(x) for x in sys.stdin.readline().split()]

l = len(mem)

previous_states = []

state = ",".join(str(x) for x in mem)

steps = 0

while not(state in previous_states):
    steps += 1

    previous_states.append(state)

    max_val = max(mem)
    max_idx = mem.index(max_val)
    mem[max_idx] = 0

    while max_val > 0:
        max_val -= 1
        max_idx += 1
        mem[max_idx % l] += 1

    state = ",".join(str(x) for x in mem)

def part1():
    global steps
    return steps

print(part1())

def part2():
    global previous_states, state
    return len(previous_states) - previous_states.index(state)

print(part2())
