#  input processed manually!!

l = 100000

tape = [0 for x in range(l - 1)]

i = l // 2

s = 1 #  Begin in state A.

for _ in range(12317297): # Perform a diagnostic checksum after 12317297 steps.
    if s == 1: # In state A:
        if not tape[i]: # If the current value is 0:
            tape[i] = 1     # Write the value 1.
            i += 1          # Move one slot to the right.
            s = 2           # Continue with state B.
        else:           # If the current value is 1:
            tape[i] = 0     # Write the value 0.
            i -= 1          # Move one slot to the left.
            s = 4           # Continue with state D.
    elif s == 2: # In state B:
        if not tape[i]: # If the current value is 0:
            tape[i] = 1     # Write the value 1.
            i += 1          # Move one slot to the right.
            s = 3           # Continue with state C.
        else:           # If the current value is 1:
            tape[i] = 0     # Write the value 0.
            i += 1          # Move one slot to the right.
            s = 6           # Continue with state F.
    elif s == 3: # In state C:
        if not tape[i]: # If the current value is 0:
            tape[i] = 1     # Write the value 1.
            i -= 1          # Move one slot to the left.
            s = 3           # Continue with state C.
        else:           # If the current value is 1:
            tape[i] = 1     # Write the value 1.
            i -= 1          # Move one slot to the left.
            s = 1           # Continue with state A.
    elif s == 4: # In state D:
        if not tape[i]: # If the current value is 0:
            tape[i] = 0     # Write the value 0.
            i -= 1          # Move one slot to the left.
            s = 5           # Continue with state E.
        else:           # If the current value is 1:
            tape[i] = 1     # Write the value 1.
            i += 1          # Move one slot to the right.
            s = 1           # Continue with state A.
    elif s == 5: # In state E:
        if not tape[i]: # If the current value is 0:
            tape[i] = 1     # Write the value 1.
            i -= 1          # Move one slot to the left.
            s = 1           # Continue with state A.
        else:           # If the current value is 1:
            tape[i] = 0     # Write the value 0.
            i += 1          # Move one slot to the right.
            s = 2           # Continue with state B.
    elif s == 6: # In state F:
        if not tape[i]: # If the current value is 0:
            tape[i] = 0     # Write the value 0.
            i += 1          # Move one slot to the right.
            s = 3           # Continue with state C.
        else:           # If the current value is 1:
            tape[i] = 0     # Write the value 0.
            i += 1          # Move one slot to the right.
            s = 5           # Continue with state E.

    # if _ % 1000000 == 0:
    #     prog = "|/-\\"
    #     print("\r{}".format(prog[_ // 1000000 % 4]), end='')

# print("\r{}".format(sum(tape)))

print(sum(tape))
