import sys

inp = [int(x) for x in sys.stdin.readline().strip()]

def part1(inp=inp):
    res = 0
    for i in range(len(inp)):
        res += (inp[i] == inp[i - 1]) * inp[i]
    return res

print(part1())

def part2(inp=inp):
    res = 0
    for i in range(len(inp)):
        res += (inp[i] == inp[i - len(inp) // 2]) * inp[i]
    return res

print(part2())
