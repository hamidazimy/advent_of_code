import sys

inp = [line.strip() for line in sys.stdin]

def part1(inp=inp):
    i = 0
    sent = 0
    recovered = 0
    reg = {}

    def val(c2):
        try:
            return int(c2)
        except ValueError:
            return reg[c2]

    while True:
        c = inp[i].split(' ')
        if c[0] == "snd":
            sent = val(c[1])
        elif c[0] == "set":
            reg[c[1]] = val(c[2])
        elif c[0] == "add":
            try:
                reg[c[1]] += val(c[2])
            except KeyError:
                reg[c[1]] = val(c[2])
        elif c[0] == "mul":
            try:
                reg[c[1]] *= val(c[2])
            except KeyError:
                reg[c[1]] = 0
        elif c[0] == "mod":
            try:
                reg[c[1]] %= val(c[2])
            except:
                reg[c[1]] = 0
        elif c[0] == "rcv":
            recovered = sent
            return recovered
        elif c[0] == "jgz":
            if reg[c[1]] > 0:
                i += val(c[2])
                continue
        i += 1

print(part1())

def part2(inp=inp):
    i0 = 0
    i1 = 0
    reg0 = {'p': 0, 'a': 0, 'b': 0, 'f': 0, 'i': 0}
    reg1 = {'p': 1, 'a': 0, 'b': 0, 'f': 0, 'i': 0}
    q0 = []
    q1 = []

    ans = 0


    def val0(c2):
        try:
            return int(c2)
        except ValueError:
            return reg0[c2]


    def val1(c2):
        try:
            return int(c2)
        except ValueError:
            return reg1[c2]

    while True:

        wait0 = False
        c0 = inp[i0].split(' ')
        # print("%d: %s"%(0, lines[i0]))
        if c0[0] == "snd":
            q1.append(val0(c0[1]))
            i0 += 1
        elif c0[0] == "set":
            reg0[c0[1]] = val0(c0[2])
            i0 += 1
        elif c0[0] == "add":
            reg0[c0[1]] += val0(c0[2])
            i0 += 1
        elif c0[0] == "mul":
            reg0[c0[1]] *= val0(c0[2])
            i0 += 1
        elif c0[0] == "mod":
            reg0[c0[1]] %= val0(c0[2])
            i0 += 1
        elif c0[0] == "rcv":
            if len(q0) > 0:
                reg0[c0[1]] = q0.pop(0)
                i0 += 1
            else:
                wait0 = True
        elif c0[0] == "jgz":
            if val0(c0[1]) > 0:
                i0 += val0(c0[2])
            else:
                i0 += 1

        wait1 = False
        c1 = inp[i1].split(' ');
        # print("%d: %s"%(1, lines[i1]))
        if c1[0] == "snd":
            q0.append(val1(c1[1]))
            i1 += 1
            ans += 1
        elif c1[0] == "set":
            reg1[c1[1]] = val1(c1[2])
            i1 += 1
        elif c1[0] == "add":
            reg1[c1[1]] += val1(c1[2])
            i1 += 1
        elif c1[0] == "mul":
            reg1[c1[1]] *= val1(c1[2])
            i1 += 1
        elif c1[0] == "mod":
            reg1[c1[1]] %= val1(c1[2])
            i1 += 1
        elif c1[0] == "rcv":
            if len(q1) > 0:
                reg1[c1[1]] = q1.pop(0)
                i1 += 1
            else:
                wait1 = True
        elif c1[0] == "jgz":
            if val1(c1[1]) > 0:
                i1 += val1(c1[2])
            else:
                i1 += 1

        # print(  "Program 0:\n" +
        #         "\nregs   : " + str(reg0) + "\n" +
        #         "\tcommand: " + lines[i0] + "\n" +
        #         "\tqueue  : " + str(q0))
        # print("\n")
        # print(  "Program 1:\n" +
        #         "\nregs   : " + str(reg1) + "\n" +
        #         "\tcommand: " + lines[i1] + "\n" +
        #         "\tqueue  : " + str(q1))
        # print("\nProgram 1 received " + str(ans))
        #
        # time.sleep(0.2)
        # os.system("clear")

        if wait0 and wait1:
            break

    return ans

print(part2())
