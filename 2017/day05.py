import sys

inp = [int(line) for line in sys.stdin]

def part1(inp=inp):
    mem = inp[:]
    index = 0;
    steps = 0;

    try:
        while True:
            mem[index] += 1;
            index += mem[index] - 1
            steps += 1
    except IndexError:
        return steps

print(part1())

def part2(inp=inp):
    mem = inp[:]
    index = 0;
    steps = 0;

    try:
        while True:
            jump = mem[index];
            mem[index] += 1 if jump < 3 else -1
            index += jump
            steps += 1
    except IndexError:
        return steps

print(part2())
