import sys

inp = dict(line.strip().split(': ') for line in sys.stdin)

def part1(inp=inp):
    ans = 0

    for layer, depth in inp.items():
        layer = int(layer)
        depth = int(depth)
        if 0 == abs(layer % (2 * depth - 2)):
            ans += layer * depth

    return ans

print(part1())

def part2(inp=inp):

    delay = -1

    while True:
        delay += 1
        ans = True
        for layer, depth in inp.items():
            layer = int(layer)
            depth = int(depth)
            if 0 == abs((layer + delay) % (2 * depth - 2)):
                ans = False
                break
        if ans:
            return delay

print(part2())
