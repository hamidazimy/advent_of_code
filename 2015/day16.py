import sys


inp = [line.strip() for line in sys.stdin]

sues = []

for line in inp:
    sue = {}
    name_len = len(line.split(": ")[0]) + 2
    for prop in line[name_len:].split(", "):
        prop = prop.split(": ")
        sue[prop[0]] = int(prop[1])
    sues.append(sue)

results = {
    "children": 3,
    "cats": 7,
    "samoyeds": 2,
    "pomeranians": 3,
    "akitas": 0,
    "vizslas": 0,
    "goldfish": 5,
    "trees": 3,
    "cars": 2,
    "perfumes": 1
    }

def part1(sues=sues):
    for i, sue in enumerate(sues):
        is_the_right_sue = True
        for p in sue:
            if sue[p] != results[p]:
                is_the_right_sue = False
                break
        if is_the_right_sue:
            return i + 1


def part2(sues=sues):
    for i, sue in enumerate(sues):
        is_the_right_sue = True
        for p in sue:
            if p in ["cats", "trees"]:
                if sue[p] <= results[p]:
                    is_the_right_sue = False
                    break
            elif p in ["pomeranians", "goldfish"]:
                if sue[p] >= results[p]:
                    is_the_right_sue = False
                    break
            elif sue[p] != results[p]:
                is_the_right_sue = False
                break
        if is_the_right_sue:
            return i + 1


print(part1())
print(part2())
