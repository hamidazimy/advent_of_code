import sys


inp = [line.strip() for line in sys.stdin]


def run(reg, mem=inp):
    ip = 0
    while ip < len(mem):
        ins = mem[ip][:3]
        if ins == 'hlf':
            reg[mem[ip][4]] //= 2
        elif ins == 'tpl':
            reg[mem[ip][4]] *= 3
        elif ins == 'inc':
            reg[mem[ip][4]] += 1
        elif ins == "jmp":
            ip += int(mem[ip][4:])
            continue
        elif ins == "jie":
            if reg[mem[ip][4]] % 2 == 0:
                ip += int(mem[ip][7:])
                continue
        elif ins == "jio":
            # print(reg['a'], reg['b'])
            if reg[mem[ip][4]] == 1:
                ip += int(mem[ip][7:])
                continue
        ip += 1
    return reg['b']


def part1():
    return run({'a': 0, 'b': 0})


def part2():
    return run({'a': 1, 'b': 0})


print(part1())
print(part2())
