import sys
from scanf import scanf

row, col = scanf("To continue, please consult the code grid in the manual.  Enter the code at row %d, column %d.", sys.stdin.readline().strip())

index = (row + col - 1) * (row + col - 2) // 2 + col

start = 20151125
base = 252533
modulus = 33554393

exponent = index - 1

coef = pow(base, exponent, modulus)

result = (start * coef) % modulus

print(result)
