import sys


inp = sys.stdin.readline().strip()


def look_and_say(input):
    result = ""
    i = 0
    while i < len(input):
        c = 0
        d = input[i]
        while i < len(input) and input[i] == d:
            c += 1
            i += 1
        result += f"{c}{d}"
    return result


def part1(inp=inp):
    res = inp[:]
    for i in range(40):
        res = look_and_say(res)
    return len(res)


def part2(inp=inp):
    res = inp[:]
    for i in range(50):
        res = look_and_say(res)
    return len(res)


print(part1())
print(part2())
