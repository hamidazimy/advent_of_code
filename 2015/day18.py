import sys
import numpy as np
from time import sleep


inp = [line.strip() for line in sys.stdin]

map = np.array([[1 if c == "#" else 0 for c in line] for line in inp])


def print_map():
    from time import sleep
    global map
    H, W = np.shape(map)
    map_str = ""
    for y in range(H):
        for x in range(W):
            map_str += ' #'[map[y, x]]
        map_str += "\n"
    print(map_str)
    sleep(.2)


def part1(map=map):
    H, W = np.shape(map)
    for round in range(100):
        new_map = np.zeros_like(map)
        for y in range(H):
            for x in range(W):
                neis = np.sum(map[max(0, y - 1):min(H, y + 2), max(0, x - 1):min(W, x + 2)]) - map[y, x]
                if map[y, x] == 1:
                    if neis in [2, 3]:
                        new_map[y, x] = 1
                    else:
                        new_map[y, x] = 0
                else:
                    if neis == 3:
                        new_map[y, x] = 1
                    else:
                        new_map[y, x] = 0
        map = new_map
        # print_map()
    return np.sum(map)


def part2(map=map):
    H, W = np.shape(map)
    for round in range(100):
        new_map = np.zeros_like(map)
        for y in range(H):
            for x in range(W):
                neis = np.sum(map[max(0, y - 1):min(H, y + 2), max(0, x - 1):min(W, x + 2)]) - map[y, x]
                if map[y, x] == 1:
                    if neis in [2, 3]:
                        new_map[y, x] = 1
                    else:
                        new_map[y, x] = 0
                else:
                    if neis == 3:
                        new_map[y, x] = 1
                    else:
                        new_map[y, x] = 0
        map = new_map
        map[ 0,  0] = 1
        map[ 0, -1] = 1
        map[-1,  0] = 1
        map[-1, -1] = 1
        # print_map()
    return np.sum(map)


print(part1())
print(part2())
