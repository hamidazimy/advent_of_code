import sys
from scanf import scanf


inp = [line.strip() for line in sys.stdin]
deers = [scanf("%s can fly %d km/s for %d seconds, but then must rest for %d seconds.", l) for l in inp]
end = 2503


def part1(inp=inp):
    winning_distance = 0
    for i, deer in enumerate(deers):
        name, speed, fly, rest = deer
        period = fly + rest
        distance = ((end // period) * fly + min(fly, end % period)) * speed
        winning_distance = max(distance, winning_distance)
    return winning_distance


def part2(inp=inp):
    scores = [0] * len(deers)
    for round in range(1, end + 1):
        distances = [0] * len(deers)
        for i, deer in enumerate(deers):
            name, speed, fly, rest = deers[i]
            period = fly + rest
            distances[i] = ((round // period) * fly + min(fly, round % period)) * speed
        max_distance = max(distances)
        for i, deer in enumerate(deers):
            if distances[i] == max_distance:
                scores[i] += 1
    return max(scores)


print(part1())
print(part2())
