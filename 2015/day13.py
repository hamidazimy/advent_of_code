import sys
from scanf import scanf
from itertools import permutations


inp = [line.strip() for line in sys.stdin]

happiness = {}

for line in inp:
    p, g, n, q = scanf("%s would %s %d happiness units by sitting next to %s.", line)
    if p not in happiness:
        happiness[p] = {}
    happiness[p][q] = n * (-1 if g == 'lose' else 1)


def solve(happiness):
    best = -float("Inf")
    for sitting in permutations(list(happiness.keys())):
        score = 0
        for i, stng in enumerate(sitting):
            score += happiness[stng][sitting[i - 1]]
            score += happiness[sitting[i - 1]][stng]
        best = max(best, score)
    return best


def part1(happiness=happiness):
    return solve(happiness)


def part2(happiness=happiness):
    for p in happiness:
        happiness[p]["Myself"] = 0
    happiness["Myself"] = {p: 0 for p in happiness}
    return solve(happiness)


print(part1())
print(part2())
