import sys
from scanf import scanf
import numpy as np


inp = [line.strip() for line in sys.stdin]


def part1(lines=inp):
    grid = np.zeros((1000, 1000), dtype=np.uint8)

    for line in lines:
        sqr = scanf("turn off %d,%d through %d,%d", line)
        if sqr is not None:
            grid[sqr[0]:sqr[2] + 1, sqr[1]:sqr[3] + 1] = 0
        sqr = scanf("turn on %d,%d through %d,%d", line)
        if sqr is not None:
            grid[sqr[0]:sqr[2] + 1, sqr[1]:sqr[3] + 1] = 1
        sqr = scanf("toggle %d,%d through %d,%d", line)
        if sqr is not None:
            grid[sqr[0]:sqr[2] + 1, sqr[1]:sqr[3] + 1] = 1 - grid[sqr[0]:sqr[2] + 1, sqr[1]:sqr[3] + 1]

    return np.sum(grid)


def part2(lines=inp):
    grid = np.zeros((1000, 1000), dtype=np.int8)

    for line in lines:
        sqr = scanf("turn off %d,%d through %d,%d", line)
        if sqr is not None:
            grid[sqr[0]:sqr[2] + 1, sqr[1]:sqr[3] + 1] = np.maximum(grid[sqr[0]:sqr[2] + 1, sqr[1]:sqr[3] + 1] - 1, 0)
        sqr = scanf("turn on %d,%d through %d,%d", line)
        if sqr is not None:
            grid[sqr[0]:sqr[2] + 1, sqr[1]:sqr[3] + 1] += 1
        sqr = scanf("toggle %d,%d through %d,%d", line)
        if sqr is not None:
            grid[sqr[0]:sqr[2] + 1, sqr[1]:sqr[3] + 1] += 2

    return np.sum(grid)


print(part1())
print(part2())
