import sys
from itertools import combinations


inp = [int(line.strip()) for line in sys.stdin]


def part1(conts=inp):
    result = 0
    for i in range(1, len(conts)):
        for comb in combinations(conts, i):
            comb_list = list(comb)
            if sum(comb_list) == 150:
                result += 1
    return result


def part2(conts=inp):
    results = []
    for i in range(1, len(conts)):
        for comb in combinations(conts, i):
            comb_list = list(comb)
            if sum(comb_list) == 150:
                results.append(len(comb_list))
    return results.count(min(results))


print(part1())
print(part2())
