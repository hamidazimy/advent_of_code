import sys


inp = sys.stdin.readline().strip()


def char_to_base26(c):
    return ''.join([chr(((ord(x) - 107) % 65 + 49) % 104 + 48) for x in c])


def base26_to_char(num):
    num26 = [num // (26 ** i) % 26 for i in range(7, -1, -1)]
    return ''.join([chr(x + 97) for x in num26])


def is_valid(password):
    rule_1 = False
    for i in range(2, len(password)):
        if ord(password[i]) - ord(password[i - 1]) == 1 and ord(password[i - 1]) - ord(password[i - 2]) == 1:
            rule_1 = True
            break
    if not rule_1:
        return False
    rule_2 = password.count('i') + password.count('o') + password.count('l') == 0
    if not rule_2:
        return False
    rule_3 = False
    i = 1
    pairs = 0
    while i < len(password):
        if password[i] == password[i - 1]:
            pairs += 1
            i += 1
        if pairs >= 2:
            rule_3 = True
            break
        i += 1
    if not rule_3:
        return False
    return True


def next_password(password):
    next = base26_to_char(int(char_to_base26(password), 26) + 1)
    while not is_valid(next):
        next = base26_to_char(int(char_to_base26(next), 26) + 1)
    return next


def part1(password=inp):
    return next_password(password)


def part2(password=inp):
    return next_password(next_password(password))


print(part1())
print(part2())
