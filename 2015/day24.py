import sys
from math import prod


inp = [int(line) for line in sys.stdin]
weight = sum(inp)


def dldfs(all=inp, goal=None, selected=[], max_len=100):
    if goal is None:
        return
    if sum(selected) == goal:
        yield selected
    if len(all) == 0:
        return
    if len(selected) < max_len:
        for result in dldfs(all[1:], goal, selected + [all[0]], max_len):
            yield result
        for result in dldfs(all[1:], goal, selected           , max_len):
            yield result


def dfs3(max_len, goal):
    for items1 in dldfs(goal=goal, max_len=max_len):
        the_rest = set(inp) - set(items1)
        for items2 in dldfs(all=list(the_rest), goal=goal):
            items3 = list(the_rest - set(items2))
            yield items1, items2, items3
            break


def dfs4(max_len, goal):
    for items1 in dldfs(goal=goal, max_len=max_len):
        the_rest = set(inp) - set(items1)
        for items2 in dldfs(all=list(the_rest), goal=goal):
            the_last = set(inp) - set(items1) - set(items2)
            for items3 in dldfs(all=list(the_last), goal=goal):
                items4 = list(the_last - set(items3))
                yield items1, items2, items3, items4
                break
            else:
                continue
            break


def part1(inp=inp):
    num = 0
    for l in range(100):
        for items in dfs3(l, goal=weight // 3):
            num += 1
            break
        else:
            continue
        break
    min_qe = float("Inf")
    for items in dfs3(l, goal=weight // 3):
        min_qe = min(prod(items[0]), min_qe)
    return min_qe


def part2(inp=inp):
    num = 0
    for l in range(100):
        for items in dfs4(l, goal=weight // 4):
            num += 1
            break
        else:
            continue
        break
    min_qe = float("Inf")
    for items in dfs4(l, goal=weight // 4):
        min_qe = min(prod(items[0]), min_qe)
    return min_qe


print(part1())
print(part2())
