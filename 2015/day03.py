import sys


inp = sys.stdin.readline().strip()


def part1(path=inp):
    house = [0, 0]

    visited = set()
    visited.add(tuple(house))

    dy = {"^": -1, ">": 0, "v": 1, "<": 0}
    dx = {"^": 0, ">": 1, "v": 0, "<": -1}

    for dir in path:
        house[0] += dy[dir]
        house[1] += dx[dir]
        visited.add(tuple(house))

    return len(visited)


def part2(path=inp):
    house = [0, 0]
    house_robo = [0, 0]
    visited = set()
    visited.add(tuple(house))

    dy = {"^": -1, ">": 0, "v": 1, "<": 0}
    dx = {"^": 0, ">": 1, "v": 0, "<": -1}
    i = 0
    for dir in path:
        if i % 2 == 0:
            house[0] += dy[dir]
            house[1] += dx[dir]
            visited.add(tuple(house))
        else:
            house_robo[0] += dy[dir]
            house_robo[1] += dx[dir]
            visited.add(tuple(house_robo))
        i += 1

    return len(visited)


print(part1())
print(part2())
