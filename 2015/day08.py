import sys


inp = [line.strip() for line in sys.stdin]


def part1(lines=inp):
    result = 0
    for line in lines:
        decoded_line = bytes(line[1:-1], "utf-8").decode("unicode_escape")
        result += len(line) - len(decoded_line)
    return result


def part2(lines=inp):
    result = 0
    for line in lines:
        encoded_line = '"' + line.replace('\\', '\\\\').replace('"', '\\"') + '"'
        result += len(encoded_line) - len(line)
    return result


print(part1())
print(part2())
