import sys
from scanf import scanf


inp = [line.strip() for line in sys.stdin]

wires = {}


def is_int(inp):
    if int == type(inp):
        return True
    try:
        int(inp)
        return True
    except ValueError:
        return False

def are_ready(args):
    global wires
    for arg in args:
        if not is_int(arg) and (arg not in wires):
            return False
    return True

def _VALUE(arg):
    global wires
    if is_int(arg):
        return int(arg)
    else:
        return wires[arg]

def _LSHIFT(args):
    global wires
    wires[args[2]] = _VALUE(args[0])  * (2 ** args[1])

def _RSHIFT(args):
    global wires
    wires[args[2]] = _VALUE(args[0]) // (2 ** args[1])

def _AND(args):
    global wires
    wires[args[2]] = _VALUE(args[0]) & _VALUE(args[1])

def _OR(args):
    global wires
    wires[args[2]] = _VALUE(args[0]) | _VALUE(args[1])

def _NOT(args):
    global wires
    wires[args[1]] = (2 ** 17 - 1) - _VALUE(args[0])

def _LOAD(args):
    global wires
    wires[args[1]] = _VALUE(args[0])

instructions = {
    "%s LSHIFT %d -> %s": _LSHIFT
,   "%s RSHIFT %d -> %s": _RSHIFT
,   "%s AND %s -> %s"   : _AND
,   "%s OR %s -> %s"    : _OR
,   "NOT %s -> %s"      : _NOT
,   "%s -> %s"          : _LOAD
}

a_signal = None


def part1(lines=inp):
    global wires, a_signal
    while not are_ready(tuple(["a"])):
        for line in lines:
            for inst in instructions:
                args = scanf(inst, line)
                if args is not None:
                    if are_ready(args[:-1]):
                        instructions[inst](args)
                    break
    a_signal = wires["a"]
    return a_signal


def part2(lines=inp):
    global wires, a_signal
    wires = {"b": a_signal}
    while not are_ready(tuple(["a"])):
        for line in lines:
            for inst in instructions:
                args = scanf(inst, line)
                if args is not None:
                    if args[-1] == "b":
                        break
                    if are_ready(args[:-1]):
                        instructions[inst](args)
                    break

    return wires["a"]


print(part1())
print(part2())
