import sys
from scanf import scanf
from itertools import permutations


inp = [line for line in sys.stdin]

distances = {}

for line in inp:
    src, dst, kms = scanf("%s to %s = %d\n", line)
    if src not in distances:
        distances[src] = {}
    distances[src][dst] = kms
    if dst not in distances:
        distances[dst] = {}
    distances[dst][src] = kms


def part1(inp=inp):
    perm = list(permutations(list(distances.keys())))
    min_dist = float("Inf")
    best_ord = None
    for order in perm:
        dist = 0
        for c in range(1, len(order)):
            dist += distances[order[c - 1]][order[c]]
        if dist < min_dist:
            best_ord = order
            min_dist = dist
    return min_dist


def part2(inp=inp):
    perm = list(permutations(list(distances.keys())))
    max_dist = 0
    best_ord = None
    for order in perm:
        dist = 0
        for c in range(1, len(order)):
            dist += distances[order[c - 1]][order[c]]
        if dist > max_dist:
            best_ord = order
            max_dist = dist
    return max_dist


print(part1())
print(part2())
