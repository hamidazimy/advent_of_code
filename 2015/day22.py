import sys
from queue import PriorityQueue


boss_hitpoint = int(sys.stdin.readline().strip().split(": ")[-1])
boss_damage   = int(sys.stdin.readline().strip().split(": ")[-1])

BOSS = {"hp": boss_hitpoint, "damage": boss_damage}
PLYR = {"hp": 50, "mana": 500}


verbose = False


def log(obj):
    global verbose
    if verbose:
        print(obj)


spells =   {"Magic Missile": {"name": "Magic Missile" , "cost":  53, "damage": 4},
            "Drain"        : {"name": "Drain"         , "cost":  73, "damage": 2, "heal": 2},
            "Shield"       : {"name": "Shield"        , "cost": 113, "duration": 6, "armor": 7},
            "Poison"       : {"name": "Poison"        , "cost": 173, "duration": 6, "damage": 3},
            "Recharge"     : {"name": "Recharge"      , "cost": 229, "duration": 5, "mana": 101}}


def is_active(spell, spell_started, round):
    global spells
    if (spell in ['Magic Missile', 'Drain'] and round == spell_started.get(spell, -1000)) or \
        (spell in ['Shield', 'Poison', 'Recharge'] and (0 < round - spell_started.get(spell, -1000) <= spells[spell]['duration'])):
        return True
    return False


def play(actions, hard_mode):
    global PLYR, BOSS, spells
    plyr = PLYR.copy()
    boss = BOSS.copy()
    spell_started = {}
    plyr_armor = 0
    total_mana = 0
    for round in range(len(actions) * 2):
        log(f"\n-- {['Player', 'Boss'][round % 2]} turn -- [round: {round}]")

        if hard_mode:
            if round % 2 == 0:
                plyr['hp'] -= 1
                if plyr['hp'] <= 0:
                    log("The player is DEAD!")
                    return False, -1

        plyr_armor = spells['Shield']['armor'] if is_active('Shield', spell_started, round) else 0
        for spell in spells:
            if spell in spell_started and not is_active(spell, spell_started, round):
                del spell_started[spell]
        log(f"- Player has {plyr['hp']} hit points, {plyr_armor} armor, {plyr['mana']} mana")
        log(f"- Boss has {boss['hp']} hit points")
        for spell in ['Recharge', 'Poison']:
            if is_active(spell, spell_started, round):
                plyr['mana'] += spells[spell].get('mana', 0)
                boss['hp'] -= spells[spell].get('damage', 0)
        if boss['hp'] <= 0:
            log("The boss is DEAD!")
            return True, total_mana
        if round % 2 == 0:
            spell = spells[actions[round // 2]]['name']
            # Check if the spell can be casted
            if plyr['mana'] <= spells[spell]['cost']:
                log(f"Not enough mana to cast {spell}. (have {plyr['mana']}, need {spells[spell]['cost']})")
                return False, -1
            if is_active(spell, spell_started, round + 1):
                log(f"You cannot cast {spell} yet")
                return False, -1
            # Cast the spell
            plyr['mana'] -= spells[spell]['cost']
            total_mana += spells[spell]['cost']
            spell_started[spell] = round
            log(f"{spell} casted.")
            if spell in ['Magic Missile', 'Drain']:
                boss['hp'] -= spells[spell].get('damage', 0)
                plyr['hp'] += spells[spell].get('heal', 0)
        else:
            attack = max(1, boss['damage'] - plyr_armor)
            log(f"Boss attacks for {attack} damage!")
            plyr['hp'] -= attack
            if plyr['hp'] <= 0:
                log("The player is DEAD!")
                return False, -1
    # log("Not enough actions!")
    return None, total_mana


def dijkstra_solve(hard_mode=False):
    global spells
    spell_list = list(spells.keys())
    q = PriorityQueue()
    q.put((0, []))
    while not q.empty():
        score, strategy = q.get()
        status, result = play(strategy, hard_mode)
        if status is None:
            for spell in spell_list:
                q.put((result, strategy + [spell]))
        elif status:
            # print(strategy)
            return result


def part1():
    return dijkstra_solve()


def part2():
    return dijkstra_solve(hard_mode=True)


print(part1())
print(part2())
