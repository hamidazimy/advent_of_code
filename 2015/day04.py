import sys
import hashlib


inp = sys.stdin.readline().strip()


def part1(inp=inp):
    num = 0
    while True:
        num += 1
        if "00000" == hashlib.md5(f"{inp}{num}".encode('utf-8')).hexdigest()[:5]:
            return num


def part2(inp=inp):
    num = 0
    while True:
        num += 1
        if "000000" == hashlib.md5(f"{inp}{num}".encode('utf-8')).hexdigest()[:6]:
            return num


print(part1())
print(part2())
