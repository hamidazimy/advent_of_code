import sys


inp = sys.stdin.readline().strip()


def part1(inp=inp):
    floor = 0
    for c in inp:
        if c == '(':
            floor += 1
        elif c == ')':
            floor -= 1
        else:
            raise ValueError(f"Unexpected character: '{c}'")
    return floor


def part2(inp=inp):
    floor = 0
    for i, c in enumerate(inp):
        if c == '(':
            floor += 1
        elif c == ')':
            floor -= 1
        else:
            raise ValueError(f"Unexpected character: '{c}'")
        if floor == -1:
            return i + 1


print(part1())
print(part2())
