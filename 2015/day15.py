import sys
from scanf import scanf
from itertools import combinations_with_replacement
from functools import reduce


inp = [line.strip() for line in sys.stdin]
ingredients = [scanf("capacity %d, durability %d, flavor %d, texture %d, calories %d", l) for l in inp]


def part1(ingredients=ingredients):
    total = 100
    best_score = 0
    for c in combinations_with_replacement(list(range(total + 1)), len(ingredients)):
        d = [0] + list(c) + [total]
        amounts = tuple([d[i] - d[i - 1] for i in range(1, len(d))])
        property_scores = [0] * 4
        for i, ing in enumerate(ingredients):
            for p in range(4):
                property_scores[p] += amounts[i] * ing[p]
        score = reduce(lambda x, y: x * y, [p if p > 0 else 0 for p in property_scores])
        best_score = max(score, best_score)
    return best_score


def part2(ingredients=ingredients):
    total = 100
    best_score = 0
    for c in combinations_with_replacement(list(range(total + 1)), len(ingredients)):
        d = [0] + list(c) + [total]
        amounts = tuple([d[i] - d[i - 1] for i in range(1, len(d))])
        total_calories = sum([amounts[i] * ing[-1] for i, ing in enumerate(ingredients)])
        if total_calories != 500:
            continue
        property_scores = [0] * 4
        for i, ing in enumerate(ingredients):
            for p in range(4):
                property_scores[p] += amounts[i] * ing[p]
        score = reduce(lambda x, y: x * y, [p if p > 0 else 0 for p in property_scores])
        best_score = max(score, best_score)
    return best_score


print(part1())
print(part2())
