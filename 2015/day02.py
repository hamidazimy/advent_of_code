import sys


inp = [line.strip().split('x') for line in sys.stdin]
inp = [sorted([int(x) for x in line]) for line in inp]


def part1(inp=inp):
    result = 0
    for dims in inp:
        result += 2 * (dims[0] * dims[1] + dims[0] * dims[2] + dims[1] * dims[2]) + dims[0] * dims[1]
    return result


def part2(inp=inp):
    result = 0
    for dims in inp:
        result += 2 * (dims[0] + dims[1]) + (dims[0] * dims[1] * dims[2])
    return result


print(part1())
print(part2())
