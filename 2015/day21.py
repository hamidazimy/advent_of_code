import sys
import math


boss_hitpoint = int(sys.stdin.readline().strip().split(": ")[-1])
boss_damage   = int(sys.stdin.readline().strip().split(": ")[-1])
boss_armor    = int(sys.stdin.readline().strip().split(": ")[-1])

plyr_hitpoint = 100

NAM = 0
CST = 1
DMG = 2
AMR = 3

weapons =  [("Dagger"    ,   8, 4, 0),
            ("Shortsword",  10, 5, 0),
            ("Warhammer" ,  25, 6, 0),
            ("Longsword" ,  40, 7, 0),
            ("Greataxe"  ,  74, 8, 0)]

armors =   [("None"      ,   0, 0, 0), # USER DEFINED
            ("Leather"   ,  13, 0, 1),
            ("Chainmail" ,  31, 0, 2),
            ("Splintmail",  53, 0, 3),
            ("Bandedmail",  75, 0, 4),
            ("Platemail" , 102, 0, 5)]

rings =    [("Damage +0" ,   0, 0, 0), # USER DEFINED
            ("Damage +1" ,  25, 1, 0),
            ("Damage +2" ,  50, 2, 0),
            ("Damage +3" , 100, 3, 0),
            ("Defence +0",   0, 0, 0), # USER DEFINED
            ("Defense +1",  20, 0, 1),
            ("Defense +2",  40, 0, 2),
            ("Defense +3",  80, 0, 3)]

items = []

for i, weapon in enumerate(weapons):
    for j, armor in enumerate(armors):
        for p, ringl in enumerate(rings):
            for q, ringr in enumerate(rings):
                if p == q:
                    continue
                cost = weapon[CST] + armor[CST] + ringl[CST] + ringr[CST]
                items.append((cost, i, j, p, q))

items.sort(key=lambda x: x[0])
# print(items)


def part1():
    for cost, we, ar, r1, r2 in items:
        plyr_damage = weapons[we][DMG] + rings[r1][DMG] + rings[r2][DMG]
        plyr_armor  =  armors[ar][AMR] + rings[r1][AMR] + rings[r2][AMR]
        plyr_attack = max(1, plyr_damage - boss_armor)
        boss_attack = max(1, boss_damage - plyr_armor)
        rounds_needed = math.ceil(boss_hitpoint // plyr_attack)
        rounds_alive  = math.ceil(plyr_hitpoint // boss_attack)
        if rounds_alive >= rounds_needed:
            return cost


def part2():
    max_gold = 0
    for cost, we, ar, r1, r2 in items:
        plyr_damage = weapons[we][DMG] + rings[r1][DMG] + rings[r2][DMG]
        plyr_armor  =  armors[ar][AMR] + rings[r1][AMR] + rings[r2][AMR]
        plyr_attack = max(1, plyr_damage - boss_armor)
        boss_attack = max(1, boss_damage - plyr_armor)
        rounds_needed = math.ceil(boss_hitpoint // plyr_attack)
        rounds_alive  = math.ceil(plyr_hitpoint // boss_attack)
        if rounds_alive < rounds_needed:
            max_gold = max(max_gold, cost)
    return max_gold


print(part1())
print(part2())
