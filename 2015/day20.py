import sys


inp = int(sys.stdin.readline())


def sum_of_divisors(n):
    result = 0
    d = 1
    while d ** 2 < n:
        if n % d == 0:
            result += d + n // d
        d += 1
    if d ** 2 == n:
        result += d
    return result


def gifts(n):
    result = 0
    d = 1
    while d ** 2 < n:
        if n % d == 0:
            if n // d <= 50:
                result += d
            if d <= 50:
                result += n // d
        d += 1
    if d ** 2 == n:
        result += d
    return result


def part1(inp=inp):
    i = 2
    while True:
        if sum_of_divisors(i) * 10 > inp:
            return i
        i += 1


def part2(inp=inp):
    i = 2
    while True:
        if gifts(i) * 11 > inp:
            return i
        i += 1


print(part1())
print(part2())
