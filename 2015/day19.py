import sys


rules = {}
rev_rules = {}


while True:
    rule = sys.stdin.readline()
    if rule == "\n":
        break
    rule = rule.strip().split(" => ")
    rules[rule[0]] = rules.get(rule[0], []) + [rule[1]]
    rev_rules[rule[1]] = rule[0]

medicine = sys.stdin.readline().strip()


def find_all(a_str, sub):
    start = 0
    while True:
        start = a_str.find(sub, start)
        if start == -1: return
        yield start
        start += len(sub)


visited = set()


def dfs(molecule=medicine, depth=0):
    global rev_rules, visited
    if molecule in visited:
        return
    visited.add(molecule)
    if rev_rules.get(molecule, "") == "e":
        return depth + 1
    for i, mlcl in enumerate(molecule):
        for r in rev_rules:
            if molecule.startswith(r, i):
                result = dfs(molecule[:i] + rev_rules[r] + molecule[i + len(r):], depth + 1)
                if result is not None:
                    return result


def part1():
    results = set()
    for atom in rules:
        for ind in find_all(medicine, atom):
            for rep in rules[atom]:
                results.add(f"{medicine[:ind]}{rep}{medicine[ind + len(atom):]}")
    return len(results)


def part2():
    return dfs()


print(part1())
print(part2())
