import sys


inp = [line.strip() for line in sys.stdin]


def part1(lines=inp):
    def is_nice(line):
        contains_dup = False
        num_vowles = int("aeiou".find(line[0]) >= 0)
        for i in range(1, len(line)):
            num_vowles += int("aeiou".find(line[i]) >= 0)
            if line[i - 1: i + 1] in ["ab", "cd", "pq", "xy"]:
                return False
            elif line[i] == line[i - 1]:
                contains_dup = True
        return contains_dup and (num_vowles > 2)

    return sum([is_nice(line) for line in lines])


def part2(lines=inp):
    def is_nice(line):
        contains_rep = False
        contains_aba = False
        pairs = [line[0 : 2]]
        for i in range(2, len(line)):
            if line[i : i + 2] in pairs:
                contains_rep = True
            pairs.append(line[i - 1 : i + 1])
            if line[i] == line[i - 2]:
                contains_aba = True
        return contains_rep and contains_aba

    return sum([is_nice(line) for line in lines])


print(part1())
print(part2())
