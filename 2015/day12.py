import sys
import json


inp = json.loads(sys.stdin.read())


def sum_all(obj, exclude=""):
    result = 0
    if type(obj) == int:
        return obj
    elif type(obj) == list:
        for i in obj:
            result += sum_all(i, exclude)
    elif type(obj) == dict and exclude not in obj.values():
        for i in obj:
            result += sum_all(obj[i], exclude)
    return result


def part1(inp=inp):
    return sum_all(inp)


def part2(inp=inp):
    return sum_all(inp, exclude="red")


print(part1())
print(part2())
