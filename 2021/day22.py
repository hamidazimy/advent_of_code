import sys
from scanf import scanf

inp = [scanf("%s x=%d..%d,y=%d..%d,z=%d..%d", l.strip()) for l in sys.stdin]
steps = [(line[0] == "on", tuple(line[1:])) for line in inp]

A, X, B, Y, C, Z = 0, 1, 2, 3, 4, 5


def show(qbs):
    import numpy as np
    import matplotlib.pyplot as plt
    x, y, z = np.indices((10, 10, 10))
    voxelarray = np.zeros_like(x)
    colors = np.empty(voxelarray.shape + (4,))
    for qb in qbs:
        cube = (qb[A] <= x) & (x <= qb[X]) & (qb[B] <= y) & (y <= qb[Y]) & (qb[C] <= z) & (z <= qb[Z])
        voxelarray = voxelarray | cube
        colors[cube] = np.concatenate((np.random.rand(3), [.5]), axis=None)
    ax = plt.figure().add_subplot(projection='3d')
    ax.voxels(voxelarray, facecolors=colors, edgecolor=(.5, .5, .5, .5))
    plt.show()


def is_inside(qb2, qb1):
    """
    Checks whether qb1 is ENTIRELY inside qb2 or not.
    """
    if qb2[A] <= qb1[A] and qb2[X] >= qb1[X] and \
       qb2[B] <= qb1[B] and qb2[Y] >= qb1[Y] and \
       qb2[C] <= qb1[C] and qb2[Z] >= qb1[Z]:
        return True
    return False


def is_outside(qb2, qb1):
    """
    Checks whether qb1 is ENTIRELY outside qb2 or not.
    """
    if qb2[X] < qb1[A] or qb2[A] > qb1[X] or \
       qb2[Y] < qb1[B] or qb2[B] > qb1[Y] or \
       qb2[Z] < qb1[C] or qb2[C] > qb1[Z]:
        return True
    return False


def break_into_pieces(qb1, qb2):
    """
    Breaks qb1 with respect to qb2.
    If they don't have any overlap at all, returns qb1 in one piece.
    If qb1 is entirely inside qb2, returns no pieces.
    If they have partial overlap, qb1 breaks into (up to) six non-overlapping pieces.
    """
    if is_outside(qb2, qb1):
        return [qb1]
    if is_inside(qb2, qb1):
        return []
    pieces = []
    if qb1[A] < qb2[A]:
        pieces.append((qb1[A], qb2[A] - 1, qb1[B], qb1[Y], qb1[C], qb1[Z]))
    if qb1[X] > qb2[X]:
        pieces.append((qb2[X] + 1, qb1[X], qb1[B], qb1[Y], qb1[C], qb1[Z]))
    if qb1[B] < qb2[B]:
        pieces.append((max(qb1[A], qb2[A]), min(qb1[X], qb2[X]), qb1[B], qb2[B] - 1, qb1[C], qb1[Z]))
    if qb1[Y] > qb2[Y]:
        pieces.append((max(qb1[A], qb2[A]), min(qb1[X], qb2[X]), qb2[Y] + 1, qb1[Y], qb1[C], qb1[Z]))
    if qb1[C] < qb2[C]:
        pieces.append((max(qb1[A], qb2[A]), min(qb1[X], qb2[X]), max(qb1[B], qb2[B]), min(qb1[Y], qb2[Y]), qb1[C], qb2[C] - 1))
    if qb1[Z] > qb2[Z]:
        pieces.append((max(qb1[A], qb2[A]), min(qb1[X], qb2[X]), max(qb1[B], qb2[B]), min(qb1[Y], qb2[Y]), qb2[Z] + 1, qb1[Z]))
    return pieces


def solve(limit):
    init_proc_area = (-limit, limit, -limit, limit, -limit, limit)
    qbs = []
    for turn_on, new in steps:
        if is_outside(init_proc_area, new):
            continue
        qbs_pieces = []
        for old in qbs:
            qbs_pieces += break_into_pieces(old, new)
        qbs = qbs_pieces + ([new] if turn_on else [])
    return sum([(qb[X] - qb[A] + 1) * (qb[Y] - qb[B] + 1) * (qb[Z] - qb[C] + 1) for qb in qbs])


def part1():
    return solve(limit=50)


def part2():
    return solve(limit=100000)


print(part1())
print(part2())
