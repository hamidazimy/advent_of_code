import sys
import numpy as np
from scipy import stats
from functools import reduce

inp = np.array([[int(x) for x in l.strip()] for l in sys.stdin], dtype=np.int8)


def vec_to_num(vec):
    return reduce(lambda a, b: 2 * a + b, vec)


def part1(inp=inp):
    n = len(inp)

    g = vec_to_num(np.array(np.sum(inp, axis=0) > (n // 2), dtype=np.int8))
    e = vec_to_num(np.array(np.sum(inp, axis=0) < (n // 2), dtype=np.int8))

    return g * e


def part2(inp=inp):
    h, w = np.shape(inp)

    o = np.copy(inp)
    for i in range(w):
        g = 1 - stats.mode(1 - o[:, i])[0]
        o = o[o[:, i] == g[0], :]
        if len(o) == 1:
            break
    o = vec_to_num(o.ravel())

    c = np.copy(inp)
    for i in range(w):
        e = stats.mode(1 - c[:, i])[0]
        c = c[c[:, i] == e[0], :]
        if len(c) == 1:
            break
    c = vec_to_num(c.ravel())

    return o * c


print(part1())
print(part2())
