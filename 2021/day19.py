import sys
import numpy as np

I, J = 0, 1

rotate_x = np.array(
    [[[ 1, 0, 0],
      [ 0, 1, 0],
      [ 0, 0, 1]],

     [[ 1, 0, 0],
      [ 0, 0, 1],
      [ 0,-1, 0]],

     [[ 1, 0, 0],
      [ 0,-1, 0],
      [ 0, 0,-1]],

     [[ 1, 0, 0],
      [ 0, 0,-1],
      [ 0, 1, 0]]], dtype=np.int32)

change_x = np.array(
    [[[ 1, 0, 0],
      [ 0, 1, 0],
      [ 0, 0, 1]],

     [[ 0, 1, 0],
      [ 0, 0, 1],
      [ 1, 0, 0]],

     [[ 0, 0,-1],
      [ 1, 0, 0],
      [ 0,-1, 0]],

     [[-1, 0, 0],
      [ 0, 1, 0],
      [ 0, 0, -1]],

     [[ 0, 1, 0],
      [ 0, 0,-1],
      [-1, 0, 0]],

     [[ 0, 0, 1],
      [-1, 0, 0],
      [ 0,-1, 0]]], dtype=np.int32)

orientations = []
for c in range(len(change_x)):
    for r in range(len(rotate_x)):
        orientations.append(np.dot(change_x[c], rotate_x[r]))
orientations = np.array(orientations, dtype=np.int32)

inverse = {}
for i in range(24):
    inv = np.array(np.linalg.inv(orientations[i]), dtype=np.int32)
    j = np.argwhere(np.all(orientations == inv, axis=(1, 2)))[0, 0]
    inverse[i] = j

transition = np.zeros((24, 24), dtype=np.int32)
for i in range(24):
    for j in range(24):
        tra = np.dot(orientations[i], orientations[j])
        k = np.argwhere(np.all(orientations == tra, axis=(1, 2)))[0, 0]
        transition[i, j] = k

scanners = []
line = sys.stdin.readline()
while line != '':
    scanner = []
    line = sys.stdin.readline().strip()
    while line != '':
        scanner.append(line.split(','))
        line = sys.stdin.readline().strip()
    scanners.append(np.array(scanner, dtype=np.int32))
    line = sys.stdin.readline()

distances = []
for scanner in scanners:
    n = len(scanner)
    coords = np.reshape(scanner, (n, 1, 3))
    P = np.tile(np.reshape(scanner, (n, 1, 3)), (1, n, 1))
    Q = np.tile(np.reshape(scanner, (1, n, 3)), (n, 1, 1))
    distances.append(np.sum((P - Q) ** 2, axis=2))

matched = {i: {} for i in range(len(scanners))}  # matched beacons

rltv_oris = {i: {} for i in range(len(scanners))}  # relative orientations
for i in range(len(scanners)):
    for j in range(i + 1, len(scanners)):
        candidates = []
        for k in range(len(scanners[i])):
            for l in range(len(scanners[j])):
                if len(set(distances[i][k, :]).intersection(set(distances[j][l, :]))) >= 12:
                    candidates.append((k, l))
        if len(candidates) >= 12:
            matched[i][j] = candidates
            si_b1 = scanners[i][candidates[0][I]]
            si_b2 = scanners[i][candidates[1][I]]
            veci = si_b1 - si_b2
            sj_b1 = scanners[j][candidates[0][J]]
            sj_b2 = scanners[j][candidates[1][J]]
            vecj = sj_b1 - sj_b2
            for o in range(24):
                vecf = np.dot(veci, orientations[o])
                if np.all(vecf == vecj):
                    rltv_oris[i][j] = o
                    rltv_oris[j][i] = inverse[o]

oris = [None] * len(scanners)
i = 0
oris[i] = 0
q = [0]
while None in oris:
    i = q.pop(0)
    for j in rltv_oris[i]:
        o = rltv_oris[i][j]
        if oris[j] is None:
            oris[j] = transition[oris[i]][o]
            q.append(j)

for i in range(len(scanners)):
    scanners[i] = np.dot(scanners[i], orientations[inverse[oris[i]]])

rltv_offs = {i: {} for i in range(len(scanners))}
for i in matched:
    for j in matched[i]:
        if i > j:
            continue
        candidates = matched[i][j]
        si_b1 = scanners[i][candidates[0][I]]
        sj_b1 = scanners[j][candidates[0][J]]
        rltv_offs[i][j] = si_b1 - sj_b1
        rltv_offs[j][i] = sj_b1 - si_b1

offs = [None] * len(scanners)
offs_calculated = [False] * len(scanners)
i = 0
offs[i] = np.array([0, 0, 0])
offs_calculated[i] = True
q = [0]
while False in offs_calculated:
    i = q.pop(0)
    for j in rltv_offs[i]:
        offset = rltv_offs[i][j]
        if offs[j] is None:
            offs[j] = offs[i] + offset
            offs_calculated[j] = True
            q.append(j)

for i in range(len(scanners)):
    scanners[i] += offs[i]

for i in matched:
    for j in matched[i]:
        if i > j:
            continue
        candidates = matched[i][j]

        si_b1 = scanners[i][candidates[0][0]]
        sj_b1 = scanners[j][candidates[0][1]]
        rltv_offs[i][j] = si_b1 - sj_b1
        rltv_offs[j][i] = sj_b1 - si_b1


def part1():
    beacons = set()
    for scanner in scanners:
        for beacon in scanner:
            beacons.add(f"{beacon[0]},{beacon[1]},{beacon[2]}")
    return len(beacons)


def part2():
    max_dist = 0
    for i in range(len(scanners)):
        for j in range(len(scanners)):
            max_dist = max(max_dist, sum([abs(offs[i][d] - offs[j][d]) for d in range(3)]))
    return max_dist


print(part1())
print(part2())
