import sys
import numpy as np

inp = np.array([int(x) for x in sys.stdin.readline().strip().split(',')])


def solve1(inp, time):
    timers = np.copy(inp)
    for i in range(time):
        timers -= 1
        timers = np.append(timers, np.ones((np.sum(timers == -1)),) * 8)
        timers[np.where(timers == -1)] = 6
    return len(timers)


def solve2(inp, time):
    cycles = np.zeros((9,))
    for c in range(9):
        cycles[c] = np.sum(inp == c)
    for days in range(time):
        zeros = cycles[0]
        cycles = np.append(cycles[1:], [zeros])
        cycles[6] += zeros
    return np.sum(cycles)


def solve3(inp, time):
    cycles = np.zeros((9,), dtype=np.uint)
    for c in range(9):
        cycles[c] = np.sum(inp == c)
    ind0 = 0
    for days in range(time):
        cycles[ind0 - 2] += cycles[ind0]
        ind0 = (ind0 + 1) % 9
    return np.sum(cycles)


def part1(inp=inp):
    return solve3(inp, 80)


def part2(inp=inp):
    return solve3(inp, 256)


print(part1())
print(part2())
