import sys

inp = [line.strip() for line in sys.stdin]


def part1(inp=inp):
    points = {')': 3, ']': 57, '}': 1197, '>': 25137}
    result = 0
    for line in inp:
        stack = []
        for c in line:
            if c in '([{<':
                stack.append(c)
            elif 0 < ord(c) - ord(stack[-1]) < 3:
                stack.pop()
            else:
                result += points[c]
                break
    return result


def part2(inp=inp):
    points = {'(': 1, '[': 2, '{': 3, '<': 4}
    scores = []
    for line in inp:
        stack = []
        corrupt = False
        for c in line:
            if c in '([{<':
                stack.append(c)
            elif 0 < ord(c) - ord(stack[-1]) < 3:
                stack.pop()
            else:
                corrupt = True
                break
        if corrupt:
            continue
        score = 0
        for r in stack[::-1]:
            score *= 5
            score += points[r]
        scores.append(score)
    scores.sort()
    return scores[len(scores) // 2]


print(part1())
print(part2())
