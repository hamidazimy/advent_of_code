import sys

inp = [line.strip().split('-') for line in sys.stdin]
adj = {}
for i, j in inp:
    if i != 'end' and j != 'start':
        adj[i] = adj.get(i, []) + [j]
    if i != 'start' and j != 'end':
        adj[j] = adj.get(j, []) + [i]


def part1(adj=adj):
    paths = []
    def dfs(path=['start']):
        if path[-1] == 'end':
            paths.append(path)
            return
        for nei in adj[path[-1]]:
            if nei.isupper() or nei not in path:
                dfs(path + [nei])
    dfs()
    return len(paths)


def part2(adj=adj):
    paths = []
    def dfs(path=['start'], one_small_visited_twice=False):
        if path[-1] == 'end':
            paths.append(path)
            return
        for nei in adj[path[-1]]:
            if nei.isupper() or nei not in path:
                dfs(path + [nei], one_small_visited_twice)
            elif not one_small_visited_twice:
                dfs(path + [nei], True)
    dfs()
    return len(paths)


print(part1())
print(part2())
