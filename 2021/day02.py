import sys

inp = [line.strip().split() for line in sys.stdin]


def part1(inp=inp):
    depth = 0
    distance = 0
    for command, steps in inp:
        if command == "forward":
            distance += int(steps)
        elif command == "down":
            depth += int(steps)
        elif command == "up":
            depth -= int(steps)
    return depth * distance


def part2(inp=inp):
    depth = 0
    distance = 0
    aim = 0
    for command, steps in inp:
        if command == "forward":
            distance += int(steps)
            depth += aim * int(steps)
        elif command == "down":
            aim += int(steps)
        elif command == "up":
            aim -= int(steps)
    return depth * distance


print(part1())
print(part2())
