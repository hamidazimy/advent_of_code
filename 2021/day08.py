import sys
from itertools import permutations

inp = [line.strip() for line in sys.stdin]


def part1(inp=inp):
    outputs = [line.split(" | ")[1].split() for line in inp]
    result = 0
    for i in outputs:
        for j in i:
            if len(j) in [2, 3, 4, 7]:
                result += 1
    return result


#   tttt
#  u    v
#  u    v
#   wwww
#  x    y
#  x    y
#   zzzz

sevseg =    [ set(list( "TUVXYZ"))  # 0
            , set(list(     "VY"))  # 1
            , set(list(  "TVWXZ"))  # 2
            , set(list(  "TVWYZ"))  # 3
            , set(list(   "UVWY"))  # 4
            , set(list(  "TUWYZ"))  # 5
            , set(list( "TUWXYZ"))  # 6
            , set(list(    "TVY"))  # 7
            , set(list("TUVWXYZ"))  # 8
            , set(list( "TUVWYZ"))  # 9
            ]


def backtrack(signal):
    #  but it's actually a recursive bruteforce!!
    def check(key):
        for s in signal:
            for i in range(7):
                s = s.replace(key[i], "TUVWXYZ"[i])
            if not s.isupper():
                continue
            if set(list(s)) not in sevseg:
                return False
        return True

    def dfs(key=list("#######")):
        if '#' not in key:
            if check(key):
                return key
        rs = [x for x in "abcdefg" if x not in key]
        for i in range(7):
            if key[i] != '#':
                continue
            for r in rs:
                key[i] = r
                res = dfs(key)
                if res is not None:
                    return res
                key[i] = '#'
            break
    return dfs()


def bruteforce(signal):
    for key in permutations(list("abcdefg")):
        found = True
        for s in signal:
            for i in range(7):
                s = s.replace(key[i], "TUVWXYZ"[i])
            if set(list(s)) not in sevseg:
                found = False
        if found:
            return key


def part2(inp=inp):
    signals = [line.split(" | ")[0].split() for line in inp]
    outputs = [line.split(" | ")[1].split() for line in inp]
    result = 0
    for l in range(len(signals)):
        key = backtrack(signals[l])
        output_value = 0
        for s in outputs[l]:
            for i in range(7):
                s = s.replace(key[i], "TUVWXYZ"[i])
            output_value = output_value * 10 + sevseg.index(set(s))
        # print(key, output_value)
        result += output_value
    return result


print(part1())
print(part2())
