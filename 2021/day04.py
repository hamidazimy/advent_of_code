import sys
import numpy as np

inp = [int(x) for x in sys.stdin.readline().strip().split(',')]
boards = []
while sys.stdin.readline():
    boards.append([[int(x) for x in sys.stdin.readline().strip().split()] for i in range(5)])
boards = np.array(boards)


def part1(inp=inp):
    called = np.zeros_like(boards)
    for i in inp:
        called += (boards == i)
        rows = set(np.where(np.sum(called, axis=2) == 5)[0])
        cols = set(np.where(np.sum(called, axis=1) == 5)[0])
        winner = list(rows.union(cols))
        if len(winner) == 1:
            return i * (np.sum((1 - called[winner[0]]) * boards[winner[0]]))


def part2(inp=inp):
    called = np.zeros_like(boards)
    all_boards = set(list(range(len(boards))))
    loser = -1
    for i in inp:
        called += (boards == i)
        rows = set(np.where(np.sum(called, axis=2) == 5)[0])
        cols = set(np.where(np.sum(called, axis=1) == 5)[0])
        loser_boards = list(all_boards.difference(rows.union(cols)))
        if len(loser_boards) == 1:
            loser = loser_boards[0]
        if len(loser_boards) == 0:
            return i * (np.sum((1 - called[loser]) * boards[loser]))


print(part1())
print(part2())
