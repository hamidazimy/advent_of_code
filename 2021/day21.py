import sys

p1 = int(sys.stdin.readline().strip().split(": ")[1])
p2 = int(sys.stdin.readline().strip().split(": ")[1])


def part1(p1=p1, p2=p2):
    s1 = 0
    s2 = 0

    dy = 1
    while s2 < 1000:
        p1 = (p1 - 1 + 3 * ((dy - 1) % 100 + 1) + 3) % 10 + 1
        dy += 3
        s1 += p1
        if s1 >= 1000:
            break
        p2 = (p2 - 1 + 3 * ((dy - 1) % 100 + 1) + 3) % 10 + 1
        dy += 3
        s2 += p2
    return min(s1, s2) * (dy - 1)


def play(p, s, d):
    p_ = (p - 1 + d) % 10 + 1
    s_ = s + p_
    return p_, s_


def part2(p1=p1, p2=p2):
    dy3 = {3: 1, 4: 3, 5: 6, 6: 7, 7: 6, 8: 3, 9: 1}
    states = {(p1, 0, p2, 0, 0): 1}
    win_pts = 21
    w1, w2 = 0, 0
    for s1c in range(win_pts):
        for s2c in range(win_pts):
            for p1c in range(1, 11):
                for p2c in range(1, 11):
                    for t in range(2):
                        n = states.get((p1c, s1c, p2c, s2c, t), 0)
                        if n != 0:
                            if t == 0:
                                for d in dy3:
                                    p1_ , s1_ = play(p1c, s1c, d)
                                    states[(p1_, s1_, p2c, s2c, 1 - t)] = states.get((p1_, s1_, p2c, s2c, 1 - t), 0) + n * dy3[d]
                                    if s1_ > 20 and s2c < 21:
                                        w1 += n
                            else:
                                for d in dy3:
                                    p2_, s2_ = play(p2c, s2c, d)
                                    states[(p1c, s1c, p2_, s2_, 1 - t)] = states.get((p1c, s1c, p2_, s2_, 1 - t), 0) + n * dy3[d]
                                    if s2_ > 20 and s1c < 21:
                                        w2 += n
    w1, w2 = 0, 0
    for s1c in range(win_pts, win_pts + 10):
        for s2c in range(win_pts):
            for p1c in range(1, 11):
                for p2c in range(1, 11):
                    w1 += states.get((p1c, s1c, p2c, s2c, 1), 0)

    for s1c in range(win_pts):
        for s2c in range(win_pts, win_pts + 10):
            for p1c in range(1, 11):
                for p2c in range(1, 11):
                    w2 += states.get((p1c, s1c, p2c, s2c, 0), 0)
    return max(w1, w2)


print(part1())
print(part2())
