import sys
from scanf import scanf

xmin, xmax, ymin, ymax = scanf("target area: x=%d..%d, y=%d..%d", sys.stdin.readline())


def part1():
    # QUICK MAFS!!
    # for the max height, vy should be positive
    # the higher vy, the higher max height
    # with a positive vy, the probe will be back at y = 0 with -vy - 1
    # what is the highest vy that the probe still hit the box in return?
    # -vy - 1 = ymin
    vy = -ymin - 1
    max_height = vy * (vy + 1) // 2
    return max_height


def check(vx, vy):
    max_height = 0
    x, y = 0, 0
    while x < xmax and y > ymin:
        x, y = x + vx, y + vy
        max_height = max(max_height, y)
        vx, vy = max(0, vx - 1), vy - 1
        if xmin <= x <= xmax and ymin <= y <= ymax:
            return max_height
    return None


def part2():
    x_l = int((2 * xmin) ** .5)
    x_u = xmax + 1
    y_l = ymin - 1
    y_u = 1 - ymin
    all_number = 0
    max_height = 0
    for vx in range(x_l, x_u + 1):
        for vy in range(y_l, y_u + 1):
            height = check(vx, vy)
            if height is not None:
                all_number += 1
                max_height = max(max_height, height)
    # print(max_height)
    return all_number


print(part1())
print(part2())
