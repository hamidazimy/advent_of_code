import sys
import numpy as np

visualize = "-v" in sys.argv

legend = {".": 0, ">": 1, "v": 2}

map = np.array([[legend[c] for c in line.strip()] for line in sys.stdin])
H, W = np.shape(map)

def print_map(map, step):
    if not visualize:
        return
    H, W = np.shape(map)
    colors = ["\x1b[37m", "\x1b[38;2;0;170;0m", "\x1b[38;2;35;209;139m"]
    map_str = "\x1bc" + f"After {step} steps:\n"
    for y in range(H):
        for x in range(W):
            map_str += f"{colors[map[y, x]]}██"
        map_str += "\x1b[0m\n"
    print(map_str, flush=True)

step = 0

while True:
    new = np.copy(map)
    print_map(new, step)
    step += 1

    for y in range(-1, -H - 1, -1):
        for x in range(-1, -W - 1, -1):
            if map[y, x] == 1 and map[y, x + 1] == 0:
                new[y, x + 1] = 1
                new[y, x] = 0

    changed = np.any(new != map)

    map = new
    new = np.copy(map)

    for y in range(-1, -H - 1, -1):
        for x in range(-1, -W - 1, -1):
            if map[y, x] == 2 and map[y + 1, x] == 0:
                new[y + 1, x] = 2
                new[y, x] = 0

    changed = changed or np.any(new != map)

    if not changed:
        print_map(new, step)
        print(step)
        break

    map = new
