import sys

inp = [int(line.strip()) for line in sys.stdin]


def part1(inp=inp):
    result = 0
    for i in range(1, len(inp)):
        if inp[i] > inp[i - 1]:
            result += 1
    return result


def part2(inp=inp):
    result = 0
    for i in range(3, len(inp)):
        if inp[i] > inp[i - 3]:
            result += 1
    return result


print(part1())
print(part2())
