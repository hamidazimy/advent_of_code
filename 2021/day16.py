import sys
from functools import reduce

bit_stream = ''.join([bin(int(h, 16))[2:].zfill(4) for h in sys.stdin.readline().strip()])

ver_sum = 0


def decode(bit_stream):
    global ver_sum
    ver = int(bit_stream[0:3], 2)
    ver_sum += ver
    tid = int(bit_stream[3:6], 2)
    bit_stream = bit_stream[6:]
    value = 0
    if tid == 4:  # literal value
        literal = ""
        last_group = False
        while not last_group:
            last_group = bit_stream[0] == '0'
            literal += bit_stream[1:5]
            bit_stream = bit_stream[5:]
        value = int(literal, 2)
    else:  # operator
        lid = bit_stream[0]
        bit_stream = bit_stream[1:]
        sub_values = []
        if lid == '0':  # total length in bits
            length = int(bit_stream[0:15], 2)
            bit_stream = bit_stream[15:]
            sub_stream = bit_stream[:length]
            while len(sub_stream) > 0:
                sub_stream, sub_value = decode(sub_stream)
                sub_values.append(sub_value)
            bit_stream = bit_stream[length:]
        else:  # number of sub-packets immediately contained
            number = int(bit_stream[0:11], 2)
            bit_stream = bit_stream[11:]
            for i in range(number):
                bit_stream, sub_value = decode(bit_stream)
                sub_values.append(sub_value)
        if tid == 0:
            value = sum(sub_values)
        elif tid == 1:
            value = reduce(lambda x, y: x * y, sub_values)
        elif tid == 2:
            value = min(sub_values)
        elif tid == 3:
            value = max(sub_values)
        elif tid == 5:
            value = int(sub_values[0] > sub_values[1])
        elif tid == 6:
            value = int(sub_values[0] < sub_values[1])
        elif tid == 7:
            value = int(sub_values[0] == sub_values[1])

    return bit_stream, value


_, result = decode(bit_stream)


def part1():
    global ver_sum
    return ver_sum


def part2():
    global result
    return result


print(part1())
print(part2())
