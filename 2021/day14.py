import sys

template = sys.stdin.readline().strip()
sys.stdin.readline()
inp = [line.strip().split(' -> ') for line in sys.stdin]
insertion = {}
for i, j in inp:
    insertion[i] = j


def part1(template=template):
    before = template
    for n in range(10):
        after = ""
        for i in range(len(before) - 1):
            after += before[i]
            after += insertion[before[i : i + 2]]
        after += before[-1]
        before = after
    elem_count = {}
    for i in after:
        elem_count[i] = elem_count.get(i, 0) + 1
    return max(elem_count.values()) - min(elem_count.values())


def part2(template=template):
    global insertion
    pair_count = {}
    for i in range(len(template) - 1):
        pair_count[template[i : i + 2]] = pair_count.get(template[i : i + 2], 0) + 1
    for n in range(40):
        new_pair_count = {}
        for p in pair_count:
            pair1 = f"{p[0]}{insertion[p]}"
            pair2 = f"{insertion[p]}{p[1]}"
            new_pair_count[pair1] = new_pair_count.get(pair1, 0) + pair_count.get(p, 0)
            new_pair_count[pair2] = new_pair_count.get(pair2, 0) + pair_count.get(p, 0)
        pair_count = new_pair_count
    elem_count = {}
    for p in pair_count:
        elem_count[p[0]] = elem_count.get(p[0], 0) + pair_count[p]
    elem_count[template[-1]] += 1
    return max(elem_count.values()) - min(elem_count.values())


print(part1())
print(part2())
