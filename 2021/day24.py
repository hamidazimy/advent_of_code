import sys

code = [line.strip().split() for line in sys.stdin]

regs = {'w': 0, 'x': 0, 'y': 0, 'z': 0}


def val(b):
    if b in regs:
        return regs[b]
    return int(b)


def inp(a, i):
    regs[a] = int(i)


def add(a, b):
    regs[a] += val(b)


def mul(a, b):
    regs[a] *= val(b)


def div(a, b):
    if val(b) == 0:
        raise ValueError()
    def sign(x):
        return 0 if x == 0 else x // abs(x)
    regs[a] = int(sign(regs[a] / val(b)) * (abs(regs[a]) // abs(val(b))))


def mod(a, b):
    if regs[a] < 0 or val(b) <= 0:
        raise ValueError()
    regs[a] %= val(b)


def eql(a, b):
    regs[a] = int(regs[a] == val(b))


def show_z():
    z = regs['z']
    result = ""
    while z != 0:
        result = f"[{z % 26:02}]" + result
        z //= 26
    return result


def run(model):
    global regs
    regs = {'w': 0, 'x': 0, 'y': 0, 'z': 0}
    i = 0
    try:
        for ins in code:
            if ins[0] == "inp":
                if len(ins) < 3:
                    ins.append("\t")
                # print("=" * 80)
                inp(ins[1], model[i])
                i += 1
            elif ins[0] == "add":
                add(ins[1], ins[2])
            elif ins[0] == "mul":
                mul(ins[1], ins[2])
            elif ins[0] == "div":
                div(ins[1], ins[2])
            elif ins[0] == "mod":
                mod(ins[1], ins[2])
            elif ins[0] == "eql":
                eql(ins[1], ins[2])
            # print(' '.join(ins), "\t| ", ' '.join([f"{k}: {regs[k]:<12d}" for k in regs]), show_z())
        return regs['z']
    except ValueError:
        print("Error!!")
        return


def part1(code=code):
    stack = []
    MONAD_max = [''] * 14
    MONAD_min = [''] * 14
    for i in range(14):
        check = int(code[i * 18 +  5][2])
        value = int(code[i * 18 + 15][2])
        if check > 0:
            stack.append((value, i))
        else:
            value, j = stack.pop()
            diff = value + check
            if diff < 0:
                MONAD_max[j] = str(9)
                MONAD_max[i] = str(9 + diff)
            else:
                MONAD_max[j] = str(9 - diff)
                MONAD_max[i] = str(9)
    return ''.join(MONAD_max)


def part2(code=code):
    stack = []
    MONAD_max = [''] * 14
    MONAD_min = [''] * 14
    for i in range(14):
        check = int(code[i * 18 +  5][2])
        value = int(code[i * 18 + 15][2])
        if check > 0:
            stack.append((value, i))
        else:
            value, j = stack.pop()
            diff = value + check
            if diff < 0:
                MONAD_max[j] = str(1 - diff)
                MONAD_max[i] = str(1)
            else:
                MONAD_max[j] = str(1)
                MONAD_max[i] = str(1 + diff)
    return ''.join(MONAD_max)


print(part1())
print(part2())
