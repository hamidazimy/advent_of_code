import sys
import numpy as np
from scanf import scanf

inp = np.array([list(scanf("%d,%d -> %d,%d\n", line)) for line in sys.stdin])
N = np.max(inp)
map = np.zeros((N + 1, N + 1))


def part1(inp=inp):
    global map
    for x1, y1, x2, y2 in inp:
        x1, x2 = min(x1, x2), max(x1, x2)
        y1, y2 = min(y1, y2), max(y1, y2)
        if x1 == x2:
            map[y1:y2 + 1, x1] += 1
        if y1 == y2:
            map[y1, x1:x2 + 1] += 1
    return np.sum(map >= 2)


def part2(inp=inp):
    global map
    for x1, y1, x2, y2 in inp:
        if x1 != x2 and y1 != y2:
            dx = 2 * (x1 < x2) - 1
            dy = 2 * (y1 < y2) - 1
            xc, yc = x1, y1
            while xc * dx <= x2 * dx:
                map[yc, xc] += 1
                xc += dx
                yc += dy
    return np.sum(map >= 2)


print(part1())
print(part2())
