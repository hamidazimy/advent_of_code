import sys
from queue import PriorityQueue

GOAL = (0 , 0 , 0 , 0 , 0 , 0 , 0 \
            , 1 , 2 , 3 , 4 \
            , 1 , 2 , 3 , 4 \
            , 1 , 2 , 3 , 4 \
            , 1 , 2 , 3 , 4 )

coords = [(0, 0), (0, 1), (0, 3), (0, 5), (0, 7), (0, 9), (0, 10),
                      (1, 2), (1, 4), (1, 6), (1, 8),
                      (2, 2), (2, 4), (2, 6), (2, 8),
                      (3, 2), (3, 4), (3, 6), (3, 8),
                      (4, 2), (4, 4), (4, 6), (4, 8)]

inp = [list(line.rstrip()) for line in sys.stdin]
init = [max(0, int(ord(inp[i + 1][j + 1]) - ord('A') + 1)) for i, j in coords[:15]]


def draw(state):
    global coords
    map = [list("#############"),
           list("#           #"),
           list("### # # # ###"),
           list("  # # # # #  "),
           list("  # # # # #  "),
           list("  # # # # #  "),
           list("  #########  ")]
    for s in range(len(coords)):
        i, j = coords[s]
        map[i + 1][j + 1] = ' ' if state[s] == 0 else chr(ord('A') - 1 + state[s])
    print('\n'.join([''.join(row) for row in map]))#[:4] + [map[6]]]))
    print()


def draw_path(prev):
    path = [(GOAL, 0)]
    while path[-1][0] in prev:
        path.append(prev[path[-1][0]])
    path = path[::-1]
    for step, dg in path:
        draw(step)


def is_empty(state, k):
    return state[k] == 0


def hlwy_spaces():
    return range(7)


def room_spaces():
    return range(7, len(coords))


def reachable_hlwy_spaces(state, k):
    for t in range((k + 1) % 4 + 1, -1, -1):  # available spaces to the left
        if is_empty(state, t):
            yield t
        else:
            break
    for t in range((k + 1) % 4 + 2, 7):  # available spaces to the right
        if is_empty(state, t):
            yield t
        else:
            break


def available_space_in_room(state, k):
    for t in range(len(state) - 5 + state[k], 6, -4):  # spaces in its room from the bottom
        if state[t] == state[k]:  # occupied by the same type of amph
            continue
        elif is_empty(state, t):
            return t
        break


def should_go_left(state, k):
    return k > state[k]


def hlwy_to_room_blocked(state, k):
    if should_go_left(state, k):
        return sum(state[state[k] + 1:k]) != 0
    else:
        return sum(state[k + 1:state[k] + 1]) != 0


def above_occupied(state, k):
    return sum(state[k - 4:6:-4]) > 0


def amph_settled(state, k):
    if state[k] != (((k - 7) % 4) + 1):  # if not in its room
        return False
    for s in state[k + 4::4]:  # spaces below in the room
        if s != state[k]:  # occupied by a different type of amph
            return False
    return True


def distance(k, l):
    return abs(coords[l][0] - coords[k][0]) + abs(coords[l][1] - coords[k][1])


def energy(amph):
    return 10 ** (amph - 1)


def swapped(state, k, t):
    new_state = list(state)
    new_state[t], new_state[k] = new_state[k], new_state[t]
    cost = distance(k, t) * energy(state[k])
    return tuple(new_state), cost


def neis(state):
    for k in room_spaces():  # move amphipods from rooms to hallways
        if is_empty(state, k) or amph_settled(state, k) or above_occupied(state, k):
            continue
        for t in reachable_hlwy_spaces(state, k):
            yield swapped(state, k, t)
    for k in hlwy_spaces():  # move amphipods from hallways to rooms
        if is_empty(state, k):
            continue
        if should_go_left(state, k):
            if hlwy_to_room_blocked(state, k):
                continue
            t = available_space_in_room(state, k)
            if t is not None:
                yield swapped(state, k, t)
        else:  # should go right!
            if hlwy_to_room_blocked(state, k):
                continue
            t = available_space_in_room(state, k)
            if t is not None:
                yield swapped(state, k, t)


def heuristic(state):
    global coords
    h = 0
    for r in range(1, 5):  # number of rooms
        amphs = state[18 + r:6:-4]  # amphs in the room from the bottom
        d = 0
        while d < 4 and amphs[d] == r:  # settled amphs
            d += 1
        # the energy required to move unsettled amphs from the top space in the room to the remaining spaces
        h += (4 - d) * (3 - d) // 2 * energy(r)
        while d < 4:
            if amphs[d] != 0:
                # the energy required to move the amph to the top space of its room
                h += ((4 - d) + max(1, abs(amphs[d] - r)) * 2 + 1) * energy(amphs[d])
            d += 1
    for k in hlwy_spaces():
        if not is_empty(state, k):
            # the energy required to move the amph to the top space of its room
            h += (abs(coords[k][1] - state[k] * 2) + 1) * energy(state[k])
    return h


def a_star(init):
    visited = set()
    prev = {}
    pq = PriorityQueue()
    h = heuristic(init)
    pq.put((h, 0, h, init))
    while True:
        f, g, h, state = pq.get()
        if state in visited:
            continue
        if state == GOAL:
            return g, prev
        visited.add(state)
        for nei, dg in neis(state):
            g_ = g + dg
            h_ = heuristic(nei)
            f_ = g_ + h_
            pq.put((f_, g_, h_, nei))
            prev[nei] = (state, dg)


def part1(init=init):
    init1 = tuple(init + [1, 2, 3, 4, 1, 2, 3, 4])
    cost, prev = a_star(init1)
    return cost


def part2(init=init):
    init2 = tuple(init[:-4] + [4, 3, 2, 1, 4, 2, 1, 3] + init[-4:])
    cost, prev = a_star(init2)
    return cost


print(part1())
print(part2())
