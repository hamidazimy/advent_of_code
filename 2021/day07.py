import sys
import numpy as np

inp = np.array([int(x) for x in sys.stdin.readline().strip().split(',')])


def part1(inp=inp):
    return int(np.sum(np.abs(inp - np.median(inp))))
    # import matplotlib.pyplot as plt
    # nmin = np.min(inp)
    # nmax = np.max(inp)
    # best = float('Inf')
    # l = []
    # for i in range(nmin, nmax):
    #     fuel = np.sum(np.abs(inp - i))
    #     best = min(best, fuel)
    #     l.append(fuel)
    # plt.figure(figsize=(6, 4))
    # plt.plot(list(range(nmin, nmax)), l)
    # plt.show()
    # return best


def part2(inp=inp):
    mean = np.mean(inp)
    def error(c):
        dists = np.abs(inp - c)
        return np.sum(np.abs(dists * (dists + 1) // 2))
    return min(error(int(mean)), error(int(mean) + 1))
    # import matplotlib.pyplot as plt
    # nmin = np.min(inp)
    # nmax = np.max(inp)
    # best = float('Inf')
    # l = []
    # for i in range(nmin, nmax):
    #     dists = np.abs(inp - i)
    #     fuel = np.sum((dists * (dists + 1)) // 2)
    #     best = min(best, fuel)
    #     l.append(fuel)
    # plt.figure(figsize=(6, 4))
    # plt.plot(list(range(nmin, nmax)), l)
    # plt.show()
    # return best


print(part1())
print(part2())
