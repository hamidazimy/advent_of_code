import sys

inp = [list(line.strip()) for line in sys.stdin]


def add(a, b):
    return process(['['] + a + [','] + b + [']'])


def process(n):
    while True:
        level = 0
        needs_explode = False
        for i in range(len(n)):
            if n[i] == '[':
                level += 1
            elif n[i] == ']':
                level -= 1
            if level > 4:
                needs_explode = True
                for j in range(i, len(n)):
                    if n[j] == ']':
                        break
                break
        if needs_explode:
            n = explode(n, i, j)
            continue
        needs_split = False
        for k in range(len(n)):
            if len(n[k]) > 1:
                needs_split = True
                break
        if needs_split:
            n = split(n, k)
            continue
        break
    return n


def explode(n, i, j):
    x, y = int(n[i + 1]), int(n[j - 1])
    for p in range(i, 0, -1):
        if n[p].isdigit():
            n[p] = str(int(n[p]) + x)
            break
    for q in range(j, len(n)):
        if n[q].isdigit():
            n[q] = str(int(n[q]) + y)
            break
    n = n[:i] + ['0'] + n[j + 1:]
    return n


def split(n, k):
    z = int(n[k])
    n = n[:k] + ['[', str(z // 2), ',', str((z + 1) // 2), ']'] + n[k + 1:]
    return n


def magnitude(n):
    while len(n) > 1:
        b, e = 0, 0
        for m in range(len(n)):
            if n[m] == '[':
                b = m
            elif n[m] == ']':
                e = m
                break
        x, y = int(n[b + 1]), int(n[e - 1])
        n = n[:b] + [str(x * 3 + y * 2)] + n[e + 1:]
    return int(n[0])


def part1(inp=inp):
    result = inp[0]
    for s in range(1, len(inp)):
        result = add(result, inp[s])
    return magnitude(result)


def part2(inp=inp):
    result = 0
    for n1 in range(len(inp)):
        for n2 in range(len(inp)):
            if n2 == n1:
                continue
            result = max(result, magnitude(add(inp[n1], inp[n2])))
    return result


print(part1())
print(part2())
