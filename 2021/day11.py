import sys
import numpy as np

inp = [[int(x) for x in line.strip()] for line in sys.stdin]
H, W = len(inp), len(inp[0])
map = np.zeros((H + 2, W + 2), dtype=np.uint32)
map[1:-1, 1:-1] = np.array(inp, dtype=np.uint32)

shade = ["\x1b[38;2;255;85;0m"
        ,"\x1b[38;2;255;102;26m"
        ,"\x1b[38;2;255;119;51m"
        ,"\x1b[38;2;255;136;77m"
        ,"\x1b[38;2;255;153;102m"
        ,"\x1b[38;2;255;170;128m"
        ,"\x1b[38;2;255;187;153m"
        ,"\x1b[38;2;255;204;179m"
        ,"\x1b[38;2;255;221;204m"
        ,"\x1b[38;2;255;238;230m"
        ,"\x1b[38;2;255;255;255m"]


shade = ["\x1b[38;2;56;56;56m"
        ,"\x1b[38;2;76;76;76m"
        ,"\x1b[38;2;96;96;96m"
        ,"\x1b[38;2;116;116;116m"
        ,"\x1b[38;2;136;136;136m"
        ,"\x1b[38;2;156;156;156m"
        ,"\x1b[38;2;176;176;176m"
        ,"\x1b[38;2;196;196;196m"
        ,"\x1b[38;2;216;216;216m"
        ,"\x1b[38;2;236;236;236m"
        ,"\x1b[38;2;256;256;256m"]


def print_map(map):
    from time import sleep
    map_string = "\x1bc"
    map_string = "\n\n\n"
    for i in range(1, 11):
        for j in range(1, 11):
            map_string += f"{shade[map[i, j] - 1]}██"
        map_string += "\n"
    print(map_string)
    sleep(.1)


def part1(map=map):
    total_flashes = 0
    for step in range(100):
        map += 1
        flashy = np.zeros_like(map)
        step_flashes = 0
        while True:
            old_step_flashes = step_flashes
            nines = np.argwhere(map[1:-1, 1:-1] >= 10)
            if len(nines) == 0:
                break
            for y, x in nines:
                if flashy[y + 1, x + 1] == 0:
                    map[y:y + 3, x: x + 3] += np.ones((3, 3), dtype=np.uint32)
                    flashy[y + 1, x + 1] += 1
                    step_flashes += 1
            if step_flashes == old_step_flashes:
                break
        total_flashes += step_flashes
        map = np.where(map > 9, 0, map)
    return total_flashes


def part2(map=map):
    total_flashes = 0
    step = 1
    while True:
        step += 1
        map += 1
        flashy = np.zeros_like(map)
        step_flashes = 0
        while True:
            old_step_flashes = step_flashes
            nines = np.argwhere(map[1:-1, 1:-1] >= 10)
            if len(nines) == 0:
                break
            for y, x in nines:
                if flashy[y + 1, x + 1] == 0:
                    map[y:y + 3, x: x + 3] += np.ones((3, 3), dtype=np.uint32)
                    flashy[y + 1, x + 1] += 1
                    step_flashes += 1
            if step_flashes == old_step_flashes:
                break
        total_flashes += step_flashes
        map = np.where(map > 9, 0, map)
        # print_map(map)
        if step_flashes == H * W:
            return step


print(part1())
print(part2())
