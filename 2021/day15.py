import sys
import numpy as np
import heapq

map = np.array([[int(x) for x in line.strip()] for line in sys.stdin], dtype=np.uint32)


def print_map(came_from=None, iters=None):
    global map
    H, W = np.shape(map)
    path = np.zeros_like(map)
    if came_from is not None:
        i = H - 1
        j = W - 1
        risk = 0
        while True:
            path[i, j] = 1
            if i == 0 and j == 0:
                break
            risk += map[i, j]
            i, j = came_from[(i, j)]
    map_str = ""
    for i in range(H):
        for j in range(W):
            if path[(i, j)]:
                map_str += "\x1b[7m"
            map_str += str(map[i, j])
            if path[(i, j)]:
                map_str += "\x1b[0m"
        map_str += "\n"
    print(f"path risk: {risk}")
    print(f"iterations: {iters if iters is not None else 'N/A'}")
    print("==========")
    print(map_str)


def is_inside(x, y):
    global map
    H, W = np.shape(map)
    return 0 <= x < W and 0 <= y < H


def is_destination(x, y):
    global map
    H, W = np.shape(map)
    return x == W - 1 and y == H - 1


def heuristic(x, y):
    global map
    H, W = np.shape(map)
    return H - 1 - y + W - 1 - x


def a_star():
    iters = 0
    x, y = 0, 0
    g, h = 0, heuristic(x, y)
    visited = set()
    came_from = {}
    queue = [(g + h, g, h, x, y, 0, 0)]
    while True:
        f_, g_, h_, x_, y_, x__, y__ = heapq.heappop(queue)
        iters += 1
        if (x_, y_) in visited:
            continue
        visited.add((x_, y_))
        came_from[(y_, x_)] = (y__, x__)
        if is_destination(x_, y_):
            break
        for dx, dy in [(1, 0), (0, 1), (-1, 0), (0, -1)]:
            x, y = x_ + dx, y_ + dy
            if is_inside(x, y):
                g, h = g_ + map[y, x], 0
                g, h = g_ + map[y, x], heuristic(x, y)
                heapq.heappush(queue, (g + h, g, h, x, y, x_, y_))
    # print_map(came_from, iters)
    return g_


def part1():
    return a_star()


def part2():
    global map
    H, W = np.shape(map)
    enlarged_map = np.zeros((5 * H, 5 * W), dtype=np.uint8)
    for i in range(5):
        for j in range(5):
            enlarged_map[i * H: (i + 1) * H, j * W : (j + 1) * W] = (map + i + j - 1) % 9 + 1
    map = enlarged_map
    return a_star()


print(part1())
print(part2())
