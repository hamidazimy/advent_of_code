import sys
import numpy as np

alg = [int(x == '#') for x in sys.stdin.readline().strip()]
sys.stdin.readline()
inp = [line.strip() for line in sys.stdin]
map = np.zeros((len(inp) + 2, len(inp[0]) + 2), dtype=np.int32)
map[1:-1, 1:-1] = np.array([[int(x == '#') for x in l] for l in inp])

code = 2 ** np.array([[8, 7, 6], [5, 4, 3], [2, 1, 0]])


def print_map(map):
    map_str = ""
    for row in map:
        for x in row:
            map_str += "██" if x else "  "
        map_str += '\n'
    print(map_str)


def enhance(map):
    H, W = np.shape(map)
    old_map = np.zeros((H + 2, W + 2), dtype=np.int32) + map[0, 0]
    old_map[1:-1, 1:-1] = map
    new_map = np.zeros_like(old_map, dtype=np.int32) + alg[map[0, 0] * 511]
    H, W = np.shape(new_map)
    for i in range(1, H - 1):
        for j in range(1, W - 1):
            new_map[i, j] = alg[np.sum(code * old_map[i-1:i+2, j-1:j+2])]
    return new_map


def part1(map=map):
    for _ in range(2):
        map = enhance(map)
        # print_map(map)
    return np.sum(map)


def part2(map=map):
    for _ in range(50):
        map = enhance(map)
        # print_map(map)
    return np.sum(map)


print(part1())
print(part2())
