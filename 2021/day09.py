import sys
import numpy as np

inp = [[int(x) for x in line.strip()] for line in sys.stdin]
H, W = len(inp) + 2, len(inp[0]) + 2
map = np.ones((H, W), dtype=np.uint8) * 9
map[1:-1, 1:-1] = np.array(inp, dtype=np.uint8)


def part1(inp=inp):
    u = map[1:-1, 1:-1] < map[:-2, 1:-1]
    d = map[1:-1, 1:-1] < map[2: , 1:-1]
    l = map[1:-1, 1:-1] < map[1:-1, :-2]
    r = map[1:-1, 1:-1] < map[1:-1, 2: ]
    lowest = np.logical_and(np.logical_and(u, d), np.logical_and(r, l))
    return np.sum(lowest * (map[1:-1, 1:-1] + 1))


def basin_size(low_coord):
    mask = np.zeros_like(map, dtype=np.uint8)
    mask[low_coord] = 1
    def dfs(cur):
        for j, i in [(-1, 0), (1, 0), (0, -1), (0, 1)]:
            nei = (cur[0] + j, cur[1] + i)
            if mask[nei] == 0 and 9 > map[nei] > map[cur]:
                mask[nei] = 1
                dfs(nei)
    dfs(low_coord)
    return np.sum(mask)


def part2(inp=inp):
    u = map[1:-1, 1:-1] < map[:-2, 1:-1]
    d = map[1:-1, 1:-1] < map[2: , 1:-1]
    l = map[1:-1, 1:-1] < map[1:-1, :-2]
    r = map[1:-1, 1:-1] < map[1:-1, 2: ]
    lowest = np.logical_and(np.logical_and(u, d), np.logical_and(r, l))
    lowest_coords = np.argwhere(lowest)
    sizes = []
    for Y, X in lowest_coords:
        sizes.append(basin_size((Y + 1, X + 1)))
    sizes.sort()
    return sizes[-3] * sizes[-2] * sizes[-1]


print(part1())
print(part2())
