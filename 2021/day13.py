import sys
from scanf import scanf
import numpy as np

inp = []
H, W = 0, 0
while True:
    line = sys.stdin.readline().strip()
    if line == '':
        break
    inp.append([int(x) for x in line.split(',')])
    W = max(W, inp[-1][0] + 1)
    H = max(H, inp[-1][1] + 1)

folds = [scanf("fold along %s=%d", line.strip()) for line in sys.stdin]

map = np.zeros((H, W), dtype=np.uint8)
for x, y in inp:
    map[y, x] = 1


def part1(map=map, folds=folds):
    for axis, line in folds:
        if axis == 'y':
            half1 = map[line-1::-1, :]
            half2 = map[line+1:, :]
            folded = half1 + half2
            return np.sum(folded[::-1, :] > 0)
        else:
            half1 = map[:, line-1::-1]
            half2 = map[:, line+1:]
            folded = half1 + half2
            return np.sum(folded[:, ::-1] > 0)


def ocr(map):
    import os
    os.environ["QT_STYLE_OVERRIDE"] = ''
    import scipy as sp
    import scipy.ndimage
    import matplotlib.pyplot as plt
    import pytesseract

    plt.figure(figsize=(8, 4), dpi=100)
    plt.axis("off")
    plt.imshow(map, cmap="binary")
    plt.savefig("temp.png")
    plt.close()

    img = plt.imread("temp.png")
    kernel = np.ones((5, 5, 1), dtype=np.uint8)
    img = sp.ndimage.morphology.binary_erosion(img, kernel, iterations=1).astype(img.dtype)
    filter = np.array([5, 5, 0])
    img = sp.ndimage.filters.gaussian_filter(img, filter, mode="constant")
    img = (img * 255).astype(np.uint8)[40:-40, 40:-40, :3]
    result = pytesseract.image_to_string(img[:, :, :3], config=r"--oem 3 --psm 6")
    return result.strip()


def part2(map=map, folds=folds):
    for axis, line in folds:
        if axis == 'y':
            half1 = map[line-1::-1, :]
            half2 = map[line+1:, :]
            folded = half1 + half2
            map = np.array(folded[::-1, :] > 0, dtype=np.uint8)
        else:
            half1 = map[:, line-1::-1]
            half2 = map[:, line+1:]
            folded = half1 + half2
            map = np.array(folded[:, ::-1] > 0, dtype=np.uint8)
    try:
        return ocr(map)
    except:
        return '\n'.join([''.join(["██" if dot == 1 else "  " for dot in row]) for row in map])


print(part1())
print(part2())
